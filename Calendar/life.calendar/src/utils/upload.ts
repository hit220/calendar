import { AppNet } from '../app/app.net';

export const uploadFileByObject = async (
  url: string,
  efile: File,
  observe,
  inputName?
) => {
  const params = {
    size: efile.size.toString(),
    name: efile.name,
    types: efile.type,
    header: '',
  };
  if (params.types === '') {
    params.types = getCaption(efile.name);
  }
  const a = new FormData();
  a.append('file', efile);
  if (observe && observe.onStart) {
    await observe.onStart(efile, a);
  }
  const data = await AppNet.upload(url, a, null, (p) => console.log(p));
  if (data.code !== 'success') {
    if (observe && observe.onFinish) {
      await observe.onFinish(data.code + ':' + data.message, null);
    }
  } else {
    if (observe && observe.onFinish) {
      await observe.onFinish('', {
        uuid: data.data.uuid,
        name: efile.name,
        org: data.data,
      });
    }
  }
};

export const uploadFileByAccept = (
  url: string,
  accept,
  observe,
  inputName?
) => {
  const id = 'input-upload-puling-a=092473917843762945';
  const el = document.getElementById(id);
  if (el) {
    document.body.removeChild(el);
  }
  const input = document.createElement('input');
  if (accept) {
    input.accept = accept;
  }
  input.type = 'file';
  input.name = inputName ? inputName : 'file';
  // console.log(input);
  input.style.overflow = 'hidden';
  input.style.width = '1px';
  input.style.height = '1px';
  input.style.top = '-1px';
  input.style.left = '-1px';
  input.style.position = 'absolute';
  input.style.zIndex = '-1';
  input.id = id;
  input.addEventListener('change', async () => {
    if (input.getAttribute('data-changed')) {
      return;
    }
    input.setAttribute('data-changed', '1');
    if (observe && observe.checkFunc) {
      const icontinue = await observe.checkFunc(input.files[0], input);
      console.log(icontinue);
      if (!icontinue) {
        return false;
      }
    }
    await uploadFileByObject(url, input.files[0], observe, inputName);
  });

  document.body.append(input);
  input.focus();
  input.click();
};
export const getCaption = (obj) => {
  const i = obj.lastIndexOf('.');
  obj = obj.substring(i + 1, obj.length);
  return obj;
};
