import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppConfig } from './app.config';
import { PageBase } from './app.page';
import { AppStore } from './app.store';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent extends PageBase implements OnInit {
  private appPagesNoLogin = [
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Search', url: '/search', icon: 'search' },
    // { title: 'Calendar', url: '/calendar', icon: 'calendar' },
    { title: 'Ranking list', url: '/ranking-list', icon: 'list' },
  ];
  private appPagesLogin = [
    { title: 'Home', url: '/my/inbox', icon: 'home' },
    { title: 'Search', url: '/search', icon: 'search' },
    { title: 'Likes', url: '/my/likes', icon: 'heart' },
    { title: 'Ranking list', url: '/ranking-list', icon: 'list' },
    { title: 'Profile', url: '/my/profile', icon: 'person' },
  ];
  public appPages = this.appPagesNoLogin;

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {
    // console.log('AppComponent ngOnInit -----');

    // init global state
    this.appStore.update((state) => ({
      sess: AppConfig.$session,
      isLogin: AppConfig.isLogin(),
      lang: AppConfig.$lang,
      debug: AppConfig.isDebug(),
    }));
  }

  //
  onStoreChanged() {
    // console.log('on store changed');
    if (this.$isLogin) {
      this.appPages = this.appPagesLogin;
    } else {
      this.appPages = this.appPagesNoLogin;
    }
  }

  onLogout() {
    this.appStore.update((state) => ({
      sess: null,
      isLogin: false,
    }));
    AppConfig.setSession(null);
    console.log(this.$sess);
  }
}
