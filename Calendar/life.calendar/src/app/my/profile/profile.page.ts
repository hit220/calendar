import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { uploadFileByAccept } from '../../../utils/upload';
import { AppConfig } from '../../app.config';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends PageBase implements OnInit {
  form = {
    nickname: '',
    head_image: '',
  };

  uploadUrl = '';
  loadingWin: any;

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute,
    protected loadingCtrl: LoadingController
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {
    this.updateSession();
  }

  async updateSession(user?) {
    console.log('this.$sess', this.$sess);
    user = user ? user : this.$sess;
    this.form.nickname = user && user.name ? user.name : '';
    this.form.head_image = user && user.head ? user.head : '';
  }

  async submitFormEdit() {
    const req = await AppNet.get('/pub/profile_apply');
    if (!req || req.code !== 'success') {
      alert(req.message);
      return;
    }

    const apply = req.data;

    const params = this.copy(this.form);
    const cols = Object.keys(this.form);
    params.cols = cols.join(',');

    const url = apply.url;
    const res = await AppNet.post(url, this.strParams(params));
    if (!res || res.code !== 'success') {
      this.toast(res ? res.message : 'fail');
      return;
    }

    const info = await AppNet.get(url);
    if (!info || info.code !== 'success') {
      this.toast(info ? info.message : 'fail');
      return;
    }

    this.$sess.name = this.form.nickname;
    this.$sess.head = this.form.head_image;

    await AppConfig.setSession(this.$sess);
    this.appStore.update((state) => ({
      sess: AppConfig.$session,
      isLogin: AppConfig.isLogin(),
    }));

    await this.alert('Save success');
    this.hrefReplace('/my/inbox', true);
  }

  /***************** upload */
  loading = false;
  async getUploadUrl() {
    if (this.uploadUrl) {
      return this.uploadUrl;
    }
    const req = await AppNet.get('/pub/upload_apply');
    if (!req || req.code !== 'success') {
      alert(req.message);
      return;
    }

    this.uploadUrl = req.data && req.data.url_upload ? req.data.url_upload : '';
    return this.uploadUrl;
  }
  async uploadFile() {
    const url = await this.getUploadUrl();
    if (!url) {
      this.toast('upload fail: #101');
      return;
    }
    uploadFileByAccept(url, 'image/*', {
      onStart: async (file, formData) => {
        this.loading = true;
        this.loadingWin = await this.loadingCtrl.create({ message: 'Upload ...', showBackdrop: true, duration: 120000 });
        this.loadingWin.present();
        
      },
      onFinish: (err, re) => {
        this.loading = false;
        this.loadingWin.dismiss();

        if (err) {
          this.alert(err);
          return;
        }

        this.form.head_image = re.uuid;
      },
    });
  }

  file2(v) {
    if (!v) {
      return '';
    }
    return this.urlPath('/pub/file/' + v);
  }
}
