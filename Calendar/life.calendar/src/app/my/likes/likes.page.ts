import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppNet } from 'src/app/app.net';
import { PageBase } from 'src/app/app.page';
import { AppStore } from 'src/app/app.store';
import { AppUtil } from 'src/app/app.util';

@Component({
  selector: 'app-likes',
  templateUrl: './likes.page.html',
  styleUrls: ['./likes.page.scss'],
})
export class LikesPage extends PageBase implements OnInit {
  reqData = [];
  dataForce = false;
  pgn = null;
  kw = '';
  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {}
  ionViewDidEnter() {
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
  }

  async getData(pg, kw = '') {
    const res = await AppNet.get('/api/user/v1/like/list', {
      pg,
      kw,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    if (res.data) {
      const pgn = AppUtil.getPaginationByData(res.data);
      const pgr = this.pg(pgn);
      this.pgn = pgn;

      this.reqData = res.data;
    }
  }

  splitImages(imgs) {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }

  async loaderMore(event: any) {
    const nextPg = this.nextPg(this.pgn);
    if (nextPg < 1) {
      this.toast('no more data');
      event.target.complete();
      event.target.disabled = true;
      return;
    }

    await this.getData(nextPg, this.kw);
    event.target.complete();
    event.target.disabled = false;
  }
  async like(i) {
    console.log(i);
    const res = await AppNet.post('/api/user/v1/like/like_set', {
      bid: i.id.toString(),
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
    this.toast('success');
  }
}
