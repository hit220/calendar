import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';
import { AppUtil } from '../../app.util';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage extends PageBase implements OnInit {
  reqData = [];
  dataForce = false;
  pgn = null;
  kw = '';

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {}

  ionViewDidEnter() {
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
  }

  async getData(pg, kw = '') {
    const res = await AppNet.get('/api/user/v1/book/list', { pg, kw });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const pgn = AppUtil.getPaginationByData(res.data);
    const pgr = this.pg(pgn);
    this.pgn = pgn;

    if (pgr === 1) {
      this.reqData = res.data.data ? res.data.data : [];
    } else {
      const arr = this.reqData ? this.reqData : [];
      for (const item of res.data.data) {
        arr.push(item);
      }
      this.reqData = arr;
    }
  }

  splitImages(imgs) {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }

  async loaderMore(event: any) {
    const nextPg = this.nextPg(this.pgn);
    if (nextPg < 1) {
      this.toast('no more data');
      event.target.complete();
      event.target.disabled = true;
      return;
    }

    await this.getData(nextPg, this.kw);
    event.target.complete();
    event.target.disabled = false;
  }

  onRemove(id) {
    this.confirm('Ary you sure remove', async () => {
      const res = await AppNet.get('/api/user/v1/book/delete', { id: id });
      if (res && res.code !== 'success') {
        this.toast(res.message);
        return;
      }

      const pg = this.pg(1);
      this.getData(pg, this.kw);
    });
  }
}
