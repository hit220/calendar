import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';
import { AppUtil } from '../../app.util';

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.page.html',
  styleUrls: ['./seasons.page.scss'],
})
export class SeasonsPage extends PageBase implements OnInit {
  reqData = [];
  dataForce = false;
  pgn = null;
  kw = '';
  itemData = null;
  itemBid = '';

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {
    this.itemData = super.getParam('item', {});
    this.itemBid = super.getParam('bid', '');

    if (this.itemBid && (!this.itemData || !this.itemData.id)) {
      this.itemData = { id: this.itemBid };

      this.getBookData();
    }
  }

  ionViewDidEnter() {
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
  }

  async getBookData() {
    const res = await AppNet.get('/api/user/v1/book/detail', {
      id: this.itemBid,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    this.itemData = res.data;
  }

  async getData(pg, kw = '') {
    const res = await AppNet.get('/api/user/v1/seasons/list', {
      bid: this.itemData.id,
      pg,
      kw,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const pgn = AppUtil.getPaginationByData(res.data);
    const pgr = this.pg(pgn);
    this.pgn = pgn;

    if (pgr === 1) {
      this.reqData = res.data.data ? res.data.data : [];
    } else {
      const arr = this.reqData ? this.reqData : [];
      for (const item of res.data.data) {
        arr.push(item);
      }
      this.reqData = arr;
    }
  }

  splitImages(imgs) {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }

  async loaderMore(event: any) {
    const nextPg = this.nextPg(this.pgn);
    if (nextPg < 1) {
      this.toast('no more data');
      event.target.complete();
      event.target.disabled = true;
      return;
    }

    await this.getData(nextPg, this.kw);
    event.target.complete();
    event.target.disabled = false;
  }

  onRemove(id) {
    this.confirm('Ary you sure remove', async () => {
      const res = await AppNet.get('/api/user/v1/seasons/delete', {
        id,
        bid: this.itemData.id,
      });
      if (res && res.code !== 'success') {
        this.toast(res.message);
        return;
      }

      const pg = this.pg(1);
      this.getData(pg, this.kw);
    });
  }

  mon(v) {
    let n = v || 0;
    n = Math.round((n * 10) / 2) / 10;
    n = n > 12 ? 12 : n;
    n = n < 1 ? 1 : n;
    return n;
  }
}
