import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateSeasonsPage } from './create-seasons.page';

const routes: Routes = [
  {
    path: '',
    component: CreateSeasonsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateSeasonsPageRoutingModule {}
