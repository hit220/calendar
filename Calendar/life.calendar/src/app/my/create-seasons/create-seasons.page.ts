import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonDatetime, NavController } from '@ionic/angular';
import { uploadFileByAccept } from '../../../utils/upload';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';
import ColorPicker from 'simple-color-picker';

@Component({
  selector: 'app-create-seasons',
  templateUrl: './create-seasons.page.html',
  styleUrls: ['./create-seasons.page.scss'],
})
export class CreateSeasonsPage extends PageBase implements OnInit {
  itemId = '';
  modelTitle = 'Create';
  modelButton = 'Create';

  form = {
    id: '',
    bid: '',
    content: '',
    description: '',
    month_start: '',
    month_end: '',
    tag_bg_color: '',
    tag_fg_color: '',
  };
  monthRange = { lower: 1, upper: 1 };

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  async ngOnInit() {
    this.itemId = super.getParam('id', '');
    this.form.bid = super.getParam('bid', '');
  }

  ionViewDidEnter() {
    this.getData();

    const colorPicker1 = new ColorPicker({
      el: '.__colorPicker1',
    });
    colorPicker1.onChange(() => {
      this.form.tag_bg_color = colorPicker1.getHexString();
    });

    const colorPicker2 = new ColorPicker({
      el: '.__colorPicker2',
    });
    colorPicker2.onChange(() => {
      this.form.tag_fg_color = colorPicker2.getHexString();
    });
  }

  async getData() {
    if (!this.itemId) {
      return;
    }
    const res = await AppNet.get('/api/user/v1/seasons/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;
    if (!v) {
      return;
    }

    this.modelTitle = 'Edit';
    this.modelButton = 'Save';

    const needReloadBase = this.form.bid ? false : true;

    this.form.id = '' + v.id;
    this.form.bid = '' + v.bid;
    this.form.content = v.content;
    this.form.description = v.description;
    this.form.month_start = v.month_start;
    this.form.month_end = v.month_end;
    this.form.tag_bg_color = v.tag_bg_color;
    this.form.tag_fg_color = v.tag_fg_color;
    this.monthRange = {
      lower: this.fixRange(Number(this.form.month_start || 0) / 2),
      upper: this.fixRange(Number(this.form.month_end || 0) / 2),
    };
  }

  parseMontCell(arg) {
    const mc = arg ? Number(arg) : 0;
    if (mc > 100) {
      let m = Math.floor(mc / 100);
      let c = mc % 100;
      m = (m + 11) % 12;
      c = (c + 3) % 4;
      return [m, c];
    }
    return null;
  }

  fixRange(v: any) {
    const n = Number(v);
    if (n < 1) {
      return 1;
    } else if (n > 12) {
      return 12;
    }
    return n;
  }

  async submitFormEdit() {
    if (!this.form.content) {
      this.alert('Please input title and content');
      return;
    }

    const range = document.getElementById('monthRange') as any;
    const val = range.value;
    const lower = this.fixRange(val && val.lower ? val.lower : 1);
    const upper = this.fixRange(val && val.upper ? val.upper : 1);
    this.form.month_start = '' + Math.round(lower * 2);
    this.form.month_end = '' + Math.round(upper * 2);

    const res = await AppNet.post(
      '/api/user/v1/seasons/add_or_update',
      this.strParams(this.form)
    );
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : 'fail');
      return;
    }

    await this.alert(res.message, 'success');

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }

    this.hrefReplace('/my/seasons', true, { bid: this.form.bid });
  }

  onRemove(f) { }

  onSelcctColor1() {
    const colorPicker = new ColorPicker();
    colorPicker.onChange(() => {
      this.form.tag_bg_color = colorPicker.getHexString();
      colorPicker.remove();
    });
  }

  onSelcctColor2() { }

  customFormatter(value: number) {
    return value + '';
  }
}
