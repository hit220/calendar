import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateSeasonsPageRoutingModule } from './create-seasons-routing.module';

import { CreateSeasonsPage } from './create-seasons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateSeasonsPageRoutingModule
  ],
  declarations: [CreateSeasonsPage]
})
export class CreateSeasonsPageModule {}
