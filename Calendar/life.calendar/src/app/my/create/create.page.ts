import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonDatetime, LoadingController, NavController } from '@ionic/angular';
import { uploadFileByAccept } from '../../../utils/upload';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';

@Component({
  selector: 'app-my-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage extends PageBase implements OnInit {
  itemId = '';
  mc = '';
  modelTitle = 'Create';
  modelButton = 'Create';

  form = {
    id: '',
    bid: '',
    title: '',
    content: '',
    images: '',
    created_at: '',
  };
  imagesArr = [];

  dtTitle = '';
  dtIndex = 0;
  dtCellIndex = -1;
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  monthArr = [];
  monthItem = [];
  loadingWin: any;

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute,
    protected loadingCtrl: LoadingController
  ) {
    super(appStore, navCtrl, route);
  }

  async ngOnInit() {
    this.itemId = super.getParam('id', '');
    this.form.bid = super.getParam('bid', '');

    this.mc = super.getParam('month_cell', '');
    console.log('mc', this.itemId, '--', this.mc);
    this.dtTitle = this.months[this.dtIndex];

    this.dtInit();
    this.getDataBase();
  }

  async getDataBase() {
    if(!this.form.bid) {
      return;
    }
    const req = await AppNet.get('/api/user/v1/calendar/month_cells', {
      bid: this.form.bid,
    });
    if (!req || req.code !== 'success') {
      this.alert(req.message);
      return;
    }

    if (!req.data) {
      return;
    }

    const arr = [];
    for (const item of req.data) {
      const mc = Number(item.month_cell);
      if (mc > 100) {
        let m = Math.floor(mc / 100);
        let c = mc % 100;
        m = (m + 11) % 12;
        c = (c + 3) % 4;

        if (!arr[m]) {
          arr[m] = [];
        }
        const img = item.images.split(',');
        item.images = img.length > 0 ? img[0] : '';
        arr[m][c] = item;
      }
    }
    this.monthArr = arr;
    this.dtInit();
  }

  ionViewDidEnter() {
    this.getData();
    
  }

  async getData() {
    if (!this.itemId) {
      if (this.mc) {
        
        const mc = this.parseMontCell(this.mc);
        console.log('#1', this.mc, mc);

        if (mc) {
          this.dtIndex = mc[0];
          this.dtCellIndex = mc[1];
          this.dtTitle = this.months[this.dtIndex];
          this.updateSelect(this.dtIndex);
        }
      }

      return;
    }
    const res = await AppNet.get('/api/user/v1/calendar/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;

    this.modelTitle = 'Edit';
    this.modelButton = 'Save';

    const needReloadBase = this.form.bid ? false : true;

    this.form.id = '' + v.id;
    this.form.bid = '' + v.bid;
    this.form.title = v.title;
    this.form.content = v.content;
    this.form.images = v.images;
    this.form.created_at = v.created_at;

    const arr = [];
    const iarr = this.form.images.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    this.imagesArr = arr;

    const mc = this.parseMontCell(v.month_cell);
    console.log('#2', this.mc, mc);
    if (mc) {
      this.dtIndex = mc[0];
      this.dtCellIndex = mc[1];
      this.dtTitle = this.months[this.dtIndex];
      this.updateSelect(this.dtIndex);
    }

    if(needReloadBase) {
      console.log('++++++++++++++');
      this.getDataBase();
    }
  }

  parseMontCell(arg) {
    const mc = arg ? Number(arg) : 0;
    if (mc > 100) {
      let m = Math.floor(mc / 100);
      let c = mc % 100;
      m = (m + 11) % 12;
      c = (c + 3) % 4;
      return [m, c];
    }
    return null;
  }

  async submitFormEdit() {
    if (!this.form.title || !this.form.content) {
      this.alert('Please input title and content');
      return;
    }

    if (!this.form.images) {
      this.alert('Please upload a image');
      return;
    }

    if (this.dtCellIndex < 0) {
      this.alert('Please select a month cell');
      return;
    }

    const m = (this.dtIndex > 8 ? '' : '0') + (this.dtIndex + 1);
    const c = (this.dtCellIndex > 8 ? '' : '0') + (this.dtCellIndex + 1);
    this.form.created_at = m + c;

    const res = await AppNet.post(
      '/api/user/v1/calendar/add_or_update',
      this.strParams(this.form)
    );
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : 'fail');
      return;
    }

    await this.alert(res.message, 'success');

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }

    this.hrefReplace('/my/calendar', true, { bid: this.form.bid });
  }

  /***************** upload */
  loading = false;
  uploadFile() {
    uploadFileByAccept('/api/upload', 'image/*', {
      onStart: async (file, formData) => {
        this.loading = true;
        this.loadingWin = await this.loadingCtrl.create({ message: 'Upload ...', showBackdrop: true, duration: 120000 });
        this.loadingWin.present();
      },
      onFinish: (err, re) => {
        this.loading = false;
        this.loadingWin.dismiss();

        if (err) {
          this.alert(err);
          return;
        }

        this.imagesArr = [re.uuid];
        this.form.images = this.imagesArr.join(',');
      },
    });
  }

  onRemove(f) {
    const nArr = [];
    for (const item of this.imagesArr) {
      if (item !== f) {
        nArr.push(item);
      }
    }
    this.imagesArr = nArr;
    this.form.images = nArr.join(',');
  }

  dtLeft() {
    this.dtIndex = (this.dtIndex + 11) % 12;
    this.dtCellIndex = -1;
    this.dtTitle = this.months[this.dtIndex];
    this.updateSelect(this.dtIndex);
  }

  dtRight() {
    this.dtIndex = (this.dtIndex + 13) % 12;
    this.dtCellIndex = -1;
    this.dtTitle = this.months[this.dtIndex];
    this.updateSelect(this.dtIndex);
  }

  dtInit() {
    this.dtTitle = this.months[this.dtIndex];
    this.updateSelect(this.dtIndex);
  }

  updateSelect(n: number) {
    let arr = this.monthArr[n] ? this.monthArr[n] : [];
    for (let i = 0; i < 4; i++) {
      arr[i] = arr[i] ? this.copy(arr[i]) : null;
    }
    this.monthItem = arr;
  }

  selectDtCell(n) {
    this.dtCellIndex = this.dtCellIndex === n ? -1 : n;
    console.log('#5', this.mc, this.dtCellIndex);
  }


}
