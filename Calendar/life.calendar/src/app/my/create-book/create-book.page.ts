import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonDatetime, LoadingController, NavController } from '@ionic/angular';
import { uploadFileByAccept } from '../../../utils/upload';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';

@Component({
  selector: 'app-my-create-book',
  templateUrl: './create-book.page.html',
  styleUrls: ['./create-book.page.scss'],
})
export class CreateBookPage extends PageBase implements OnInit {
  itemId = '';
  modelTitle = 'Create';
  modelButton = 'Create';

  form = {
    id: '',
    title: '',
    content: '',
    images: '',
  };
  imagesArr = [];

  dtTitle = '';
  dtIndex = 0;
  dtCellIndex = -1;
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  monthArr = [];
  monthItem = [];
  loadingWin: any;

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute,
    protected loadingCtrl: LoadingController
  ) {
    super(appStore, navCtrl, route);
  }

  async ngOnInit() {
    this.itemId = super.getParam('id', '');
  }

  ionViewDidEnter() {
    this.getData();
  }

  async getData() {
    if (!this.itemId) {
      return;
    }
    const res = await AppNet.get('/api/user/v1/book/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;

    this.modelTitle = 'Edit';
    this.modelButton = 'Save';

    this.form.id = '' + v.id;
    this.form.title = v.title;
    this.form.content = v.content;
    this.form.images = v.images;

    const arr = [];
    const iarr = this.form.images.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    this.imagesArr = arr;
  }

  async submitFormEdit() {
    if (!this.form.title || !this.form.content) {
      this.alert('Please input title and content');
      return;
    }

    if (!this.form.images) {
      this.alert('Please upload a image');
      return;
    }

    const res = await AppNet.post(
      '/api/user/v1/book/add_or_update',
      this.strParams(this.form)
    );
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : 'fail');
      return;
    }

    await this.alert(res.message, 'success');

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }

    this.hrefReplace('/my/inbox');
  }

  /***************** upload */
  loading = false;
  uploadFile() {
    uploadFileByAccept('/api/upload', 'image/*', {
      onStart: async (file, formData) => {
        this.loading = true;
        this.loadingWin = await this.loadingCtrl.create({ message: 'Upload ...', showBackdrop: true, duration: 120000 });
        this.loadingWin.present();
      },
      onFinish: (err, re) => {
        this.loading = false;
        this.loadingWin.dismiss();

        if (err) {
          this.alert(err);
          return;
        }

        this.imagesArr = [re.uuid];
        this.form.images = this.imagesArr.join(',');
      },
    });
  }

  onRemove(f) {
    const nArr = [];
    for (const item of this.imagesArr) {
      if (item !== f) {
        nArr.push(item);
      }
    }
    this.imagesArr = nArr;
    this.form.images = nArr.join(',');
  }
}
