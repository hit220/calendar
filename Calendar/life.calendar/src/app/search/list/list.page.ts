import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppNet } from 'src/app/app.net';
import { PageBase } from 'src/app/app.page';
import { AppStore } from 'src/app/app.store';
import { AppUtil } from 'src/app/app.util';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage extends PageBase implements OnInit {
  reqData = [];
  dataForce = false;
  pgn = null;
  kw = '';
  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {
    this.kw = super.getParam('kw', '');
  }

  ionViewDidEnter() {
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
    console.log(this.$sess);
  }

  async getData(pg, kw = '') {
    const res = await AppNet.get('/api/v1/book/list', {
      pg,
      pgz:50,
      kw,
      uid: this.$isLogin ? this.$sess.id : '',
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const pgn = AppUtil.getPaginationByData(res.data);
    const pgr = this.pg(pgn);
    this.pgn = pgn;

    if (pgr === 1) {
      this.reqData = res.data.data ? res.data.data : [];
    } else {
      const arr = this.reqData ? this.reqData : [];
      for (const item of res.data.data) {
        arr.push(item);
      }
      this.reqData = arr;
    }
  }

  splitImages(imgs) {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }

  async loaderMore(event: any) {
    const nextPg = this.nextPg(this.pgn);
    if (nextPg < 1) {
      this.toast('no more data');
      event.target.complete();
      event.target.disabled = true;
      return;
    }

    await this.getData(nextPg, this.kw);
    event.target.complete();
    event.target.disabled = false;
  }
  async like(i) {
    console.log(i);
    const res = await AppNet.post('/api/user/v1/like/like_set', {
      bid: i.id.toString(),
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
    this.toast('success');
  }
}

