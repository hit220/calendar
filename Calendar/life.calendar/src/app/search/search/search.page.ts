import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { PageBase } from 'src/app/app.page';
import { AppStore } from 'src/app/app.store';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage extends PageBase implements OnInit {
  reqData = [];
  dataForce = false;
  pgn = null;
  kw = '';
  degArr = new Array(48);

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {}
  search() {
    this.hrefTo('search/list', false, { kw: this.kw });
  }
}
