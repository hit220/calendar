import { Component, OnInit } from '@angular/core';
import { ModalController ,NavController} from '@ionic/angular';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';
import { ActivatedRoute } from '@angular/router';
import { NavParams } from '@ionic/angular';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage extends PageBase implements OnInit {
  get = {};
  selectMonthCellData = null;
  constructor(
    public modalController: ModalController,
    public navParams: NavParams,
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute,
  ) {
    super(appStore, navCtrl, route);
    // this.get = this.navParams.get('selectMonthCellData');
    this.selectMonthCellData = this.navParams.get('selectMonthCellData');
  }

  ngOnInit() {
    // console.log(this.get);
    console.log(this.selectMonthCellData);
  }

  splitImages(imgs): string[] {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController
      .dismiss({
        dismissed: true,
      })
      .then(() => {
        this.selectMonthCellData = null;
      });
  }
}
