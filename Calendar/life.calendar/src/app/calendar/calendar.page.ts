import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import ArcText from 'arc-text';
import { AppNet } from '../app.net';
import { PageBase } from '../app.page';
import { AppStore } from '../app.store';
import { AppUtil } from '../app.util';
import { IonRouterOutlet } from '@ionic/angular';
import { ModalPage } from './modal/modal.page';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage extends PageBase implements OnInit {
  reqData = [];
  dataForce = false;
  pgn = null;
  kw = '';
  bid = '0';
  uid = '0';

  monthKWArr;
  imgs = [];
  monthArr = [];
  seasonsArr = [];
  degArr = new Array(48);

  monthItem = [null, null, null, null];
  monthIndex = 0;
  selectMonthCell = '';
  selectMonthCellData = null;
  sessItemArr = [];

  wrapperWidth = window.innerWidth;
  wrapperHeight = this.wrapperWidth;

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public routerOutlet: IonRouterOutlet
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {
    this.bid = super.getParam('bid', '');
    this.uid = super.getParam('uid', '');

    this.getDataBase();
  }

  ionViewDidEnter() {
    const pg = this.pg(this.pgn);
    this.getData(pg, this.kw);
    this.getSeasons();

    this.drawArcText();
  }

  async getDataBase() {
    const req = await AppNet.get('/api/v1/calendar/month_cells', {
      bid: this.bid,
    });
    if (!req || req.code !== 'success') {
      return;
    }

    if (!req.data) {
      return;
    }

    const arr = [];
    for (const item of req.data) {
      const mc = this.parseMontCell(item.month_cell);
      if (mc) {
        let k = mc[0];
        if (!arr[k]) {
          arr[k] = [null, null, null, null];
        }
        const img = item.images.split(',');
        item.images = img.length > 0 ? img[0] : '';
        arr[k][mc[1]] = item;
      }
    }
    this.monthArr = arr;
    const marr = arr[this.monthIndex];
    this.monthItem = [marr[3], marr[0], marr[2], marr[1]];
    console.log(this.monthItem);
  }

  async getSeasons() {
    const req = await AppNet.get('/api/v1/seasons/list', {
      bid: this.bid,
    });
    if (!req || req.code !== 'success') {
      return;
    }

    this.seasonsArr = req.data && req.data.data ? req.data.data : [];
    const arr = new Array(48);

    for (let i = 0; i < arr.length; i++) {
      arr[i] = this.getSeasonsBackgroundColor(i);
    }
    this.degArr = arr;

    this.updateSeasons();
  }

  getSeasonsItem(arr, index) {
    if (!arr) {
      return null;
    }

    const nIndex = ((index + 1) % 48) + 1;
    for (let i = arr.length - 1; i >= 0; i--) {
      const item = arr[i];

      if (!item.month_start || !item.month_end) {
        continue;
      }

      const start = (item.month_start - 1) * 2;
      const end = (item.month_end - 1) * 2;
      if (nIndex >= start && nIndex < end) {
        return item;
      }
    }
    return null;
  }

  // [0 ~ 24)
  getSeasonsBackgroundColor(index) {
    if (!this.seasonsArr || index < 0 || index >= 48) {
      return '#eee';
    }
    const item = this.getSeasonsItem(this.seasonsArr, (index + 36) % 48);
    if (!item) {
      return '#eee';
    }
    return item.tag_bg_color;
  }

  parseMontCell(arg) {
    const mc = arg ? Number(arg) : 0;
    if (mc > 100) {
      let m = Math.floor(mc / 100);
      let c = mc % 100;
      m = (m + 11) % 12;
      c = (c + 3) % 4;
      return [m, c];
    }
    return null;
  }

  async getData(pg, kw = '') {
    const res = await AppNet.get('/api/v1/calendar/list', {
      pg,
      kw,
      month: (this.monthIndex > 8 ? '' : '0') + (this.monthIndex + 1),
      bid: this.bid,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    if (!res.data) {
      this.reqData = [];
      this.updateSelectMonthCell();
      this.updateSeasons();
      return;
    }

    const pgn = AppUtil.getPaginationByData(res.data);
    const pgr = this.pg(pgn);
    this.pgn = pgn;

    if (pgr === 1) {
      this.reqData = res.data.data ? res.data.data : [];
    } else {
      const arr = this.reqData ? this.reqData : [];
      for (const item of res.data.data) {
        arr.push(item);
      }
      this.reqData = arr;
    }

    this.updateSelectMonthCell();
    this.updateSeasons();
  }

  splitImages(imgs): string[] {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }
  async loaderMore(event: any) {
    const nextPg = this.nextPg(this.pgn);
    if (nextPg < 1) {
      this.toast('no more data');
      event.target.complete();
      event.target.disabled = true;
      return;
    }

    await this.getData(nextPg, this.kw);
    event.target.complete();
    event.target.disabled = false;
  }

  async drawArcText() {
    const elb = document.getElementById('timeBox');
    const elm = document.getElementById('timeCircle');
    if (!elm || !elb) {
      return;
    }

    elb.style.width = this.wrapperWidth + 'px';
    elb.style.height = this.wrapperHeight + 'px';

    elm.style.width = this.wrapperWidth - 48 + 'px';
    elm.style.height = this.wrapperHeight - 48 + 'px';
    elm.style.top = '24px';
    elm.style.left = '24px';

    const cells = elm.querySelectorAll('.cell-month');
    cells.forEach((v, k) => {
      // const arcText = new ArcText(v as HTMLElement);
      // if (![0, 6].includes(k)) {
      // arcText.direction(0.5);
      // }
      // arcText.arc(wrapperWidth);
    });
  }

  onClickAct(monthIndex: number) {
    // const elm = document.getElementById('cellMonthBox');
    const ela = document.getElementById('timeAvatar');
    /*
    // words rotate
    elm.classList.forEach((value) => {
      if (value.indexOf('deg-') === 0) {
        elm.classList.remove(value);
      }
    });
    */
    ela.classList.forEach((value) => {
      if (value.indexOf('deg-') === 0) {
        ela.classList.remove(value);
      }
    });

    // let dn = ((13 - monthIndex) % 12) + 1;
    // elm.classList.add('deg-' + dn);
    // ela.classList.add('deg-' + dn);
    ela.classList.add('deg-' + monthIndex);
    console.log(monthIndex);

    //随机排序图片
    this.monthIndex = monthIndex % 12;
    this.updateImages(this.monthIndex);

    this.getData(1, this.kw);
  }

  getKeyword(monthIndex): string {
    if (monthIndex < 1) {
      return '';
    }
    return monthIndex > 9 ? '' + monthIndex : '0' + monthIndex;
  }

  updateImages(n: number) {
    let arr = this.monthArr[n] ? this.monthArr[n] : [];

    const marr = [];
    for (let i = 0; i < 4; i++) {
      marr[i] = arr[i] ? this.copy(arr[i]) : null;
    }

    this.monthItem = [marr[3], marr[0], marr[2], marr[1]];
  }

  onClickCell(cellIndex: number, out: boolean) {
    // this.toast('create function...');

    const ci = cellIndex - 1;

    const xarr = this.monthArr[this.monthIndex];
    const item = xarr && xarr.length > ci ? xarr[ci] : null;
    const m = (this.monthIndex > 8 ? '' : '0') + (this.monthIndex + 1);
    const c = (ci > 8 ? '' : '0') + (ci + 1);

    this.selectMonthCell = m + c;
    this.updateSelectMonthCell();
    this.updateSeasons();
    this.presentModal(this.selectMonthCellData);
  }

  onTimeBoxClick($e) {
    if (!$e) {
      return;
    }

    const r1 = (this.wrapperWidth - 20) / 2; // px
    const r2 = r1 - 58; // px
    const r22 = r2 - 16; // px
    const r3 = 32; // px
    const w = this.wrapperWidth / 2 - $e.offsetX;
    const h = this.wrapperHeight / 2 - $e.offsetY;
    const r = Math.pow(w * w + h * h, 0.5);

    const pos = Math.atan2(w, h) + Math.PI; // 取值修正到  0 ~ 2PI
    const deg = 360 - ((Math.round((pos * 360) / 2 / Math.PI) + 195) % 360);
    const mothIndex = Math.round((((deg + 360) % 360) / 360) * 12) % 12; // 12月份象限
    const cellIndex =
      ((Math.round((((deg + 360 + 45) % 360) / 360) * 4) + 3) % 4) + 1; // 每月四区块象限, 向后偏移45度

    // 月份环
    if (r > r2 && r <= r1) {
      this.onClickAct(mothIndex);
    } else if (r < r2 && r > r3) {
      this.onClickCell(cellIndex, r > r22);
    }
  }

  updateSelectMonthCell() {
    if (!this.selectMonthCell) {
      this.selectMonthCellData = null;
      return;
    }

    if (!this.reqData || this.reqData.length < 1) {
      this.selectMonthCellData = null;
      return;
    }

    for (const item of this.reqData) {
      if (item.month_cell === this.selectMonthCell) {
        this.selectMonthCellData = item;
        return;
      }
    }

    this.selectMonthCellData = null;
  }

  updateSeasons() {
    console.log(
      this.monthIndex * 4,
      this.monthIndex * 4 + 1,
      this.monthIndex * 4 + 2,
      this.monthIndex * 4 + 3
    );
    const item1 = this.getSeasonsItem(this.seasonsArr, this.monthIndex * 4);
    const item2 = this.getSeasonsItem(
      this.seasonsArr,
      this.monthIndex * 4 + 1
    );
    const item3 = this.getSeasonsItem(
      this.seasonsArr,
      this.monthIndex * 4 + 2
    );
    const item4 = this.getSeasonsItem(
      this.seasonsArr,
      this.monthIndex * 4 + 3
    );
    const ids = [];
    const items = [item1, item2, item3, item4];
    const items2 = [];
    for (const item of items) {
      if (item && !ids.includes(item.id)) {
        ids.push(item.id);
        items2.push(item);
      }
    }

    this.sessItemArr = items2;
  }
  async presentModal(selectMonthCellData) {
    const modal = await this.modalController.create({
      component: ModalPage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      breakpoints: [0.1, 0.5, 1],
      initialBreakpoint: 0.5,
      componentProps: {
        selectMonthCellData: selectMonthCellData,
      },
    });
    return await modal.present();
  }
}
