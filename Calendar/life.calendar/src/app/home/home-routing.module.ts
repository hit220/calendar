import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageHome } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: PageHome
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageHomeRoutingModule {}
