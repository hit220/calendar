import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PageHomeRoutingModule } from './home-routing.module';

import { PageHome } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PageHomeRoutingModule
  ],
  declarations: [PageHome]
})
export class PageHomeModule {}
