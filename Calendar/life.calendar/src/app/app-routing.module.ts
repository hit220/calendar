import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.PageHomeModule)
  },
  {
    path: 'my/inbox',
    loadChildren: () => import('./my/inbox/inbox.module').then( m => m.InboxPageModule)
  },
  {
    path: 'my/create',
    loadChildren: () => import('./my/create/create.module').then( m => m.CreatePageModule)
  },
  {
    path: 'my/calendar',
    loadChildren: () => import('./my/calendar/calendar.module').then( m => m.CalendarPageModule)
  },
  {
    path: 'my/profile',
    loadChildren: () => import('./my/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'calendar',
    loadChildren: () => import('./calendar/calendar.module').then( m => m.CalendarPageModule)
  },
  {
    path: 'my/likes',
    loadChildren: () => import('./my/likes/likes.module').then( m => m.LikesPageModule)
  },
  {
    path: 'my/calendar',
    loadChildren: () => import('./my/calendar/calendar.module').then( m => m.CalendarPageModule)
  },
  {
    path: 'ranking-list',
    loadChildren: () => import('./ranking-list/ranking-list.module').then( m => m.RankingListPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'auth/login',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'auth/register',
    loadChildren: () => import('./auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'my/create-book',
    loadChildren: () => import('./my/create-book/create-book.module').then( m => m.CreateBookPageModule)
  },
  {
    path: 'my/create-seasons',
    loadChildren: () => import('./my/create-seasons/create-seasons.module').then( m => m.CreateSeasonsPageModule)
  },
  {
    path: 'my/seasons',
    loadChildren: () => import('./my/seasons/seasons.module').then( m => m.SeasonsPageModule)
  },
  {
    path: 'search/list',
    loadChildren: () => import('./search/list/list.module').then( m => m.ListPageModule)
  },





];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
