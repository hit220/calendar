import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppConfig } from '../../app.config';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';
import { AppUtil } from '../../app.util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends PageBase implements OnInit {
  checkedMe = false;
  form = {
    account: '',
    password: '',
  };

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  async ngOnInit() {
    this.checkedMe = await AppConfig.getWithTimeout(
      'loginRememberAccount',
      30 * 86400,
      false
    );
    if (this.checkedMe) {
      this.form.account = await AppConfig.get('loginAccount', '');
      AppConfig.set('loginRememberAccount', this.checkedMe);
    }
  }

  onChanged() {
    AppConfig.set('loginRememberAccount', this.checkedMe);
    console.log('checkedMe', this.checkedMe);
  }

  async onSignIn() {
    if (this.form.account.length < 4 || this.form.password.length < 4) {
      alert('Please input valid value');
      return;
    }
    const req = await AppNet.get('/pub/login_apply');
    if (!req || req.code !== 'success') {
      this.alert(req.message);
      return;
    }
    const apply = req.data;

    const params = {
      type: 'user',
      device: '123456',
      apply_token: apply.apply_token,
      module: apply.module,
      account: this.form.account,
      password: AppUtil.toSha256(this.form.password),
    };

    const url = apply.url;

    const data = await AppNet.post(url, this.strParams(params));

    if (!data || data.code !== 'success') {
      this.alert('Login failed');
      return;
    }
    await AppConfig.setSessToken({ token: data.data.token });
    await AppConfig.setSession(data.data.user);
    await AppConfig.set('loginAccount', this.form.account);
    await AppConfig.set('loginPassword', this.form.password);


    this.appStore.update((state) => ({
      sess: AppConfig.$session,
      isLogin: AppConfig.isLogin(),
    }));
    
    this.hrefReplace('/my/inbox');
    await this.toast('Successful');
  }
}
