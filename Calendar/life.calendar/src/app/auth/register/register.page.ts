import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppNet } from '../../app.net';
import { PageBase } from '../../app.page';
import { AppStore } from '../../app.store';
import { AppUtil } from '../../app.util';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage extends PageBase implements OnInit {
  form = {
    name: '',
    account: '',
    password: '',
    rePassword: '',
  };

  agree = false;
  ccc = '';

  constructor(
    protected appStore: AppStore,
    protected navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    super(appStore, navCtrl, route);
  }

  ngOnInit() {}

  async onSubmit() {
    if (
      this.form.account.length < 4 ||
      this.form.password.length < 4 ||
      this.form.rePassword.length < 4
    ) {
      this.alert('Please input valid value');
      return;
    }

    if (!this.agree) {
      this.alert('You need agree to the user terms');
      return;
    }

    if (this.form.rePassword !== this.form.password) {
      this.alert('Password not equal confirm password');
      return;
    }

    const req = await AppNet.get('/pub/register_apply');
    if (!req || req.code !== 'success') {
      this.alert('Login failed');
      return;
    }

    const apply = req.data;

    const params = {
      type: 'user',
      device: '123456',
      module: apply.module,
      username: '',
      email: '',
      phone: '',
      password: AppUtil.toSha256(this.form.password),
    };
    if (this.form.account.includes('@')) {
      params.email = this.form.account;
    } else {
      const num = this.form.account.replace(/[^0-9]/g, '');
      if (num.length === this.form.account.length && num.length >= 10) {
        params.phone = this.form.account;
      } else {
        params.username = this.form.account;
      }
    }

    const url = apply.url_by_username;
    const res = await AppNet.post(url, this.strParams(params));

    if (res.code != 'success') {
      await this.alert('Register failed');
      return;
    }

    await this.toast('Register success');
    this.hrefReplace('/auth/login');
  }
}
