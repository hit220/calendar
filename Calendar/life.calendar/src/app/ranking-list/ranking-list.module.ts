import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RankingListPageRoutingModule } from './ranking-list-routing.module';

import { RankingListPage } from './ranking-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RankingListPageRoutingModule
  ],
  declarations: [RankingListPage]
})
export class RankingListPageModule {}
