import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppConfig } from './app/app.config';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
// import VConsole from 'vconsole';

(async () => {
  // const vConsole = new VConsole();
  // or init with options
  // const vConsole = new VConsole({ maxLogNumber: 1000 });

  // call `console` methods as usual
  // console.log('Hello world');

  if (environment.production) {
    enableProdMode();
  }

  await AppConfig.init();

  platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .catch((err) => console.log(err));

  // vConsole.destroy();
})();
