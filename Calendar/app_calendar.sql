/*
Navicat MySQL Data Transfer

Source Server         : 日历正式
Source Server Version : 50505
Source Host           : 161.117.177.248:3306
Source Database       : app_calendar

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-04-16 13:21:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_book
-- ----------------------------
DROP TABLE IF EXISTS `app_book`;
CREATE TABLE `app_book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0,
  `title` text NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL DEFAULT '',
  `images` mediumtext NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `pub` int(11) NOT NULL DEFAULT 1,
  `fabulous` mediumtext NOT NULL DEFAULT '',
  `fabulous_num` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_app_book_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_app_book_updated_at_idx_ym` (`updated_at_idx_ym`),
  KEY `IDX_app_book_pub` (`pub`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_book
-- ----------------------------
INSERT INTO `app_book` VALUES ('3', '1', 'Gulumoerrgin (Larrakia) seasons calendar', 'Gulumoerrgin is the Indigenous language for Darwin and the surrounding regions of Cox Peninsula and Gunn Point in the Northern Territory.\nMembers of the language group, Lorraine Williams, Judith Williams, Maureen Ogden, Keith Risk and Anne Risk, contributed to the development of the Gulumoerrgin (Larrakia) seasonal calendar.', '5a11ee77-01ad-4ccc-8989-97d037811c23.jpg', '2022-04-11 07:46:05', '202204', '2022-04-10 23:49:12', '202204', '1', '', '0');

-- ----------------------------
-- Table structure for app_calendar
-- ----------------------------
DROP TABLE IF EXISTS `app_calendar`;
CREATE TABLE `app_calendar` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bid` bigint(20) NOT NULL DEFAULT 0,
  `title` text NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL DEFAULT '',
  `images` mediumtext NOT NULL DEFAULT '',
  `month_cell` char(10) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_m` char(10) NOT NULL DEFAULT '',
  `created_at_idx_md` char(10) NOT NULL DEFAULT '',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `likes` int(11) NOT NULL DEFAULT 0,
  `like_users` mediumtext NOT NULL DEFAULT '',
  `pub` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `IDX_app_calendar_created_at_idx_md` (`created_at_idx_md`),
  KEY `IDX_app_calendar_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_app_calendar_updated_at_idx_ym` (`updated_at_idx_ym`),
  KEY `IDX_app_calendar_likes` (`likes`),
  KEY `IDX_app_calendar_pub` (`pub`),
  KEY `IDX_app_calendar_month_cell` (`month_cell`),
  KEY `IDX_app_calendar_created_at_idx_m` (`created_at_idx_m`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_calendar
-- ----------------------------
INSERT INTO `app_calendar` VALUES ('1', '3', 'Damibila', 'Damibila (Barramundi) are Bush Currant is fruiting now. multiplying upstream on the foodplains.', 'c2b585cb-739f-448c-8594-8668d91994a1.jpg', '0101', '2022-04-11 07:58:30', '01', '0411', '202204', '2022-04-11 00:16:13', '202204', '0', '', '1');
INSERT INTO `app_calendar` VALUES ('2', '3', 'Danggalaba', 'Danggalaba (Saltwater Crocodile) are laying their eggs. ', 'ba62b6aa-c05c-48aa-af7b-a3bce89deb08.jpg', '0102', '2022-04-11 08:02:23', '01', '0411', '202204', '2022-04-11 00:14:30', '202204', '0', '', '1');
INSERT INTO `app_calendar` VALUES ('3', '3', 'Mindilima', '\nMindilima (Big Red Apple) are fruiting.', '23ffcaa3-e688-4b30-a747-455eace9bedb.jpg', '0103', '2022-04-11 08:08:41', '01', '0411', '202204', '2022-04-11 00:09:59', '202204', '0', '', '1');
INSERT INTO `app_calendar` VALUES ('4', '3', 'Mindimilma', 'Mindimilma (Pink Wild Apple) are fruiting.', '04cdac53-d54c-4768-ad0f-0b69ce8f9ed6.jpg', '0104', '2022-04-11 08:10:57', '01', '0411', '202204', '2022-04-11 08:10:57', '202204', '0', '', '1');

-- ----------------------------
-- Table structure for app_comment
-- ----------------------------
DROP TABLE IF EXISTS `app_comment`;
CREATE TABLE `app_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fid` bigint(20) NOT NULL DEFAULT 0,
  `uid` bigint(20) NOT NULL DEFAULT 0,
  `from_key` varchar(255) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_app_comment_from_key` (`from_key`),
  KEY `IDX_app_comment_created_at_idx_ym` (`created_at_idx_ym`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_comment
-- ----------------------------

-- ----------------------------
-- Table structure for app_file
-- ----------------------------
DROP TABLE IF EXISTS `app_file`;
CREATE TABLE `app_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL DEFAULT '',
  `user_type` char(32) NOT NULL DEFAULT '',
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `hash` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `content_type` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(200) NOT NULL DEFAULT '',
  `ext` char(32) NOT NULL DEFAULT '',
  `file_size` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_app_file_uuid` (`uuid`),
  KEY `IDX_app_file_user_type` (`user_type`),
  KEY `IDX_app_file_user_id` (`user_id`),
  KEY `IDX_app_file_created_at_idx_ym` (`created_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_file
-- ----------------------------
INSERT INTO `app_file` VALUES ('1', 'adec5d59-6843-427b-9e8b-090a75d25ab4.jpg', '', '0', '', 'A039E553-75D8-41D4-8D01-B52E6A301E45.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('2', 'a56b6e7f-2eda-4428-859f-b9529e4940e2.jpg', '', '0', '', '58A7897C-345E-4FC5-A5A4-6F8D594F385E.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('3', 'c2205862-e468-4d1a-9b2d-d6ed3a80ce3f.jpg', '', '0', '', '33DD9337-F25D-43EB-8F61-675AECAAFF65.png', 'image/png', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('4', 'e7c0360a-6b28-4c34-8a00-57b354e823eb.jpg', '', '0', '', '08B4628C-A7F0-4777-9D3C-ADF8A73B66CD.png', 'image/png', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('5', '06d0d653-0746-4712-815e-f22e01b94f69.jpg', '', '0', '', '66708846-BD87-49C9-8CA1-10CD0D6736F4.png', 'image/png', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('6', 'bc900675-3bc6-458f-b53e-dba00b9433b3.png', '', '0', '', '1.png', 'image/png', '', '.png', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('7', '1b9037f6-8199-4204-a97e-ceb6adde1c34.43.png', '', '0', '', '截屏2021-05-07 下午12.47.43.png', 'image/png', '', '.43.png', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('8', 'af66cb06-6878-4010-9f57-02cef9e340a6.23.png', '', '0', '', '截屏2021-05-07 下午12.40.23.png', 'image/png', '', '.23.png', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('9', 'ef8e6768-cd78-4016-a840-b0fd948489a8.jpg', '', '0', '', '9A47B3C8-CAEF-48B1-B8BE-96614D05F37B.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('10', 'b26f967e-9a0b-4ee8-ae50-67e9871293ca.jpg', '', '0', '', 'image.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('11', '3573a5ca-cce2-45f6-88f0-36c09d95872d.jpg', '', '0', '', '15.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('12', '53254039-0ae4-4891-a347-ec647367087b.09.png', '', '0', '', '截屏2021-05-07 下午12.52.09.png', 'image/png', '', '.09.png', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('13', '5a11ee77-01ad-4ccc-8989-97d037811c23.jpg', '', '0', '', '1649634348907.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('14', 'eb218232-15c3-40ef-966c-571577611b27.jpg', '', '0', '', '1649634348907.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('15', 'c4340cea-b11a-478e-abc7-bdda88707141.jpg', '', '0', '', '12.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('16', 'd18b8b95-918b-41ba-a983-eb062c6d69f4.jpg', '', '0', '', '12.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('17', 'ba62b6aa-c05c-48aa-af7b-a3bce89deb08.jpg', '', '0', '', '13.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('18', '825c6fa4-fc6c-48ea-ba79-d48778ee3fff.jpg', '', '0', '', '13.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('19', '23ffcaa3-e688-4b30-a747-455eace9bedb.jpg', '', '0', '', '14.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('20', '04cdac53-d54c-4768-ad0f-0b69ce8f9ed6.jpg', '', '0', '', '15.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('21', 'c2b585cb-739f-448c-8594-8668d91994a1.jpg', '', '0', '', '16.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('22', '9e2d6023-ba2c-4ef9-ab93-363662fe59ef.jpg', '', '0', '', '906cfffbgy1g5uiu45mvzj20j60sr11g.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('23', '5b4387f6-dc63-4404-b3f5-0637f71409e2.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('24', '15630a00-f3b4-49d2-88ac-a63ca61e7928.jpeg', '', '0', '', 'B5095074-68CF-4740-855F-28DA340D5AAE.jpeg', 'image/jpeg', '', '.jpeg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('25', 'abf0766e-667d-45bb-96b9-71a26e5558c8.jpeg', '', '0', '', 'A808E25B-08E1-4F0C-AD89-B1B28211161B.jpeg', 'image/jpeg', '', '.jpeg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('26', '6d9267bc-d845-4049-8d78-10841f213afc.jpeg', '', '0', '', 'E7F6B611-C64E-4098-A635-ECBFC788C0F6.jpeg', 'image/jpeg', '', '.jpeg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('27', '67f4ae9b-f85d-4b75-9bfb-8da9fb7897cb.jpg', '', '0', '', 'B3FEC453-6600-4AA1-9D10-BF05A18EF970.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('28', '960340d3-02e5-4cf6-bc0f-baa607d5cf19.jpg', '', '0', '', 'CC6A1FD7-BD09-49FE-BC8D-1BD499B18BE5.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('29', '8661d242-b650-4b19-bb13-355c3a567a7e.jpg', '', '0', '', 'F11F1A40-2B39-4F55-86A8-006E3651AB7D.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('30', 'ad78b484-f8ab-47ab-ab38-1629795bd19d.jpg', '', '0', '', '3F1DED76-0661-42CA-9DB4-F1DBDD806416.jpeg', 'image/jpeg', '', '.jpg', '0', '0001-01-01 08:05:43', '0');
INSERT INTO `app_file` VALUES ('31', '3b44eb68-d0d9-44e6-bbf7-be2cf86fd9be.jpg', '', '0', '', 'F7FF913B-263A-4F52-B648-25F11B23F421.png', 'image/png', '', '.jpg', '0', '0001-01-01 08:05:43', '0');

-- ----------------------------
-- Table structure for app_likes
-- ----------------------------
DROP TABLE IF EXISTS `app_likes`;
CREATE TABLE `app_likes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0,
  `key_type` int(11) NOT NULL DEFAULT 0,
  `from_key` varchar(255) NOT NULL DEFAULT '',
  `like` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `IDX_app_likes_from_key` (`from_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_likes
-- ----------------------------

-- ----------------------------
-- Table structure for app_likes_count
-- ----------------------------
DROP TABLE IF EXISTS `app_likes_count`;
CREATE TABLE `app_likes_count` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key_type` int(11) NOT NULL DEFAULT 0,
  `from_key` varchar(255) NOT NULL DEFAULT '',
  `likes` bigint(20) NOT NULL DEFAULT 0,
  `unlikes` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_app_likes_count_from_key` (`from_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_likes_count
-- ----------------------------

-- ----------------------------
-- Table structure for app_seasons
-- ----------------------------
DROP TABLE IF EXISTS `app_seasons`;
CREATE TABLE `app_seasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bid` bigint(20) NOT NULL DEFAULT 0,
  `content` text NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL DEFAULT '',
  `tag_bg_color` varchar(32) NOT NULL DEFAULT '',
  `tag_fg_color` varchar(32) NOT NULL DEFAULT '',
  `month_start` int(11) NOT NULL DEFAULT 0,
  `month_end` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_seasons
-- ----------------------------
INSERT INTO `app_seasons` VALUES ('1', '2', 'hh', '1992-1988\nThe survey found that the statistical chances of someone from a poor background being accepted at one of the country\'s most respected universities are far lower than those of a student from a wealthy family. This means that theinequalities in society are likely to be passed down from one generation to the next. ', '#8E3232', '#000000', '2', '6');
INSERT INTO `app_seasons` VALUES ('2', '3', 'Dalay – Monsoon season', 'Dalay is the monsoon season. The monsoon replenishes a dry thirsty land. Water gushes from rivers and creeks, and foodplains are buried under lakes of water. The water disperses seeds and new plants are sprouting. Manmanma (cyclones) seem more frequent and are a sign of a troubled planet.', '#1282F8', '#010101', '2', '6');

-- ----------------------------
-- Table structure for app_sessions
-- ----------------------------
DROP TABLE IF EXISTS `app_sessions`;
CREATE TABLE `app_sessions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bid` bigint(20) NOT NULL DEFAULT 0,
  `content` text NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL DEFAULT '',
  `tag_bg_color` varchar(32) NOT NULL DEFAULT '',
  `tag_fg_color` varchar(32) NOT NULL DEFAULT '',
  `month_start` int(11) NOT NULL DEFAULT 0,
  `month_end` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` char(40) NOT NULL DEFAULT '',
  `account` char(64) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `auth` mediumtext NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `activation` int(11) NOT NULL DEFAULT 0,
  `nickname` varchar(200) NOT NULL DEFAULT '',
  `ext_info` mediumtext NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_app_user_activation` (`activation`),
  KEY `IDX_app_user_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_app_user_updated_at_idx_ym` (`updated_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=535 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('1', '3e56a8dc-aa0b-41c2-9510-6557d1bf01eb', '3681314@qq.com', '', '1-4-2022-04-08 10:46:16', 'ba-aedb9134-3b8e-40b4-94e1-fa557e4fb59b.jpg', '0', 'Keason', '', '2022-04-08 13:43:08', '202204', '2022-04-15 11:19:16', '202204', '0');
INSERT INTO `app_user` VALUES ('2', '554f8129-908f-47bf-b0f8-f299d5aff71f', 'beyond3681314@gmail.com', '', '2-4-2022-04-08 10:46:16', '', '0', 'NewUser', '', '2022-04-08 13:50:11', '202204', '2022-04-08 13:50:11', '202204', '0');
INSERT INTO `app_user` VALUES ('3', '8afba75c-fbb9-404d-b2e9-37dbdbaa0fe2', '347786293@qq.com', '', '3-4-2022-04-08 10:46:16', '', '0', 'NewUser', '', '2022-04-12 09:55:57', '202204', '2022-04-12 09:55:57', '202204', '0');
INSERT INTO `app_user` VALUES ('5', '1f3ddc6f-9f88-4159-a828-57b6e34c06c5', '222222', '', '5-4-2022-04-08 10:46:16', 'ba-e0d6e590-9b5e-4110-9931-29af728b93fb.jpg', '0', 'NewUser', '', '2022-04-13 00:52:31', '202204', '2022-04-14 00:20:18', '202204', '0');
INSERT INTO `app_user` VALUES ('534', '8a8e4059-c10a-43e3-832a-8bc43d8fc627', 'beyond3681314@gmail.com', '', '534-4-2022-03-10 02:12:22', '', '0', 'NewUser', '', '2022-04-07 18:50:50', '202204', '2022-04-08 00:13:37', '202204', '0');

-- ----------------------------
-- Table structure for new_likes
-- ----------------------------
DROP TABLE IF EXISTS `new_likes`;
CREATE TABLE `new_likes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0,
  `bid` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of new_likes
-- ----------------------------
INSERT INTO `new_likes` VALUES ('12', '1', '3', '2022-04-14 18:44:26');
