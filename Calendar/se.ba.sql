/*
Navicat MySQL Data Transfer

Source Server         : 日历正式
Source Server Version : 50505
Source Host           : 161.117.177.248:3306
Source Database       : se.ba

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-04-16 13:20:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ba_agent
-- ----------------------------
DROP TABLE IF EXISTS `ba_agent`;
CREATE TABLE `ba_agent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL DEFAULT 0,
  `uuid` char(40) NOT NULL DEFAULT '',
  `open_id` text NOT NULL DEFAULT '',
  `username` char(64) NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `address` text NOT NULL DEFAULT '',
  `ext_info` mediumtext NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `activation` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  `redirect` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_agent_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_ba_agent_updated_at_idx_ym` (`updated_at_idx_ym`),
  KEY `IDX_ba_agent_rid` (`rid`),
  KEY `IDX_ba_agent_email` (`email`),
  KEY `IDX_ba_agent_activation` (`activation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_agent
-- ----------------------------

-- ----------------------------
-- Table structure for ba_app
-- ----------------------------
DROP TABLE IF EXISTS `ba_app`;
CREATE TABLE `ba_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` char(150) NOT NULL DEFAULT '',
  `module` char(64) NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `data_url` varchar(200) NOT NULL DEFAULT '',
  `visit_url_master` varchar(200) NOT NULL DEFAULT '',
  `visit_url_enterprise` varchar(200) NOT NULL DEFAULT '',
  `visit_url_user` varchar(200) NOT NULL DEFAULT '',
  `visit_url_agent` varchar(200) NOT NULL DEFAULT '',
  `visit_url_developer` varchar(200) NOT NULL DEFAULT '',
  `ips` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `private_key` text NOT NULL DEFAULT '',
  `public_key` text NOT NULL DEFAULT '',
  `public_key_client` text NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_app_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_ba_app_updated_at_idx_ym` (`updated_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_app
-- ----------------------------
INSERT INTO `ba_app` VALUES ('1', 'Base Admin', 'soft-se.ba-F5uhE1SI', '', '', '', '', '', '', '', '', '', '-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA3K9XL0yDSx3s1X2VkG5f63REpJPEjmmV2yamE2ujF0kwS4jq\nHKxE+myAYv+eqtKIMbLpMD61REdVSt1siSKF4b00IHZPvKzgbu1YXgGIA+dQCn3C\nnTk3ybtuMdNvN4KsgJrzIKCvrzoskMB6xPE6q6nd8L3DOiIXXnDqbyij2pkJb7tm\nL2cMcJFCuacFA97ao2XaQ4s6j6pVYruoOiiHXb+FFscCcDYajHk1OcijqJq4WukR\nlC/YHEQFHGpUNu7vTdHdC08IfNDEXQzbcMQyR7wx2pmAtqTuc6VSLIaecgB7Ot0b\n77buggY4zh0mRYPMulnSjnroOm9ZNxpK+FyspwIDAQABAoIBABhLmW6UntOzPLKY\nO9IFxLuYNkJkHjyLdTsGOzxiGjdjEFUQgq2bbZCgR1YezCoEOYg3KSLtlGkq7UHr\ntJo4WcLrYIS+YbLk2WR87sCSrJW5bn9pLva+KN0lrFEB9PmIglM6O8/4wL6qyeM5\nvYpXAF2LuupZCdgpjqcQCU8pizhyclclvvdTxFWdbcc/zT3PecK3fLASvjtO79nk\nAHt4jWCKsJutGF/7aJn1jpJqOdDRP9zpkGGzVMkwXz8r812FGGJ0X3M1TYX+T9v6\nqDjt74+9jdsf1aA5sZFSEncwMPkY19qorgiy6Maesqsa8czp+c0ALjUXHsIha13z\nbqtEr4kCgYEA8V7mKcyCnuMIIfGqU7jeO6Kbnm+Y7MjrbO09oHu/OHBIUUgZAunF\n9OOUpntPnv4JlLcaUxZnF6ddcvaOfH0nMSBv6xknbzXWmDn2zwVr/J/nce5XJsia\n7Z8IfetaVlK/b6Kp5BeBNIIfEp6ZSXj4qYkCoTowYuv/H+W5qZp17J0CgYEA6g97\nSTFMZrD9fm28mGYMqiMyyKJ4RYr0BLVdTY4MdVC9qeCSrOvRGQPlx9XBf8AjMWao\nV7EQug0up3P7eSaD3rHF74xLsW4DS+H+lodMaYqN+mh0tWY9USkSWhUCsQfy/Mfn\nZ5/5NqnOw1c7lvnloDID8gU59Mfiva/NHHaVgRMCgYEAqANp7YCZD+Zy+8XPzVVr\nBFHtRzzhUzmMfxcpBbP96FhoToQEPP3jONLN35EIXGDy5F0Mave2LUXoC6VKi6dG\nh7DPlvO8enIcfXwjq/lXMNNR9QF2Et4i0fMYoyYXjO0uqpOs9MPTwWwuGvThOL6H\nQOPJO9dYZRNnVJnBQijtmxUCgYA69QNdAGn6RALWYdXrnWj7Qjp8h1A8VC/7L6P8\n8Rb6RghATi0kGZJl9rFekN1YzJEQ1Ib678MC2mcyDuj9L1JwOd9d47z9SHTUHG0A\nhbwdqaTxunYLa2FunCXfQwOWjCYMvl3voEAqIMPQOJzt/qx0maScxew/WEoo9at4\ny/YEuQKBgQDen1xc5ci7yMQC9Muhu83uaA60btBCouyUliZDvwNgSSFIEkSOMwNY\n5jB6KjsPFdEL9VFXtEqxBb2uQPm9b3SMNAFRjosbj7VwIJomkPe6pRC1vAaPe+mh\n87sL2MUXod3Bwd5o4Gkh2hdDsAj/AJeFi1BQ+kWRErAjTSQVYg9zLQ==\n-----END RSA PRIVATE KEY-----\n', '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3K9XL0yDSx3s1X2VkG5f\n63REpJPEjmmV2yamE2ujF0kwS4jqHKxE+myAYv+eqtKIMbLpMD61REdVSt1siSKF\n4b00IHZPvKzgbu1YXgGIA+dQCn3CnTk3ybtuMdNvN4KsgJrzIKCvrzoskMB6xPE6\nq6nd8L3DOiIXXnDqbyij2pkJb7tmL2cMcJFCuacFA97ao2XaQ4s6j6pVYruoOiiH\nXb+FFscCcDYajHk1OcijqJq4WukRlC/YHEQFHGpUNu7vTdHdC08IfNDEXQzbcMQy\nR7wx2pmAtqTuc6VSLIaecgB7Ot0b77buggY4zh0mRYPMulnSjnroOm9ZNxpK+Fys\npwIDAQAB\n-----END PUBLIC KEY-----\n', '', '10', '2022-04-08 10:46:16', '202204', '2022-04-08 10:46:16', '202204');
INSERT INTO `ba_app` VALUES ('2', 'Calendar', 'task-calendar-vx5CtJ09', '', 'http://ad.seasonalcal.life', '', '', '', '', '', '', '', '-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEA2e8+D7oTBoZUZI0mp1jynUJDy8odtKUPlptdleiS/mNMDtQC\nVYuJ1he6e3tQsCrRJDDSSPzslQiUSpr1kWT2TFU7rY0Yj0NndS4kRyaa/Tl/CjC6\nI2XLYAUHziUid8ipQ5Y6W6v1+jcFaDgEzINNxpjBuOP+OTaltRRqhAx7ddz69v90\n1yPYvq/mA8n6nil5fu6Eg/Wa3vmEg/fQIFUFLLjeelbp1wwalvrJtC1i1n9h+flY\nZRcEN5cWxZOMAL4gm5PfjirHScRkaEnlrfyXMGnm8Mk0BEr6ygfcLohCebZpQvbA\nfc5oanlBL0/U7GemNzLvl9reqxMaG70RDW4a2QIDAQABAoIBAQDDoMN4AJMFf12Q\nKThybiDS02HiNBs4RhbZHbwDThNtMRtcECnMOobPXNtz7rz+dmDoeL667/c6zMm4\nsqMPEa8Z1ETOxGIwb9QV1waPq/LirlMAhb8A+WZ3WeyouqLXHZP7bIYUNieGpQ9A\nf1CneJI/XG5BFr/lAc3730jCIiZNJBjVrWAzVkez20jJMhExbTNKrEqka4IJimOT\n38j9aktS8Sjvo3irTS29sNwDvPHxr2ZMXl4sTL5vSn9qWs74YYeSM1tvyC32wsAs\nHEm/uXRvHbIQ+bAtp2uOZQlPnxzzr5MOHrQCBigi0gxjxvS2Q+oQRcZJPFB8q9sL\ntrlR7k6lAoGBAPbyx4612tfSG5O0gqwYRYbWiuifoj02kteNoN9ae5ZKAmZzDZFE\nXWww+KAa8KFTlJ5sSKXqWY8SAtbcBBJtj3QR0bijnE7TPaVm2L0xzQSC/wycCF2R\n2nAsfK14xCuPEBO5LNXLB+AOAqoMWttCr8IkL6hFQiceUl3fT9qHrBQ3AoGBAOHs\nNstO23pnFeaJNkOiF2Xklia/inxK91OVbZHEmEg+DhR+zATJ0VZQZVPzNycFzYaM\nqim/2jtEHVJjzfMID0TK/BqR9a/pSbGK2s1sbqK15wZbivbaFDZ3DjtmmHZX9sCz\nszNunaK7YSLXRt7Bjthhds1lLxeIc7aaQ/JX2+FvAoGBANPmx+5hhgQjup4oCuj5\nes+VUWUDdgfoyGBGm4CM9/eiUiTvHPlGHJQ2AWIBO/Jy8dhvOneEfI0T+3O7H5Ng\nh/QlRjyVyA6rnP5NHjZagX4ZmhmL2nHQMgyHYATK2Co4fKcwApUBvyQ3YXnhWCVF\nN+PzgSF3LKi8kBkVAkSWnsZJAoGAWr7as4cVOXE2psbuePQemdrbRO0i3xHtjjEz\nNZ3V3Q6v8SONJ085OoZIbRQATzhReX3+y444Rh3x+SoFi1ztZSBzaui92s7Fvv6b\nLHAlcE3IYhFbHBQ0kjv4B3wKOV7Tyktj64C+6KKDPc9itA7jJZ7gbVf7NmSkte/4\n4trPgxkCgYEAzCCDcL9vCAeiIzkNmH7wLy5D0Qm0EJAUGyVrMjBIJvx+jCbEbfJb\nk7TZlTNo4RsiHB7+uC3wSpngyu2wJ3wVx1n7qj4NPv92LxFImtyxaogx2lqTgCzx\nKQhetKI7AKGfALrtxxEWnpz9PaNRPeeqDg/VLR455sh7tJL+coTy2y8=\n-----END RSA PRIVATE KEY-----\n', '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2e8+D7oTBoZUZI0mp1jy\nnUJDy8odtKUPlptdleiS/mNMDtQCVYuJ1he6e3tQsCrRJDDSSPzslQiUSpr1kWT2\nTFU7rY0Yj0NndS4kRyaa/Tl/CjC6I2XLYAUHziUid8ipQ5Y6W6v1+jcFaDgEzINN\nxpjBuOP+OTaltRRqhAx7ddz69v901yPYvq/mA8n6nil5fu6Eg/Wa3vmEg/fQIFUF\nLLjeelbp1wwalvrJtC1i1n9h+flYZRcEN5cWxZOMAL4gm5PfjirHScRkaEnlrfyX\nMGnm8Mk0BEr6ygfcLohCebZpQvbAfc5oanlBL0/U7GemNzLvl9reqxMaG70RDW4a\n2QIDAQAB\n-----END PUBLIC KEY-----\n', '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyoss72IOIWwnIfQ22GB7\n+DpcLmBeXEbA6mrQNWGNSWz0Eq634E5oUjGcYouuOw6Q9uDpBIYKpgAso9wB9QhY\no/8edNHF7TYi/UIUAGmZJR+zj7awVe6/Rgya91ivtwq9if/ywQGK6BjnzjTPdoo1\nqUibbGOlKAXgUPYQeUju4rRwLNLGrSBYCX/TfSIGS6lb2IXRrc7DLvG6cqCzxlJO\nSUjCABzpDNpBNHLi7kV4pUItfc/87PYR/jx1+JrqhkTPCIybFlTBjmrJyCMHL7WO\n45DhJuJzQ5x5/Kc5b9tcg3hBVoRpcz10BDMgSIM/yv8Hio/aKhTKObDtMvaWybSO\n0wIDAQAB\n-----END PUBLIC KEY-----\n', '10', '2022-04-08 13:22:08', '202204', '2022-04-08 13:22:08', '202204');

-- ----------------------------
-- Table structure for ba_auth_path
-- ----------------------------
DROP TABLE IF EXISTS `ba_auth_path`;
CREATE TABLE `ba_auth_path` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `module` char(64) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `methods` varchar(100) NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_auth_path_updated_at_idx_ym` (`updated_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_auth_path
-- ----------------------------
INSERT INTO `ba_auth_path` VALUES ('1', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/hello', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('2', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/calendar/delete', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('3', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/calendar/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('4', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/seasons/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('5', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/like/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('6', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/seasons/add_or_update', 'POST', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('7', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/seasons/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('8', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/calendar/add_or_update', 'POST', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('9', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/calendar/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('10', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/book/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('11', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/seasons/delete', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('12', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/book/add_or_update', 'POST', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('13', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/calendar/add_or_update', 'POST', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('14', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/calendar/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('15', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/like/like_set', 'POST', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('16', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/user/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('17', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/user/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('18', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/book/delete', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('19', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/book/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('20', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/book/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('21', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/seasons/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('22', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/calendar/month_cells', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('23', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/book/detail', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('24', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/seasons/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('25', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/user/v1/calendar/list', 'GET', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('26', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/book/add_or_update', 'POST', '2022-04-08 13:23:33', '202204');
INSERT INTO `ba_auth_path` VALUES ('27', 'Calendar', 'task-calendar-vx5CtJ09', '/task-calendar-vx5CtJ09/api/master/v1/seasons/add_or_update', 'POST', '2022-04-08 13:23:33', '202204');

-- ----------------------------
-- Table structure for ba_developer
-- ----------------------------
DROP TABLE IF EXISTS `ba_developer`;
CREATE TABLE `ba_developer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL DEFAULT 0,
  `uuid` char(40) NOT NULL DEFAULT '',
  `open_id` text NOT NULL DEFAULT '',
  `username` char(64) NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `activation` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL DEFAULT '',
  `nickname` varchar(200) NOT NULL DEFAULT '',
  `ext_info` mediumtext NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  `redirect` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_developer_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_ba_developer_updated_at_idx_ym` (`updated_at_idx_ym`),
  KEY `IDX_ba_developer_rid` (`rid`),
  KEY `IDX_ba_developer_email` (`email`),
  KEY `IDX_ba_developer_activation` (`activation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_developer
-- ----------------------------

-- ----------------------------
-- Table structure for ba_enterprise
-- ----------------------------
DROP TABLE IF EXISTS `ba_enterprise`;
CREATE TABLE `ba_enterprise` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL DEFAULT 0,
  `uuid` char(40) NOT NULL DEFAULT '',
  `open_id` text NOT NULL DEFAULT '',
  `username` char(64) NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `address` text NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `activation` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL DEFAULT '',
  `ext_info` mediumtext NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 1,
  `apps` text NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  `redirect` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_enterprise_activation` (`activation`),
  KEY `IDX_ba_enterprise_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_ba_enterprise_updated_at_idx_ym` (`updated_at_idx_ym`),
  KEY `IDX_ba_enterprise_rid` (`rid`),
  KEY `IDX_ba_enterprise_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_enterprise
-- ----------------------------

-- ----------------------------
-- Table structure for ba_file
-- ----------------------------
DROP TABLE IF EXISTS `ba_file`;
CREATE TABLE `ba_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL DEFAULT '',
  `user_type` char(32) NOT NULL DEFAULT '',
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `hash` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `content_type` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(200) NOT NULL DEFAULT '',
  `ext` char(32) NOT NULL DEFAULT '',
  `file_size` bigint(20) NOT NULL DEFAULT 0,
  `create_time` bigint(20) NOT NULL DEFAULT 0,
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_file_uuid` (`uuid`),
  KEY `IDX_ba_file_user_type` (`user_type`),
  KEY `IDX_ba_file_user_id` (`user_id`),
  KEY `IDX_ba_file_created_at_idx_ym` (`created_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_file
-- ----------------------------
INSERT INTO `ba_file` VALUES ('1', 'ba-aedb9134-3b8e-40b4-94e1-fa557e4fb59b.jpg', '', '0', '', '17.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('2', 'ba-ebf7c0b3-e4e8-4e74-b4ab-5331b7250bb2.jpg', '', '0', '', '17.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('3', 'ba-8e960c47-b4b2-4743-a74b-909893a1acf0.png', '', '0', '', '5c9ccc9934a7c.png', 'image/png', '', '.png', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('4', 'ba-e0d6e590-9b5e-4110-9931-29af728b93fb.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('5', 'ba-523bd1e7-9ff6-4a99-b169-49d9a1a63a0f.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('6', 'ba-731e0bc0-b38c-437f-b524-cc42bfd1878f.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('7', 'ba-806bb103-aef1-4ad1-80a8-6c50b7b34923.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('8', 'ba-92ab5b84-bfc1-486b-84b0-50f5f5c30c57.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('9', 'ba-432e78c5-2895-45d5-a703-540cd442592a.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('10', 'ba-26cdeeac-e42d-40a6-9af4-44f631bc6c07.jpg', '', '0', '', 'fj43.jpg', 'image/jpeg', '', '.jpg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('11', 'ba-451e04a6-e70e-4223-b0fd-05e0a51daf2d.webp', '', '0', '', 'fj2.webp', 'image/webp', '', '.webp', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('12', 'ba-689a1c4c-ede4-4daf-a9b5-37863a87b57b.jpeg', '', '0', '', '0213EF10-471E-4C82-AFFA-29F8AAABADF5.jpeg', 'image/jpeg', '', '.jpeg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('13', 'ba-43f57fa4-9195-430a-8c5f-639aadcb6771.jpeg', '', '0', '', '60E859FD-84CB-4CEC-9879-59AD2119E5E2.jpeg', 'image/jpeg', '', '.jpeg', '0', '0', '0');
INSERT INTO `ba_file` VALUES ('14', 'ba-fd07d7bc-4d81-4e86-8607-ffbab282e475.jpeg', '', '0', '', 'DBE1E0B5-34DB-4765-8F0C-AEEB4E42D864.jpeg', 'image/jpeg', '', '.jpeg', '0', '0', '0');

-- ----------------------------
-- Table structure for ba_master
-- ----------------------------
DROP TABLE IF EXISTS `ba_master`;
CREATE TABLE `ba_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL DEFAULT 0,
  `uuid` char(40) NOT NULL DEFAULT '',
  `open_id` text NOT NULL DEFAULT '',
  `username` char(64) NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `activation` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL DEFAULT '',
  `ext_info` mediumtext NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  `redirect` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_master_rid` (`rid`),
  KEY `IDX_ba_master_email` (`email`),
  KEY `IDX_ba_master_activation` (`activation`),
  KEY `IDX_ba_master_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_ba_master_updated_at_idx_ym` (`updated_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_master
-- ----------------------------
INSERT INTO `ba_master` VALUES ('1', '1', '', '', 'admin', '', '', '', '$2a$10$wcs.reQAGwvdB95.5HYet.4sdwqowrVgJncoH.TG7ek7HmcsajI9W', '0', '超级管理员', '', '0', '2022-04-08 10:46:16', '202204', '2022-04-08 10:46:16', '202204', '0', '0');

-- ----------------------------
-- Table structure for ba_role
-- ----------------------------
DROP TABLE IF EXISTS `ba_role`;
CREATE TABLE `ba_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` char(64) NOT NULL DEFAULT '',
  `auth_paths` mediumtext NOT NULL DEFAULT '',
  `user_type` mediumtext NOT NULL DEFAULT '',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_role_updated_at_idx_ym` (`updated_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_role
-- ----------------------------
INSERT INTO `ba_role` VALUES ('1', '超级管理员', '** [ANY]', '', '2022-04-08 10:46:16', '202204');
INSERT INTO `ba_role` VALUES ('2', '默认代理商权限', '/*/api/agent/** [ANY]', '', '2022-04-08 10:46:16', '202204');
INSERT INTO `ba_role` VALUES ('3', '默认商家权限', '/*/api/enterprise/** [ANY]', '', '2022-04-08 10:46:16', '202204');
INSERT INTO `ba_role` VALUES ('4', '默认用户权限', '/*/api/user/** [ANY]', '', '2022-04-08 10:46:16', '202204');
INSERT INTO `ba_role` VALUES ('5', '默认开发者权限', '/*/api/develop/** [ANY]', '', '2022-04-08 10:46:16', '202204');

-- ----------------------------
-- Table structure for ba_user
-- ----------------------------
DROP TABLE IF EXISTS `ba_user`;
CREATE TABLE `ba_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL DEFAULT 0,
  `uuid` char(40) NOT NULL DEFAULT '',
  `open_id` text NOT NULL DEFAULT '',
  `username` char(64) NOT NULL DEFAULT '',
  `head_image` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `activation` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL DEFAULT '',
  `nickname` varchar(200) NOT NULL DEFAULT '',
  `ext_info` mediumtext NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at_idx_ym` int(11) NOT NULL DEFAULT 0,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  `redirect` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_ba_user_rid` (`rid`),
  KEY `IDX_ba_user_email` (`email`),
  KEY `IDX_ba_user_activation` (`activation`),
  KEY `IDX_ba_user_created_at_idx_ym` (`created_at_idx_ym`),
  KEY `IDX_ba_user_updated_at_idx_ym` (`updated_at_idx_ym`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_user
-- ----------------------------
INSERT INTO `ba_user` VALUES ('1', '4', '3e56a8dc-aa0b-41c2-9510-6557d1bf01eb', '', '', 'ba-ebf7c0b3-e4e8-4e74-b4ab-5331b7250bb2.jpg', '3681314@qq.com', '', '$2a$10$LlTyoiDlDCAAxq2OGcamB.sbg/ig1l4mYJMMIg4JQsDXG46ww2Pme', '0', 'NewUser', 'NewUser', '{}', '0', '2022-04-08 13:42:49', '202204', '2022-04-08 13:42:49', '202204', '0', '0');
INSERT INTO `ba_user` VALUES ('2', '4', '554f8129-908f-47bf-b0f8-f299d5aff71f', '', '', '', 'beyond3681314@gmail.com', '', '$2a$10$XFB6gt46VF.d1eUmvTn8d.oHrJ4DuSS3HYGXXPzOIdXuEdCkK71PG', '0', 'NewUser', '', '{}', '0', '2022-04-08 13:50:04', '202204', '2022-04-08 13:50:04', '202204', '0', '0');
INSERT INTO `ba_user` VALUES ('3', '4', '8afba75c-fbb9-404d-b2e9-37dbdbaa0fe2', '', '', 'ba-8e960c47-b4b2-4743-a74b-909893a1acf0.png', '347786293@qq.com', '', '$2a$10$rwLcYiH9am/9ytd7u0hEvu8XQGb/xmyvnFGDuke5GKaRGqblxopdG', '0', 'NewUser', 'NewUser', '{}', '0', '2022-04-12 09:55:31', '202204', '2022-04-12 09:55:31', '202204', '0', '0');
INSERT INTO `ba_user` VALUES ('4', '4', 'f5213776-5e56-466b-b435-c1fd477f0e08', '', 'w1ww1', '', '', '', '$2a$10$szL.38Gh4kEV7hmHdfb6eOHJFafGYXN0cCwefWA/vP8Nz6ibcBGPS', '0', 'NewUser', '', '{}', '0', '2022-04-13 00:46:48', '202204', '2022-04-13 00:46:48', '202204', '0', '0');
INSERT INTO `ba_user` VALUES ('5', '4', '1f3ddc6f-9f88-4159-a828-57b6e34c06c5', '', '222222', 'ba-e0d6e590-9b5e-4110-9931-29af728b93fb.jpg', '', '', '$2a$10$nMsfIFYwznl4AKlSfMYVOuXBg/ZQtLyPrmTjlHh1kuNDSAbmGvPIa', '0', 'NewUser', 'NewUser', '{}', '0', '2022-04-13 00:52:13', '202204', '2022-04-13 00:52:13', '202204', '0', '0');

-- ----------------------------
-- Table structure for ba_verify_code
-- ----------------------------
DROP TABLE IF EXISTS `ba_verify_code`;
CREATE TABLE `ba_verify_code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` char(20) NOT NULL DEFAULT '',
  `module` char(64) NOT NULL DEFAULT '',
  `account` char(64) NOT NULL DEFAULT '',
  `code` char(64) NOT NULL DEFAULT '',
  `expire_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ba_verify_code
-- ----------------------------
