package pub

import (
	"seasonalcal.life/rechy/glog"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

func init() {
	RegisterCommand("open_module", CommandOpenModule)
	RegisterCommand("close_module", CommandCloseModule)
}

// 开启模块
func CommandOpenModule(req *baapi.Command) (string, interface{}, interface{}) {

	glog.Infolns("run pub command:", req)

	return "success", "成功", nil
}

// 关闭模块
func CommandCloseModule(req *baapi.Command) (string, interface{}, interface{}) {

	glog.Infolns("run pub command:", req)

	return "success", "成功", nil
}
