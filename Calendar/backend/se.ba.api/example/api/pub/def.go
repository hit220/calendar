package pub

import (
	"sync"

	baapi "seasonalcal.life/rechy/se.ba.api"
)

// 命令对应方法的键值对集
// cmd => func(*baapi.Command) (string, interface {}, interface{})
var fns sync.Map

func FindHandler(cmd string) (func(*baapi.Command) (string, interface{}, interface{}), bool) {
	r, ok := fns.Load(cmd)
	if !ok {
		return nil, false
	}
	f, ok := r.(func(*baapi.Command) (string, interface{}, interface{}))
	if !ok {
		return nil, false
	}

	return f, true
}

func RegisterCommand(cmd string, fn func(*baapi.Command) (string, interface{}, interface{})) {
	fns.Store(cmd, fn)
}
