package api

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

//
func Unauthorized(c *gin.Context, code int, message string) {
	baapi.JSONReply(c, "nologin", fmt.Sprintf(message))
}

// 登录
func LoginResponse(c *gin.Context, code int, token string, tm time.Time) {
	user, ok := c.Get("user")
	if !ok {
		baapi.JSONReply(c, "error", "用户信息不存在")
		return
	}
	dd := gin.H{
		"token": token,
		"user":  user,
	}
	baapi.JSONReply(c, "success", "登录成功", dd)
}

// 登出
func LogoutResponse(c *gin.Context, code int) {
	baapi.JSONReply(c, "success", "退出成功")
}
