import { AppConfig } from './../../app.config';
import Component from 'vue-class-component';
import { AppVue } from '../../app.vue';
import './home.scss';
import { AppUtil } from '../../app.util';
import { AppNet } from '../../app.net';
import { uploadFileByAccept } from '../../utils/upload';

@Component({
    template: require('./home.html'),
    props: {},
})
export class PageHome extends AppVue {
    toChildrenParams = [];
    activeKey = 12;

    //
    count = 12;
    data = '---';
    dataLogin1 = null;
    dataLogin2 = null;
    dataLogin11 = null;
    dataRegister1 = null;
    dataRegister2 = null;
    dataRegister3 = null;
    activationData1 = null;
    resetPasswordData1 = null;
    resetPasswordData2 = null;
    getProfileData = null;
    setProfileData = null;
    dataBind12 = null;
    dataBind13 = null;

    loginForm1 = {
        type: 'master',
        account: '', // 账号/邮箱/手机号
        password: '',
    };

    loginForm2 = {
        type: 'master',
        phone: '', // 手机号
        code: '',
    };

    loginForm11 = {
        type: 'master',
    };

    actionLock = 0;
    actionMax = 60;

    regForm1 = {
        type: 'master',
        username: '',
        password: '',
        rePassword: '',
    };

    regForm2 = {
        type: 'master',
        phone: '',
        code: '',
    };

    regForm3 = {
        type: 'master',
        email: '',
        password: '',
        rePassword: '',
    };

    activationForm1 = {
        type: 'master',
        email: '',
        code: '',
    };

    resetPasswordForm1 = {
        type: 'master',
        phone: '',
        code: '',
        password: '',
        rePassword: '',
    };

    resetPasswordForm2 = {
        type: 'master',
        email: '',
        code: '',
        password: '',
        rePassword: '',
    };

    profileEditForm = {
        name: '',
        nickname: '',
        head_image: '',
    };

    bindForm12 = {
        phone: '', // 手机号
        code: '',
    };

    bindForm13 = {
        email: '', // 邮箱
        code: '',
    };

    constructor() {
        super();
    }

    mounted() {
        const urlObject = AppUtil.parseURL(location.href);
        const user = urlObject ? urlObject.params['user'] : '';
        const token = urlObject ? urlObject.params['token'] : '';
        const action = urlObject ? urlObject.params['action'] : '';
        if (user && token) {
            this.loginByQuery(urlObject, user, token);
            return;
        }

        if (action == 'login_wx_pub') {
            this.onLoginByWxPubResult(urlObject.params);
        }

        // 已经登录
        if (AppConfig.isLogin()) {
            // this.hrefTo('/platform/home');

            this.updateSession();
        }
    }

    async updateSession(user?) {
        const sess = user ? user : this.$store.state.sess;
        this.profileEditForm.name = sess && sess.name ? sess.name : '';
        this.profileEditForm.nickname = sess && sess.nickname ? sess.nickname : '';
        this.profileEditForm.head_image = sess && sess.head ? sess.head : '';
    }

    async loginByQuery(urlObject, userinfo, inToken) {
        try {
            const uinfo = JSON.parse(userinfo);
            if (uinfo.id) {
                await AppConfig.setSessToken({ token: inToken });
                await AppConfig.setSession(uinfo);

                delete urlObject.params['user'];
                delete urlObject.params['token'];

                this.updateSession();

                location.href = AppUtil.rebuildUrl(urlObject);
            }
        } catch (_) {
            return;
        }
    }

    // 生命周期 - onload
    created() {
        console.log('created');

        console.log('父级参数', this.$props);
    }

    onClick() {
        this.count++;
    }

    async onRequest() {
        const params = {
            param1: 'abc...',
        };
        const data = await AppNet.get('/api/master/v1/hello', params);
        this.data = data;

        this.data = JSON.stringify(data, null, '\t');
    }

    async onLogin() {
        if (this.loginForm1.account.length < 4 || this.loginForm1.password.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/login_apply');
        if (!req || req.code !== 'success') {
            this.alert(req.message);
            return;
        }
        const apply = req.data;

        const params = {
            type: this.loginForm1.type,
            device: '123456',
            apply_token: apply.apply_token,
            module: apply.module,
            account: this.loginForm1.account,
            password: AppUtil.toSha256(this.loginForm1.password),
        };

        const url = apply.url;

        const data = await AppNet.post(url, this.strParams(params));
        this.dataLogin1 = data;

        if (!data || data.code !== 'success') {
            this.alert(data.message);
            return;
        }
        await AppConfig.setSessToken({ token: data.data.token });
        await AppConfig.setSession(data.data.user);
        await AppConfig.set('loginAccount', this.loginForm1.account);
        await AppConfig.set('loginPassword', this.loginForm1.password);

        this.updateSession();

        this.alert(data.message);
    }

    async onLoginByPhone() {
        if (this.loginForm2.phone.length < 4 || this.loginForm2.code.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/login_apply');
        if (!req || req.code !== 'success') {
            this.alert(req.message);
            return;
        }
        const apply = req.data;

        const params = {
            type: this.loginForm2.type,
            device: '123456',
            apply_token: apply.apply_token,
            module: apply.module,
            phone: this.loginForm2.phone,
            code: this.loginForm2.code,
        };

        const url = apply.url_by_phone;

        const data = await AppNet.post(url, this.strParams(params));
        this.dataLogin2 = data;

        if (!data || data.code !== 'success') {
            this.alert(data.message);
            return;
        }
        await AppConfig.setSessToken({ token: data.data.token });
        await AppConfig.setSession(data.data.user);
        await AppConfig.set('loginAccount', this.loginForm2.phone);

        this.updateSession();

        this.alert(data.message);
    }

    async onLoginByWxPub() {
        const req = await AppNet.get('/pub/login_apply');
        if (!req || req.code !== 'success') {
            this.alert(req.message);
            return;
        }
        const apply = req.data;

        const params = {
            type: this.loginForm2.type,
            device: '123456',
            apply_token: apply.apply_token,
            module: apply.module,
            request_url: apply.url_by_wx_pub2,
            // return_url: location.href, // 正式服带域名时方可
            // 暂用3g3e.com中转
            return_url: 'https://3g3e.com/redirect?url=' + encodeURIComponent(location.href),
        };

        const url = apply.url_by_wx_pub1;

        const res = await AppNet.post(url, this.strParams(params));
        this.dataLogin11 = res;

        if (!res || res.code !== 'success') {
            this.alert(res.message);
            return;
        }

        location.href = res.data;
    }

    async onLoginByWxPubResult(locationParams) {
        const params = {
            type: locationParams['type'],
            device: locationParams['device'],
            apply_token: locationParams['apply_token'],
            module: locationParams['module'],
            code: locationParams['code'],
            state: locationParams['state'],
        };

        const url = locationParams['request_url'];

        const res = await AppNet.post(url, this.strParams(params));
        this.dataLogin11 = res;
        if (!res || res.code !== 'success') {
            this.alert(res.message);
            return;
        }

        await AppConfig.setSessToken({ token: res.data.token });
        await AppConfig.setSession(res.data.user);
        await AppConfig.set('loginAccount', this.loginForm2.phone);

        this.updateSession();

        // this.alert(data.message);
    }

    async onLogout() {
        // const data = await AppNet.post('/api/logout', null);
        // if (!data || data.code !== 'success') {
        //     alert(data.message);
        //     return;
        // }
        // console.log(data);

        AppConfig.setSession(null);

        this.updateSession();

        console.log('登出成功');
    }

    async onRegisterByUsername() {
        if (this.regForm1.username.length < 4 || this.regForm1.password.length < 4 || this.regForm1.rePassword.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        if (this.regForm1.rePassword !== this.regForm1.password) {
            this.alert('确认密码与密码不一致');
            return;
        }

        const req = await AppNet.get('/pub/register_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = {
            type: this.regForm1.type,
            device: '123456',
            module: apply.module,
            username: this.regForm1.username,
            password: AppUtil.toSha256(this.regForm1.password),
        };

        const url = apply.url_by_username;
        const data = await AppNet.post(url, this.strParams(params));
        this.dataRegister1 = data;

        this.alert(data.message);
    }

    async onPhoneVerifyCode($e, form) {
        if (this.actionLock < this.actionMax) {
            if (form.phone.length < 11) {
                this.alert('请输入有效手机号');
                return;
            }

            if (this.actionLock === 0) {
                const req = await AppNet.get('/pub/verify_code_apply');
                if (!req || req.code !== 'success') {
                    alert(req.message);
                    return;
                }

                const apply = req.data;

                const params = {
                    type: form.type,
                    device: '123456',
                    module: apply.module,
                    phone: form.phone,
                };

                const url = apply.url_by_phone;
                const data = await AppNet.get(url, this.strParams(params));
                this.dataRegister2 = data;

                this.alert(data.message);
            }
            this.actionLock++;
            setTimeout(() => {
                this.onPhoneVerifyCode($e, form);
            }, 1000);
            return;
        }
        this.actionLock = 0;
    }

    async onEmailVerifyCode($e, form) {
        if (this.actionLock < this.actionMax) {
            if (form.email.indexOf('@') < 1) {
                this.alert('请输入有效邮箱');
                return;
            }

            if (this.actionLock === 0) {
                const req = await AppNet.get('/pub/verify_code_apply');
                if (!req || req.code !== 'success') {
                    alert(req.message);
                    return;
                }

                const apply = req.data;

                const params = {
                    type: form.type,
                    device: '123456',
                    module: apply.module,
                    email: form.email,
                };

                const url = apply.url_by_email;
                const data = await AppNet.get(url, this.strParams(params));
                this.activationData1 = data;

                this.alert(data.message);
            }
            this.actionLock++;
            setTimeout(() => {
                this.onEmailVerifyCode($e, form);
            }, 1000);
            return;
        }
        this.actionLock = 0;
    }

    async onRegisterByPhone() {
        if (this.regForm2.phone.length < 11 || this.regForm2.code.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/register_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = {
            type: this.regForm2.type,
            device: '123456',
            module: apply.module,
            phone: this.regForm2.phone,
            code: this.regForm2.code,
        };

        const url = apply.url_by_phone;
        const data = await AppNet.post(url, this.strParams(params));
        this.dataRegister2 = data;

        this.alert(data.message);
    }

    async onRegisterByEmail() {
        if (this.activationForm1.email.length < 4 || this.activationForm1.code.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/register_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = {
            type: this.regForm3.type,
            device: '123456',
            module: apply.module,
            email: this.regForm3.email,
            password: AppUtil.toSha256(this.regForm3.password),
        };

        const url = apply.url_by_email;
        const data = await AppNet.post(url, this.strParams(params));
        this.dataRegister1 = data;

        this.alert(data.message);
    }

    async onActivationByEmail() {
        if (this.activationForm1.email.indexOf('@') < 1 || this.activationForm1.code.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/activation_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = {
            type: this.activationForm1.type,
            device: '123456',
            module: apply.module,
            email: this.activationForm1.email,
            code: this.activationForm1.code,
        };

        const url = apply.url_by_email;
        const data = await AppNet.post(url, this.strParams(params));
        this.dataRegister2 = data;

        this.alert(data.message);
    }

    async onResetPasswordByPhone() {
        if (this.resetPasswordForm1.phone.length < 11 || this.resetPasswordForm1.code.length < 4 || this.resetPasswordForm1.password.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        if (this.resetPasswordForm1.rePassword !== this.resetPasswordForm1.password) {
            this.alert('确认密码与密码不一致');
            return;
        }

        const req = await AppNet.get('/pub/reset_password_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = {
            type: this.resetPasswordForm1.type,
            device: '123456',
            module: apply.module,
            phone: this.resetPasswordForm1.phone,
            code: this.resetPasswordForm1.code,
            password: AppUtil.toSha256(this.resetPasswordForm1.password),
        };

        const url = apply.url_by_phone;
        const data = await AppNet.post(url, this.strParams(params));
        this.resetPasswordData1 = data;

        this.alert(data.message);
    }

    async onResetPasswordByEmail() {
        if (this.resetPasswordForm2.email.indexOf('@') < 1 || this.resetPasswordForm2.code.length < 4 || this.resetPasswordForm2.password.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        if (this.resetPasswordForm2.rePassword !== this.resetPasswordForm2.password) {
            this.alert('确认密码与密码不一致');
            return;
        }

        const req = await AppNet.get('/pub/reset_password_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = {
            type: this.resetPasswordForm2.type,
            device: '123456',
            module: apply.module,
            email: this.resetPasswordForm2.email,
            code: this.resetPasswordForm2.code,
            password: AppUtil.toSha256(this.resetPasswordForm2.password),
        };

        const url = apply.url_by_email;
        const data = await AppNet.post(url, this.strParams(params));
        this.resetPasswordData2 = data;

        this.alert(data.message);
    }

    async onGetProfile() {
        if (!this.$store.state.isLogin) {
            this.alert('未登录');
            return;
        }

        const req = await AppNet.get('/pub/profile_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const url = apply.url;
        const data = await AppNet.get(url);
        if (data.code !== 'success') {
            this.alert(data.message);
            return;
        }

        /****** >> START 新获取的信息包含最新的Token */
        // 该方法可以用于Token刷新
        await AppConfig.setSessToken({ token: data.data.token });
        await AppConfig.setSession(data.data.user);
        await AppConfig.set('loginAccount', this.loginForm1.account);
        await AppConfig.set('loginPassword', this.loginForm1.password);

        this.updateSession(data.data.user);
        /************ << END */

        this.getProfileData = data;
    }

    async onSetProfile() {
        if (!this.$store.state.isLogin) {
            this.alert('未登录');
            return;
        }

        const req = await AppNet.get('/pub/profile_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }

        const apply = req.data;

        const params = this.copy(this.profileEditForm);
        const cols = Object.keys(this.profileEditForm);
        params.cols = cols.join(',');

        const url = apply.url;
        const data = await AppNet.post(url, this.strParams(params));

        this.alert(data.message);

        this.setProfileData = data;
    }

    /***************** 上传 */
    loading = false;
    async uploadFile() {
        const req = await AppNet.get('/pub/profile_apply');
        if (!req || req.code !== 'success') {
            alert(req.message);
            return;
        }
        const apply = req.data;

        uploadFileByAccept(apply.url_upload, 'image/*', {
            onStart: (file, formData) => {
                this.loading = true;
            },
            onFinish: (err, re) => {
                this.loading = false;

                if (err) {
                    this.alert(err);
                    return;
                }

                this.profileEditForm.head_image = re.uuid;
            },
        });
    }

    async onAccountBindPhone() {
        if (this.bindForm12.phone.length < 4 || this.bindForm12.code.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/profile_apply');
        if (!req || req.code !== 'success') {
            this.alert(req.message);
            return;
        }
        const apply = req.data;

        const params = {
            phone: this.bindForm12.phone,
            code: this.bindForm12.code,
        };

        const url = apply.url_by_phone;

        const data = await AppNet.post(url, this.strParams(params));
        this.dataBind12 = data;

        if (!data || data.code !== 'success') {
            this.alert(data.message);
            return;
        }

        this.alert(data.message);
    }

    async onAccountBindEmail() {
        if (this.bindForm13.email.length < 4 || this.bindForm13.code.length < 4) {
            this.alert('请输入有效值');
            return;
        }

        const req = await AppNet.get('/pub/profile_apply');
        if (!req || req.code !== 'success') {
            this.alert(req.message);
            return;
        }
        const apply = req.data;

        const params = {
            phone: this.bindForm13.email,
            code: this.bindForm13.code,
        };

        const url = apply.url_by_email;

        const data = await AppNet.post(url, this.strParams(params));
        this.dataBind13 = data;

        if (!data || data.code !== 'success') {
            this.alert(data.message);
            return;
        }

        this.alert(data.message);
    }
}
