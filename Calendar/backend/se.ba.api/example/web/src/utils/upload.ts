import { AppNet } from "../app.net";

export const uploadFileByObject = async (uploadUrl, efile: File, observe, inputName?) => {
    const params = {
        size: efile.size.toString(),
        name: efile.name,
        types: efile.type,
        header: ''
    };
    if (params.types === '') {
        params.types = getCaption(efile.name);
    }

    const a = new FormData();
    a.append('file', efile);

    if(observe && observe.onStart) {
        observe.onStart(efile, a);
    }
    const data = await AppNet.upload(uploadUrl, a, null, (p) => console.log(p));
    if (data.code !== 'success') {
        if(observe && observe.onFinish) {
            observe.onFinish(data.code + ':' + data.message, null);
        }
    } else {
        if(observe && observe.onFinish) {
        observe.onFinish('', {
            uuid: data.data.uuid,
            name: efile.name,
            org: data.data,
        });
    }
    }
};

export const uploadFileByAccept = (uploadUrl, accept, observe, inputName?) => {
    const input = document.createElement('input');
    if (accept) {
        input.accept = accept;
    }
    input.type = 'file';
    input.name = inputName ? inputName : 'file';
    input.onchange = async () => {
        if (observe && observe.checkFunc) {
            const icontinue = await observe.checkFunc(input.files[0], input);
            console.log(icontinue);
            if (!icontinue) {
                return false;
            }
        }
        await uploadFileByObject(uploadUrl, input.files[0], observe, inputName);
    };
    console.log(input);
    input.click();
};
export const getCaption = (obj) => {
    const i = obj.lastIndexOf('.');
    obj = obj.substring(i + 1, obj.length);
    return obj;
}