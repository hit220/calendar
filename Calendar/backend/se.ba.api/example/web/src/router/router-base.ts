import Vue from 'vue';
import VueRouter from 'vue-router';
import { PageHome } from '../pages/home/home';

// 同一path多次push不提示错误
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};

Vue.use(VueRouter);

const routes: any = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        component: PageHome,
        meta: {
            title: '首页'
        }
    },
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {

    // ...

    next();

});

export default router;