
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const delayDuration = 500;
const delayUploadState = {
    // upload
    uploadLoaded: 0,
    uploadTotal: 0,
    uploadTaskNum: 0,
    uploadTasks: [],

    timerId: 0,
    lastTime: 0,
    startTime: 0,

    updateToState(state, force?) {
        clearTimeout(this.timerId);

        const nowTime = (new Date()).getTime();
        if (force || nowTime - this.lastTime > delayDuration) {
            this.lastTime = nowTime;
            this.update(state);
            return;
        }
        this.timerId = setTimeout(() => {
            this.update(state);
        }, delayDuration);
    },

    update(state) {
        state.uploadLoaded = this.uploadLoaded;
        state.uploadTotal = this.uploadTotal;
        state.uploadTaskNum = this.uploadTaskNum;
        state.uploadTasks = this.uploadTasks;

        if (this.uploadTotal < 1) {
            state.uploadRemainMinutes = 0;
        } else {
            if (this.uploadLoaded < 1) {
                state.uploadRemainMinutes = 9999;
            } else {
                let d = ((new Date()).getTime() - this.startTime) / 60000.0;
                if (d > 9999) {
                    d = 9999;
                } else if (d < 1) {
                    d = 1;
                }
                state.uploadRemainMinutes = Math.ceil(d * this.uploadTotal / this.uploadLoaded - d);
            }
        }
    },


    //  
    setUploadTask(s, data) {
        if (this.uploadTasks.length < 1) {
            this.startTime = (new Date()).getTime();
        }
        for (const item of this.uploadTasks) {
            if (item.key === data.key) {
                return;
            }
        }
        this.uploadTasks.push({
            key: data.key,
            name: data.name,
            size: data.size,
            percent: 0,
        });
        this.uploadTaskNum = this.uploadTasks.length;
        this.uploadTotal += data.size;

        this.updateToState(s, true);
    },

    updateUploadTask(s, data, force?) {
        let loaded = 0;
        let total = 0;
        let item;
        for (let i = 0; i < this.uploadTasks.length; i++) {
            item = this.uploadTasks[i];
            if (this.uploadTasks[i].key === data.key) {
                item = {
                    ...item,
                    percent: data.percent,
                };
                this.uploadTasks[i] = item;
            }

            loaded += item.size * item.percent;
            total += item.size;
        }

        this.uploadLoaded = loaded;
        this.uploadTotal = total;

        this.updateToState(s, force);
    },

    removeUploadTask(s, data) {

        this.updateUploadTask(s, { key: data.key, percent: 1.0 }, true);

        const res = [];
        let loaded = 0;
        let total = 0;
        for (const item of this.uploadTasks) {
            if (item.key !== data.key) {
                res.push(item);
                loaded += item.size * item.percent;
                total += item.size;
            }
        }
        this.uploadTasks = res;
        this.uploadTaskNum = res.length;
        this.uploadLoaded = loaded;
        this.uploadTotal = total;

        if (this.uploadTaskNum < 1) {
            setTimeout(() => {
                this.updateToState(s);
            }, 1000);
            return;
        }
        this.updateToState(s);
    },

};

export const AppState = new Vuex.Store({
    state: {
        // 选中的菜单
        selectkey: { selectedKeys: ['/ucenter/home/core'], openKeys: [''] },

        // 左侧栏展开与否
        collapsed: screen.width < 752 ? true : false,

        // 登录/session
        isLogin: false,
        sess: null,
        isAdmin: false,

        // 语言
        lang: 'en',

        // old
        msg: -1,

        // upload
        uploadLoaded: 0,
        uploadTotal: 0,
        uploadTaskNum: 0,
        uploadRemainMinutes: 0,
        uploadTasks: [],

    },
    mutations: {

        changeCollapsed(state, collapsed) {
            state.collapsed = collapsed;
        },

        async setSessionStatus(state, data) {
            state.sess = data.sess;
            state.isLogin = data.isLogin;
            console.log('state.setSessionStatus.isLogin', state.isLogin);
        },

        setSelectedKeys(state, data) {
            state.selectkey = data;
        },

        setSelectedOpenKeys(state, data) {
            state.selectkey.openKeys = data;
        },

        setLang(state, data) {
            state.lang = data;
        },

        // old
        setMsgLength(state, msg) {
            state.msg = msg;
        },

        //  
        setUploadTask(s, data) {
            delayUploadState.setUploadTask(s, data);
        },
        updateUploadTask(s, data) {
            delayUploadState.updateUploadTask(s, data);
        },
        removeUploadTask(s, data) {
            delayUploadState.removeUploadTask(s, data);
        },
    },
});
