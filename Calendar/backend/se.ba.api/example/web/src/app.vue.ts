import Component from 'vue-class-component';
import { mapState, mapMutations } from 'vuex';
import { sprintf } from 'sprintf-js';
import { BigNumber } from 'bignumber.js';
import { AppUtil } from './app.util';
import { Modal } from 'ant-design-vue';
import { Language } from './lang/lang';
import { AppConfig } from './app.config';
import Vue from 'vue';

@Component({
    computed: {
        ...mapState(['selectKey', 'collapsed', 'isLogin', 'isAdmin', 'sess', 'lang']),
    },
    methods: {
        ...mapMutations(['changeCollapsed']),
    },
})
export class AppVue extends Vue {

    LANG(key: string): string {
        return Language.lang(this.$store.state.lang, key);
    }

    setLang(key) {
        AppConfig.setLang(key);
        console.log(key);
    }

    json(v) {
        return JSON.stringify(v);
    }

    sprintf(...args) {
        const fargs = [];
        for (const item of args) {
            if (typeof (item) !== 'undefined') {
                fargs.push(item);
            }
        }
        if (fargs.length < 2) {
            return '-';
        }
        return sprintf(...fargs);
    }

    numberFormat(v, n = 6) {
        if (!v) {
            return (new BigNumber(0)).toFormat(n);
        }
        return (new BigNumber(v)).toFormat(n);
    }

    numberFixed(v, n = 6) {
        if (!v || v.length === 0 || isNaN(v)) {
            v = '0';
        }
        return (new BigNumber(v)).toFixed(n);
    }

    toFloat(v) {
        if (v instanceof Number) {
            return v;
        }
        const fv = parseFloat(v);
        return isNaN(fv) ? 0 : fv;
    }

    percent(v, onlyInt?) {
        const fv = parseFloat(v);
        if (onlyInt) {
            return isNaN(fv) ? 0 : Math.round(fv * 100.0);
        }
        return isNaN(fv) ? 0 : (Math.round(fv * 1000.0) / 10.0);
    }

    num(v) {
        // tslint:disable-next-line: radix
        return parseFloat((parseInt((parseFloat(v) * 100).toString()) / 100).toString());
    }

    // fmt: yyyy-MM-dd hh:mm:ss
    date(v, fmt) {
        return AppUtil.formatDate(AppUtil.newDate(v), fmt);
    }

    xstr(v, left, right) {
        return AppUtil.xstrMid(v, left, right);
    }

    isWeiXin() {
        const chk = new RegExp('MicroMessenger', 'i');
        const ua = navigator.userAgent.toLowerCase();
        return chk.test(ua);
    }

    isNotWeiXin() {
        return !this.isWeiXin();
    }

    showNeedLogin() {
        Modal.warning({
            title: this.LANG('Message.LoginTimeout'),
            content: this.LANG('Message.LoginTimeoutDesc'),
            okText: this.LANG('OK'),
            onOk: () => {
                this.$router.push('/ucenter/Login');
            }
        });

    }

    canNav() {
        if (this.$store.state.uploadTaskNum > 0) {
            Modal.warning({
                title: this.LANG('Notice'),
                content: 'Please wait for upload complete',
                okText: this.LANG('OK'),
                onOk: () => { }
            });
            return false;
        }
        return true;
    }

    async hrefTo(path, checkLogin?, checkAdmin?, params?) {
        if (!this.canNav()) {
            return;
        }

        if (checkLogin && !this.$store.state.isLogin) {
            this.showNeedLogin();
            return;
        }
        if (checkAdmin && !this.$store.state.isAdmin) {
            Modal.warning({
                title: this.LANG('Failed'),
                content: 'Not if admin',
                okText: this.LANG('OK'),
                onOk: () => {
                    this.$router.push('/ucenter/Login');
                }
            });
            return;
        }

        if (params) {
            /*
            // 重编码
            for (const key of Object.keys(params)) {
                if (typeof (params[key]) === 'object') {
                    params[key] = 'o:' + JSON.stringify(params[key]);
                } else if (typeof (params[key]) === 'boolean') {
                    params[key] = 'b:' + (params[key] ? 'true' : 'false');
                } else {
                    params[key] = 's:' + params[key];
                }
            }
            */
        }

        // 页面回到顶部
        window.scrollTo(0, 0);
        this.$router.push({ path, query: params });
    }

    hrefBack() {
        if (!this.canNav()) {
            return;
        }
        this.$router.go(-1);
    }

    file(path: any) {
        if (!path) {
            return '';
        }
        if (typeof (path) === 'object') {
            return path;
        }
        if (('' + path).indexOf('/') > -1) {
            return path;
        }
        if (('' + path).indexOf('ba-') === 0) {
                return AppConfig.getBAFileBaseUrl(path);
            }
        return AppConfig.getFileBaseUrl(path);
    }

    copy(obj) {
        if (!obj) {
            return obj;
        }
        if (typeof (obj) !== 'object') {
            return obj;
        }
        return JSON.parse(JSON.stringify(obj));
    }

    int(v) {
        if (!v) {
            return 0;
        }
        const r = parseInt(v, 10);
        return isNaN(r) ? 0 : r;
    }

    float(v) {
        if (!v) {
            return 0.0;
        }
        const r = parseFloat(v);
        return isNaN(r) ? 0 : r;
    }

    sel(...args) {
        // console.log('this.$store.state.selectKey', this.$store.state.selectKey);
        for (const v of args) {

            if (v === this.$store.state.selectKey) {
                return true;
            }
        }
        return false;
    }

    isEmpty(v, k?) {
        return AppUtil.isEmpty(v, k);
    }

    alert(msg) {
        alert(msg);
    }

    strParams(params) {
        if (params === null) {
            return null;
        }
        for (const k of Object.keys(params)) {
            params[k] = '' + params[k];
        }
        return params;
    }

    ntf(v) {
        return v.replace(/[\n\r]+/g, '\\n');
    }


    delayScrollTimer;
    scrollToBottom(id) {
        // 滚动到底部
        clearTimeout(this.delayScrollTimer);
        this.delayScrollTimer = setTimeout(() => {
            const a = document.getElementById(id);
            if (a) {
                a.scrollTop = a.scrollHeight;
            }
        }, 500);
    }

    hrefBlank(url) {
        const a = document.createElement('a');
        a.href = url;
        a.target = '_blank';
        a.click();
    }
}