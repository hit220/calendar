export class AppUploadOptions {
    public action = 'http://localhost/uplad.do';            // 上传接口
    public filter = '';                                     // 文件类型过滤器
    public maxsize = 50;                                    // 文件上传大小限制，单位：M
    public params: Map<string, any>;                        // 请求参数
    public selectFileCallback: (...args: any[]) => Promise<any>;    // 选中文件了检测
    public startCallback: (...args: any[]) => void;                 // 开始上传的回调函数
    public progressCallback: (...args: any[]) => void;      //  上传进度回调函数
    public successCallback: (...args: any[]) => void;       // 上传成功回调函数
    public errorCallback: (...args: any[]) => void;         // 上传错误的回调函数

}

export class AppUploader {
    private $lastTimestamp = 0;     // 上传时间增量
    private $lastLoaded = 0;        // 上传进度增量

    // 上传配置项
    public options: AppUploadOptions = new AppUploadOptions();

    constructor() { }

    // 配置方法
    public config(action, params: Map<string, any>, filter, maxsize, startcb) {
        if (action && action !== '') {
            this.options.action = action;
        }
        if (filter) {
            this.options.filter = filter;
        }
        if (maxsize && !isNaN(maxsize)) {
            this.options.maxsize = maxsize;
        }
        if (params) {
            this.options.params = params;
        }
        this.options.startCallback = startcb;
    }

    // 设置配置对象，建议使用
    public setOption(opt: AppUploadOptions) {
        if (opt !== undefined && opt) {
            this.options.action = opt.action || 'http://localhost/uplad.do';
            this.options.filter = opt.filter || '';
            this.options.maxsize = opt.maxsize || 50;
            this.options.params = opt.params || null;
            this.options.selectFileCallback = opt.selectFileCallback || null;
            this.options.startCallback = opt.startCallback || null;
            this.options.progressCallback = opt.progressCallback || null;
            this.options.successCallback = opt.successCallback || null;
            this.options.errorCallback = opt.errorCallback || null;
        }
    }

    // 上传方法
    public upload(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const input = document.createElement('input');
            input.type = 'file';
            input.click();

            input.onchange = async () => {
                const file = input.files[0];
                const tmparr = file.name.split('.');
                const fileType = tmparr[tmparr.length - 1].toLowerCase();
                if (this.options.selectFileCallback != null) {
                    const params = await this.options.selectFileCallback(file, fileType);
                    if (!params) {
                        this.options.errorCallback('上传失败');
                        return;
                    }
                    if (!this.options.params) {
                        this.options.params = new Map<string, any>();
                    }

                    for (const key of params.keys()) {
                        this.options.params.set(key, params.get(key));
                    }
                }


                console.log('上传参数 => ', this.options.params);
                if (this.options.filter) {
                    const filterArr = this.options.filter.split(',');
                    let notFilter = true;
                    for (const f of filterArr) {
                        if (fileType === f.toLowerCase()) {
                            notFilter = false;
                            break;
                        }
                    }
                    console.log(444);

                    if (notFilter) {
                        // 文件类型不符合要求
                        if (this.options.errorCallback) {
                            this.options.errorCallback('文件类型不符合要求');
                        }
                        return;
                    }
                }

                const sizeM = file.size / 1024 / 1024;
                if (sizeM > this.options.maxsize) {
                    // 文件大小不符合要求
                    if (this.options.errorCallback) {
                        this.options.errorCallback('文件大小不符合要求');
                    }
                    // reject("文件大小不符合要求");
                    return;
                }
                const form = new FormData();
                form.append('file', file);
                form.append('fileName', file.name);
                form.append('size', '' + file.size);
                console.log('-------------', this.options.params);
                if (this.options.params != null) {
                    for (const key of this.options.params.keys()) {
                        console.log('-------------', key, this.options.params.get(key));
                        form.append(key, this.options.params.get(key));
                    }
                }
                console.log(666);

                const xhr = new XMLHttpRequest();
                const action = this.options.action;
                xhr.open('POST', action);

                // ****** 各种事件的监听 ******
                // 开始上传
                xhr.upload.onloadstart = (ievent) => {
                    console.log('开始上传');
                    this.$lastTimestamp = ievent.timeStamp;

                    this.$lastLoaded = 0;
                    if (this.options.startCallback) {
                        this.options.startCallback(file);
                    }
                };

                // 上传进度
                xhr.upload.onprogress = (ievent) => {
                    let offtimestamp = ievent.timeStamp - this.$lastTimestamp;
                    this.$lastTimestamp = ievent.timeStamp;

                    let offloaded = ievent.loaded - this.$lastLoaded;
                    this.$lastLoaded = ievent.loaded;

                    offtimestamp = offtimestamp / 1000; // 转换成秒
                    offloaded = offloaded / 1024; // 转换成KB
                    const ispeed = Math.round(offloaded / offtimestamp); // KB/S
                    const iprogress = ievent.loaded / ievent.total; // 0~1
                    const itotal = Math.round(ievent.total / 1024); // KB
                    if (this.options.progressCallback) {
                        this.options.progressCallback({
                            computable: ievent.lengthComputable,
                            speed: ispeed,
                            progress: iprogress,
                            total: itotal,
                            event: ievent
                        });
                    }
                };
                // 上传完成
                xhr.onload = (event) => {
                    if (this.options.successCallback !== undefined && this.options.successCallback != null) {
                        let resultObj;
                        try {
                            resultObj = JSON.parse(xhr.responseText);
                        } catch (error) {
                            resultObj = xhr.responseText;
                        }
                        this.options.successCallback(resultObj);
                    }
                };
                // 上传错误
                xhr.onerror = (event) => {
                    if (this.options.errorCallback) {
                        this.options.errorCallback(event);
                    }
                };
                // 状态变化
                xhr.onreadystatechange = () => {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            try {
                                const resultObj = JSON.parse(xhr.responseText);
                                resolve(resultObj);
                            } catch (error) {
                                resolve(xhr.responseText);
                            }
                        }
                        // else{
                        //     reject("上传失败，网络错误");
                        // }
                    }
                };

                xhr.send(form);
            };
        });
    }
}



