import { AppVue } from '../../app.vue';
import Component from 'vue-class-component';
import './hello-world.scss';

@Component({
    name: 'HelloWorld',
    props: {
        param1: Array,
    },
    template: require('./hello-world.html'),
})
export class HelloWorld extends AppVue {


}

