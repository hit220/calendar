let HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // 公共路径(必须有的)
    publicPath: "./",
    // 输出文件目录
    outputDir: "www",
    // 静态资源存放的文件夹(相对于ouputDir)
    assetsDir: "",
    // eslint-loader 是否在保存的时候检查(果断不用，这玩意儿我都没装)
    // lintOnSave:false,
    // 我用的only，打包后小些
    //runtimeCompiler: false,
    //productionSourceMap: true, // 不需要生产环境的设置false可以减小dist文件大小，加速构建
    devServer: {
        open: false,  // npm run serve后自动打开页面
        host: '0.0.0.0',  // 匹配本机IP地址(默认是0.0.0.0)
        port: 8081, // 开发服务器运行端口号
    },
    runtimeCompiler: true,

    //
    configureWebpack: { // 这里是webpack的配置
        entry: ['babel-polyfill', './src/main.ts'],
        module: {
            rules: [
                { test: /\.ts$/, loader: 'ts-loader', exclude: '/node_modules/' },
                { test: /\.(html|tpl|tpml)$/, loader: 'html-loader', exclude: '/node_modules/' },
                { test: /\.css$/, loader: 'sass-loader', exclude: '/node_modules/' },
                { test: /\.scss$/, loader: 'sass-loader', exclude: '/node_modules/' },
                { test: /\.vue$/, loader: 'vue-loader', exclude: '/node_modules/' },
            ],
        },
        resolve: { // 这里是指定引入模块省略扩展名时的查找顺序
            extensions: ['.ts', '.js'],
        },
        plugins: [
            new HtmlWebpackPlugin({
                title: '首页 - 应用',
                filename: 'index.html',
                template: 'src/index.html', // 使用src/index.html替换webpack默认index.html
            }),
        ],
    },

}