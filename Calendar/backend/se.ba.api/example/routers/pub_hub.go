package routers

import (
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba.api/example/api/pub"
	"seasonalcal.life/rechy/se.ba.api/example/keys"
)

type PubHub struct {
}

func (pb *PubHub) Sign(isBA bool, moduleTo string, data string) (string, error) {
	kmgr := keys.GetKeyManager()
	return kmgr.Sign(isBA, moduleTo, data)
}

func (pb *PubHub) Verify(isBA bool, moduleFrom string, data string, sign string) error {
	kmgr := keys.GetKeyManager()
	return kmgr.Verify(isBA, moduleFrom, data, sign)
}

func (pb *PubHub) Exec(req *baapi.Command) (string, interface{}, interface{}) {
	fn, ok := pub.FindHandler(req.Cmd)
	if !ok {
		return "error", "not support command", nil
	}
	return fn(req)
}
