package routers

import (
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba.api/example/api"
	v1 "seasonalcal.life/rechy/se.ba.api/example/api/v1"
	"seasonalcal.life/rechy/se.ba.api/example/keys"
)

func Init(e *gin.Engine) {

	/*******初始化操作权限路由范围 ******/
	baapi.InitBind(e, &PubHub{})

	// 初始化密钥管理器
	keys.GetKeyManager()

	/**********************/
	authMiddleware, err := getJwtAuth()
	if err != nil {
		glog.Fatalf("get jwt middleware err [%+v]", err)
		return
	}
	apiGroup := e.Group("/api")
	{

		// 无权限验证
		apiGroup.GET("/test", api.Test)
		apiGroup.POST("/login", authMiddleware.LoginHandler)
	}

	adminGroup := e.Group(baapi.RouteApiMaster)
	adminGroup.Use(authMiddleware.MiddlewareFunc())
	{
		adminGroup.GET("/v1/hello", v1.HelloHandler)
	}
	/**********************/

	e.StaticFS("/fe", newStaticDir("/fe", lib.EnsureDir("./web/fe"), ""))
	e.StaticFS("/static", newStaticDir("/static", lib.EnsureDir("./web/static"), ""))
	e.StaticFS("/ui", newStaticDir("/ui", lib.EnsureDir("./web/www"), "index.html"))
	e.GET("/", func(c *gin.Context) {
		toURL := &url.URL{
			Path:     "/ui/",
			RawQuery: c.Request.URL.RawQuery, // 保留参数
			Fragment: c.Request.URL.Fragment, // 保留锚
		}
		c.Redirect(http.StatusFound, toURL.String())
	})
}
