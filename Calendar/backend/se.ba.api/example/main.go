package main

import (
	"fmt"
	"path/filepath"
	"runtime"
	"time"

	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	"seasonalcal.life/rechy/se.ba.api/example/keys"
	"seasonalcal.life/rechy/se.ba.api/example/routers"
)

const ServicePort = 8085

func main() {
	cfgDir := lib.EnsureDir("./conf")
	cfg.InitWithFile("json", filepath.Join(cfgDir, "default.conf"))

	gin.SetMode(gin.DebugMode)
	// gin.DefaultWriter = ioutil.Discard

	keys.GetKeyManager()

	e := gin.Default()

	/*******初始化路由 ******/
	routers.Init(e)

	// 自动打开浏览器
	if runtime.GOOS == "windows" {
		go func() {
			url := fmt.Sprintf("http://localhost:%v/", ServicePort)
			glog.Infoln("自动打开web浏览器:", url)

			time.Sleep(time.Second * 2)
			lib.StartChrome(url)
		}()
	}
	e.Run(fmt.Sprintf(":%v", ServicePort))
}
