module seasonalcal.life/rechy/se.ba.api/example

go 1.14

replace seasonalcal.life/rechy/glog => ./../../../../seasonalcal.life/rechy/glog

replace seasonalcal.life/rechy/config => ./../../../../seasonalcal.life/rechy/config

replace seasonalcal.life/rechy/lib => ./../../../../seasonalcal.life/rechy/lib

replace seasonalcal.life/rechy/se.ba.api => ./../

require (
	github.com/gin-gonic/gin v1.7.1
	seasonalcal.life/rechy/config v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/glog v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/lib v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/se.ba.api v0.0.0-00010101000000-000000000000
)
