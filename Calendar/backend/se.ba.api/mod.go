package baapi

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/bmatcuk/doublestar/v2"
	"github.com/gin-gonic/gin"

	// "github.com/syndtr/goleveldb/leveldb"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	"seasonalcal.life/rechy/se.ba.api/jwt-v2"
)

const (
	RouteApiMaster             = "/api/master/"              // Admin manager
	RouteApiUser               = "/api/user/"                // User center
	RouteApiAgent              = "/api/agent/"               // Agent (enterprise or user)
	RouteApiEnterprise         = "/api/enterprise/"          // 企业
	RouteApiDeveloper          = "/api/develop/"             // 开发商
	RouteApiLogin              = "/api/login"                // 登入(账号/邮箱/手机号 + 密码)
	RouteApiLoginByPhone       = "/api/login_by_phone"       // 登入(手机号 + 验证码)
	RouteApiLoginByWxPub1      = "/api/login_by_wx_pub1"     // 微信公众号第一步
	RouteApiLoginByWxPub2      = "/api/login_by_wx_pub2"     // 微信公众号第二步
	RouteApiLoginByICBCELive   = "/api/login_by_icbc_elive"  // 工行e生活
	RouteApiLogout             = "/api/logout"               // 登出
	RouteApiRegisterUsername   = "/api/register/username"    // 注册 - 用户名
	RouteApiRegisterPhone      = "/api/register/phone"       // 注册 - 手机
	RouteApiRegisterEmail      = "/api/register/email"       // 注册 - email
	RouteApiVerifyCodePhone    = "/api/verify/code/phone"    // 验证码 - 手机
	RouteApiVerifyCodeEmail    = "/api/verify/code/email"    // 验证码 - 邮箱
	RouteApiActivationEmail    = "/api/activation/email"     // 激活 - 邮箱
	RouteApiResetPasswordPhone = "/api/reset_password/phone" // 重置密码 - 手机 + 验证码
	RouteApiResetPasswordEmail = "/api/reset_password/email" // 重置密码 - 邮箱 + 验证码
	RouteApiProfile            = "/api/profile"              // 个人中心
	RouteApiUpload             = "/api/upload"               // 文件上传
	RouteApiFile               = "/api/file/:uuid"
	RouteInfo                  = "/info"           // 资料获取/编辑
	RouteBindEmail             = "/bind_email"     // 绑定邮箱
	RouteBindPhone             = "/bind_phone"     // 绑定手机
	RouteUpload                = "/upload"         // 文件上传
	RouteRefreshToken          = "/refresh_token"  // 更新Token
	RouteApiHeadImage          = "/api/head_image" // 头像

	RoutePub                   = "/pub/"
	RoutePubCommand            = RoutePub + "command"              // /pub/command
	RoutePubInfo               = RoutePub + "info"                 // /pub/info
	RoutePubHeadImage          = RoutePub + "head_image"           // /pub/head_image
	RoutePubFile               = RoutePub + "file/:uuid"           // /pub/file/:uuid
	RoutePubLoginApply         = RoutePub + "login_apply"          // /pub/login_apply
	RoutePubRegisterApply      = RoutePub + "register_apply"       // /pub/register_apply
	RoutePubVerifyCodeApply    = RoutePub + "verify_code_apply"    // /pub/verify_code_apply
	RoutePubActivationApply    = RoutePub + "activation_apply"     // /pub/activation_apply
	RoutePubResetPasswordApply = RoutePub + "reset_password_apply" // /pub/reset_password_apply
	RoutePubProfileApply       = RoutePub + "profile_apply"        // /pub/profile_apply
	RoutePubUploadApply        = RoutePub + "upload_apply"         // /pub/upload_apply
)

var routeAreas = []string{
	RouteApiAgent,
	RouteApiEnterprise,
	RouteApiMaster,
	RouteApiUser,
	RouteApiDeveloper,
}

var ge *gin.Engine
var gModCommandHub interface{}

type KVDBInterface interface {
	Put(key string, val string, def string) error
	Get(key string, def string) (string, error)
}

func InitBasic(e *gin.Engine) {
	if err := cfg.TryInit("json"); err != nil {

		panic(err)
	}

	// 整体跨域
	e.Use(Cors())
	e.Use(ErrorLogRecordHandler())
	//
	ge = e
}

//
func InitBind(e *gin.Engine, msigner ModCommandHub) {
	if err := cfg.TryInit("json"); err != nil {

		panic(err)
	}

	gModCommandHub = msigner

	// 整体跨域
	e.Use(Cors())
	e.Use(ErrorLogRecordHandler())

	e.GET(RoutePubInfo, pubInfo)
	e.GET(RoutePubHeadImage, pubHeadImage)
	e.GET(RoutePubFile, pubFile)
	e.POST(RoutePubCommand, pubCommand)

	e.GET(RoutePubLoginApply, loginRequestApply)
	e.GET(RoutePubRegisterApply, registerRequestApply)
	e.GET(RoutePubProfileApply, profileRequestApply)
	e.GET(RoutePubVerifyCodeApply, verifyCodeRequestApply)
	e.GET(RoutePubResetPasswordApply, resetPasswordApply)
	e.GET(RoutePubActivationApply, activationRequestApply)
	e.GET(RoutePubUploadApply, uploadRequestApply)

	e.POST(RoutePubLoginApply, loginRequestApply)
	e.POST(RoutePubRegisterApply, registerRequestApply)
	e.POST(RoutePubProfileApply, profileRequestApply)
	e.POST(RoutePubVerifyCodeApply, verifyCodeRequestApply)
	e.POST(RoutePubResetPasswordApply, resetPasswordApply)
	e.POST(RoutePubActivationApply, activationRequestApply)

	//
	ge = e
}

// 模块名称
func ModuleName() string {
	module := cfg.String("module.key", "")
	if module == "" {
		path := lib.GetCurrentDir()
		path = strings.ReplaceAll(path, ":", "")
		path = strings.ReplaceAll(path, "\\", "/")
		arr := strings.Split(path, "/")
		hv := false
		for i := len(arr); i > 0; i-- {
			if arr[i-1] != "" {
				if !hv {
					module = arr[i-1]
					hv = true
				} else {
					module = arr[i-1] + "-" + module
					break
				}
			}
		}
		module += "-" + lib.KindRand(8, lib.KC_RAND_KIND_ALL)

		cfg.Set("module.key", module)
	}
	return module
}

// 模块路由列表
func ModuleRoutes() url.Values {
	if ge == nil {
		return nil
	}
	rs := []gin.RouteInfo(ge.Routes())
	re := make(url.Values)

	modelName := ModuleName()
	for _, v := range rs {
		for _, vi := range routeAreas {
			if strings.HasPrefix(v.Path, vi) {
				re.Add("/"+modelName+v.Path, v.Method)
			}
		}
	}
	return re
}

// 模块路由匹配判断方式
func IsMatch(reqPath, rolePattern string) bool {
	isMatch, _ := doublestar.Match(rolePattern, reqPath)
	return isMatch
}

//
func GetModAuthorID(c *gin.Context) (Type, int64) {
	claims := jwt.ExtractClaims(c)
	if claims == nil {
		return TypeUnknown, 0
	}
	cid := lib.ValueToString(claims["id"], "")
	r := strings.Split(cid, "-")
	if len(r) < 2 {
		return TypeUnknown, 0
	}

	id, _ := strconv.ParseInt(r[1], 10, 64)
	return Type(r[0]), id
}

func GetModAuthor(c *gin.Context) *ModAuthor {
	claims := jwt.ExtractClaims(c)
	if claims == nil {
		return nil
	}
	author, ok := claims["author"].(string)
	if !ok {
		return nil
	}
	return NewModAuthor([]byte(author), true)
}

func GetUserID(c *gin.Context) (int64, error) {
	uType, uid := GetModAuthorID(c)
	if uType != TypeUser {
		return 0, errors.New("unknown user")
	}
	if uid < 1 {
		return 0, errors.New("user is not login")
	}
	return uid, nil
}

func GetAgentID(c *gin.Context) (int64, error) {
	uType, uid := GetModAuthorID(c)
	if uType != TypeAgent {
		return 0, errors.New("unknown agent")
	}
	if uid < 1 {
		return 0, errors.New("agent is not login")
	}
	return uid, nil
}

func GetDeveloperID(c *gin.Context) (int64, error) {
	uType, uid := GetModAuthorID(c)
	if uType != TypeDeveloper {
		return 0, errors.New("unknown developer")
	}
	if uid < 1 {
		return 0, errors.New("developer is not login")
	}
	return uid, nil
}

func GetEnterpriseID(c *gin.Context) (int64, error) {
	uType, uid := GetModAuthorID(c)
	if uType != TypeEnterprise {
		return 0, errors.New("unknown enterprise")
	}
	if uid < 1 {
		return 0, errors.New("enterprise is not login")
	}
	return uid, nil
}

func GetMasterID(c *gin.Context) (int64, error) {
	uType, uid := GetModAuthorID(c)
	if uType != TypeEnterprise {
		return 0, errors.New("unknown master")
	}
	if uid < 1 {
		return 0, errors.New("master is not login")
	}
	return uid, nil
}

func GetCacheAuthPaths(a KVDBInterface, uid int64, Types Type, Auth string) (string, error) {
	//第一步  缓存
	dbdata, err := a.Get(Auth, "")
	if err == nil {
		glog.Errorln(string(dbdata))
		if string(dbdata) != "" {
			return string(dbdata), nil
		}
	}

	data := make(map[string]interface{})
	data["uid"] = lib.ValueToString(uid, "")
	data["type"] = Types

	data3, err := json.Marshal(data)
	if err != nil {
		glog.Errorln(err)
		return "", err
	}

	dataURL := cfg.String("kvdb_rolepath_url", "http://ba.seasonalcal.life/api/public/get_auth")
	header := http.Header{}
	header.Set("Content-Type", "application/json;charset=UTF-8")
	_, resp, _, _, err := lib.HttpRequest(nil, dataURL, "POST", data3, header, nil, false, time.Second*5, time.Second*3)

	if err != nil {
		glog.Errorln(err)
		return "", err
	} else {
		var arrRes *ResponseMessage
		err := json.Unmarshal(resp, &arrRes)
		if err != nil {
			glog.Errorln(err)
			return "", err
		}
		glog.Errorln(arrRes)

		if arrRes.Code != "success" {
			glog.Errorln(arrRes.Message)
			return "", errors.New(arrRes.Message)
		}
		glog.Errorln(lib.ValueToString(arrRes.Data, ""))
		err = a.Put(Auth, lib.ValueToString(arrRes.Data, ""), "")
		if err != nil {
			glog.Errorln(err)
		}
		return lib.ValueToString(arrRes.Data, ""), nil

	}
}
