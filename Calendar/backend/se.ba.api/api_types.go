package baapi

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

const (
	PageLinksMax  = 5
	PageLinksTag1 = "_NUM1_"
	PageLinksTag2 = "_NUM2_"
)

var (
	Debug = true

	PageLinksZh = []string{"首页", "上一页", "下一页", "尾页"}
	PageLinksEn = []string{"Home", "Prev", "Next", "Last"}
)

// ResponseMessage 通用接口消息
type ResponseMessage struct {
	Code       string      `json:"code"`        // 状态
	Message    string      `json:"message"`     // 消息
	Data       interface{} `json:"data"`        // 数据
	TransId    string      `json:"trans_id"`    // 序列ID
	CreateTime time.Time   `json:"create_time"` // 时间
	Version    string      `json:"version"`     // 接口版本
}

// 带分页的多条记录值
type PageData struct {
	PageSize    int         `json:"page_size"`    // 单页记录数
	CurrentPage int         `json:"current_page"` // 当前第几页
	TotalPage   int         `json:"total_page"`   // 总页数
	Start       int         `json:"start"`        // 记录开始于
	End         int         `json:"end"`          // 记录结束于
	Total       int         `json:"total"`        // 总记录数
	Data        interface{} `json:"data"`         // 数据
}

//
type ResponsePageData struct {
	Code       string    `json:"code"`        // 状态
	Message    string    `json:"message"`     // 消息
	Data       *PageData `json:"data"`        // 数据
	TransId    string    `json:"trans_id"`    // 序列ID
	CreateTime time.Time `json:"create_time"` // 时间
	Version    string    `json:"version"`     // 接口版本
}

//
type GinJSONResponser interface {
	JSON(code int, obj interface{})
}

type GaJSONResponser interface {
	JSON(code int, obj map[string]interface{})
}

func buildJSONReply(code string, message interface{}, data ...interface{}) *ResponseMessage {
	var messageStr string
	switch message.(type) {
	case error:
		messageStr = (message.(error)).Error()
	case string:
		messageStr = message.(string)
	default:
		messageStr = fmt.Sprintln(message)
	}

	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	m := &ResponseMessage{
		Code:       code,
		Message:    messageStr,
		Data:       d,
		TransId:    uuid.NewV4().String(),
		CreateTime: time.Now(),
		Version:    "1.0",
	}
	if Debug {
		glog.InfoDepth(2, m)
		// glog.Defer("")
	}
	return m
}

//
func JSONReply(c interface{}, code string, message interface{}, data ...interface{}) {
	m := buildJSONReply(code, message, data...)

	status := http.StatusNonAuthoritativeInfo
	if code == "success" {
		status = http.StatusOK
	}

	// GIN
	ginC, ginOK := c.(GinJSONResponser)
	if ginOK {
		if code == "fail" || code == "error" {
			tt, uid := GetUserID(c.(*gin.Context))
			glog.ErrorExtln("Rep", tt, uid, m)
		}
		ginC.JSON(status, m)
		return
	}
	if code == "fail" || code == "error" {
		glog.ErrorExtln("Rep", "-", 0, m)
	}

	// GO-ADMIN
	gaC, gaOK := c.(GaJSONResponser)
	if gaOK {
		gaC.JSON(status, lib.ValueToMap(m))
		return
	}

	wtC, wtOK := c.(http.ResponseWriter)
	if wtOK {
		JSON(wtC, status, m)
		return
	}

}

func JSON(w http.ResponseWriter, status int, m interface{}) {
	b, _ := json.Marshal(m)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(b)
}

//
func WrapMessage(v ...interface{}) string {
	return fmt.Sprintln(v...)
}

// 规范页码
func FormatPageParams(pg int, pgz int) (int, int) {
	if pgz <= 0 {
		pgz = 1000
	}
	if pg < 1 {
		pg = 1
	}
	return pg, pgz
}

//
func ToPageData(data interface{}, currentPage int, pageSize int, total int) *PageData {

	if total < 0 {
		total = 0
	}

	currentPage, pageSize = FormatPageParams(currentPage, pageSize)
	println("currentPage, pageSize", currentPage, pageSize)

	r := &PageData{
		CurrentPage: currentPage,
		PageSize:    pageSize,
		TotalPage:   ((total-1)/pageSize + 1),
		Start:       ((currentPage-1)*pageSize + 1),
		End:         currentPage * pageSize,
		Total:       total,
		Data:        data,
	}

	if r.CurrentPage > r.TotalPage {
		r.CurrentPage = r.TotalPage
	}
	return r

}

// 分页链接
type PageLinks struct {
	PageSize    int             `json:"page_size"`    // 单页记录数
	CurrentPage int             `json:"current_page"` // 当前第几页
	TotalPage   int             `json:"total_page"`   // 总页数
	Start       int             `json:"start"`        // 记录开始于
	End         int             `json:"end"`          // 记录结束于
	Total       int             `json:"total"`        // 总记录数
	Links       []template.HTML `json:"links"`        // 分页链接
}

func ToParseLinkByString(u string, params map[string]string) string {
	if u == "" {
		return ""
	}
	r, _ := url.Parse(u)
	return ToParseLink(r, params)
}

func ToParseLink(u *url.URL, params map[string]string) string {
	if u == nil {
		return ""
	}
	q := u.Query()
	for k, v := range params {
		q.Set(k, v)
	}
	u.RawQuery = q.Encode()
	return u.String()
}

/*

if($allpage > self::$limitshow && $nowpage < ($allpage - pk + 1)){
	$a['link'][] = str_replace("_NUM2_","...",str_replace("_NUM1_",(int)($nowpage)+1,$tag1));
}

if($nowpage + 1 <= $allpage){
	$a['link'][] = str_replace("_NUM2_",self::$lang[2],str_replace("_NUM1_",(int)($nowpage)+1,$tag1));
	if($allpage > self::$limitshow && $nowpage < ($allpage - pk + 1)){
		$a['link'][] = str_replace("_NUM2_",self::$lang[3],str_replace("_NUM1_",$allpage,$tag1));
	}
}
return $a;
*/
// 获取分页链接
func ToPageLink(currentPage, pageSize, total int, tplNormalLink,
	tplCurrentLink, tplNoneLink string, isLangEnglish bool) *PageLinks {

	pd := ToPageData(nil, currentPage, pageSize, total)

	pn := PageLinksZh
	if isLangEnglish {
		pn = PageLinksEn
	}

	kp := PageLinksMax%2 + PageLinksMax/2
	haveFirst := pd.TotalPage > PageLinksMax && 1 <= (pd.CurrentPage-kp)
	haveLast := pd.TotalPage > PageLinksMax && pd.TotalPage > (pd.CurrentPage+kp-1)

	links := make([]template.HTML, 0)
	if pd.CurrentPage > 1 {
		if haveFirst {
			link := strings.ReplaceAll(strings.ReplaceAll(tplNormalLink, PageLinksTag1, "1"), PageLinksTag2, pn[0])
			links = append(links, template.HTML(link))
		}

		{
			link := strings.ReplaceAll(strings.ReplaceAll(tplNormalLink, PageLinksTag1, strconv.Itoa(pd.CurrentPage-1)), PageLinksTag2, pn[1])
			links = append(links, template.HTML(link))
		}
	}

	if tplNoneLink != "" && haveFirst {
		link := strings.ReplaceAll(tplNoneLink, PageLinksTag2, "...")
		links = append(links, template.HTML(link))
	}

	for i := 1; i <= pd.TotalPage; i++ {
		if (pd.CurrentPage <= kp && i <= PageLinksMax) ||
			(i >= (pd.TotalPage-PageLinksMax+1) && i <= (pd.CurrentPage+kp-1)) ||
			(i >= (pd.CurrentPage-kp+1) && i <= (pd.CurrentPage+kp-1)) {

			istr := strconv.Itoa(i)
			if i == pd.CurrentPage {
				link := strings.ReplaceAll(strings.ReplaceAll(tplCurrentLink, PageLinksTag1, istr), PageLinksTag2, istr)
				links = append(links, template.HTML(link))
			} else {
				link := strings.ReplaceAll(strings.ReplaceAll(tplNormalLink, PageLinksTag1, istr), PageLinksTag2, istr)
				links = append(links, template.HTML(link))
			}
		}
	}

	if tplNoneLink != "" && haveLast {
		link := strings.ReplaceAll(tplNoneLink, PageLinksTag2, "...")
		links = append(links, template.HTML(link))
	}

	if (pd.CurrentPage + 1) <= pd.TotalPage {
		{
			link := strings.ReplaceAll(strings.ReplaceAll(tplNormalLink, PageLinksTag1, strconv.Itoa(pd.CurrentPage+1)), PageLinksTag2, pn[2])
			links = append(links, template.HTML(link))
		}

		if haveLast {
			link := strings.ReplaceAll(strings.ReplaceAll(tplNormalLink, PageLinksTag1, strconv.Itoa(pd.CurrentPage+1)), PageLinksTag2, pn[3])
			links = append(links, template.HTML(link))
		}

	}

	return &PageLinks{
		PageSize:    pd.PageSize,
		CurrentPage: pd.CurrentPage,
		TotalPage:   pd.TotalPage,
		Start:       pd.Start,
		End:         pd.End,
		Total:       pd.Total,
		Links:       links,
	}
}

//
func ToPageLinkDefault(urlSrc interface{}, currentPage, pageSize, total int, isLangEnglish ...bool) *PageLinks {
	isEn := false
	if len(isLangEnglish) > 0 && isLangEnglish[0] {
		isEn = true
	}

	var plUrl string
	switch urlSrc.(type) {
	case string:
		plUrl = ToParseLinkByString(urlSrc.(string), map[string]string{"pg": "_NUM1_"})
	case *url.URL:
		plUrl = ToParseLink(urlSrc.(*url.URL), map[string]string{"pg": "_NUM1_"})
	default:
		return &PageLinks{}
	}
	return ToPageLink(currentPage, pageSize, total,
		`<a class="pg-item pg-lk" href="`+plUrl+`">_NUM2_</a>`,
		`<a class="pg-item pg-lk pg-lk-cur" href="`+plUrl+`">_NUM2_</a>`,
		`<span class="pg-item pg-nn">_NUM2_</span>`,
		isEn,
	)
}
