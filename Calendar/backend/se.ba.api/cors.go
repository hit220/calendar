package baapi

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Cors is 跨域
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request == nil {
			c.Next()
			return
		}

		CorsByWriter(c.Writer, c.Request)

		// 处理请求
		c.Next()
	}
}

func CorsHandleFunc(f func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		CorsByWriter(w, r)
		f(w, r)
	}
}

func CorsByWriter(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	origin := r.Header.Get("Origin") //请求头部
	wh := w.Header()
	if origin != "" {
		wh.Set("Access-Control-Allow-Origin", origin)
	} else {
		wh.Set("Access-Control-Allow-Origin", "*")
	}
	wh.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
	wh.Set("Access-Control-Allow-Headers", "Authorization, Content-Length, X-CSRF-Token, Token,session,X_Requested_With,Accept, Origin, Host, Connection, Accept-Encoding, Accept-Language,DNT, X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Type, Pragma")
	wh.Set("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers,Cache-Control,Content-Language,Content-Type,Type,Expires,Last-Modified,Pragma,FooBar") // 跨域关键设置 让浏览器可以解析
	wh.Set("Access-Control-Max-Age", "172800")                                                                                                                                                                // 缓存请求信息 单位为秒
	wh.Set("Access-Control-Allow-Credentials", "true")                                                                                                                                                        // 设置返回格式是json

	//放行所有OPTIONS方法
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Options Request!"))
	}

}
