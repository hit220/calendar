package baapi

const (
	JwtSigningAlgorithm = "RS256"
)

// B前面工具
type ModSigner interface {
	Sign(isBA bool, moduleTo, data string) (string, error)
	Verify(isBA bool, moduleFrom, data, sign string) error
}

// 命令执行工具
type ModCommandHub interface {
	ModSigner
	Exec(req *Command) (code string, message, data interface{})
}
