package baapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"seasonalcal.life/rechy/glog"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
)

type Command struct {
	Module string `form:"module" json:"module" binding:"required"`
	Cmd    string `form:"cmd" json:"cmd" binding:"required"`
	Data   string `form:"data" json:"data" binding:"required"`
	Seq    string `form:"seq" json:"seq" binding:"required"`
	Sign   string `form:"sign" json:"sign" binding:"required"`
}

// BA发送命令
func SendCommand(toMudle, url string, cmd *Command) (*ResponseMessage, error) {
	moduleSelf := ModuleName()
	seq := ApplyToken()
	cmd.Module = moduleSelf
	cmd.Seq = seq

	sgin, err := gModCommandHub.(ModCommandHub).Sign(true, toMudle, cmd.Module+"&"+cmd.Cmd+"&"+cmd.Data+"&"+cmd.Seq)
	if err != nil {
		return nil, err
	}

	cmd.Sign = sgin

	b, err := json.Marshal(cmd)
	if err != nil {
		return nil, err
	}

	glog.InfoExtln("Mod", "Send:", string(b))

	header := http.Header{}
	header.Set("Content-Type", "application/json;charset=UTF-8")
	_, body, _, _, err := lib.HttpRequest(nil, url, "POST", b, header, nil, false, time.Second*5, time.Second*3)
	glog.InfoExtln("Mod", "Recv:", string(body), err)
	if err != nil {
		return nil, err
	}

	var reply *Command
	err = json.Unmarshal(body, &reply)
	if err != nil {
		return nil, err
	}

	err = gModCommandHub.(ModCommandHub).Verify(true, toMudle, reply.Module+"&"+reply.Cmd+"&"+reply.Data+"&"+reply.Seq, reply.Sign)
	if err != nil {
		return nil, err
	}

	if seq != reply.Seq {
		return nil, errors.New("reply seq is unknown result")
	}

	var resp *ResponseMessage
	err = json.Unmarshal([]byte(reply.Data), &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func CommandReply(c *gin.Context, from *Command, code string, message interface{}, data ...interface{}) {
	if from == nil {
		c.String(http.StatusBadRequest, "code: "+code+": "+fmt.Sprint(message))
		return
	}

	m := buildJSONReply(code, message, data...)
	b, _ := json.Marshal(m)
	moduleSelf := ModuleName()

	cmd := &Command{
		Module: moduleSelf,
		Cmd:    "reply",
		Data:   string(b),
		Seq:    from.Seq,
	}
	sgin, err := gModCommandHub.(ModCommandHub).Sign(false, from.Module, cmd.Module+"&"+cmd.Cmd+"&"+cmd.Data+"&"+cmd.Seq)
	if err != nil {
		c.String(http.StatusBadRequest, "system error: "+err.Error())
		return
	}
	cmd.Sign = sgin

	c.JSON(http.StatusOK, cmd)
}
