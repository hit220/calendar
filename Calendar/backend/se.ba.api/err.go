package baapi

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/glog"
)

// 记录崩溃错误
func ErrorLogRecordHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				errStr := fmt.Sprintln(err)
				glog.ErrorExtln("GIN", errStr, glog.DeferString())
				c.String(http.StatusInternalServerError, errStr)
				return
			}
		}()

		c.Next()
	}
}
