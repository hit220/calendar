module seasonalcal.life/rechy/se.ba.api

go 1.14

replace seasonalcal.life/rechy/glog => ./../../../seasonalcal.life/rechy/glog

replace seasonalcal.life/rechy/config => ./../../../seasonalcal.life/rechy/config

replace seasonalcal.life/rechy/lib => ./../../../seasonalcal.life/rechy/lib

require (
	github.com/appleboy/gofight/v2 v2.1.2
	github.com/bmatcuk/doublestar/v2 v2.0.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/kirinlabs/HttpRequest v1.1.1
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/tidwall/gjson v1.7.5
	seasonalcal.life/rechy/config v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/glog v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/lib v0.0.0-00010101000000-000000000000
)
