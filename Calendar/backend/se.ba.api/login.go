package baapi

import (
	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
)

func loginRequest(c *gin.Context) {
	JSONReply(c, "success", "成功", gin.H{
		"module":      ModuleName(),
		"apply_token": lib.KindRand(32, lib.KC_RAND_KIND_ALL),
		"url":         cfg.String("module.ba_login_url", "https://ba.example.com/api/login"),
	})
}
