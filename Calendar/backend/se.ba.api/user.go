package baapi

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
)

const (
	TypeMaster     Type = "master"
	TypeEnterprise Type = "enterprise"
	TypeUser       Type = "user"
	TypeAgent      Type = "agent"
	TypeDeveloper  Type = "developer"
	TypeUnknown    Type = "unknown"

	OpenGZipEncode = false

	UidBase    int64 = 72743
	OuidLength       = 6
)

var authors sync.Map
var authorTime = time.Now()
var authorTimeLock sync.Mutex

// 鉴权类型
type Type string

// jwt权限用户
// 短码节约长度
type ModAuthor struct {
	ID         int64                  `json:"id"`   // ID
	Nickname   string                 `json:"name"` // 昵称
	HeadImage  string                 `json:"head"` // 头像
	Type       Type                   `json:"t"`    // 类型
	UUID       string                 `json:"u"`    // UUID
	Account    string                 `json:"c"`    // 账户
	Phone      string                 `json:"p"`    // 手机号
	Device     string                 `json:"d"`    // 设备
	ApplyToken string                 `json:"k"`    // 请求Token
	Activation int                    `json:"v"`    // 激活状态
	Auth       string                 `json:"a"`    // 权限
	ExtraInfo  map[string]interface{} `json:"e"`    // 其他信息
	routes     url.Values
}

//
func NewModAuthor(author []byte, fromGZip ...bool) *ModAuthor {
	t := md5.New()
	t.Write(author)
	key := string(t.Sum(nil))

	authorTimeLock.Lock()
	tmx := time.Now().Sub(authorTime)
	if tmx > time.Hour {
		authorTime = time.Now()
		authorTimeLock.Unlock()

		authors.Range(func(k, v interface{}) bool {
			authors.Delete(k)
			return true
		})
	} else {
		authorTimeLock.Unlock()

		r, ok := authors.Load(key)
		if ok {
			return r.(*ModAuthor)
		}
	}

	openGZip := false
	if len(fromGZip) > 0 {
		openGZip = fromGZip[0]
	}
	a, _ := ModAuthorDecode(string(author), openGZip)
	if a != nil {
		a.routes = ParseAuth(a.Auth)
		authors.Store(key, a)
	}
	return a
}

func (a *ModAuthor) String() string {
	return ModAuthorEncode(a, false)
}

func (a *ModAuthor) EncodeToGZip() string {
	return ModAuthorEncode(a, true)
}

func (a *ModAuthor) HasAuth(req *http.Request) bool {
	modelName := ModuleName()
	p := "/" + modelName + req.URL.Path
	return a.doHasAuth(p, req.Method)
}

func (a *ModAuthor) doHasAuth(path, method string) bool {
	path = strings.ToLower(path)
	method = strings.ToUpper(method)
	for k, v := range a.routes {
		if !IsMatch(path, k) {
			continue
		}

		if v[0] == "ANY" {
			return true
		}

		for _, vi := range v {
			if vi == method {
				return true
			}
		}

	}

	return false
}

func DoHasAuthNew(req *http.Request, pathStr string) bool {
	modelName := ModuleName()
	p := "/" + modelName + req.URL.Path

	path := strings.ToLower(p)
	// method := strings.ToUpper(req.Method)

	glog.Errorln(path)

	arrPath := ParseAuth(pathStr)

	glog.Errorln(arrPath)

	for k, v := range arrPath {
		if !IsMatch(path, k) {
			continue
		}

		if v[0] == "ANY" {
			return true
		}

		for _, vi := range v {
			if strings.ToLower(vi) == path {
				return true
			}
		}

	}

	return false
}

func ParseAuth(str string) url.Values {
	re := url.Values{}
	arr := strings.Split(str, "\n")

	// 添加默认可操作路由
	arr = append(arr, lib.UrlJoin("/*", RouteApiProfile, "/** [ANY]"))

	for _, v := range arr {
		varr := strings.Split(v, "[")
		if len(varr) < 2 {
			continue
		}

		var mthStr string
		for _, v := range varr[1:] {
			if len(v) > 2 {
				mthStr = v
				break
			}
		}
		if mthStr == "" {
			continue
		}

		if mthStr[len(mthStr)-1] != ']' {
			continue
		}

		key := strings.ToLower(strings.TrimSpace(varr[0]))
		m := mthStr[:len(mthStr)-1]
		methods := strings.Split(m, ",")
		for _, mth := range methods {
			mth = strings.ToUpper(strings.TrimSpace(mth))
			if mth == "ANY" {
				re.Set(key, mth)
				break
			}
			re.Add(key, mth)
		}
	}

	return re
}

// 同步交换信息的用户结构
type ModUser struct {
	Type      `json:"type"`
	UUID      string                 `json:"uuid"`
	Account   string                 `json:"account"`
	Name      string                 `json:"name"`
	Nickname  string                 `json:"nickname"`
	HeadImage string                 `json:"head_image"`
	Phone     string                 `json:"phone"`
	ExtraInfo map[string]interface{} `json:"extra_info"` // 扩展信息
}

func (mu *ModUser) String() string {
	r, _ := json.Marshal(mu)
	return string(r)
}

// 减少编码长度
func ModAuthorEncode(a *ModAuthor, openGZip bool) string {
	s, err := json.Marshal(a)
	if err != nil {
		return ""
	}

	if !OpenGZipEncode || !openGZip {
		return string(s)
	}

	b := bytes.NewBuffer(nil)
	w := gzip.NewWriter(b)
	defer w.Close()

	w.Write(s)
	w.Flush()

	return b.String()
}

func ModAuthorDecode(src string, openGZip bool) (*ModAuthor, error) {
	var rb []byte
	var err error
	if OpenGZipEncode && openGZip {
		b := bytes.NewBuffer(nil)
		_, err = b.Write([]byte(src))
		if err != nil {
			return nil, err
		}
		r, err := gzip.NewReader(b)
		if err != nil {
			return nil, err
		}
		defer r.Close()

		rb, _ = ioutil.ReadAll(r)
	} else {
		rb = []byte(src)
	}

	var a *ModAuthor
	err = json.Unmarshal(rb, &a)
	if err != nil {
		return nil, err
	}

	return a, nil
}

func DecodeExtInfo(extInfo string, info map[string]interface{}) map[string]interface{} {
	if extInfo != "" {

		var mp map[string]interface{}
		json.Unmarshal([]byte(extInfo), &mp)
		if len(mp) < 1 {
			return info
		}
		for k, v := range info {
			mp[k] = v
		}
		return mp
	}

	return info
}

func EncodeExtInfo(v interface{}) string {
	mp := lib.ValueToMap(v)
	if mp == nil {
		return ""
	}
	b, err := json.Marshal(mp)
	if err != nil {
		return ""
	}
	return string(b)
}

func OuidToUid(ouid interface{}) int64 {
	str := lib.ValueToString(ouid, "")
	str = regexp.MustCompile(`[^0-9]+`).ReplaceAllString(str, "")
	if str == "" {
		return 0
	}

	r := lib.ValueToInt64(str, 0) - UidBase
	if r < 0 {
		return 0
	}
	return r
}

func UidToOuid(uid interface{}, tag ...string) string {
	iuid := lib.ValueToInt64(uid, 0)
	rl := lib.ValueToString(OuidLength, "6")

	if len(tag) < 1 {
		tag = []string{"M"}
	}

	return fmt.Sprintf("%s%0"+rl+"d", tag[0], iuid+UidBase)
}
