package baapi

import (
	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
)

func loginRequestApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":            ModuleName(),
		"apply_token":       ApplyToken(),
		"url":               lib.UrlJoin(baseURL, RouteApiLogin),
		"url_by_phone":      lib.UrlJoin(baseURL, RouteApiLoginByPhone),
		"url_by_wx_pub1":    lib.UrlJoin(baseURL, RouteApiLoginByWxPub1),
		"url_by_wx_pub2":    lib.UrlJoin(baseURL, RouteApiLoginByWxPub2),
		"url_by_icbc_elive": lib.UrlJoin(baseURL, RouteApiLoginByICBCELive),
	})
}

func registerRequestApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":          ModuleName(),
		"url_by_username": lib.UrlJoin(baseURL, RouteApiRegisterUsername),
		"url_by_phone":    lib.UrlJoin(baseURL, RouteApiRegisterPhone),
		"url_by_email":    lib.UrlJoin(baseURL, RouteApiRegisterEmail),
	})
}

func profileRequestApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":            ModuleName(),
		"url":               lib.UrlJoin(baseURL, RouteApiProfile, RouteInfo),
		"url_bind_phone":    lib.UrlJoin(baseURL, RouteApiProfile, RouteBindPhone),
		"url_bind_email":    lib.UrlJoin(baseURL, RouteApiProfile, RouteBindEmail),
		"url_upload":        lib.UrlJoin(baseURL, RouteApiProfile, RouteUpload),
		"url_refresh_token": lib.UrlJoin(baseURL, RouteApiProfile, RouteRefreshToken),
	})
}

func verifyCodeRequestApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":       ModuleName(),
		"url_by_phone": lib.UrlJoin(baseURL, RouteApiVerifyCodePhone),
		"url_by_email": lib.UrlJoin(baseURL, RouteApiVerifyCodeEmail),
	})
}

func activationRequestApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":       ModuleName(),
		"url_by_email": lib.UrlJoin(baseURL, RouteApiActivationEmail),
	})
}

func resetPasswordApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":       ModuleName(),
		"url_by_phone": lib.UrlJoin(baseURL, RouteApiResetPasswordPhone),
		"url_by_email": lib.UrlJoin(baseURL, RouteApiResetPasswordEmail),
	})
}

func uploadRequestApply(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	JSONReply(c, "success", "成功", gin.H{
		"module":     ModuleName(),
		"url_upload": lib.UrlJoin(baseURL, RouteApiUpload),
		"url_file":   lib.UrlJoin(baseURL, RouteApiFile),
	})
}
