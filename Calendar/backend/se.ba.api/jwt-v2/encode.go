package jwt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

const (
	TokenByteEncode = false
)

func TokenSignedString(t *jwt.Token, key interface{}) (string, error) {
	if !TokenByteEncode {
		return t.SignedString(key)
	}

	var sig, sstr string
	var err error
	if sstr, err = TokenSigningString(t); err != nil {
		return "", err
	}
	if sig, err = t.Method.Sign(sstr, key); err != nil {
		return "", err
	}

	return strings.Join([]string{sstr, sig}, "."), nil
}

func TokenSigningString(t *jwt.Token) (string, error) {

	var err error
	parts := make([]string, 2)
	for i := range parts {
		var packValue []byte
		if i == 0 {
			if packValue, err = json.Marshal(t.Header); err != nil {
				return "", err
			}
		} else {
			if packValue, err = json.Marshal(t.Claims); err != nil {
				return "", err
			}
		}

		parts[i] = jwt.EncodeSegment(packValue)
	}
	return strings.Join(parts, "."), nil
}

type TokenParser struct {
	jwt.Parser
}

func (p *TokenParser) ParseUnverified(tokenString string, claims jwt.Claims) (token *jwt.Token, parts []string, err error) {
	parts = strings.Split(tokenString, ".")
	if len(parts) != 3 {
		return nil, parts, jwt.NewValidationError("token contains an invalid number of segments",
			jwt.ValidationErrorMalformed)
	}

	token = &jwt.Token{Raw: tokenString}
	// parse Header
	var headerBytes []byte
	if headerBytes, err = jwt.DecodeSegment(parts[0]); err != nil {
		if strings.HasPrefix(strings.ToLower(tokenString), "bearer ") {
			return token, parts, jwt.NewValidationError("tokenstring should not contain 'bearer '",
				jwt.ValidationErrorMalformed)
		}
		return token, parts, &jwt.ValidationError{Inner: err, Errors: jwt.ValidationErrorMalformed}
	}

	if err = json.Unmarshal(headerBytes, &token.Header); err != nil {
		return token, parts, &jwt.ValidationError{Inner: err, Errors: jwt.ValidationErrorMalformed}
	}

	// parse Claims
	var claimBytes []byte
	token.Claims = claims

	if claimBytes, err = jwt.DecodeSegment(parts[1]); err != nil {
		return token, parts, &jwt.ValidationError{Inner: err, Errors: jwt.ValidationErrorMalformed}
	}

	dec := json.NewDecoder(bytes.NewBuffer(claimBytes))
	if p.UseJSONNumber {
		dec.UseNumber()
	}
	if c, ok := token.Claims.(jwt.MapClaims); ok {
		err = dec.Decode(&c)
	} else {
		err = dec.Decode(&claims)
	}
	// Handle decode error
	if err != nil {
		return token, parts, &jwt.ValidationError{Inner: err, Errors: jwt.ValidationErrorMalformed}
	}

	// Lookup signature method
	if method, ok := token.Header["alg"].(string); ok {
		if token.Method = jwt.GetSigningMethod(method); token.Method == nil {
			return token, parts, jwt.NewValidationError("signing method (alg) is unavailable.", jwt.ValidationErrorUnverifiable)
		}
	} else {
		return token, parts, jwt.NewValidationError("signing method (alg) is unspecified.", jwt.ValidationErrorUnverifiable)
	}

	return token, parts, nil
}

func (p *TokenParser) ParseWithClaims(tokenString string, claims jwt.Claims, keyFunc jwt.Keyfunc) (*jwt.Token, error) {
	token, parts, err := p.ParseUnverified(tokenString, claims)
	if err != nil {
		return token, err
	}

	// Verify signing method is in the required set
	if p.ValidMethods != nil {
		var signingMethodValid = false
		var alg = token.Method.Alg()
		for _, m := range p.ValidMethods {
			if m == alg {
				signingMethodValid = true
				break
			}
		}
		if !signingMethodValid {
			// signing method is not in the listed set
			return token, jwt.NewValidationError(fmt.Sprintf("signing method %v is invalid",
				alg), jwt.ValidationErrorSignatureInvalid)
		}
	}

	// Lookup key
	var key interface{}
	if keyFunc == nil {
		// keyFunc was not provided.  short circuiting validation
		return token, jwt.NewValidationError("no Keyfunc was provided.",
			jwt.ValidationErrorUnverifiable)
	}
	if key, err = keyFunc(token); err != nil {
		// keyFunc returned an error
		if ve, ok := err.(*jwt.ValidationError); ok {
			return token, ve
		}
		return token, &jwt.ValidationError{Inner: err, Errors: jwt.ValidationErrorUnverifiable}
	}

	vErr := &jwt.ValidationError{}

	// Validate Claims
	if !p.SkipClaimsValidation {
		if err := token.Claims.Valid(); err != nil {

			// If the Claims Valid returned an error, check if it is a validation error,
			// If it was another error type, create a ValidationError with a generic ClaimsInvalid flag set
			if e, ok := err.(*jwt.ValidationError); !ok {
				vErr = &jwt.ValidationError{Inner: err, Errors: jwt.ValidationErrorClaimsInvalid}
			} else {
				vErr = e
			}
		}
	}

	// Perform validation
	token.Signature = parts[2]
	if err = token.Method.Verify(strings.Join(parts[0:2], "."), token.Signature, key); err != nil {
		vErr.Inner = err
		vErr.Errors |= jwt.ValidationErrorSignatureInvalid
	}

	if vErr.Errors == 0 {
		token.Valid = true
		return token, nil
	}

	return token, vErr
}

func TokenParse(tokenString string, keyFunc jwt.Keyfunc) (*jwt.Token, error) {
	if !TokenByteEncode {
		return jwt.Parse(tokenString, keyFunc)
	}
	return new(TokenParser).ParseWithClaims(tokenString, jwt.MapClaims{}, keyFunc)
}
