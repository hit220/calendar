package baapi

import (
	"encoding/json"
	"fmt"
	"testing"
)

func jsonEncode(r interface{}) string {
	bb, _ := json.MarshalIndent(r, "", "\t")
	return (string(bb))

}

func testToPageLink(t *testing.T) {
	url := "http://www.example.com/demo/test?a=44&pg=33&t=abc"
	plUrl := ToParseLinkByString(url, map[string]string{"pg": "_NUM1_"})
	fmt.Println(plUrl)

	r := ToPageLink(12, 20, 3356,
		`<a class="pg-item pg-link" href="`+plUrl+`">_NUM2_</a>`,
		`<a class="pg-item pg-link pg-link-cur" href="`+plUrl+`">_NUM2_</a>`,
		`<span class="pg-item">_NUM2_</span>`,
		true)

	fmt.Println(jsonEncode(r))

	//r = ToPageLinkDefault(url, 12, 20, 3356)
	//fmt.Println(jsonEncode(r))

	r = ToPageLinkDefault(url, 12, 20, 40)
	fmt.Println(jsonEncode(r))
}
