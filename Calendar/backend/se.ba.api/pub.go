package baapi

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
)

type ModInfo struct {
	Module  string `json:"module"`  // 模块名称
	Routers string `json:"routers"` // 模块路由列表
	Sign    string `json:"sign"`    // 签名
}

func pubInfo(c *gin.Context) {
	module := ModuleName()
	b, err := json.Marshal(ModuleRoutes())
	if err != nil {
		JSONReply(c, "error", err)
		return
	}

	routerStr := string(b)
	sign, err := gModCommandHub.(ModSigner).Sign(false, "", module+"&"+routerStr)
	if err != nil {
		JSONReply(c, "error", err)
		return
	}

	JSONReply(c, "success", "成功", &ModInfo{
		Module:  module,
		Routers: routerStr,
		Sign:    sign,
	})
}

func pubHeadImage(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	redirectUrl := lib.UrlJoin(baseURL, RouteApiHeadImage)
	if c.Request.URL.RawQuery != "" {
		redirectUrl += "?" + c.Request.URL.RawQuery
	}
	c.Redirect(http.StatusFound, redirectUrl)
}

func pubFile(c *gin.Context) {
	baseURL := cfg.String("module.ba_url", "https://ba.example.com")
	redirectUrl := lib.UrlJoin(baseURL, strings.ReplaceAll(RouteApiFile, ":uuid", c.Param("uuid")))
	if c.Request.URL.RawQuery != "" {
		redirectUrl += "?" + c.Request.URL.RawQuery
	}
	c.Redirect(http.StatusFound, redirectUrl)
}

func pubCommand(c *gin.Context) {
	var req Command
	if err := c.ShouldBind(&req); err != nil {
		CommandReply(c, nil, "error", err, nil)
		return
	}

	err := gModCommandHub.(ModCommandHub).Verify(false, req.Module, req.Module+"&"+req.Cmd+"&"+req.Data+"&"+req.Seq, req.Sign)
	if err != nil {
		CommandReply(c, &req, "error", err, nil)
		return
	}

	code, message, data := gModCommandHub.(ModCommandHub).Exec(&req)
	CommandReply(c, &req, code, message, data)
}
