package baapi

import (
	"encoding/json"
	"fmt"
	"testing"

	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
)

func init() {
	if err := cfg.TryInit("json"); err != nil {
		panic(err)
	}
}

func TestIsMatch(t *testing.T) {
	if IsMatch("/aa-cc.e33/ccd/dasdf", "**") == false {
		t.FailNow()
	}

	if IsMatch("/aa-cc.e33/ccd/dasdf", "/**/ccd") == true {
		t.FailNow()
	}

	if IsMatch("/aa-cc.e33/ccd/dasdf", "/aa-cc.e33/**") == false {
		t.FailNow()
	}
}

func TestUserHaveAuth(t *testing.T) {

	a := &ModAuthor{}

	path := "/api/cc"
	modelName := ModuleName()

	a.routes = ParseAuth("**[aNy]")
	if a.doHasAuth("/"+modelName+path, "get") == false {
		t.FailNow()
	}

	a.routes = ParseAuth("**   [[[[get]")
	if a.doHasAuth("/"+modelName+path, "post") == true {
		t.FailNow()
	}

	a.routes = ParseAuth("**[get,  post]")
	if a.doHasAuth("/"+modelName+path, "post") == false {
		t.FailNow()
	}
}

func TestEncodeDecode(t *testing.T) {
	mu := &ModAuthor{
		ID:        1,
		Nickname:  "test",
		Phone:     "08613900000000",
		HeadImage: lib.KindRand(150, lib.KC_RAND_KIND_ALL),
	}

	juser, _ := json.Marshal(mu)

	user := ModAuthorEncode(mu, false)
	println(len(juser), len(user), fmt.Sprintf("%0.2f%%", float32(len(user))*100.0/float32(len(juser))))

	mu2, err := ModAuthorDecode(user, true)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	if mu2.Nickname != mu.Nickname ||
		mu2.ID != mu.ID ||
		mu2.Phone != mu.Phone {
		t.Error("decode fail")
		t.FailNow()
	}
}

func TestUid2Ouid(t *testing.T) {
	r := UidToOuid(1341)
	t.Log(r)

	if OuidToUid(r) != 1341 {
		t.FailNow()
	}
}
