package main

import (
	"fmt"
	"io/ioutil"
	"runtime"

	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	"seasonalcal.life/rechy/se.ba/keys"
	"seasonalcal.life/rechy/se.ba/routers"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils/email"
	"seasonalcal.life/rechy/se.ba/utils/sms"
	"seasonalcal.life/rechy/se.ba/ws"
)

const (
	srvName        = "Base Admin Mnaager"
	srvDescription = "Base Admin Mnaager by S.S.E"
)

var (
	ServicePort int = 8100
)

func (app *exitApp) main() {
	defer app.exit()

	xormDebug := cfg.Bool("debug.xorm", true)
	httpDebug := cfg.Bool("debug.http", true)

	_, err := tables.DBInit(xormDebug)
	if err != nil {
		glog.Errorln("数据库连接失败:", err)
		return
	}
	defer tables.DBClose()
	glog.Infoln("数据库连接成功")

	kvDBPath := cfg.String("kv_db", "./_data/kv_cache")
	kvDBPath = lib.EnsureDir(kvDBPath)
	_, err = tables.KVDBInit(kvDBPath)
	if err != nil {
		glog.Errorln("KVDB库初始化失败:", err)
		return
	}
	defer tables.KVDBClose()

	/************初始化keys*************/
	kmr := keys.GetKeyManager()
	if kmr == nil {
		glog.Errorln("认证密钥库初始化失败")
		return
	}

	ServicePort = cfg.Int("port", ServicePort)
	if httpDebug {
		gin.SetMode(gin.DebugMode)
		// gin.DefaultWriter = ioutil.Discard
	} else {
		gin.SetMode(gin.ReleaseMode)
		gin.DefaultWriter = ioutil.Discard
	}

	e := gin.Default()

	// 初始化路由
	routers.Init(e)

	// 初始化短信
	sms.Init()

	// 初始化邮箱
	email.Init()

	// 启动WebSocket服务
	go ws.Serve(app.ctx)

	// 自动打开浏览器
	if runtime.GOOS == "windows" {
		/*
			go func() {
				url := fmt.Sprintf("http://localhost:%v/", ServicePort)
				glog.Infoln("自动打开web浏览器:", url)

				time.Sleep(time.Second * 2)
				lib.StartChrome(url)
			}()
		*/
	}

	err = e.Run(fmt.Sprintf(":%v", ServicePort))
	if err != nil {
		glog.Errorln("服务器异常退出:", err)
		return
	}

}
