module seasonalcal.life/rechy/se.ba

go 1.14

replace seasonalcal.life/rechy/glog => ./../../../seasonalcal.life/rechy/glog

replace seasonalcal.life/rechy/config => ./../../../seasonalcal.life/rechy/config

replace seasonalcal.life/rechy/lib => ./../../../seasonalcal.life/rechy/lib

replace seasonalcal.life/rechy/se.ba.api => ./../../../seasonalcal.life/rechy/se.ba.api

require (
	github.com/chanxuehong/wechat v0.0.0-20201110083048-0180211b69fd
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/websocket v1.4.2
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/o1egl/govatar v0.4.1
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/takama/daemon v1.0.0
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	seasonalcal.life/rechy/config v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/glog v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/lib v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/se.ba.api v0.0.0-00010101000000-000000000000
	xorm.io/xorm v1.0.5
)
