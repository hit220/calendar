package tables

import (
	"strconv"
	"time"

	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   应用
 *   -- 平台管理和服务模块(程序)
 *
 **********************************************/

// BaAppTableName is 表名
const BaAppTableName = "ba_app"

// BaApp is
type BaApp struct {
	ID                 int64     `json:"id" xorm:"pk autoincr 'id'"`
	Name               string    `json:"name" xorm:"char(150) notnull default('') 'name'"`
	Module             string    `json:"module" xorm:"char(64) notnull default('') 'module'"`
	HeadImage          string    `json:"head_image" xorm:"varchar(255) notnull default('') 'head_image'"`
	DataURL            string    `json:"data_url" xorm:"varchar(200) notnull default('') 'data_url'"`
	VisitURLMaster     string    `json:"visit_url_master" xorm:"varchar(200) notnull default('') 'visit_url_master'"`
	VisitURLEnterprise string    `json:"visit_url_enterprise" xorm:"varchar(200) notnull default('') 'visit_url_enterprise'"`
	VisitURLUser       string    `json:"visit_url_user" xorm:"varchar(200) notnull default('') 'visit_url_user'"`
	VisitURLAgent      string    `json:"visit_url_agent" xorm:"varchar(200) notnull default('') 'visit_url_agent'"`
	VisitURLDeveloper  string    `json:"visit_url_developer" xorm:"varchar(200) notnull default('') 'visit_url_developer'"`
	IPs                string    `json:"ips" xorm:"varchar(200) notnull default('') 'ips'"`
	Description        string    `json:"description" xorm:"varchar(250) notnull default('') 'description'"`
	PrivateKey         string    `json:"private_key" xorm:"text(20000) notnull default('') 'private_key'"`
	PublicKey          string    `json:"public_key" xorm:"text(20000) notnull default('') 'public_key'"`
	PublicKeyClient    string    `json:"public_key_client" xorm:"text(20000) notnull default('') 'public_key_client'"`
	Status             int8      `json:"status" xorm:"notnull default(0) 'status'"`
	CreatedAt          time.Time `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxYM     int       `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt          time.Time `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM     int       `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
}

func init() {
	tables = append(tables, new(BaApp))
}

func initBaAppIfNeeded() error {
	n, err := _db.Count(new(BaApp))
	if err != nil {
		return err
	}

	if n > 0 {
		return nil
	}

	app := new(BaApp)
	app.Name = "Base Admin"
	app.Module = baapi.ModuleName()

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))

	privKey, pubKey, err := lib.GenKeyPair(2048)
	if err != nil {
		panic(err)
	}

	privKey2, pubKey2, err := lib.GenKeyPair(2048)
	if err != nil {
		panic(err)
	}

	app.PrivateKey = privKey
	app.PublicKey = pubKey
	app.CreatedAt = now
	app.CreatedAtIdxYM = idxTM
	app.UpdatedAt = now
	app.Status = StatusOK // 10正常运行中
	app.UpdatedAtIdxYM = idxTM
	_, err = _db.Insert(app)
	if err != nil {
		return err
	}

	cfg.String("module.jwt.self.private_key", privKey2)
	cfg.String("module.jwt.self.public_key", pubKey2)
	cfg.String("module.jwt.ba.public_key", pubKey)

	return err
}

func BaAppAllKeys() ([]*BaApp, error) {
	var res []*BaApp
	err := _db.Where("status = ?", 10).Cols(
		"module",
		"private_key",
		"public_key_client").Find(&res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func BaAppByID(id int64) (*BaApp, bool, error) {
	re := new(BaApp)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

func BaAppList(currentPage, pageSize int, kw string) ([]*BaApp, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("name LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(BaApp))
	if err != nil {
		return nil, 0, err
	}

	var res []*BaApp
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func BaAppAddOrUpdate(
	reqID,
	reqName,
	reqModule,
	reqHeadImage,
	reqDataURL,
	reqVisitURLMaster,
	reqVisitURLEnterprise,
	reqVisitURLUser,
	reqVisitURLAgent,
	reqVisitURLDeveloper,
	reqIPs,
	reqPublicKeyClient,
	reqDescription string) (isAdd bool, offwOrID int64, err error) {

	if reqID == "" || reqID == "0" {
		isAdd = true
	}

	info := new(BaApp)
	info.Name = reqName
	info.Module = reqModule
	info.HeadImage = reqHeadImage
	info.DataURL = reqDataURL
	info.VisitURLMaster = reqVisitURLMaster
	info.VisitURLEnterprise = reqVisitURLEnterprise
	info.VisitURLUser = reqVisitURLUser
	info.VisitURLAgent = reqVisitURLAgent
	info.VisitURLDeveloper = reqVisitURLDeveloper
	info.IPs = reqIPs
	info.PublicKeyClient = reqPublicKeyClient
	info.Description = reqDescription

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	info.Description = reqDescription
	if !isAdd {

		minfo := map[string]interface{}{
			"name":                 info.Name,
			"module":               info.Module,
			"head_image":           info.HeadImage,
			"data_url":             info.DataURL,
			"visit_url_master":     info.VisitURLMaster,
			"visit_url_enterprise": info.VisitURLEnterprise,
			"visit_url_user":       info.VisitURLUser,
			"visit_url_agent":      info.VisitURLAgent,
			"visit_url_developer":  info.VisitURLDeveloper,
			"ips":                  info.IPs,
			"public_key_client":    info.PublicKeyClient,
			"description":          info.Description,
			"updated_at_idx_ym":    info.UpdatedAtIdxYM,
		}

		offwOrID, err = _db.Table(BaAppTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	privKey, pubKey, err := lib.GenKeyPair(2048)
	if err != nil {
		return isAdd, offwOrID, err
	}

	info.PrivateKey = privKey
	info.PublicKey = pubKey
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	info.Status = StatusOK // 10正常运行中
	offwOrID, err = _db.Insert(info)
	return isAdd, info.ID, err
}

func BaAppForbid(id string, forbid bool) (int64, error) {
	upInfo := new(BaApp)
	if forbid {
		upInfo.Status = StatusForbid
	} else {
		upInfo.Status = StatusOK
	}
	return _db.ID(id).Cols("status").Update(upInfo)
}
