package tables

import (
	"errors"
	"strconv"
	"sync"
	"time"

	guuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/utils"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   开发者
 *   -- 开放平台开发者
 *
 **********************************************/

// BaAppTableName is 表名
const BaDeveloperTableName = "ba_developer"

// BaDeveloper is
// .Redirect 发生账户合并时 指向合并后的账户ID
type BaDeveloper struct {
	ID             int64     `json:"id" xorm:"pk autoincr 'id'"`
	RID            int64     `json:"rid" xorm:"notnull default(0) index 'rid'"`
	UUID           string    `json:"uuid" xorm:"char(40) notnull default('') 'uuid'"`
	OpenID         string    `json:"open_id" xorm:"text(500) notnull default('') 'open_id'"`
	Username       string    `json:"username" xorm:"char(64) notnull default('') 'username'"`
	HeadImage      string    `json:"head_image" xorm:"varchar(255) notnull default('') 'head_image'"`
	Email          string    `json:"email" xorm:"varchar(200) notnull index default('') 'email'"`
	Phone          string    `json:"phone" xorm:"varchar(30) notnull default('') 'phone'"`
	Password       string    `json:"-" xorm:"varchar(200) notnull default('') 'password'"`
	Activation     int       `json:"activation" xorm:"notnull default(0) index 'activation'"`
	Name           string    `json:"name" xorm:"varchar(200) notnull default('') 'name'"`
	Nickname       string    `json:"nickname" xorm:"varchar(200) notnull default('') 'nickname'"`
	ExtInfo        string    `json:"ext_info" xorm:"mediumtext notnull default('') 'ext_info'"`
	Status         int8      `json:"status" xorm:"notnull default(1) 'status'"`
	CreatedAt      time.Time `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxYM int       `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt      time.Time `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int       `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
	Balance        int64     `json:"balance" xorm:"notnull default(0) 'balance'"`
	Redirect       int64     `json:"redirect" xorm:"notnull default(0) 'redirect'"`
}

var lockBaDeveloperRegister sync.RWMutex

func init() {
	tables = append(tables, new(BaDeveloper))
}

// BaDeveloperLogin is 用户登录
func BaDeveloperLogin(id int64, account string, password string, mustPassword bool) (*BaDeveloper, error) {
	var userInfo *BaDeveloper
	var err error
	var exists bool
	if id > 0 {
		userInfo, exists, err = BaDeveloperByID(id)
		if err != nil {
			return nil, err
		}
		if !exists {
			return nil, errors.New("not exists")
		}
	} else {
		userInfo, err = BaDeveloperByAccount(account)
		if err != nil {
			return nil, err
		}
	}

	//检验密码
	if mustPassword {
		if err := bcrypt.CompareHashAndPassword([]byte(userInfo.Password), []byte(password)); err != nil {
			return nil, errors.New("password do not match")
		}
	}
	return userInfo, nil
}

func BaDeveloperByPhone(phone string, autoInsert bool) (*BaDeveloper, error) {
	re := &BaDeveloper{}
	has, err := _db.Where("phone = ?", phone).Get(re)
	if err != nil {
		return nil, err
	}
	if !has {
		if autoInsert {
			err = BaDeveloperRegisterByPhone(phone, "")
			if err != nil {
				return nil, err
			}

			has, err = _db.Where("phone = ?", phone).Get(re)
			if err != nil {
				return nil, err
			}
			if has {
				return re, nil
			}
		}
		return nil, errors.New("no record")
	}
	return re, nil
}

func BaDeveloperByAccount(account string) (*BaDeveloper, error) {
	whStr := "username = ?"
	if utils.IsPhone(account) {
		whStr = "phone = ?"
	} else if utils.IsEmail(account) {
		whStr = "email = ?"
	}
	re := &BaDeveloper{}
	has, err := _db.Where(whStr, account).Get(re)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.New("no record")
	}
	return re, err
}

func BaDeveloperByID(id int64) (*BaDeveloper, bool, error) {
	re := &BaDeveloper{}
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		return nil, exists, err
	}
	if exists {
		re.Password = ""
	}
	return re, exists, err
}

func BaDeveloperByKey(key string) (*BaDeveloper, bool, error) {
	re := &BaDeveloper{}
	exists, err := _db.Where("id = ? or uuid = ?", key, key).Get(re)
	if err != nil {
		return nil, exists, err
	}
	if exists {
		re.Password = ""
	}
	return re, exists, err
}

func BaDeveloperList(currentPage, pageSize int, kw string) ([]*BaDeveloper, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("name LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(BaDeveloper))
	if err != nil {
		return nil, 0, err
	}

	var res []*BaDeveloper
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}

	for _, v := range res {
		v.Password = ""
	}

	return res, total, nil
}

func BaDeveloperAddOrUpdate(
	reqID string,
	reqRID int64,
	reqUsername,
	reqName,
	reqHeadImage,
	reqEmail,
	reqPhone,
	reqPassword string) (isAdd bool, offwOrID int64, err error) {

	if reqID == "" || reqID == "0" {
		isAdd = true
	}

	info := new(BaDeveloper)
	info.RID = reqRID
	info.Name = reqName
	info.HeadImage = reqHeadImage
	info.Email = reqEmail
	info.Phone = reqPhone
	if reqPassword != "" {
		pw, err := GenUserHashPassword(reqPassword)
		if err != nil {
			return isAdd, offwOrID, err
		}

		info.Password = pw
	}

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.Username = reqUsername

	if !isAdd {
		if reqUsername != "" {
			c, err := _db.Where("username = ? AND id != ?", reqUsername, reqID).Count(new(BaDeveloper))
			if err != nil {
				return isAdd, offwOrID, err
			}

			if c > 0 {
				return isAdd, offwOrID, errors.New("username be used")
			}
		}

		minfo := map[string]interface{}{
			"rid":               info.RID,
			"name":              info.Name,
			"head_image":        info.HeadImage,
			"email":             info.Email,
			"phone":             info.Phone,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
		}
		if reqUsername != "" {
			minfo["username"] = reqUsername
		}
		if info.Password != "" {
			minfo["password"] = info.Password
		}

		offwOrID, err = _db.Table(BaDeveloperTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	c, err := _db.Where("username = ?", reqUsername).Count(new(BaDeveloper))
	if err != nil {
		return isAdd, offwOrID, err
	}

	if c > 0 {
		return isAdd, offwOrID, errors.New("username be used")
	}

	info.UUID = guuid.NewV4().String()
	if info.RID < 1 {
		info.RID = DefaultRoleIDDeveloper
	}
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	offwOrID, err = _db.Insert(info)
	return isAdd, info.ID, err
}

func BaDeveloperRegisterByUE(reqUsername, reqEmail,
	reqPhone, reqPassword, reqExtInfo string) error {
	lockBaDeveloperRegister.Lock()
	defer lockBaDeveloperRegister.Unlock()

	if len(reqUsername) < 4 && len(reqEmail) < 4 && len(reqPhone) < 4 {
		return errors.New("input value is too short")
	}

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return err
	}

	keys := make([]string, 0)
	if reqUsername != "" {
		keys = append(keys, reqUsername)
	}
	if reqEmail != "" {
		keys = append(keys, reqEmail)
	}
	if reqPhone != "" {
		keys = append(keys, reqPhone)
	}

	for _, key := range keys {
		c, err := sess.Where("username = ? or email = ? or phone = ?", key, key, key).
			Count(new(BaDeveloper))
		if err != nil {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln(err)
			sess.Rollback()
			return errors.New("register fail")
		}

		if c > 0 {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln("开发者已经存在")
			sess.Rollback()
			return errors.New("register fail")
		}
	}

	info := new(BaDeveloper)
	info.Name = "新开发着"
	if reqPassword != "" {
		pw, err := GenUserHashPassword(reqPassword)
		if err != nil {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln(err)
			sess.Rollback()
			return errors.New("register fail")
		}

		info.Password = pw
	}

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.Username = reqUsername
	info.Email = reqEmail
	info.Phone = reqPhone
	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDDeveloper
	info.ExtInfo = baapi.EncodeExtInfo(baapi.DecodeExtInfo(reqExtInfo, nil))
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if info.ID < 1 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("添加失败")
		sess.Rollback()
		return errors.New("register fail")
	}

	sess.Commit()

	return nil
}

func BaDeveloperRegisterByPhone(reqPhone, reqExtInfo string) error {
	lockBaDeveloperRegister.Lock()
	defer lockBaDeveloperRegister.Unlock()

	if len(reqPhone) < 11 {
		return errors.New("phone is too short")
	}

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return err
	}

	c, err := sess.Where("phone = ? ", reqPhone).Count(new(BaDeveloper))
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if c > 0 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("开发者已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	time.Sleep(time.Second)

	info := new(BaDeveloper)
	info.Name = "新开发着"
	info.Phone = reqPhone
	info.Activation = 1

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDDeveloper
	info.ExtInfo = baapi.EncodeExtInfo(baapi.DecodeExtInfo(reqExtInfo, nil))
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if info.ID < 1 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("添加失败")
		sess.Rollback()
		return errors.New("register fail")
	}

	sess.Commit()

	return nil
}

func (b *BaDeveloper) ToModAuthor(applyToken, device, auths string) *baapi.ModAuthor {
	account := b.Phone
	if b.Email != "" {
		account = b.Email
	}
	if b.Username != "" {
		account = b.Username
	}

	nickname := b.Name
	if b.Nickname != "" {
		nickname = b.Nickname
	}

	r := &baapi.ModAuthor{
		Type:       baapi.TypeDeveloper,
		ID:         b.ID,
		UUID:       b.UUID,
		Account:    account,
		Phone:      b.Phone,
		Nickname:   nickname,
		HeadImage:  b.HeadImage,
		ApplyToken: applyToken,
		Device:     device,
		Auth:       auths,
	}

	if b.OpenID != "" {
		r.ExtraInfo = baapi.DecodeExtInfo(b.ExtInfo, map[string]interface{}{
			"openid": b.OpenID,
		})
	} else {
		r.ExtraInfo = baapi.DecodeExtInfo(b.ExtInfo, nil)
	}
	return r
}

func BaDeveloperActivationByEmail(email string) error {
	minfo := map[string]interface{}{
		"activation": 1,
	}

	offwOrID, err := _db.Table(BaDeveloperTableName).Where("email = ?", email).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("activation or no change")
	}
	return nil
}

// 重置密码
func BaDeveloperResetPassword(reqPhone, reqEmail, reqPassword string) error {
	if reqPassword == "" {
		return errors.New("password can't empty")
	}

	account := reqEmail
	whereStr := "email = ?"
	if reqPhone != "" {
		account = reqPhone
		whereStr = "phone = ?"
	}

	pw, err := GenUserHashPassword(reqPassword)
	if err != nil {
		return err
	}

	minfo := map[string]interface{}{
		"password": pw,
	}

	offwOrID, err := _db.Table(BaDeveloperTableName).Where(whereStr, account).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("update password fail or no change")
	}

	return nil
}

//
var baseDeveloperCols = []string{"name", "head_image", "nickname"}

func BaDeveloperProfileUpdate(id int64, cols []string, vals map[string]interface{}) error {
	minfo := make(map[string]interface{})
	for _, col := range cols {
		if utils.InArray(baseDeveloperCols, col) {
			val, ok := vals[col]
			if ok {
				minfo[col] = val
			}
		}

	}

	if len(minfo) < 1 {
		return errors.New("no change")
	}

	offwOrID, err := _db.Table(BaDeveloperTableName).Where("id = ?", id).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("update password fail or no change")
	}

	return nil
}

func BaDeveloperAddOrUpdateByOpenID(
	reqOpenID,
	reqName,
	reqHeadImage string,
	extInfo interface{}) (id int64, err error) {

	if reqOpenID == "" {
		return 0, errors.New("key is empty")
	}

	sess := _db.NewSession()
	re := &BaDeveloper{}
	has, err := sess.Where("open_id = ?", reqOpenID).Get(re)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	info := new(BaDeveloper)
	info.OpenID = reqOpenID
	info.Name = reqName
	info.HeadImage = reqHeadImage
	info.ExtInfo = lib.JsonEncode(extInfo)

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if has {
		minfo := map[string]interface{}{
			"name":              info.Name,
			"head_image":        info.HeadImage,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
			"ext_info":          info.ExtInfo,
		}

		_, err = sess.Table(BaDeveloperTableName).Where("id = ?", re.ID).Update(minfo)
		if err != nil {
			sess.Rollback()
			return 0, err
		}
		return re.ID, nil
	}

	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDAgent
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	sess.Commit()

	return info.ID, nil
}
func BaDeveloperBindPhone(id int64, reqPhone string, force bool) error {
	sess := _db.NewSession()

	find := &BaDeveloper{}
	has, err := sess.Where("id != ? and username = ? or email = ? or phone = ?",
		id, reqPhone, reqPhone, reqPhone).Get(find)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if has && !force {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("用户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	minfo := map[string]interface{}{
		"phone": reqPhone,
	}
	if has {
		dinfo := map[string]interface{}{
			"redirect": id,
		}
		if find.Username != "" {
			minfo["username"] = find.Username
			dinfo["username"] = ""
		}
		if find.Email != "" {
			minfo["email"] = find.Email
			dinfo["email"] = ""
		}
		if find.Phone != "" {
			minfo["phone"] = reqPhone
			dinfo["phone"] = ""
		}
		if find.OpenID != "" {
			minfo["open_id"] = find.OpenID
			dinfo["open_id"] = ""
		}
		if find.Name != "" {
			minfo["name"] = find.Name
		}
		if find.HeadImage != "" {
			minfo["head_image"] = find.HeadImage
		}
		if find.ExtInfo != "" {
			minfo["ext_info"] = find.ExtInfo
		}

		_, err = sess.Table(BaDeveloperTableName).Where("id = ?", find.ID).Update(dinfo)
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	_, err = sess.Table(BaDeveloperTableName).Where("id = ?", id).Update(minfo)
	if err != nil {
		sess.Rollback()
		return err
	}

	sess.Commit()
	return nil
}
func BaDeveloperBindEmail(id int64, reqEmail string, force bool) error {
	sess := _db.NewSession()

	find := &BaDeveloper{}
	has, err := sess.Where("id != ? and username = ? or email = ? or phone = ?",
		id, reqEmail, reqEmail, reqEmail).Get(find)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if has && !force {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("用户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	minfo := map[string]interface{}{
		"email": reqEmail,
	}
	if has {
		dinfo := map[string]interface{}{
			"redirect": id,
		}
		if find.Username != "" {
			minfo["username"] = find.Username
			dinfo["username"] = ""
		}
		if find.Email != "" {
			minfo["email"] = reqEmail
			dinfo["email"] = ""
		}
		if find.Phone != "" {
			minfo["phone"] = find.Phone
			dinfo["phone"] = ""
		}
		if find.OpenID != "" {
			minfo["open_id"] = find.OpenID
			dinfo["open_id"] = ""
		}
		if find.Name != "" {
			minfo["name"] = find.Name
		}
		if find.HeadImage != "" {
			minfo["head_image"] = find.HeadImage
		}
		if find.ExtInfo != "" {
			minfo["ext_info"] = find.ExtInfo
		}

		_, err = sess.Table(BaDeveloperTableName).Where("id = ?", find.ID).Update(dinfo)
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	_, err = sess.Table(BaDeveloperTableName).Where("id = ?", id).Update(minfo)
	if err != nil {
		sess.Rollback()
		return err
	}

	sess.Commit()
	return nil
}
