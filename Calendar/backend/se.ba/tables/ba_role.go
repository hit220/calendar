package tables

import (
	"strconv"
	"time"

	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   角色
 *   -- 所有人员(管理员/企业/代理商/用户/开发者)的操作权限分类
 *
 **********************************************/

// BaRoleTableName is 表名
const BaRoleTableName = "ba_role"

const (
	DefaultRoleIDMaster     int64 = 1
	DefaultRoleIDAgent      int64 = 2
	DefaultRoleIDEnterprise int64 = 3
	DefaultRoleIDUser       int64 = 4
	DefaultRoleIDDeveloper  int64 = 5
)

// BaRole is
type BaRole struct {
	ID             int64     `json:"id" xorm:"pk autoincr 'id'"`
	Name           string    `json:"name" xorm:"char(64) notnull default('') 'name'"`
	AuthPaths      string    `json:"auth_paths" xorm:"mediumtext notnull default('') 'auth_paths'"`
	UserType       string    `json:"user_type" xorm:"mediumtext notnull default('') 'user_type'"`
	UpdatedAt      time.Time `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int       `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
}

func init() {
	tables = append(tables, new(BaRole))
}

func initBaRoleIfNeeded() error {
	n, err := _db.Count(new(BaRole))
	if err != nil {
		return err
	}

	if n > 0 {
		return nil
	}

	role := new(BaRole)
	role.ID = DefaultRoleIDMaster
	role.Name = "超级管理员"
	role.AuthPaths = "** [ANY]"

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))

	role.UpdatedAt = now
	role.UpdatedAtIdxYM = idxTM
	_, err = _db.Insert(role)
	if err != nil {
		return err
	}

	role.ID = DefaultRoleIDEnterprise
	role.Name = "默认商家权限"
	role.AuthPaths = lib.UrlJoin("/*", string(baapi.RouteApiEnterprise), "** [ANY]")
	_, err = _db.Insert(role)
	if err != nil {
		return err
	}

	role.ID = DefaultRoleIDAgent
	role.Name = "默认代理商权限"
	role.AuthPaths = lib.UrlJoin("/*", string(baapi.RouteApiAgent), "** [ANY]")
	_, err = _db.Insert(role)
	if err != nil {
		return err
	}

	role.ID = DefaultRoleIDUser
	role.Name = "默认用户权限"
	role.AuthPaths = lib.UrlJoin("/*", string(baapi.RouteApiUser), "** [ANY]")
	_, err = _db.Insert(role)
	if err != nil {
		return err
	}

	role.ID = DefaultRoleIDDeveloper
	role.Name = "默认开发者权限"
	role.AuthPaths = lib.UrlJoin("/*", string(baapi.RouteApiDeveloper), "** [ANY]")
	_, err = _db.Insert(role)
	if err != nil {
		return err
	}

	return err
}

func BaRoleByID(id int64) (*BaRole, bool, error) {
	re := new(BaRole)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

func BaRoleList(currentPage, pageSize int, kw string) ([]*BaRole, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("name LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(BaRole))
	if err != nil {
		return nil, 0, err
	}

	var res []*BaRole
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func BaRoleAddOrUpdate(
	reqID,
	reqName,
	reqAuthPaths,
	UserType string) (isAdd bool, offwOrID int64, err error) {

	if reqID == "" || reqID == "0" {
		isAdd = true
	}

	info := new(BaRole)
	info.Name = reqName
	info.AuthPaths = reqAuthPaths
	info.UserType = UserType

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if !isAdd {

		minfo := map[string]interface{}{
			"name":              info.Name,
			"auth_paths":        info.AuthPaths,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
			"user_type":         info.UserType,
		}

		offwOrID, err = _db.Table(BaRoleTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	offwOrID, err = _db.Insert(info)
	return isAdd, info.ID, err
}
