package tables

import (
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	guuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/utils"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   管理员
 *   -- 企业内部各级管理人员
 *
 **********************************************/

// BaMasterTableNmae is 表名
const BaMasterTableName = "ba_master"

// BaMaster is
// .Redirect 发生账户合并时 指向合并后的账户ID
type BaMaster struct {
	ID             int64     `json:"id" xorm:"pk autoincr 'id'"`
	RID            int64     `json:"rid" xorm:"notnull default(0) index 'rid'"`
	UUID           string    `json:"uuid" xorm:"char(40) notnull default('') 'uuid'"`
	OpenID         string    `json:"open_id" xorm:"text(500) notnull default('') 'open_id'"`
	Username       string    `json:"username" xorm:"char(64) notnull default('') 'username'"`
	HeadImage      string    `json:"head_image" xorm:"varchar(255) notnull default('') 'head_image'"`
	Email          string    `json:"email" xorm:"varchar(200) notnull index default('') 'email'"`
	Phone          string    `json:"phone" xorm:"varchar(30) notnull default('') 'phone'"`
	Password       string    `json:"-" xorm:"varchar(200) notnull default('') 'password'"`
	Activation     int       `json:"activation" xorm:"notnull default(0) index 'activation'"`
	Name           string    `json:"name" xorm:"varchar(200) notnull default('') 'name'"`
	ExtInfo        string    `json:"ext_info" xorm:"mediumtext notnull default('') 'ext_info'"`
	Status         int8      `json:"status" xorm:"notnull default(1) 'status'"`
	CreatedAt      time.Time `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxYM int       `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt      time.Time `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int       `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
	Balance        int64     `json:"balance" xorm:"notnull default(0) 'balance'"`
	Redirect       int64     `json:"redirect" xorm:"notnull default(0) 'redirect'"`
}

func init() {
	tables = append(tables, new(BaMaster))
}

var lockBaMasterRegister sync.RWMutex

func initBaMasterIfNeeded() error {
	n, err := _db.Count(new(BaMaster))
	if err != nil {
		return err
	}

	if n > 0 {
		return nil
	}

	user := new(BaMaster)
	user.ID = 1
	user.Username = "admin"
	user.Name = "超级管理员"
	pw, err := GenUserHashPassword(GenUserSha256("123456"))
	if err != nil {
		return err
	}
	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))

	user.RID = 1
	user.Password = pw
	user.CreatedAt = now
	user.CreatedAtIdxYM = idxTM
	user.UpdatedAt = now
	user.UpdatedAtIdxYM = idxTM
	_, err = _db.Insert(user)
	return err
}

// BaMasterLogin is 用户登录
func BaMasterLogin(id int64, account string, password string, mustPassword bool) (*BaMaster, error) {
	var userInfo *BaMaster
	var err error
	var exists bool
	if id > 0 {
		userInfo, exists, err = BaMasterByID(id)
		if err != nil {
			return nil, err
		}
		if !exists {
			return nil, errors.New("not exists")
		}
	} else {
		userInfo, err = BaMasterByAccount(account)
		if err != nil {
			return nil, err
		}
	}

	//检验密码
	if mustPassword {
		if err := bcrypt.CompareHashAndPassword([]byte(userInfo.Password), []byte(password)); err != nil {
			return nil, errors.New("password do not match")
		}
	}
	return userInfo, nil
}

func BaMasterByPhone(phone string, autoInsert bool) (*BaMaster, error) {
	re := &BaMaster{}
	has, err := _db.Where("phone = ?", phone).Get(re)
	if err != nil {
		return nil, err
	}
	if !has {
		if autoInsert {
			err = BaMasterRegisterByPhone(phone, "")
			if err != nil {
				return nil, err
			}

			has, err = _db.Where("phone = ?", phone).Get(re)
			if err != nil {
				return nil, err
			}
			if has {
				return re, nil
			}
		}
		return nil, errors.New("no record")
	}
	return re, nil
}

func BaMasterByAccount(account string) (*BaMaster, error) {
	whStr := "username = ?"
	if utils.IsPhone(account) {
		whStr = "phone = ?"
	} else if utils.IsEmail(account) {
		whStr = "email = ?"
	}
	re := &BaMaster{}
	has, err := _db.Where(whStr, account).Get(re)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.New("no record")
	}
	return re, err
}

func BaMasterByID(id int64) (*BaMaster, bool, error) {
	re := &BaMaster{}
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		return nil, exists, err
	}
	if exists {
		re.Password = ""
	}
	return re, exists, err
}

func BaMasterByKey(key string) (*BaMaster, bool, error) {
	re := &BaMaster{}
	exists, err := _db.Where("id = ? or uuid = ?", key, key).Get(re)
	if err != nil {
		return nil, exists, err
	}
	if exists {
		re.Password = ""
	}
	return re, exists, err
}

func BaMasterList(currentPage, pageSize int, kw string) ([]*BaMaster, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("name LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(BaMaster))
	if err != nil {
		return nil, 0, err
	}

	var res []*BaMaster
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}

	for _, v := range res {
		v.Password = ""
	}

	return res, total, nil
}

func BaMasterAddOrUpdate(
	reqID string,
	reqRID int64,
	reqUsername,
	reqName,
	reqHeadImage,
	reqEmail,
	reqPhone,
	reqAddress,
	reqPassword string) (isAdd bool, offwOrID int64, err error) {

	if reqID == "" || reqID == "0" {
		isAdd = true
	}

	info := new(BaMaster)
	info.RID = reqRID
	info.Name = reqName
	info.HeadImage = reqHeadImage
	info.Email = reqEmail
	info.Phone = reqPhone
	if reqPassword != "" {
		pw, err := GenUserHashPassword(reqPassword)
		if err != nil {
			return isAdd, offwOrID, err
		}

		info.Password = pw
	}

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if !isAdd {
		if reqUsername != "" {
			c, err := _db.Where("username = ? AND id != ?", reqUsername, reqID).Count(new(BaMaster))
			if err != nil {
				return isAdd, offwOrID, err
			}

			if c > 0 {
				return isAdd, offwOrID, errors.New("username be used")
			}
		}

		minfo := map[string]interface{}{
			"rid":               info.RID,
			"name":              info.Name,
			"head_image":        info.HeadImage,
			"email":             info.Email,
			"phone":             info.Phone,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
		}
		if reqUsername != "" {
			minfo["username"] = reqUsername
		}
		if info.Password != "" {
			minfo["password"] = info.Password
		}

		offwOrID, err = _db.Table(BaMasterTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	info.UUID = guuid.NewV4().String()
	if info.RID < 1 {
		info.RID = DefaultRoleIDMaster
	}
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	offwOrID, err = _db.Insert(info)

	if offwOrID > 0 {
		upInfo := new(BaMaster)
		upInfo.Username = fmt.Sprintf("SE%s%d%07d", "m", ULevelBottom, info.ID)
		_, _ = _db.ID(info.ID).Cols("username").Update(upInfo)
	}
	return isAdd, info.ID, err
}

func BaMasterRegisterByUE(reqUsername, reqEmail,
	reqPhone, reqPassword, reqExtInfo string) error {
	lockBaMasterRegister.Lock()
	defer lockBaMasterRegister.Unlock()

	if len(reqUsername) < 4 && len(reqEmail) < 4 && len(reqPhone) < 4 {
		return errors.New("input value is too short")
	}

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return err
	}

	keys := make([]string, 0)
	if reqUsername != "" {
		keys = append(keys, reqUsername)
	}
	if reqEmail != "" {
		keys = append(keys, reqEmail)
	}
	if reqPhone != "" {
		keys = append(keys, reqPhone)
	}

	for _, key := range keys {
		c, err := sess.Where("username = ? or email = ? or phone = ?", key, key, key).
			Count(new(BaMaster))
		if err != nil {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln(err)
			sess.Rollback()
			return errors.New("register fail")
		}

		if c > 0 {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln("管理员已经存在")
			sess.Rollback()
			return errors.New("register fail")
		}
	}

	info := new(BaMaster)
	info.Name = "新管理员"
	if reqPassword != "" {
		pw, err := GenUserHashPassword(reqPassword)
		if err != nil {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln(err)
			sess.Rollback()
			return errors.New("register fail")
		}

		info.Password = pw
	}

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.Username = reqUsername
	info.Email = reqEmail
	info.Phone = reqPhone
	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDMaster
	info.ExtInfo = baapi.EncodeExtInfo(baapi.DecodeExtInfo(reqExtInfo, nil))
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if info.ID < 1 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("添加失败")
		sess.Rollback()
		return errors.New("register fail")
	}

	sess.Commit()

	return nil
}

func BaMasterRegisterByPhone(reqPhone, reqExtInfo string) error {
	lockBaMasterRegister.Lock()
	defer lockBaMasterRegister.Unlock()

	if len(reqPhone) < 11 {
		return errors.New("phone is too short")
	}

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return err
	}

	c, err := sess.Where("phone = ? ", reqPhone).Count(new(BaMaster))
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if c > 0 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("管理员已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	info := new(BaMaster)
	info.Name = "新管理员"
	info.Phone = reqPhone
	info.Activation = 1

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDMaster
	info.ExtInfo = baapi.EncodeExtInfo(baapi.DecodeExtInfo(reqExtInfo, nil))
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if info.ID < 1 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("添加失败")
		sess.Rollback()
		return errors.New("register fail")
	}

	sess.Commit()

	return nil
}

func (b *BaMaster) ToModAuthor(applyToken, device, auths string) *baapi.ModAuthor {
	account := b.Phone
	if b.Email != "" {
		account = b.Email
	}
	if b.Username != "" {
		account = b.Username
	}

	nickname := b.Name

	r := &baapi.ModAuthor{
		Type:       baapi.TypeMaster,
		ID:         b.ID,
		UUID:       b.UUID,
		Account:    account,
		Phone:      b.Phone,
		Nickname:   nickname,
		HeadImage:  b.HeadImage,
		ApplyToken: applyToken,
		Device:     device,
		Auth:       auths,
	}
	if b.OpenID != "" {
		r.ExtraInfo = baapi.DecodeExtInfo(b.ExtInfo, map[string]interface{}{
			"openid": b.OpenID,
		})
	} else {
		r.ExtraInfo = baapi.DecodeExtInfo(b.ExtInfo, nil)
	}
	return r
}

func BaMasterActivationByEmail(email string) error {
	minfo := map[string]interface{}{
		"activation": 1,
	}

	offwOrID, err := _db.Table(BaMasterTableName).Where("email = ?", email).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("activation or no change")
	}
	return nil
}

// 重置密码
func BaMasterResetPassword(reqPhone, reqEmail, reqPassword string) error {
	if reqPassword == "" {
		return errors.New("password can't empty")
	}

	account := reqEmail
	whereStr := "email = ?"
	if reqPhone != "" {
		account = reqPhone
		whereStr = "phone = ?"
	}

	pw, err := GenUserHashPassword(reqPassword)
	if err != nil {
		return err
	}

	minfo := map[string]interface{}{
		"password": pw,
	}

	offwOrID, err := _db.Table(BaMasterTableName).Where(whereStr, account).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("update password fail or no change")
	}

	return nil
}

//
var baseMasterCols = []string{"name", "head_image"}

func BaMasterProfileUpdate(id int64, cols []string, vals map[string]interface{}) error {
	minfo := make(map[string]interface{})
	for _, col := range cols {
		if utils.InArray(baseMasterCols, col) {
			val, ok := vals[col]
			if ok {
				minfo[col] = val
			}
		}

	}

	if len(minfo) < 1 {
		return errors.New("no change")
	}

	offwOrID, err := _db.Table(BaMasterTableName).Where("id = ?", id).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("update password fail or no change")
	}

	return nil
}

func BaMasterAddOrUpdateByOpenID(
	reqOpenID,
	reqName,
	reqHeadImage string,
	extInfo interface{}) (id int64, err error) {

	if reqOpenID == "" {
		return 0, errors.New("key is empty")
	}

	sess := _db.NewSession()
	re := &BaMaster{}
	has, err := sess.Where("open_id = ?", reqOpenID).Get(re)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	info := new(BaMaster)
	info.OpenID = reqOpenID
	info.Name = reqName
	info.HeadImage = reqHeadImage
	info.ExtInfo = lib.JsonEncode(extInfo)

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if has {
		minfo := map[string]interface{}{
			"name":              info.Name,
			"head_image":        info.HeadImage,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
			"ext_info":          info.ExtInfo,
		}

		_, err = sess.Table(BaMasterTableName).Where("id = ?", re.ID).Update(minfo)
		if err != nil {
			sess.Rollback()
			return 0, err
		}
		return re.ID, nil
	}

	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDAgent
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	sess.Commit()

	return info.ID, nil
}

func BaMasterBindPhone(id int64, reqPhone string, force bool) error {
	sess := _db.NewSession()

	find := &BaMaster{}
	has, err := sess.Where("id != ? and username = ? or email = ? or phone = ?",
		id, reqPhone, reqPhone, reqPhone).Get(find)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if has && !force {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("用户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	minfo := map[string]interface{}{
		"phone": reqPhone,
	}
	if has {
		dinfo := map[string]interface{}{
			"redirect": id,
		}
		if find.Username != "" {
			minfo["username"] = find.Username
			dinfo["username"] = ""
		}
		if find.Email != "" {
			minfo["email"] = find.Email
			dinfo["email"] = ""
		}
		if find.Phone != "" {
			minfo["phone"] = reqPhone
			dinfo["phone"] = ""
		}
		if find.OpenID != "" {
			minfo["open_id"] = find.OpenID
			dinfo["open_id"] = ""
		}
		if find.Name != "" {
			minfo["name"] = find.Name
		}
		if find.HeadImage != "" {
			minfo["head_image"] = find.HeadImage
		}
		if find.ExtInfo != "" {
			minfo["ext_info"] = find.ExtInfo
		}

		_, err = sess.Table(BaMasterTableName).Where("id = ?", find.ID).Update(dinfo)
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	_, err = sess.Table(BaMasterTableName).Where("id = ?", id).Update(minfo)
	if err != nil {
		sess.Rollback()
		return err
	}

	sess.Commit()
	return nil
}

func BaMasterBindEmail(id int64, reqEmail string, force bool) error {
	sess := _db.NewSession()

	find := &BaMaster{}
	has, err := sess.Where("id != ? and username = ? or email = ? or phone = ?",
		id, reqEmail, reqEmail, reqEmail).Get(find)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if has && !force {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("用户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	minfo := map[string]interface{}{
		"email": reqEmail,
	}
	if has {
		dinfo := map[string]interface{}{
			"redirect": id,
		}
		if find.Username != "" {
			minfo["username"] = find.Username
			dinfo["username"] = ""
		}
		if find.Email != "" {
			minfo["email"] = reqEmail
			dinfo["email"] = ""
		}
		if find.Phone != "" {
			minfo["phone"] = find.Phone
			dinfo["phone"] = ""
		}
		if find.OpenID != "" {
			minfo["open_id"] = find.OpenID
			dinfo["open_id"] = ""
		}
		if find.Name != "" {
			minfo["name"] = find.Name
		}
		if find.HeadImage != "" {
			minfo["head_image"] = find.HeadImage
		}
		if find.ExtInfo != "" {
			minfo["ext_info"] = find.ExtInfo
		}

		_, err = sess.Table(BaMasterTableName).Where("id = ?", find.ID).Update(dinfo)
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	_, err = sess.Table(BaMasterTableName).Where("id = ?", id).Update(minfo)
	if err != nil {
		sess.Rollback()
		return err
	}

	sess.Commit()
	return nil
}
