package tables

import (
	"fmt"
	"sync"
	"testing"
	"time"

	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
)

// 测试账号注册重复
func TestXORMSession(t *testing.T) {
	cfg.TryInit("json")
	cfg.Set("db.driver", "mysql")
	cfg.Set("db.source", "root:123456@tcp(test.seasonalcal.life:3306)/se.ba?charset=utf8")

	db, err := DBInit(false)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	defer db.Close()

	wg := &sync.WaitGroup{}
	mp := make(map[string]int)
	for i := 0; i < 50; i++ {
		mp[lib.KindRand(20, lib.KC_RAND_KIND_UPPER)] = i

	}

	n := 0
	for k, v := range mp {
		n++
		if n > 10 {
			break
		}
		for i := 0; i < 3; i++ {
			time.Sleep(time.Millisecond * 500)
			wg.Add(1)
			go func(a int, b string) {
				defer wg.Done()

				err := BaUserRegisterByUE("", "", b, "123456", "")
				fmt.Println(b, a, err)
			}(v, k)
		}
	}

	wg.Wait()
	fmt.Println("----------- over -----------")

}
