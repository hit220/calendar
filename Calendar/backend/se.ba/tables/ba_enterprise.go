package tables

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	guuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/utils"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   企业
 *   -- 各个业务层的企业用户
 *
 **********************************************/

// BaAppTableName is 表名
// .Redirect 发生账户合并时 指向合并后的账户ID
const BaEnterpriseTableName = "ba_enterprise"

// BaEnterprise is
type BaEnterprise struct {
	ID             int64     `json:"id" xorm:"pk autoincr 'id'"`
	RID            int64     `json:"rid" xorm:"notnull default(0) index 'rid'"`
	UUID           string    `json:"uuid" xorm:"char(40) notnull default('') 'uuid'"`
	OpenID         string    `json:"open_id" xorm:"text(500) notnull default('') 'open_id'"`
	Username       string    `json:"username" xorm:"char(64) notnull default('') 'username'"`
	HeadImage      string    `json:"head_image" xorm:"varchar(255) notnull default('') 'head_image'"`
	Email          string    `json:"email" xorm:"varchar(200) notnull index default('') 'email'"`
	Phone          string    `json:"phone" xorm:"varchar(30) notnull default('') 'phone'"`
	Address        string    `json:"address" xorm:"text(20000) notnull default('') 'address'"`
	Password       string    `json:"-" xorm:"varchar(200) notnull default('') 'password'"`
	Activation     int       `json:"activation" xorm:"notnull default(0) index 'activation'"`
	Name           string    `json:"name" xorm:"varchar(200) notnull default('') 'name'"`
	ExtInfo        string    `json:"ext_info" xorm:"mediumtext notnull default('') 'ext_info'"`
	Status         int8      `json:"status" xorm:"notnull default(1) 'status'"`
	Apps           string    `json:"apps" xorm:"text(20000) notnull default('') 'apps'"`
	CreatedAt      time.Time `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxYM int       `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt      time.Time `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int       `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
	Balance        int64     `json:"balance" xorm:"notnull default(0) 'balance'"`
	Redirect       int64     `json:"redirect" xorm:"notnull default(0) 'redirect'"`
}

var lockBaEnterpriseRegister sync.RWMutex

func init() {
	tables = append(tables, new(BaEnterprise))
}

// BaEnterpriseLogin is 用户登录
func BaEnterpriseLogin(id int64, account string, password string, mustPassword bool) (*BaEnterprise, error) {
	var userInfo *BaEnterprise
	var err error
	var exists bool
	if id > 0 {
		userInfo, exists, err = BaEnterpriseByID(id)
		if err != nil {
			return nil, err
		}
		if !exists {
			return nil, errors.New("not exists")
		}
	} else {
		userInfo, err = BaEnterpriseByAccount(account)
		if err != nil {
			return nil, err
		}
	}

	//检验密码
	if mustPassword {
		if err := bcrypt.CompareHashAndPassword([]byte(userInfo.Password), []byte(password)); err != nil {
			return nil, errors.New("password do not match")
		}
	}
	return userInfo, nil
}

func BaEnterpriseByPhone(phone string, autoInsert bool) (*BaEnterprise, error) {
	re := &BaEnterprise{}
	has, err := _db.Where("phone = ?", phone).Get(re)
	if err != nil {
		return nil, err
	}
	if !has {
		if autoInsert {
			err = BaEnterpriseRegisterByPhone(phone, "")
			if err != nil {
				return nil, err
			}

			has, err = _db.Where("phone = ?", phone).Get(re)
			if err != nil {
				return nil, err
			}
			if has {
				return re, nil
			}
		}
		return nil, errors.New("no record")
	}
	return re, nil
}

func BaEnterpriseByAccount(account string) (*BaEnterprise, error) {
	whStr := "username = ?"
	if utils.IsPhone(account) {
		whStr = "phone = ?"
	} else if utils.IsEmail(account) {
		whStr = "email = ?"
	}
	re := &BaEnterprise{}
	has, err := _db.Where(whStr, account).Get(re)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.New("no record")
	}
	return re, err
}

func BaEnterpriseByID(id int64) (*BaEnterprise, bool, error) {
	re := &BaEnterprise{}
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		return nil, exists, err
	}
	if exists {
		re.Password = ""
	}
	return re, exists, err
}

func BaEnterpriseByKey(key string) (*BaEnterprise, bool, error) {
	re := &BaEnterprise{}
	exists, err := _db.Where("id = ? or uuid = ?", key, key).Get(re)
	if err != nil {
		return nil, exists, err
	}
	if exists {
		re.Password = ""
	}
	return re, exists, err
}

func BaEnterpriseList(currentPage, pageSize int, kw string) ([]*BaEnterprise, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("name LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(BaEnterprise))
	if err != nil {
		return nil, 0, err
	}

	var res []*BaEnterprise
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}

	for _, v := range res {
		v.Password = ""
	}

	return res, total, nil
}

func BaEnterpriseAddOrUpdate(
	reqID string,
	reqRID int64,
	reqUsername,
	reqName,
	reqULevel,
	reqHeadImage,
	reqEmail,
	reqPhone,
	reqAddress,
	reqPassword string) (isAdd bool, offwOrID int64, err error) {

	if reqID == "" || reqID == "0" {
		isAdd = true
	}

	info := new(BaEnterprise)
	info.RID = reqRID
	info.Name = reqName
	info.HeadImage = reqHeadImage
	info.Email = reqEmail
	info.Phone = reqPhone
	info.Address = reqAddress
	if reqPassword != "" {
		pw, err := GenUserHashPassword(reqPassword)
		if err != nil {
			return isAdd, offwOrID, err
		}

		info.Password = pw
	}

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if !isAdd {
		if reqUsername != "" {
			c, err := _db.Where("username = ? AND id != ?", reqUsername, reqID).Count(new(BaEnterprise))
			if err != nil {
				return isAdd, offwOrID, err
			}

			if c > 0 {
				return isAdd, offwOrID, errors.New("username be used")
			}
		}

		minfo := map[string]interface{}{
			"rid":               info.RID,
			"name":              info.Name,
			"head_image":        info.HeadImage,
			"email":             info.Email,
			"phone":             info.Phone,
			"address":           info.Address,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
		}
		if reqUsername != "" {
			minfo["username"] = reqUsername
		}
		if info.Password != "" {
			minfo["password"] = info.Password
		}

		offwOrID, err = _db.Table(BaEnterpriseTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	info.UUID = guuid.NewV4().String()
	if info.RID < 1 {
		info.RID = DefaultRoleIDEnterprise
	}
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	offwOrID, err = _db.Insert(info)

	if offwOrID > 0 {
		upInfo := new(BaEnterprise)
		upInfo.Username = fmt.Sprintf("SE%s%d%07d", "e", lib.ValueToInt(reqULevel, int(ULevelBottom)), info.ID)
		_, _ = _db.ID(info.ID).Cols("username").Update(upInfo)
	}
	return isAdd, info.ID, err
}

func BaEnterpriseOpenMoudle(id string, appID string, open bool, callback func(*BaEnterprise, *xorm.Session) error) (int64, error) {

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return 0, err
	}

	one := new(BaEnterprise)
	exists, err := sess.ID(id).Get(one)
	if err != nil || !exists {
		sess.Rollback()
		return 0, err
	}

	arr := strings.Split(one.Apps, ",")
	nArr := make([]string, 0)
	appID = strings.TrimSpace(appID)
	for _, v := range arr {
		if v == "" {
			continue
		}
		if appID == v {
			if open {
				// 已经存在, 不用更新
				sess.Commit()
				return 1, nil
			}
		} else {
			nArr = append(nArr, v)
		}
	}
	if open {
		nArr = append(nArr, appID)
	}

	upInfo := new(BaEnterprise)
	upInfo.Apps = strings.Join(nArr, ",")

	offw, err := _db.ID(id).Cols("apps").Update(upInfo)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	err = callback(one, sess)
	if err != nil {
		sess.Rollback()
		return 0, err
	}
	sess.Commit()

	return offw, nil
}

func BaEnterpriseRegisterByUE(reqUsername, reqEmail,
	reqPhone, reqPassword, reqExtInfo string) error {
	lockBaEnterpriseRegister.Lock()
	defer lockBaEnterpriseRegister.Unlock()

	if len(reqUsername) < 4 && len(reqEmail) < 4 && len(reqPhone) < 4 {
		return errors.New("input value is too short")
	}

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return err
	}

	keys := make([]string, 0)
	if reqUsername != "" {
		keys = append(keys, reqUsername)
	}
	if reqEmail != "" {
		keys = append(keys, reqEmail)
	}
	if reqPhone != "" {
		keys = append(keys, reqPhone)
	}

	for _, key := range keys {
		c, err := sess.Where("username = ? or email = ? or phone = ?", key, key, key).
			Count(new(BaEnterprise))
		if err != nil {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln(err)
			sess.Rollback()
			return errors.New("register fail")
		}

		if c > 0 {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln("商户已经存在")
			sess.Rollback()
			return errors.New("register fail")
		}
	}

	info := new(BaEnterprise)
	info.Name = "新商户"
	if reqPassword != "" {
		pw, err := GenUserHashPassword(reqPassword)
		if err != nil {
			// 不显示具体失败原因, 防止被撞库
			glog.Errorln(err)
			sess.Rollback()
			return errors.New("register fail")
		}

		info.Password = pw
	}

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.Username = reqUsername
	info.Email = reqEmail
	info.Phone = reqPhone
	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDEnterprise
	info.ExtInfo = baapi.EncodeExtInfo(baapi.DecodeExtInfo(reqExtInfo, nil))
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if info.ID < 1 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("添加失败")
		sess.Rollback()
		return errors.New("register fail")
	}

	sess.Commit()

	return nil
}

func BaEnterpriseRegisterByPhone(reqPhone, reqExtInfo string) error {
	lockBaEnterpriseRegister.Lock()
	defer lockBaEnterpriseRegister.Unlock()

	if len(reqPhone) < 11 {
		return errors.New("phone is too short")
	}

	sess := _db.NewSession()
	defer sess.Close()

	err := sess.Begin()
	if err != nil {
		return err
	}

	c, err := sess.Where("phone = ? ", reqPhone).Count(new(BaEnterprise))
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if c > 0 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("商户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	info := new(BaEnterprise)
	info.Name = "新商户"
	info.Phone = reqPhone
	info.Activation = 1

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM
	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDEnterprise
	info.ExtInfo = baapi.EncodeExtInfo(baapi.DecodeExtInfo(reqExtInfo, nil))
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if info.ID < 1 {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("添加失败")
		sess.Rollback()
		return errors.New("register fail")
	}

	sess.Commit()

	return nil
}

func (b *BaEnterprise) ToModAuthor(applyToken, device, auths string) *baapi.ModAuthor {
	account := b.Phone
	if b.Email != "" {
		account = b.Email
	}
	if b.Username != "" {
		account = b.Username
	}

	nickname := b.Name

	r := &baapi.ModAuthor{
		Type:       baapi.TypeEnterprise,
		ID:         b.ID,
		UUID:       b.UUID,
		Account:    account,
		Phone:      b.Phone,
		Nickname:   nickname,
		HeadImage:  b.HeadImage,
		ApplyToken: applyToken,
		Device:     device,
		Auth:       auths,
	}
	if b.OpenID != "" {
		r.ExtraInfo = baapi.DecodeExtInfo(b.ExtInfo, map[string]interface{}{
			"openid": b.OpenID,
		})
	} else {
		r.ExtraInfo = baapi.DecodeExtInfo(b.ExtInfo, nil)
	}
	return r
}

func BaEnterpriseActivationByEmail(email string) error {
	minfo := map[string]interface{}{
		"activation": 1,
	}

	offwOrID, err := _db.Table(BaEnterpriseTableName).Where("email = ?", email).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("activation or no change")
	}
	return nil
}

// 重置密码
func BaEnterpriseResetPassword(reqPhone, reqEmail, reqPassword string) error {
	if reqPassword == "" {
		return errors.New("password can't empty")
	}

	account := reqEmail
	whereStr := "email = ?"
	if reqPhone != "" {
		account = reqPhone
		whereStr = "phone = ?"
	}

	pw, err := GenUserHashPassword(reqPassword)
	if err != nil {
		return err
	}

	minfo := map[string]interface{}{
		"password": pw,
	}

	offwOrID, err := _db.Table(BaEnterpriseTableName).Where(whereStr, account).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("update password fail or no change")
	}

	return nil
}

//
var baseEnterpriseCols = []string{"name", "head_image"}

func BaEnterpriseProfileUpdate(id int64, cols []string, vals map[string]interface{}) error {
	minfo := make(map[string]interface{})
	for _, col := range cols {
		if utils.InArray(baseEnterpriseCols, col) {
			val, ok := vals[col]
			if ok {
				minfo[col] = val
			}
		}

	}

	if len(minfo) < 1 {
		return errors.New("no change")
	}

	offwOrID, err := _db.Table(BaEnterpriseTableName).Where("id = ?", id).
		Update(minfo)
	if err != nil {
		return err
	}
	if offwOrID < 1 {
		return errors.New("update password fail or no change")
	}

	return nil
}

func BaEnterpriseAddOrUpdateByOpenID(
	reqOpenID,
	reqName,
	reqHeadImage string,
	extInfo interface{}) (id int64, err error) {

	if reqOpenID == "" {
		return 0, errors.New("key is empty")
	}

	sess := _db.NewSession()
	re := &BaEnterprise{}
	has, err := sess.Where("open_id = ?", reqOpenID).Get(re)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	info := new(BaEnterprise)
	info.OpenID = reqOpenID
	info.Name = reqName
	info.HeadImage = reqHeadImage
	info.ExtInfo = lib.JsonEncode(extInfo)

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if has {
		minfo := map[string]interface{}{
			"name":              info.Name,
			"head_image":        info.HeadImage,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
			"ext_info":          info.ExtInfo,
		}

		_, err = sess.Table(BaEnterpriseTableName).Where("id = ?", re.ID).Update(minfo)
		if err != nil {
			sess.Rollback()
			return 0, err
		}
		return re.ID, nil
	}

	info.UUID = guuid.NewV4().String()
	info.RID = DefaultRoleIDAgent
	info.CreatedAt = now
	info.CreatedAtIdxYM = idxTM
	_, err = sess.Insert(info)
	if err != nil {
		sess.Rollback()
		return 0, err
	}

	sess.Commit()

	return info.ID, nil
}

func BaEnterpriseBindPhone(id int64, reqPhone string, force bool) error {
	sess := _db.NewSession()

	find := &BaEnterprise{}
	has, err := sess.Where("id != ? and username = ? or email = ? or phone = ?",
		id, reqPhone, reqPhone, reqPhone).Get(find)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if has && !force {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("用户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	minfo := map[string]interface{}{
		"phone": reqPhone,
	}
	if has {
		dinfo := map[string]interface{}{
			"redirect": id,
		}
		if find.Username != "" {
			minfo["username"] = find.Username
			dinfo["username"] = ""
		}
		if find.Email != "" {
			minfo["email"] = find.Email
			dinfo["email"] = ""
		}
		if find.Phone != "" {
			minfo["phone"] = reqPhone
			dinfo["phone"] = ""
		}
		if find.OpenID != "" {
			minfo["open_id"] = find.OpenID
			dinfo["open_id"] = ""
		}
		if find.Name != "" {
			minfo["name"] = find.Name
		}
		if find.HeadImage != "" {
			minfo["head_image"] = find.HeadImage
		}
		if find.ExtInfo != "" {
			minfo["ext_info"] = find.ExtInfo
		}

		_, err = sess.Table(BaEnterpriseTableName).Where("id = ?", find.ID).Update(dinfo)
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	_, err = sess.Table(BaEnterpriseTableName).Where("id = ?", id).Update(minfo)
	if err != nil {
		sess.Rollback()
		return err
	}

	sess.Commit()
	return nil
}

func BaEnterpriseBindEmail(id int64, reqEmail string, force bool) error {
	sess := _db.NewSession()

	find := &BaEnterprise{}
	has, err := sess.Where("id != ? and username = ? or email = ? or phone = ?",
		id, reqEmail, reqEmail, reqEmail).Get(find)
	if err != nil {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln(err)
		sess.Rollback()
		return errors.New("register fail")
	}

	if has && !force {
		// 不显示具体失败原因, 防止被撞库
		glog.Errorln("用户已经存在")
		sess.Rollback()
		return errors.New("register fail")
	}

	minfo := map[string]interface{}{
		"email": reqEmail,
	}
	if has {
		dinfo := map[string]interface{}{
			"redirect": id,
		}
		if find.Username != "" {
			minfo["username"] = find.Username
			dinfo["username"] = ""
		}
		if find.Email != "" {
			minfo["email"] = reqEmail
			dinfo["email"] = ""
		}
		if find.Phone != "" {
			minfo["phone"] = find.Phone
			dinfo["phone"] = ""
		}
		if find.OpenID != "" {
			minfo["open_id"] = find.OpenID
			dinfo["open_id"] = ""
		}
		if find.Name != "" {
			minfo["name"] = find.Name
		}
		if find.HeadImage != "" {
			minfo["head_image"] = find.HeadImage
		}
		if find.ExtInfo != "" {
			minfo["ext_info"] = find.ExtInfo
		}

		_, err = sess.Table(BaEnterpriseTableName).Where("id = ?", find.ID).Update(dinfo)
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	_, err = sess.Table(BaEnterpriseTableName).Where("id = ?", id).Update(minfo)
	if err != nil {
		sess.Rollback()
		return err
	}

	sess.Commit()
	return nil
}
