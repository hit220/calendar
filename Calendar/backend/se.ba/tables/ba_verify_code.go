package tables

import (
	"errors"
	"time"

	"seasonalcal.life/rechy/lib"
)

const (
	MaxSmsVerifyCodeExpire = time.Second * 300
)

type BaVerifyCode struct {
	ID         int64     `json:"id" xorm:"pk autoincr 'id'"`
	Type       string    `json:"type" xorm:"char(20) notnull default('') 'type'"`
	Module     string    `json:"module" xorm:"char(64) notnull default('') 'module'"`
	Account    string    `json:"account" xorm:"char(64) notnull default('') 'account'"`
	Code       string    `json:"code" xorm:"char(64) notnull default('') 'code'"`
	ExpireTime time.Time `json:"expire_time" xorm:"notnull default('0000-00-00 00:00:00') 'expire_time'"`
}

func init() {
	tables = append(tables, new(BaVerifyCode))
}

func BaVerifyCodeAdd(uType, module, account string, runSendVerifyCode func(string, string) error) error {
	sess := _db.NewSession()

	err := sess.Begin()
	if err != nil {
		return err
	}

	code := lib.KindRand(6, lib.KC_RAND_KIND_NUM)
	bean := new(BaVerifyCode)
	_, err = sess.Where("account = ? and code = ?", account, code).Delete(bean)
	if err != nil {
		sess.Rollback()
		return err
	}

	bean.Account = account
	bean.Code = code
	bean.Type = uType
	bean.Module = module
	bean.ExpireTime = time.Now().Add(MaxSmsVerifyCodeExpire)
	_, err = sess.Insert(bean)
	if err != nil {
		sess.Rollback()
		return err
	}

	if bean.ID < 1 {
		sess.Rollback()
		return errors.New("verify code insert fail")
	}

	sess.Commit()

	err = runSendVerifyCode(account, code)
	if err != nil {
		// sess.Rollback()
		return err
	}

	return nil
}

func BaVerifyCodeVerify(uType, account, code string) bool {
	if len(account) < 4 || len(code) < 4 {
		return false
	}
	c, err := _db.Where("type = ? and account = ? and code = ? and expire_time > ? ",
		uType, account, code, time.Now()).Count(new(BaVerifyCode))
	if err != nil {
		return false
	}

	return c > 0
}
