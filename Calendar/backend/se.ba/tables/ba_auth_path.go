package tables

import (
	"strconv"
	"time"

	"seasonalcal.life/rechy/lib"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   权限(路径集合)
 *   -- 平台所有模块和服务的权限路径集合(缓存所有路径, 并定时更新)
 *
 **********************************************/

// BaAppTableName is 表名
const BaAuthPathTableName = "ba_auth_path"

// BaAuthPath is
type BaAuthPath struct {
	ID             int64     `json:"id" xorm:"pk autoincr 'id'"`
	Name           string    `json:"name" xorm:"varchar(100) notnull default('') 'name'"`
	Module         string    `json:"module" xorm:"char(64) notnull default('') 'module'"`
	Path           string    `json:"path" xorm:"varchar(255) notnull default('') 'path'"`
	Methods        string    `json:"methods" xorm:"varchar(100) notnull default('') 'methods'"`
	UpdatedAt      time.Time `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int       `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
}

func init() {
	tables = append(tables, new(BaAuthPath))
}

func BaAuthPathByID(id int64) (*BaAuthPath, bool, error) {
	re := new(BaAuthPath)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

func BaAuthPathList(currentPage, pageSize int, kw string) ([]*BaAuthPath, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("module LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(BaAuthPath))
	if err != nil {
		return nil, 0, err
	}

	var res []*BaAuthPath
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func BaAuthPathAddOrUpdate(
	reqID,
	reqName,
	reqModule,
	reqPath,
	reqMethods string) (isAdd bool, offwOrID int64, err error) {

	if reqID == "" || reqID == "0" {
		isAdd = true
	}

	info := new(BaAuthPath)
	info.Name = reqName
	info.Module = reqModule
	info.Path = reqPath
	info.Methods = reqMethods

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = now
	info.UpdatedAtIdxYM = idxTM

	if !isAdd {

		minfo := map[string]interface{}{
			"name":              info.Name,
			"module":            info.Module,
			"path":              info.Path,
			"methods":           info.Methods,
			"updated_at":        info.UpdatedAt,
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
		}

		offwOrID, err = _db.Table(BaAuthPathTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	offwOrID, err = _db.Insert(info)
	return isAdd, info.ID, err
}

func BaAuthPathClearAll() error {
	_, err := _db.Exec("DELETE FROM " + BaAuthPathTableName + " WHERE id > 0")
	return err
}
