package api

import (
	"errors"

	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
)

var gmw *jwt.GinJWTMiddleware

//
func SetJwtAuthMiddleware(mw *jwt.GinJWTMiddleware) {
	gmw = mw
}

//
func GetJwtAuthMiddleware() *jwt.GinJWTMiddleware {
	return gmw
}

//
func GetLoginTokenByID(module string, tp baapi.Type, id int64, applyToken string, device string) (*baapi.ModAuthor, string, error) {
	switch tp {
	case baapi.TypeMaster:
		user, err := MasterLogin(id, "", "", false, tp, applyToken, device)
		if err != nil {
			return nil, "", err
		}
		tokenString, _, err := gmw.TokenGenerator(user, module, "master")
		if err != nil {
			return nil, "", err
		}
		return user, tokenString, nil
	case baapi.TypeAgent:
		user, err := AgentLogin(id, "", "", false, tp, applyToken, device)
		if err != nil {
			return nil, "", err
		}
		tokenString, _, err := gmw.TokenGenerator(user, module, "agent")
		if err != nil {
			return nil, "", err
		}
		return user, tokenString, nil
	case baapi.TypeEnterprise:
		user, err := EnterpriseLogin(id, "", "", false, tp, applyToken, device)
		if err != nil {
			return nil, "", err
		}
		tokenString, _, err := gmw.TokenGenerator(user, module, "enterprise")
		if err != nil {
			return nil, "", err
		}
		return user, tokenString, nil
	case baapi.TypeDeveloper:
		user, err := DeveloperLogin(id, "", "", false, tp, applyToken, device)
		if err != nil {
			return nil, "", err
		}
		tokenString, _, err := gmw.TokenGenerator(user, module, "developer")
		if err != nil {
			return nil, "", err
		}
		return user, tokenString, nil
	case baapi.TypeUser:
		user, err := UserLogin(id, "", "", false, tp, applyToken, device)
		if err != nil {
			return nil, "", err
		}
		tokenString, _, err := gmw.TokenGenerator(user, module, "user")
		if err != nil {
			return nil, "", err
		}
		return user, tokenString, nil
	default:
	}
	return nil, "", errors.New("not support")
}
