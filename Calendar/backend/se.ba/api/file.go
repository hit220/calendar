package api

import (
	"io/ioutil"
	"net/url"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/o1egl/govatar"
	guuid "github.com/satori/go.uuid"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

const (
	DefaultMultipartMemory = 32 * 1024 * 1024 // 32MB
	MaxFileSize            = 80 * 1024 * 1024 // 80MB
)

// @Summary 访问文件
// @Produce  json
// @Param uuid path string true "UUID"
// @Success 200 {string} string "file bytes"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/file/{uuid} [get]
func FileHandler(c *gin.Context) {
	uuid := c.Param("uuid")
	if uuid == "" {
		baapi.JSONReply(c, "error", "not found")
		return
	}

	filepath := filepath.Join(utils.OpenUploadDir(), uuid, uuid)
	c.File(filepath)
}

// @Summary 文件上传 - 开放(不需要登录)
// @Accept  multipart/form-data
// @Produce  json
// @Param   file formData file true  "文件上传"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/upload [post]
func OpenFileUploadHandler(c *gin.Context) {

	err := c.Request.ParseMultipartForm(DefaultMultipartMemory)
	form := c.Request.MultipartForm
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	files := form.File["file"]
	if len(files) < 1 {
		baapi.JSONReply(c, "error", "上传失败, 未接收到任何文件")
		return
	}

	if files[0].Size > MaxFileSize {
		baapi.JSONReply(c, "error", "上传失败, 您的存储空间不足")
		return
	}

	// 文件唯一id
	uuid := guuid.NewV4().String()
	ext := utils.FileExt(files[0].Filename)
	uuid = "ba-" + uuid + strings.ToLower(ext)

	// 添加到数据库
	err = tables.BaFileAdd(
		"",
		0,
		files[0].Size,
		uuid, files[0].Filename,
		ext,
		files[0].Header.Get("Content-Type"))
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	// 保存文件
	uploadDir := utils.OpenUploadDir()
	// {baseDir}/{userId}/{uuid}
	filename := lib.EnsureFilePath(filepath.Join(uploadDir, uuid, uuid))
	err = c.SaveUploadedFile(files[0], filename)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	formValues := url.Values(form.Value)
	name := formValues.Get("name")
	if name != "" {
		name = lib.ToFilename(name) + ".txt"
	} else {
		name = files[0].Filename + ".txt"
	}

	filenameInfo := lib.EnsureFilePath(filepath.Join(uploadDir,
		uuid, name))
	err = ioutil.WriteFile(filenameInfo,
		[]byte(lib.JsonEncodeIndent(lib.MapHttpMap(formValues))),
		0777)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"uuid":         uuid,
		"ext":          ext,
		"size":         files[0].Size,
		"filename":     files[0].Filename,
		"content_type": files[0].Header.Get("Content-Type"),
	})
}

func HeadImage(c *gin.Context) {
	var req struct {
		Type baapi.Type `form:"type" json:"type" binding:"required"`
		UID  string     `form:"uid" json:"uid" binding:"-"`
		UUID string     `form:"uuid" json:"uuid" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if req.UID == "" && req.UUID == "" {
		baapi.JSONReply(c, "error", "not found", nil)
		return
	}

	key := req.UID
	if key == "" {
		key = req.UUID
	}

	p := filepath.Join(utils.HeadImageDir(), string(req.Type), key, "head_image.jpg")
	if lib.IsExists(p) {
		c.File(p)
		return
	}

	img := ""
	var uid int64
	var uuid string
	switch req.Type {
	case baapi.TypeAgent:
		r, ok, _ := tables.BaAgentByKey(key)
		if ok {
			img = r.HeadImage
			uid = r.ID
			uuid = r.UUID
		}
	case baapi.TypeDeveloper:
		r, ok, _ := tables.BaDeveloperByKey(key)
		if ok {
			img = r.HeadImage
			uid = r.ID
			uuid = r.UUID
		}
	case baapi.TypeEnterprise:
		r, ok, _ := tables.BaEnterpriseByKey(key)
		if ok {
			img = r.HeadImage
			uid = r.ID
			uuid = r.UUID
		}
	case baapi.TypeMaster:
		r, ok, _ := tables.BaMasterByKey(key)
		if ok {
			img = r.HeadImage
			uid = r.ID
			uuid = r.UUID
		}
	case baapi.TypeUser:
		r, ok, _ := tables.BaUserByKey(key)
		if ok {
			img = r.HeadImage
			uid = r.ID
			uuid = r.UUID
		}
	default:
		baapi.JSONReply(c, "error", "not found", nil)
		return
	}

	if img != "" {
		imgPath := filepath.Join(utils.OpenUploadDir(), img, img)
		if lib.IsExists(imgPath) {
			p1 := filepath.Join(utils.HeadImageDir(), string(req.Type), lib.ValueToString(uid, "0"), "head_image.jpg")
			p2 := filepath.Join(utils.HeadImageDir(), string(req.Type), uuid, "head_image.jpg")

			p1 = lib.EnsureFilePath(p1)
			p2 = lib.EnsureFilePath(p2)

			lib.CopyFile(imgPath, p1)
			lib.CopyFile(imgPath, p2)
		}
	}

	p = lib.EnsureFilePath(p)
	if lib.IsExists(p) {
		c.File(p)
		return
	}

	err := govatar.GenerateFile(govatar.MALE, p)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	c.File(p)
}
