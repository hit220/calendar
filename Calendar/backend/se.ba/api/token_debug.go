package api

import (
	"fmt"
	"strings"

	"github.com/dgrijalva/jwt-go"
	jwtV2 "seasonalcal.life/rechy/se.ba.api/jwt-v2"
)

func PrintlnToken(tokenString string) {
	privateKeyStr := "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA2/R3BBmdpt2bYbSYN/dpfPm89RBvM5KAnxs4YBa6M1tRuJd2\nQxy1/M0PJS4G8h20GAzFFRuPTkb15LssLYdD1r1Mk9IRYX/DsNwC32C0coXD0ttQ\nIiDLRGH95O07s0LwgPCDNOi+eHotoOVOoORghSj8k8QAIJj6cHggDozy+C3g4EFI\nv2lvIyMKADqiYgKooVJkgeiWairLXSfY2rvSjQKXhwLKNFLoI+Q0Kkp0tzRZwzqf\nqvce+AFIBIQ2iw2Vr9q+LwwEwhDbiRnVdQcQijOsgx3ndl4u8M+CwaD1WlP0h2Cl\n1TFfqHxJv8U5z+dPpx9urXnSrT625lej7/MPrQIDAQABAoIBAHJD16hbnljXeycQ\n6Jr6fuSb4Jv3A63pB8SgIXNZmzWe1R9uH9PRkIFaDeoktRsGb06nI6NJg/MITimb\nSuIECSv/f06zrK/X7dNtGFjvJg7wisB5inGQU8icl4db7s27krGJQMA1wO1Zm2ag\neW5/9dx3ZG9nPSGyFzhiT1T/yCFAzZN3aZI14O7DMP+6a7VThF5KaDo68gCsX8BA\nKirUf4ZZyPNticzFaUTMsvaZLZejkohIoQ9g3Ff7R/JU35ixduzQqzeryajpKi8E\ns0xmWOPOgBtGeOaYHcdjPo6sajbzV2ByltuXp1bflCweESMm9sbwrTq63yCCkz85\nvxBB7sECgYEA8+zGllU2kpiuZi7R6meLlV3bfB6L4FW5pbnZwlcIvzvBwq3fpiTK\n/SzNHRRgB/vkaX297GBddgPZX8nrDb234YST0k2p3BfPwoSnnfRGIjrXtxq9ENCJ\nuTbvIZEZSJ3Sus0xN1FctslLqMbjwocZWHnE0dsT8UUqTBnuGcHF2CUCgYEA5tfr\n3mPi+8k103P23djgAEGfu7vuEkibjWYRqQMGeedMHZSHLsAtxuLkzaSZiAU3tdU/\nCFGjitciayv9YFeqbvW6YvexZtBXLlcuZBGXhTCcMEj5PLDcSseouP/2oxJFxqOw\nHwUsPJsalu3D91AVGRrM8ugVK4kugsGbb21JHukCgYEA4Gc/+yXB4vPb3d3M+Jzx\n6SRDu9hOTsAO3swv7X4saeILy5IyYqMOrAP/X3VTM4UlCXfItiGhWeUIofsc57NV\n638QjzUrw3OUixuHKOWFcBA6TBWpQkLXsTE8peDbJdcr+vazBHUxmmdt14PCHUA0\nrRS5YWbu3dQHqaNhTQjbfpECgYEAlU/YOrJ1INMWw7zgUsJysPfBb9awuJbKNn1s\nsttIFxVqgD+P6MdZVKlxwSnUPMzTKgPSj3juxWE93ABKFUTe8oJymo0H7C6aFVlC\nomtWjEnS3NB2lbD37E6UbYTybKvnAgLiak4YPOM+IZ2NAayE2QYixoMjHlPPB7he\n54XSo/ECgYB3pHqCRmr538q2FLbWQ5TzwnVCTJAzZHrqqbk1RD4zGxE3lzPPszhb\n1l9gGO7PYELU6QV4Xb7HzWEVeCKM2owCB1RJ9Q9ghuQ5H6fU0E/xOExwd+JXeoXq\npX6s56RRHWWRK2rIPB1sl6une5grGcdvwf0ED2BL+W6/4lPy0LM5ww==\n-----END RSA PRIVATE KEY-----\n"
	publicKeyStr := "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2/R3BBmdpt2bYbSYN/dp\nfPm89RBvM5KAnxs4YBa6M1tRuJd2Qxy1/M0PJS4G8h20GAzFFRuPTkb15LssLYdD\n1r1Mk9IRYX/DsNwC32C0coXD0ttQIiDLRGH95O07s0LwgPCDNOi+eHotoOVOoORg\nhSj8k8QAIJj6cHggDozy+C3g4EFIv2lvIyMKADqiYgKooVJkgeiWairLXSfY2rvS\njQKXhwLKNFLoI+Q0Kkp0tzRZwzqfqvce+AFIBIQ2iw2Vr9q+LwwEwhDbiRnVdQcQ\nijOsgx3ndl4u8M+CwaD1WlP0h2Cl1TFfqHxJv8U5z+dPpx9urXnSrT625lej7/MP\nrQIDAQAB\n-----END PUBLIC KEY-----\n"

	privateKey, err := jwtV2.ParseRSAPrivateKeyFromPEM([]byte(privateKeyStr))
	if err != nil {
		fmt.Println(err)
		return
	}
	publicKey, err := jwtV2.ParseRSAPublicKeyFromPEM([]byte(publicKeyStr))
	if err != nil {
		fmt.Println(err)
		return
	}

	// 验签
	tokenResult, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	fmt.Println("自验", err, tokenResult != nil)

	claimsNew := jwt.MapClaims{}
	tokenPart, parts, err := new(jwt.Parser).ParseUnverified(tokenString, claimsNew)
	if err != nil {
		fmt.Println(err)
		return
	}
	tokenPart.Signature = parts[2]
	signOrg, err := jwt.SigningMethodRS256.Sign(strings.Join(parts[0:2], "."), privateKey)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(strings.Join(parts[0:2], "."))

	return

	fmt.Println(tokenPart.Signature)
	fmt.Println(signOrg)
}
