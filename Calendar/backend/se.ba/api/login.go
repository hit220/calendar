package api

import (
	"errors"
	"net/url"
	"time"

	"github.com/chanxuehong/wechat/mp/core"
	mpoauth2 "github.com/chanxuehong/wechat/mp/oauth2"
	"github.com/chanxuehong/wechat/mp/user"
	"github.com/chanxuehong/wechat/oauth2"
	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

const (
	TokenExpireUser       = time.Hour * 24 * 30 // x天过期
	TokenExpireMaster     = time.Hour * 3       //
	TokenExpireAgent      = time.Hour * 24 * 7  //
	TokenExpireDeveloper  = time.Hour * 24 * 3  //
	TokenExpireEnterprise = time.Hour * 24 * 15 //
)

type loginRequest struct {
	Type       baapi.Type `form:"type" json:"type" binding:"required"`
	Module     string     `form:"module" json:"module" binding:"required"`
	Account    string     `form:"account" json:"account" binding:"-"`
	Password   string     `form:"password" json:"password" binding:"-"`
	Phone      string     `form:"phone" json:"phone" binding:"-"`
	Code       string     `form:"code" json:"code" binding:"-"`
	ApplyToken string     `form:"apply_token" json:"apply_token" binding:"-"`
	Device     string     `form:"device" json:"device" binding:"-"`
}

// @Summary 用户登录
// @Accept  json
// @Produce  json
// @Param account query string true "账号/邮箱/密码"
// @Param password query string true "密码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/login [post]
func Login(c *gin.Context) (interface{}, time.Duration, error) {

	var req *loginRequest
	if err := c.ShouldBind(&req); err != nil {
		return "", 0, jwt.ErrMissingLoginValues
	}

	if req.Account != "" && req.Password != "" {
		// 账号密码登录
		// ....

		switch req.Type {
		case baapi.TypeMaster:
			user, err := MasterLogin(0, req.Account, req.Password, true,
				req.Type, req.ApplyToken, req.Device)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireMaster, err
		case baapi.TypeAgent:
			user, err := AgentLogin(0, req.Account, req.Password, true,
				req.Type, req.ApplyToken, req.Device)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireAgent, err
		case baapi.TypeEnterprise:
			user, err := EnterpriseLogin(0, req.Account, req.Password, true,
				req.Type, req.ApplyToken, req.Device)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireEnterprise, err
		case baapi.TypeDeveloper:
			user, err := DeveloperLogin(0, req.Account, req.Password, true,
				req.Type, req.ApplyToken, req.Device)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireDeveloper, err
		case baapi.TypeUser:
			user, err := UserLogin(0, req.Account, req.Password, true,
				req.Type, req.ApplyToken, req.Device)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireUser, err
		default:
		}
	} else if req.Phone != "" && req.Code != "" {
		// 手机号 + 验证码登录
		// ...

		if !utils.IsPhone(req.Phone) {
			return nil, 0, errors.New("need input phone number")
		}

		verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Phone, req.Code)
		if !verifySuccess {
			return nil, 0, errors.New("verify fail")
		}

		switch req.Type {
		case baapi.TypeMaster:
			one, err := tables.BaMasterByPhone(req.Phone, true)
			if err != nil {
				return nil, 0, err
			}

			paths := ""
			if one.RID > 0 {
				role, has, err := tables.BaRoleByID(one.RID)
				if err != nil {
					return nil, 0, err
				}
				if has {
					paths = role.AuthPaths
				}
			}

			user := one.ToModAuthor(req.ApplyToken, req.Device, paths)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireMaster, err
		case baapi.TypeAgent:
			one, err := tables.BaAgentByPhone(req.Phone, true)
			if err != nil {
				return nil, 0, err
			}

			paths := ""
			if one.RID > 0 {
				role, has, err := tables.BaRoleByID(one.RID)
				if err != nil {
					return nil, 0, err
				}
				if has {
					paths = role.AuthPaths
				}
			}

			user := one.ToModAuthor(req.ApplyToken, req.Device, paths)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireAgent, err
		case baapi.TypeEnterprise:
			one, err := tables.BaEnterpriseByPhone(req.Phone, true)
			if err != nil {
				return nil, 0, err
			}

			paths := ""
			if one.RID > 0 {
				role, has, err := tables.BaRoleByID(one.RID)
				if err != nil {
					return nil, 0, err
				}
				if has {
					paths = role.AuthPaths
				}
			}

			user := one.ToModAuthor(req.ApplyToken, req.Device, paths)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireEnterprise, err
		case baapi.TypeDeveloper:
			one, err := tables.BaDeveloperByPhone(req.Phone, true)
			if err != nil {
				return nil, 0, err
			}

			paths := ""
			if one.RID > 0 {
				role, has, err := tables.BaRoleByID(one.RID)
				if err != nil {
					return nil, 0, err
				}
				if has {
					paths = role.AuthPaths
				}
			}

			user := one.ToModAuthor(req.ApplyToken, req.Device, paths)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireDeveloper, err
		case baapi.TypeUser:
			one, err := tables.BaUserByPhone(req.Phone, true)
			if err != nil {
				return nil, 0, err
			}

			paths := ""
			if one.RID > 0 {
				role, has, err := tables.BaRoleByID(one.RID)
				if err != nil {
					return nil, 0, err
				}
				if has {
					paths = role.AuthPaths
				}
			}

			user := one.ToModAuthor(req.ApplyToken, req.Device, paths)
			c.Set("user", user)
			c.Set("JWT_AUD", req.Module)
			return user, TokenExpireUser, err
		default:
		}
	} else {
		return nil, 0, errors.New("please input valid info")
	}
	return nil, 0, errors.New("not support")
}

// @Summary 退出登录
// @Accept  json
// @Produce  json
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/logout [get]
func Logout(c *gin.Context) {}

// 微信公众号登陆
func LoginByWxPub1(c *gin.Context) {
	var req struct {
		Type       baapi.Type `form:"type" json:"type" binding:"required"`
		Module     string     `form:"module" json:"module" binding:"required"`
		ApplyToken string     `form:"apply_token" json:"apply_token" binding:"-"`
		Device     string     `form:"device" json:"device" binding:"-"`
		RequestURL string     `json:"request_url" form:"request_url" binding:"required"`
		ReturnURL  string     `json:"return_url" form:"return_url" binding:"required"`
	}

	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	wxAppID := cfg.String("login.wx.app_id", "")
	oauth2Scope := cfg.String("login.wx.scope", "")

	rr, err := url.Parse(req.ReturnURL)
	if err != nil {
		baapi.JSONReply(c, "error", err)
	}
	qr, err := url.ParseQuery(rr.RawQuery)
	if err != nil {
		baapi.JSONReply(c, "error", err)
	}
	qr.Set("type", string(req.Type))
	qr.Set("module", req.Module)
	qr.Set("apply_token", req.ApplyToken)
	qr.Set("device", req.Device)
	qr.Set("action", "login_wx_pub")
	qr.Set("request_url", req.RequestURL)
	rr.RawQuery = qr.Encode()

	authCodeURL := mpoauth2.AuthCodeURL(wxAppID, rr.String(), oauth2Scope, lib.KindRand(20, lib.KC_RAND_KIND_ALL))
	baapi.JSONReply(c, "success", "成功", authCodeURL)
}

// 微信公众号登陆
func LoginByWxPub2(c *gin.Context) {
	var req struct {
		Type       baapi.Type `form:"type" json:"type" binding:"required"`
		Module     string     `form:"module" json:"module" binding:"required"`
		ApplyToken string     `form:"apply_token" json:"apply_token" binding:"-"`
		Device     string     `form:"device" json:"device" binding:"-"`
		Code       string     `json:"code" form:"code" binding:"required"`
	}

	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", baapi.WrapMessage("Invalid parameter: ", err))
		return
	}

	if req.Code == "" {
		baapi.JSONReply(c, "error", "登陆失败, 用户禁止授权")
		return
	}

	wxAppID := cfg.String("login.wx.app_id", "")
	wxAppSecret := cfg.String("login.wx.secret", "")
	oauth2Endpoint := mpoauth2.NewEndpoint(wxAppID, wxAppSecret)
	oauth2Client := oauth2.Client{Endpoint: oauth2Endpoint}

	token, err := oauth2Client.ExchangeToken(req.Code)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	if token.OpenId == "" {
		baapi.JSONReply(c, "error", "请先授权登陆")
		return
	}

	accessTokenServer := core.NewDefaultAccessTokenServer(wxAppID, wxAppSecret, nil)
	wechatClient := core.NewClient(accessTokenServer, nil)

	// 高级获取信息
	var openID string
	var nickname string
	var headImage string
	var extInfo interface{}
	userinfo, err := user.Get(wechatClient, token.OpenId, "zh_CN")
	if err != nil {
		// 基本信息获取
		userinfo2, err := mpoauth2.GetUserInfo(token.AccessToken, token.OpenId, "", nil)
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}

		openID = "wx:" + token.OpenId
		nickname = userinfo2.Nickname
		headImage = userinfo2.HeadImageURL
		extInfo = userinfo2
	} else {
		openID = "wx:" + token.OpenId
		nickname = userinfo.Nickname
		headImage = userinfo.HeadImageURL
		extInfo = userinfo
	}

	needSubscriber := cfg.Bool("login.wx.need_subscriber", false)
	if needSubscriber && (userinfo == nil || userinfo.IsSubscriber == 0) {
		baapi.JSONReply(c, "guanzhu", "请关注公众号'"+cfg.String("login.wx.name", "<公众号名>")+"'")
		return
	}

	var id int64
	switch req.Type {
	case "agent":
		id, err = tables.BaAgentAddOrUpdateByOpenID(openID, nickname, headImage, extInfo)
	case "developer":
		id, err = tables.BaDeveloperAddOrUpdateByOpenID(openID, nickname, headImage, extInfo)
	case "enterprise":
		id, err = tables.BaEnterpriseAddOrUpdateByOpenID(openID, nickname, headImage, extInfo)
	case "user":
		id, err = tables.BaUserAddOrUpdateByOpenID(openID, nickname, headImage, extInfo)
	case "master":
		id, err = tables.BaMasterAddOrUpdateByOpenID(openID, nickname, headImage, extInfo)
	default:
	}
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	user, tokenString, err := GetLoginTokenByID(req.Module, req.Type, id, req.ApplyToken, req.Device)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	data := gin.H{
		"user":  user,
		"token": tokenString,
	}

	baapi.JSONReply(c, "success", "成功", data)

}
