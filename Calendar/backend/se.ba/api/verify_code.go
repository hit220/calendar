package api

import (
	"github.com/gin-gonic/gin"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
	"seasonalcal.life/rechy/se.ba/utils/email"
	"seasonalcal.life/rechy/se.ba/utils/sms"
)

// @Summary 手机验证码
// @Accept  json
// @Produce  json
// @Param phone query string true "手机号"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/verify/code/phone [get]
func SendVerifyCodeByPhone(c *gin.Context) {

	var req struct {
		Type   baapi.Type `form:"type" json:"type" binding:"required"`
		Module string     `form:"module" json:"module" binding:"-"`
		Device string     `form:"device" json:"device" binding:"-"`
		Phone  string     `form:"phone" json:"phone" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !utils.IsPhone(req.Phone) {
		baapi.JSONReply(c, "error", "need input phone number", nil)
		return
	}

	err := tables.BaVerifyCodeAdd(string(req.Type), req.Module, req.Phone, sms.SendVerifyCode)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "发送成功")
}

// @Summary 邮箱验证码
// @Accept  json
// @Produce  json
// @Param email query string true "邮箱"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/verify/code/email [get]
func SendVerifyCodeByEmail(c *gin.Context) {

	var req struct {
		Type   baapi.Type `form:"type" json:"type" binding:"required"`
		Module string     `form:"module" json:"module" binding:"-"`
		Device string     `form:"device" json:"device" binding:"-"`
		Email  string     `form:"email" json:"email" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !utils.IsEmail(req.Email) {
		baapi.JSONReply(c, "error", "need input valid email account", nil)
		return
	}

	err := tables.BaVerifyCodeAdd(string(req.Type), req.Module, req.Email, email.SendVerifyCode)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "发送成功")
}
