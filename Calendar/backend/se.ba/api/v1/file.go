package v1

import (
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	guuid "github.com/satori/go.uuid"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

const (
	DefaultMultipartMemory = 32 * 1024 * 1024 // 32MB
	MaxFileSize            = 80 * 1024 * 1024 // 80MB
)

// @Summary 文件上传
// @Accept  multipart/form-data
// @Produce  json
// @Param   file formData file true  "文件上传"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/v1/upload [post]
func FileUploadHandler(c *gin.Context) {
	userType, userID := baapi.GetModAuthorID(c)
	if userType == "" || userID < 1 {
		baapi.JSONReply(c, "error", "未知用户无权操作")
		return
	}

	err := c.Request.ParseMultipartForm(DefaultMultipartMemory)
	form := c.Request.MultipartForm
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	files := form.File["file"]
	if len(files) < 1 {
		baapi.JSONReply(c, "error", "上传失败, 未接收到任何文件")
		return
	}

	if files[0].Size > MaxFileSize {
		baapi.JSONReply(c, "error", "上传失败, 您的存储空间不足")
		return
	}

	// 文件唯一id
	uuid := guuid.NewV4().String()
	ext := utils.FileExt(files[0].Filename)
	uuid = uuid + strings.ToLower(ext)

	// 添加到数据库
	err = tables.BaFileAdd(
		string(userType),
		userID,
		files[0].Size,
		uuid, files[0].Filename,
		ext,
		files[0].Header.Get("Content-Type"))
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	// 保存文件
	uploadDir := utils.OpenUploadDir()
	// {baseDir}/{userId}/{uuid}
	filename := lib.EnsureFilePath(filepath.Join(uploadDir, uuid))
	err = c.SaveUploadedFile(files[0], filename)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"uuid":         uuid,
		"ext":          ext,
		"size":         files[0].Size,
		"filename":     files[0].Filename,
		"content_type": files[0].Header.Get("Content-Type"),
	})
}

// @Summary 文件分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/file/list [get]
func FileListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw  string `form:"kw" json:"kw" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.BaFileList(pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 文件详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/file/detail [get]
func FileDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.BaFileByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "成功", one)
}
