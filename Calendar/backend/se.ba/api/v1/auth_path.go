package v1

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/keys"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/ws"
)

var auther = &authUpgrader{}

type authUpgrader struct {
	sync.Mutex
	isRunning bool
}

func (au *authUpgrader) Recv(message []byte) {}
func (au *authUpgrader) Run() {
	au.Lock()
	if au.isRunning {
		au.Unlock()
		return
	}
	au.isRunning = true
	au.Unlock()

	defer func() {
		au.Lock()
		au.isRunning = false
		au.Unlock()

		ws.WSManager.Broadcast("update-over", []byte("更新结束"))
		time.Sleep(time.Second) // 延迟退出, 等待消息发出
	}()

	apps, _, err := tables.BaAppList(1, 10000000, "")
	if err != nil {
		ws.WSManager.Broadcast("update-log", []byte(err.Error()))
		return
	}

	err = tables.BaAuthPathClearAll()
	if err != nil {
		ws.WSManager.Broadcast("update-log", []byte(err.Error()))
		return
	}

	for _, app := range apps {
		dataURL := lib.UrlJoin(app.DataURL, baapi.RoutePubInfo)
		_, resp, _, _, err := lib.HttpRequest(nil, dataURL, "GET", nil, nil, nil, false,
			time.Second*10, time.Second*5)
		if err != nil {
			ws.WSManager.Broadcast("update-log", []byte(err.Error()))
			continue
		}
		if len(resp) < 1 {
			ws.WSManager.Broadcast("update-log", []byte("response is empty"))
			continue
		}

		br := &baapi.ResponseMessage{Data: &baapi.ModInfo{}}
		err = json.Unmarshal(resp, &br)
		if err != nil {
			ws.WSManager.Broadcast("update-log", []byte(err.Error()))
			continue
		}

		kmgr := keys.GetKeyManager()
		b := br.Data.(*baapi.ModInfo)
		err = kmgr.Verify(true, b.Module, b.Module+"&"+b.Routers, b.Sign)
		if err != nil {
			ws.WSManager.Broadcast("update-log", []byte(err.Error()))
			continue
		}

		var as url.Values
		err = json.Unmarshal([]byte(b.Routers), &as)
		if err != nil {
			ws.WSManager.Broadcast("update-log", []byte(err.Error()))
			continue
		}

		n := 0
		mp := map[string][]string(as)
		for path, vs := range mp {
			methods := strings.Join(vs, ",")

			_, _, err = tables.BaAuthPathAddOrUpdate("", app.Name,
				b.Module, path, methods)
			if err != nil {
				ws.WSManager.Broadcast("update-log", []byte(err.Error()))
				continue
			}

			n++
		}

		ws.WSManager.Broadcast("update-log", []byte(fmt.Sprintf("%v模块, 获取%d项权限路径", app.Name, n)))
		time.Sleep(time.Millisecond * 300)
	}

	/*
		n := 0
		for n < 1000 {
			ws.WSManager.Broadcast("update-log", []byte(fmt.Sprintf("计数: %d", n)))
			n++
			time.Sleep(time.Second)
		}
	*/
}

// @Summary 权限分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/auth_path/list [get]
func AuthPathListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw  string `form:"kw" json:"kw" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.BaAuthPathList(pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 权限详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/auth_path/detail [get]
func AuthPathDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.BaAuthPathByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "成功", one)
}

// @Summary 权限新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/auth_path/add_or_update [post]
func AuthPathAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID      string `form:"id" json:"id" binding:"-"`
		Name    string `form:"name" json:"name" binding:"-"`
		Module  string `form:"module" json:"module" binding:"required"`
		Path    string `form:"path" json:"path" binding:"required"`
		Methods string `form:"methods" json:"methods" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	isAdd, offwOrID, err := tables.BaAuthPathAddOrUpdate(
		req.ID,
		req.Name,
		req.Module,
		req.Path,
		req.Methods)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}

// websocket
func AuthPathWSSyncRouters(c *gin.Context) {
	userType, userID := baapi.GetUserID(c)

	fmt.Println("-------11111111111111111-----")
	// change the reqest to websocket model
	conn, err := (&websocket.
		Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).
		Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		http.NotFound(c.Writer, c.Request)
		return
	}

	// websocket connect
	id := fmt.Sprintf("%s-%d", userType, userID)
	client := ws.NewWSClient(id, conn)
	ws.WSManager.Register <- client

	// 开始即时更新, 所有app的权限路径列表
	go auther.Run()
}
