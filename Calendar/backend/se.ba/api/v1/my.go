package v1

import (
	"github.com/gin-gonic/gin"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 个人中心信息
// @Accept  json
// @Produce  json
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/my/info [get]
func MyInfoHandler(c *gin.Context) {

	baapi.JSONReply(c, "success", gin.H{})
}
