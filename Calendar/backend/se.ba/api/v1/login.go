package v1

import (
	"errors"
	"net/url"

	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
	"seasonalcal.life/rechy/se.ba/api"
	"seasonalcal.life/rechy/se.ba/tables"
)

var gmw *jwt.GinJWTMiddleware

func SetJwtAuthMiddleware(mw *jwt.GinJWTMiddleware) {
	gmw = mw
}

func loginTo(uType baapi.Type, userID int64, app *tables.BaApp) (string, error) {
	randToken := baapi.ApplyToken()

	switch uType {
	case baapi.TypeMaster:
		if app.VisitURLMaster == "" {
			return "", errors.New("no visit url")
		}

		toURL, err := url.Parse(app.VisitURLMaster)
		if err != nil {
			return "", err
		}

		user, err := api.MasterLogin(userID, "", "", false,
			uType, randToken, "ba")
		if err != nil {
			return "", err
		}

		tokenString, _, err := gmw.TokenGenerator(user, app.Module, "master")
		if err != nil {
			return "", err
		}
		query := toURL.Query()
		query.Set("user", lib.JsonEncode(user))
		query.Set("token", tokenString)
		toURL.RawQuery = query.Encode()
		return toURL.String(), nil
	case baapi.TypeAgent:
		if app.VisitURLAgent == "" {
			return "", errors.New("no visit url")
		}

		toURL, err := url.Parse(app.VisitURLAgent)
		if err != nil {
			return "", err
		}
		user, err := api.AgentLogin(userID, "", "", false,
			uType, randToken, "ba")
		if err != nil {
			return "", err
		}

		tokenString, _, err := gmw.TokenGenerator(user, app.Module, "agent")
		if err != nil {
			return "", err
		}
		query := toURL.Query()
		query.Set("user", lib.JsonEncode(user))
		query.Set("token", tokenString)
		toURL.RawQuery = query.Encode()
		return toURL.String(), nil
	case baapi.TypeEnterprise:
		if app.VisitURLEnterprise == "" {
			return "", errors.New("no visit url")
		}

		toURL, err := url.Parse(app.VisitURLEnterprise)
		if err != nil {
			return "", err
		}
		user, err := api.EnterpriseLogin(userID, "", "", false,
			uType, randToken, "ba")
		if err != nil {
			return "", err
		}

		tokenString, _, err := gmw.TokenGenerator(user, app.Module, "enterprise")
		if err != nil {
			return "", err
		}
		query := toURL.Query()
		query.Set("user", lib.JsonEncode(user))
		query.Set("token", tokenString)
		toURL.RawQuery = query.Encode()
		return toURL.String(), nil
	case baapi.TypeDeveloper:
		if app.VisitURLDeveloper == "" {
			return "", errors.New("no visit url")
		}

		toURL, err := url.Parse(app.VisitURLDeveloper)
		if err != nil {
			return "", err
		}
		user, err := api.DeveloperLogin(userID, "", "", false,
			uType, randToken, "ba")
		if err != nil {
			return "", err
		}

		tokenString, _, err := gmw.TokenGenerator(user, app.Module, "developer")
		if err != nil {
			return "", err
		}
		query := toURL.Query()
		query.Set("user", lib.JsonEncode(user))
		query.Set("token", tokenString)
		toURL.RawQuery = query.Encode()
		return toURL.String(), nil
	case baapi.TypeUser:
		if app.VisitURLUser == "" {
			return "", errors.New("no visit url")
		}

		toURL, err := url.Parse(app.VisitURLUser)
		if err != nil {
			return "", err
		}
		user, err := api.UserLogin(userID, "", "", false,
			uType, randToken, "ba")
		if err != nil {
			return "", err
		}

		tokenString, _, err := gmw.TokenGenerator(user, app.Module, "user")
		if err != nil {
			return "", err
		}
		query := toURL.Query()
		query.Set("user", lib.JsonEncode(user))
		query.Set("token", tokenString)
		toURL.RawQuery = query.Encode()
		return toURL.String(), nil
	default:
	}
	return "", errors.New("not support")
}
