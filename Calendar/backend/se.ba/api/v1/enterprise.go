package v1

import (
	"errors"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"xorm.io/xorm"
)

// @Summary 企业详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/enterprise/detail [get]
func EnterpriseDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.BaEnterpriseByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "成功", one)
}

// @Summary 企业分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/enterprise/list [get]
func EnterpriseListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw  string `form:"kw" json:"kw" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.BaEnterpriseList(pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 企业新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/enterprise/add_or_update [post]
func EnterpriseAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID        string `form:"id" json:"id" binding:"-"`
		RID       string `form:"rid" json:"rid" binding:"-"`
		Name      string `form:"name" json:"name" binding:"required"`
		Username  string `form:"username" json:"username" binding:"-"`
		HeadImage string `form:"head_image" json:"head_image" binding:"-"`
		Password  string `form:"password" json:"password" binding:"-"`
		Email     string `form:"email" json:"email" binding:"-"`
		Phone     string `form:"phone" json:"phone" binding:"-"`
		ULevel    string `form:"u_level" json:"u_level" binding:"-"`
		Address   string `form:"address" json:"address" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	req.Password = tables.GenUserSha256(req.Password)

	isAdd, offwOrID, err := tables.BaEnterpriseAddOrUpdate(
		req.ID,
		lib.ValueToInt64(req.RID, 0),
		req.Username,
		req.Name,
		req.ULevel,
		req.HeadImage,
		req.Email,
		req.Phone,
		req.Address,
		req.Password)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}

// @Summary 企业模块开通
// @Accept  json
// @Produce  json
// @Param id query string false "ID"
// @Param app_id query string false "APP ID"
// @Param status query string true "是否开通"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/enterprise/open_module [post]
func EnterpriseOpenModuleHandler(c *gin.Context) {
	var req struct {
		ID     string `form:"id" json:"id" binding:"required"`
		AppID  string `form:"app_id" json:"app_id" binding:"required"`
		Status string `form:"status" json:"status" binding:"required"` // 1开通  0关闭
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	var b bool
	if req.Status == "1" {
		b = true
	} else if req.Status == "0" {
		b = false
	} else {
		baapi.JSONReply(c, "error", "status not equal '0' or '1'", nil)
		return
	}

	app, exists, err := tables.BaAppByID(lib.ValueToInt64(req.AppID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		baapi.JSONReply(c, "error", "no app", nil)
		return
	}

	if app.DataURL == "" {
		baapi.JSONReply(c, "error", "app data url is empty", nil)
		return
	}

	callback := func(one *tables.BaEnterprise, sess *xorm.Session) error {
		account := one.Phone
		if one.Email != "" {
			account = one.Email
		}
		if one.Username != "" {
			account = one.Username
		}
		mu := &baapi.ModUser{
			Type:      baapi.TypeEnterprise,
			UUID:      one.UUID,
			Account:   account,
			Name:      one.Name,
			Nickname:  one.Name,
			HeadImage: one.HeadImage,
			Phone:     one.Phone,
			ExtraInfo: baapi.DecodeExtInfo(one.ExtInfo, map[string]interface{}{
				"email":   one.Email,
				"phone":   one.Phone,
				"address": one.Address,
			}),
		}

		// 执行打开命令
		cmd := &baapi.Command{
			Data: mu.String(),
		}
		if b {
			cmd.Cmd = "open_module"
		} else {
			cmd.Cmd = "close_module"
		}

		cmdURL := lib.UrlJoin(app.DataURL, baapi.RoutePubCommand)
		resp, err := baapi.SendCommand(app.Module, cmdURL, cmd)
		if err != nil {
			return err
		}

		if resp.Code != "success" {
			return errors.New("cmd response is not success: " + resp.Message)
		}

		return nil
	}

	offw, err := tables.BaEnterpriseOpenMoudle(req.ID, req.AppID, b, callback)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"offw": offw,
	})
}
