package v1

import (
	"github.com/gin-gonic/gin"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary Hello
// @Accept  json
// @Produce  json
// @Param param1 query string false "参数1"
// @Param param2 query string false "参数2"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/v1/hello [get]
func HelloHandler(c *gin.Context) {
	var req struct {
		Param1 string `form:"param1" json:"param1" binding:"-"`
		Param2 string `form:"param2" json:"param2" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "hello world!")
}
