package v1

import (
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/keys"
	"seasonalcal.life/rechy/se.ba/tables"
)

// @Summary 应用分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/app/list [get]
func AppListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw  string `form:"kw" json:"kw" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.BaAppList(pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 应用详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/app/detail [get]
func AppDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.BaAppByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "成功", one)
}

// @Summary 应用新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Param module query string true "模块名称"
// @Param head_image query string true "头像"
// @Param data_url query string true "数据网址"
// @Param ips query string true "ip白名单"
// @Param description query string true "描述"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/app/add_or_update [post]
func AppAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID                 string `form:"id" json:"id" binding:"-"`
		Name               string `form:"name" json:"name" binding:"required"`
		Module             string `form:"module" json:"module" binding:"required"`
		HeadImage          string `form:"head_image" json:"head_image" binding:"-"`
		DataURL            string `form:"data_url" json:"data_url" binding:"-"`
		VisitURLMaster     string `form:"visit_url_master" json:"visit_url_master" binding:"-"`
		VisitURLEnterprise string `form:"visit_url_enterprise" json:"visit_url_enterprise" binding:"-"`
		VisitURLUser       string `form:"visit_url_user" json:"visit_url_user" binding:"-"`
		VisitURLAgent      string `form:"visit_url_agent" json:"visit_url_agent" binding:"-"`
		VisitURLDeveloper  string `form:"visit_url_developer" json:"visit_url_developer" binding:"-"`
		IPs                string `form:"ips" json:"ips" binding:"-"`
		Description        string `form:"description" json:"description" binding:"-"`
		PublicKeyClient    string `form:"public_key_client" json:"public_key_client" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	req.PublicKeyClient = strings.ReplaceAll(req.PublicKeyClient, "\\n", "\n")

	dUrl, _ := url.Parse(req.DataURL)
	dataURL := ""
	if dUrl != nil {
		if dUrl.Scheme == "" {
			dUrl.Scheme = "http"
		}

		dataURL = dUrl.Scheme + "://" + dUrl.Host
		if !strings.HasPrefix(dUrl.Path, baapi.RoutePub) {
			dataURL += dUrl.Path
		}
	}

	isAdd, offwOrID, err := tables.BaAppAddOrUpdate(
		req.ID,
		req.Name,
		req.Module,
		req.HeadImage,
		dataURL,
		req.VisitURLMaster,
		req.VisitURLEnterprise,
		req.VisitURLUser,
		req.VisitURLAgent,
		req.VisitURLDeveloper,
		req.IPs,
		req.PublicKeyClient,
		req.Description)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	keys.ResetKeyManager()

	baapi.JSONReply(c, "success", "成功", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}

// @Summary 应用模块禁止
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param status query string true "是否禁止"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/app/forbid [post]
func AppForbidHandler(c *gin.Context) {
	var req struct {
		ID     string `form:"id" json:"id" binding:"required"`
		Status string `form:"status" json:"status" binding:"required"` // 1禁止  0正常
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	var b bool
	if req.Status == "1" {
		b = true
	} else if req.Status == "0" {
		b = false
	} else {
		baapi.JSONReply(c, "error", "status not equal '0' or '1'", nil)
		return
	}

	offw, err := tables.BaAppForbid(req.ID, b)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	keys.ResetKeyManager()

	baapi.JSONReply(c, "success", "成功", gin.H{
		"offw": offw,
	})
}

// @Summary 应用模块登录
// @Accept  json
// @Produce  json
// @Param type query string true "type"
// @Param user_id query string true "user_id"
// @Param app_id query string true "app_id"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/app/login_to [get]
func AppLoginToHandler(c *gin.Context) {
	var req struct {
		Type   baapi.Type `form:"type" json:"type" binding:"required"`
		UserID string     `form:"user_id" json:"user_id" binding:"required"`
		AppID  string     `form:"app_id" json:"app_id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	app, isExists, err := tables.BaAppByID(lib.ValueToInt64(req.AppID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !isExists {
		baapi.JSONReply(c, "error", "app not found", nil)
		return
	}

	loginURL, err := loginTo(req.Type, lib.ValueToInt64(req.UserID, 0), app)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"url": loginURL,
	})
}
