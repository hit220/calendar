package v1

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
)

// @Summary 管理员分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/master/list [get]
func MasterListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw  string `form:"kw" json:"kw" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.BaMasterList(pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 管理员详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/master/detail [get]
func MasterDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.BaMasterByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "成功", one)
}

// @Summary 管理员新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/master/add_or_update [post]
func MasterAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID        string `form:"id" json:"id" binding:"-"`
		RID       string `form:"rid" json:"rid" binding:"-"`
		Name      string `form:"name" json:"name" binding:"required"`
		Username  string `form:"username" json:"username" binding:"-"`
		HeadImage string `form:"head_image" json:"head_image" binding:"-"`
		Password  string `form:"password" json:"password" binding:"-"`
		Email     string `form:"email" json:"email" binding:"-"`
		Phone     string `form:"phone" json:"phone" binding:"-"`
		Address   string `form:"address" json:"address" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	req.Password = tables.GenUserSha256(req.Password)

	isAdd, offwOrID, err := tables.BaMasterAddOrUpdate(
		req.ID,
		lib.ValueToInt64(req.RID, 0),
		req.Username,
		req.Name,
		req.HeadImage,
		req.Email,
		req.Phone,
		req.Address,
		req.Password)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}
