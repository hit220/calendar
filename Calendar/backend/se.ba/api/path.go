package api

import (
	"errors"
	"strings"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
)

func GetPathArr(c *gin.Context) {
	var req struct {
		Type baapi.Type `form:"type" json:"type" binding:"required"`
		Uid  string     `form:"uid" json:"uid" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	var rid int64
	//通过类型和uid查用户
	switch req.Type {
	case baapi.TypeAgent:
		userInfo, exists, err := tables.BaAgentByID(lib.ValueToInt64(req.Uid, 0))
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
		if !exists {
			baapi.JSONReply(c, "error", errors.New("用户不存在"))
			return
		}

		rid = userInfo.RID
	case baapi.TypeUser:
		userInfo, exists, err := tables.BaUserByID(lib.ValueToInt64(req.Uid, 0))
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
		if !exists {
			baapi.JSONReply(c, "error", errors.New("用户不存在"))
			return
		}

		rid = userInfo.RID
	case baapi.TypeMaster:
		userInfo, exists, err := tables.BaMasterByID(lib.ValueToInt64(req.Uid, 0))
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
		if !exists {
			baapi.JSONReply(c, "error", errors.New("用户不存在"))
			return
		}

		rid = userInfo.RID
	case baapi.TypeEnterprise:
		userInfo, exists, err := tables.BaEnterpriseByID(lib.ValueToInt64(req.Uid, 0))
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
		if !exists {
			baapi.JSONReply(c, "error", errors.New("用户不存在"))
			return
		}

		rid = userInfo.RID
	case baapi.TypeDeveloper:
		userInfo, exists, err := tables.BaDeveloperByID(lib.ValueToInt64(req.Uid, 0))
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
		if !exists {
			baapi.JSONReply(c, "error", errors.New("用户不存在"))
			return
		}

		rid = userInfo.RID
	default:
		baapi.JSONReply(c, "error", errors.New("无该用户类型"))
		return
	}
	//通过rid 查角色信息
	roleInfo, exists, err := tables.BaRoleByID(rid)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}
	if !exists {
		baapi.JSONReply(c, "error", errors.New("权限不足"))
		return
	}
	rolePath := roleInfo.AuthPaths

	if rolePath == "" {
		baapi.JSONReply(c, "success", rolePath)
		return
	}
	//拆分
	rolePastArr := strings.Split(rolePath, "\n")
	glog.Errorln(rolePastArr)

	for k, v := range rolePastArr {
		rolePastArr[k] = strings.TrimSpace(v)
	}

	if len(rolePastArr) == 1 {
		if len(rolePastArr[0]) < 2 {
			glog.Errorln("权限配置有误")
			baapi.JSONReply(c, "error", errors.New("权限不足"))
			return
		}
		checkStr := rolePastArr[0][0:2]
		if checkStr == "**" || checkStr == "/*" {
			resStr := lib.ValueToString(rolePastArr[0], "")
			baapi.JSONReply(c, "success", "成功", resStr)
			return
		} else {
			checkStrTwo := rolePastArr[0][0:1]
			if checkStrTwo == "[" {
				resStr, err := PinStr(rolePastArr[0], string(req.Type))
				if err != nil {
					baapi.JSONReply(c, "error", err)
					return
				}
				baapi.JSONReply(c, "success", "成功", resStr)
				return
			} else {
				baapi.JSONReply(c, "error", errors.New("权限不足"))
				return
			}
		}
	}

	resStr := lib.ValueToString(rolePath[0], "")

	if len(rolePastArr) > 1 && rolePastArr[1] != "" {
		resStr, err = PinStr(rolePastArr[1], string(req.Type))
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
	}
	baapi.JSONReply(c, "success", "成功", resStr)
}

func PinStr(str, reqType string) (string, error) {
	resStr := "/*/api/" + reqType + "/** ["
	//去掉[]
	str1 := strings.Trim(str, "[")
	str2 := strings.Trim(str1, "]")
	arr1 := strings.Split(str2, ",")
	if len(arr1) > 0 {
		for _, v := range arr1 {
			//查出每个的路径
			pathInfo, exists, err := tables.BaAuthPathByID(lib.ValueToInt64(v, 0))
			if err != nil {
				return "", err
			}
			if !exists {
				continue
			}
			resStr += pathInfo.Path + ","
		}
		resStr = strings.TrimRight(resStr, ",")
	}
	resStr += "]"
	return resStr, nil
}
