package api

import (
	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

// @Summary 注册 - 用户名/邮箱
// @Accept  json
// @Produce  json
// @Param username query string true "用户名"
// @Param password query string true "密码"
// @Param email query string false "邮箱"
// @Param mobile query string false "手机号"
// @Param code query string false "验证码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/register/username [post]
func RegisterByUE(c *gin.Context) {

	var req struct {
		Type     baapi.Type `form:"type" json:"type" binding:"required"`
		Module   string     `form:"module" json:"module" binding:"-"`
		Device   string     `form:"device" json:"device" binding:"-"`
		Username string     `form:"username" json:"username" binding:"-"`
		Phone    string     `form:"phone" json:"phone" binding:"-"`
		Email    string     `form:"email" json:"email" binding:"-"`
		Password string     `form:"password" json:"password" binding:"required"`
		Code     string     `form:"code" json:"code" binding:"-"`
		ExtInfo  string     `form:"ext_info" json:"ext_info" binding:"-"` // 附加信息
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	mustVerifyCode := cfg.Bool("register.must_verify_code", true)

	if mustVerifyCode {
		if (req.Email != "" || req.Phone != "") && req.Code == "" {
			// baapi.JSONReply(c, "error", "Email or Phone need verify verification-code", nil)
			baapi.JSONReply(c, "error", "请输入验证码", nil)
			return
		} else if req.Code != "" {
			if req.Email != "" {
				verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Email, req.Code)
				if !verifySuccess {
					// baapi.JSONReply(c, "error", "verify fail", nil)
					baapi.JSONReply(c, "error", "验证码不正确", nil)
					return
				}
			} else if req.Phone != "" {
				verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Phone, req.Code)
				if !verifySuccess {
					// baapi.JSONReply(c, "error", "verify fail", nil)
					baapi.JSONReply(c, "error", "验证码不正确", nil)
					return
				}
			}
		}
	}

	if req.Username == "" && req.Email == "" {
		// baapi.JSONReply(c, "error", "Username or Email is required", nil)
		baapi.JSONReply(c, "error", "请填写用户名", nil)
		return
	}

	if req.Username != "" {
		if !utils.IsUsername(req.Username) {
			// baapi.JSONReply(c, "error", "Username can't be email or phone", nil)
			baapi.JSONReply(c, "error", "用户名不能是邮箱或者手机号", nil)
			return
		}
	}

	// 登录验证
	switch req.Type {
	case baapi.TypeMaster:
		canRegister := cfg.Bool("register.master.by_username", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by username not support", nil)
			return
		}
		err := tables.BaMasterRegisterByUE(req.Username, req.Email, req.Phone, req.Password,
			req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeAgent:
		canRegister := cfg.Bool("register.agent.by_username", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by username not support", nil)
			return
		}
		err := tables.BaAgentRegisterByUE(req.Username, req.Email, req.Phone, req.Password,
			req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeEnterprise:
		canRegister := cfg.Bool("register.enterprise.by_username", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by username not support", nil)
			return
		}
		err := tables.BaEnterpriseRegisterByUE(req.Username, req.Email, req.Phone, req.Password,
			req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeDeveloper:
		canRegister := cfg.Bool("register.developer.by_username", true)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by username not support", nil)
			return
		}
		err := tables.BaDeveloperRegisterByUE(req.Username, req.Email, req.Phone, req.Password,
			req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeUser:
		canRegister := cfg.Bool("register.user.by_username", true)
		if !canRegister {
			// baapi.JSONReply(c, "error", "register by username not support", nil)
			baapi.JSONReply(c, "error", "不支持用户名注册", nil)
			return
		}
		err := tables.BaUserRegisterByUE(req.Username, req.Email, req.Phone, req.Password,
			req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	default:
		baapi.JSONReply(c, "error", "not support", nil)
		return
	}
	baapi.JSONReply(c, "success", "注册成功")
}

// @Summary 注册 - 邮箱
// @Accept  json
// @Produce  json
// @Param email query string true "邮箱"
// @Param password query string true "密码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/register/email [post]
func RegisterByUE_1(c *gin.Context) {
}

// @Summary 注册 - 手机号+验证码
// @Accept  json
// @Produce  json
// @Param phone query string true "手机号"
// @Param code query string true "验证码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/register/username [post]
func RegisterByPhone(c *gin.Context) {

	var req struct {
		Type    baapi.Type `form:"type" json:"type" binding:"required"`
		Module  string     `form:"module" json:"module" binding:"-"`
		Device  string     `form:"device" json:"device" binding:"-"`
		Phone   string     `form:"phone" json:"phone" binding:"required"`
		Code    string     `form:"code" json:"code" binding:"-"`
		ExtInfo string     `form:"ext_info" json:"ext_info" binding:"-"` //附加信息
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !utils.IsPhone(req.Phone) {
		baapi.JSONReply(c, "error", "need input valid phone number", nil)
		return
	}

	verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Phone, req.Code)
	if !verifySuccess {
		baapi.JSONReply(c, "error", "verify fail", nil)
		return
	}

	// 登录验证
	switch req.Type {
	case baapi.TypeMaster:
		canRegister := cfg.Bool("register.master.by_phone", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaMasterRegisterByPhone(req.Phone, req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeAgent:
		canRegister := cfg.Bool("register.agent.by_phone", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaAgentRegisterByPhone(req.Phone, req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeEnterprise:
		canRegister := cfg.Bool("register.enterprise.by_phone", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaEnterpriseRegisterByPhone(req.Phone, req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeDeveloper:
		canRegister := cfg.Bool("register.developer.by_phone", true)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaDeveloperRegisterByPhone(req.Phone, req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeUser:
		canRegister := cfg.Bool("register.user.by_phone", true)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaUserRegisterByPhone(req.Phone, req.ExtInfo)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	default:
		baapi.JSONReply(c, "error", "not support", nil)
		return
	}
	baapi.JSONReply(c, "success", "注册成功")
}
