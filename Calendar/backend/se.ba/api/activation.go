package api

import (
	"github.com/gin-gonic/gin"
	cfg "seasonalcal.life/rechy/config"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
)

// @Summary 激活 - 邮箱+验证码
// @Accept  json
// @Produce  json
// @Param email query string true "邮箱"
// @Param code query string true "验证码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/activation/email [post]
func ActivationByEmail(c *gin.Context) {

	var req struct {
		Type   baapi.Type `form:"type" json:"type" binding:"required"`
		Module string     `form:"module" json:"module" binding:"-"`
		Device string     `form:"device" json:"device" binding:"-"`
		Email  string     `form:"email" json:"email" binding:"required"`
		Code   string     `form:"code" json:"code" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Email, req.Code)
	if !verifySuccess {
		baapi.JSONReply(c, "error", "verify fail", nil)
		return
	}

	// 登录验证
	switch req.Type {
	case baapi.TypeMaster:
		canRegister := cfg.Bool("register.master.by_phone", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaMasterActivationByEmail(req.Email)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeAgent:
		canRegister := cfg.Bool("register.agent.by_phone", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaAgentActivationByEmail(req.Email)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeEnterprise:
		canRegister := cfg.Bool("register.enterprise.by_phone", false)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaEnterpriseActivationByEmail(req.Email)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeDeveloper:
		canRegister := cfg.Bool("register.developer.by_phone", true)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaDeveloperActivationByEmail(req.Email)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeUser:
		canRegister := cfg.Bool("register.user.by_phone", true)
		if !canRegister {
			baapi.JSONReply(c, "error", "register by phone not support", nil)
			return
		}
		err := tables.BaUserActivationByEmail(req.Email)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	default:
		baapi.JSONReply(c, "error", "not support", nil)
		return
	}
	baapi.JSONReply(c, "success", "激活成功")
}
