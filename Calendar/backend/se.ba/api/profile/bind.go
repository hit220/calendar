package profile

import (
	"github.com/gin-gonic/gin"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

// @Summary 账户绑定 - 手机号
// @Accept  json
// @Produce  json
// @Param phone query string true "手机号"
// @Param code query string true "验证码"
// @Param force query string false "强制合并"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/prfile/bind_phone [post]
func BindPhoneHandler(c *gin.Context) {

	var req struct {
		Phone string `form:"phone" json:"phone" binding:"required"`
		Code  string `form:"code" json:"code" binding:"required"`
		Force string `form:"force" json:"force" binding:"-"` // 强制合并, 非空则强制合并
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !utils.IsPhone(req.Phone) {
		baapi.JSONReply(c, "error", "need input valid phone number", nil)
		return
	}

	uType, id := baapi.GetModAuthorID(c)
	verifySuccess := tables.BaVerifyCodeVerify(string(uType), req.Phone, req.Code)
	if !verifySuccess {
		baapi.JSONReply(c, "error", "verify fail", nil)
		return
	}

	force := req.Force != ""

	// 保存绑定
	switch uType {
	case baapi.TypeMaster:
		err := tables.BaMasterBindPhone(id, req.Phone, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeAgent:
		err := tables.BaAgentBindPhone(id, req.Phone, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeEnterprise:
		err := tables.BaEnterpriseBindPhone(id, req.Phone, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeDeveloper:
		err := tables.BaDeveloperBindPhone(id, req.Phone, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeUser:
		err := tables.BaUserBindPhone(id, req.Phone, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	default:
		baapi.JSONReply(c, "error", "not support", nil)
		return
	}
	baapi.JSONReply(c, "success", "绑定成功")
}

// @Summary 账户绑定 - 邮箱
// @Accept  json
// @Produce  json
// @Param email query string true "邮箱"
// @Param code query string true "验证码"
// @Param force query string false "强制合并"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/prfile/bind_email [post]
func BindEmailHandler(c *gin.Context) {

	var req struct {
		Email string `form:"email" json:"email" binding:"required"`
		Code  string `form:"code" json:"code" binding:"required"`
		Force string `form:"force" json:"force" binding:"-"` // 强制合并, 非空则强制合并
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !utils.IsEmail(req.Email) {
		baapi.JSONReply(c, "error", "need input valid phone number", nil)
		return
	}

	uType, id := baapi.GetModAuthorID(c)
	verifySuccess := tables.BaVerifyCodeVerify(string(uType), req.Email, req.Code)
	if !verifySuccess {
		baapi.JSONReply(c, "error", "verify fail", nil)
		return
	}

	force := req.Force != ""

	// 保存绑定
	switch uType {
	case baapi.TypeMaster:
		err := tables.BaMasterBindEmail(id, req.Email, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeAgent:
		err := tables.BaAgentBindEmail(id, req.Email, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeEnterprise:
		err := tables.BaEnterpriseBindEmail(id, req.Email, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeDeveloper:
		err := tables.BaDeveloperBindEmail(id, req.Email, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeUser:
		err := tables.BaUserBindEmail(id, req.Email, force)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	default:
		baapi.JSONReply(c, "error", "not support", nil)
		return
	}
	baapi.JSONReply(c, "success", "绑定成功")
}
