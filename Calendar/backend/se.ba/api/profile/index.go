package profile

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

// @Summary 个人详情
// @Accept  json
// @Produce  json
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/profile [get]
func ProfileHandler(c *gin.Context) {

	na, token, err := RefreshLoginAuthor(c)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"user":  na,
		"token": token,
	})
}

// @Summary 资料编辑保存
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/profile [post]
func ProfileUpdateHandler(c *gin.Context) {
	var req struct {
		Cols      string `form:"cols" json:"cols" binding:"required"`
		Name      string `form:"name" json:"name" binding:"-"`
		Nickname  string `form:"nickname" json:"nickname" binding:"-"`
		HeadImage string `form:"head_image" json:"head_image" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	author := baapi.GetModAuthor(c)

	var err error
	switch author.Type {
	case baapi.TypeMaster:
		err = tables.BaMasterProfileUpdate(author.ID, strings.Split(req.Cols, ","),
			map[string]interface{}{
				"name":       req.Name,
				"nickname":   req.Nickname,
				"head_image": req.HeadImage,
			})
	case baapi.TypeAgent:
		err = tables.BaAgentProfileUpdate(author.ID, strings.Split(req.Cols, ","),
			map[string]interface{}{
				"name":       req.Name,
				"nickname":   req.Nickname,
				"head_image": req.HeadImage,
			})
	case baapi.TypeDeveloper:
		err = tables.BaDeveloperProfileUpdate(author.ID, strings.Split(req.Cols, ","),
			map[string]interface{}{
				"name":       req.Name,
				"nickname":   req.Nickname,
				"head_image": req.HeadImage,
			})
	case baapi.TypeEnterprise:
		err = tables.BaEnterpriseProfileUpdate(author.ID, strings.Split(req.Cols, ","),
			map[string]interface{}{
				"name":       req.Name,
				"nickname":   req.Nickname,
				"head_image": req.HeadImage,
			})
	case baapi.TypeUser:
		err = tables.BaUserProfileUpdate(author.ID, strings.Split(req.Cols, ","),
			map[string]interface{}{
				"name":       req.Name,
				"nickname":   req.Nickname,
				"head_image": req.HeadImage,
			})
	default:
		err = errors.New("not support")
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	//
	p1 := filepath.Join(utils.HeadImageDir(), string(author.Type), lib.ValueToString(author.ID, "0"), "head_image.jpg")
	p2 := filepath.Join(utils.HeadImageDir(), string(author.Type), author.UUID, "head_image.jpg")

	os.Remove(p1)
	os.Remove(p2)

	baapi.JSONReply(c, "success", "成功", nil)
}
