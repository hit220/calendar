package profile

import (
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	guuid "github.com/satori/go.uuid"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/api"
	"seasonalcal.life/rechy/se.ba/tables"
	"seasonalcal.life/rechy/se.ba/utils"
)

// @Summary 文件上传
// @Accept  multipart/form-data
// @Produce  json
// @Param   file formData file true  "文件上传"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/profile/upload [post]
func FileUploadHandler(c *gin.Context) {
	userType, userID := baapi.GetModAuthorID(c)
	if userType == "" || userID < 1 {
		baapi.JSONReply(c, "error", "未知用户无权操作")
		return
	}

	err := c.Request.ParseMultipartForm(api.DefaultMultipartMemory)
	form := c.Request.MultipartForm
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	files := form.File["file"]
	if len(files) < 1 {
		baapi.JSONReply(c, "error", "上传失败, 未接收到任何文件")
		return
	}

	if files[0].Size > api.MaxFileSize {
		baapi.JSONReply(c, "error", "上传失败, 您的存储空间不足")
		return
	}

	// 文件唯一id
	uuid := guuid.NewV4().String()
	ext := utils.FileExt(files[0].Filename)
	uuid = "ba-" + uuid + strings.ToLower(ext)

	// 添加到数据库
	err = tables.BaFileAdd(
		string(userType),
		userID,
		files[0].Size,
		uuid, files[0].Filename,
		ext,
		files[0].Header.Get("Content-Type"))
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	// 保存文件
	uploadDir := utils.OpenUploadDir()
	// {baseDir}/{userId}/{uuid}
	filename := lib.EnsureFilePath(filepath.Join(uploadDir, uuid))
	err = c.SaveUploadedFile(files[0], filename)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	baapi.JSONReply(c, "success", "成功", gin.H{
		"uuid":         uuid,
		"ext":          ext,
		"size":         files[0].Size,
		"filename":     files[0].Filename,
		"content_type": files[0].Header.Get("Content-Type"),
	})
}
