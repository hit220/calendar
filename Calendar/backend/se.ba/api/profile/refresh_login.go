package profile

import (
	jwtBase "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/api"
)

//
func RefreshLoginAuthor(c *gin.Context) (*baapi.ModAuthor, string, error) {
	tk, err := api.GetJwtAuthMiddleware().ParseToken(c)
	if err != nil {
		return nil, "", err
	}

	aud := lib.ValueToString(tk.Header["aud"], "")
	claims, _ := tk.Claims.(jwtBase.MapClaims)
	author, _ := claims["author"].(string)
	u := baapi.NewModAuthor([]byte(author), true)

	return api.GetLoginTokenByID(aud, u.Type, u.ID, u.ApplyToken, u.Device)
}
