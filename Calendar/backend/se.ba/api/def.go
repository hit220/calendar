package api

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
)

//
func Unauthorized(c *gin.Context, code int, message string) {
	baapi.JSONReply(c, "nologin", fmt.Sprintf(message))
}

// 登录
func LoginResponse(c *gin.Context, code int, token string, tm time.Time) {
	user, ok := c.Get("user")
	if !ok {
		baapi.JSONReply(c, "error", "用户信息不存在")
		return
	}
	dd := gin.H{
		"token": token,
		"user":  user,
	}
	baapi.JSONReply(c, "success", "登录成功", dd)
}

// 登出
func LogoutResponse(c *gin.Context, code int) {
	baapi.JSONReply(c, "success", "退出成功")
}

func MasterLogin(id int64, account, password string, mustPassword bool,
	uType baapi.Type, applyToken, device string) (*baapi.ModAuthor, error) {

	one, err := tables.BaMasterLogin(id, account, password, mustPassword)
	if err != nil {
		return nil, err
	}

	paths := ""
	if one.RID > 0 {
		role, has, err := tables.BaRoleByID(one.RID)
		if err != nil {
			return nil, err
		}
		if has {
			// paths = role.AuthPaths
			paths = lib.ValueToString(one.ID, "") + "-" + lib.ValueToString(role.ID, "") + "-" + lib.ValueToString(role.UpdatedAt, "")
		}
	}

	return one.ToModAuthor(applyToken, device, paths), nil
}

func AgentLogin(id int64, account, password string, mustPassword bool,
	uType baapi.Type, applyToken, device string) (*baapi.ModAuthor, error) {

	one, err := tables.BaAgentLogin(id, account, password, mustPassword)
	if err != nil {
		return nil, err
	}

	paths := ""
	if one.RID > 0 {
		role, has, err := tables.BaRoleByID(one.RID)
		if err != nil {
			return nil, err
		}
		if has {
			// paths = role.AuthPaths
			paths = lib.ValueToString(one.ID, "") + "-" + lib.ValueToString(role.ID, "") + "-" + lib.ValueToString(role.UpdatedAt, "")
		}
	}

	return one.ToModAuthor(applyToken, device, paths), nil
}

func EnterpriseLogin(id int64, account, password string, mustPassword bool,
	uType baapi.Type, applyToken, device string) (*baapi.ModAuthor, error) {

	one, err := tables.BaEnterpriseLogin(id, account, password, mustPassword)
	if err != nil {
		return nil, err
	}

	paths := ""
	if one.RID > 0 {
		role, has, err := tables.BaRoleByID(one.RID)
		if err != nil {
			return nil, err
		}
		if has {
			// paths = role.AuthPaths
			paths = lib.ValueToString(one.ID, "") + "-" + lib.ValueToString(role.ID, "") + "-" + lib.ValueToString(role.UpdatedAt, "")
		}
	}

	return one.ToModAuthor(applyToken, device, paths), nil
}

func DeveloperLogin(id int64, account, password string, mustPassword bool,
	uType baapi.Type, applyToken, device string) (*baapi.ModAuthor, error) {

	one, err := tables.BaDeveloperLogin(id, account, password, mustPassword)
	if err != nil {
		return nil, err
	}

	paths := ""
	if one.RID > 0 {
		role, has, err := tables.BaRoleByID(one.RID)
		if err != nil {
			return nil, err
		}
		if has {
			// paths = role.AuthPaths
			paths = lib.ValueToString(one.ID, "") + "-" + lib.ValueToString(role.ID, "") + "-" + lib.ValueToString(role.UpdatedAt, "")
		}
	}

	return one.ToModAuthor(applyToken, device, paths), nil
}

func UserLogin(id int64, account, password string, mustPassword bool,
	uType baapi.Type, applyToken, device string) (*baapi.ModAuthor, error) {

	one, err := tables.BaUserLogin(id, account, password, mustPassword)
	if err != nil {
		return nil, err
	}

	paths := ""
	if one.RID > 0 {
		role, has, err := tables.BaRoleByID(one.RID)
		if err != nil {
			return nil, err
		}
		if has {
			// paths = role.AuthPaths
			paths = lib.ValueToString(one.ID, "") + "-" + lib.ValueToString(role.ID, "") + "-" + lib.ValueToString(role.UpdatedAt, "")
		}
	}

	return one.ToModAuthor(applyToken, device, paths), nil
}
