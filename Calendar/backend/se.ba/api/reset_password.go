package api

import (
	"github.com/gin-gonic/gin"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/tables"
)

// @Summary 重置密码 - 手机号 + 验证码
// @Accept  json
// @Produce  json
// @Param phone query string true "手机号"
// @Param code query string true "验证码"
// @Param password query string true "密码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/reset_password/phone [post]
func ResetPasswordByPE(c *gin.Context) {

	var req struct {
		Type     baapi.Type `form:"type" json:"type" binding:"required"`
		Module   string     `form:"module" json:"module" binding:"-"`
		Device   string     `form:"device" json:"device" binding:"-"`
		Phone    string     `form:"phone" json:"phone" binding:"-"`
		Email    string     `form:"email" json:"email" binding:"-"`
		Code     string     `form:"code" json:"code" binding:"required"`
		Password string     `form:"password" json:"password" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if req.Phone != "" {
		verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Phone, req.Code)
		if !verifySuccess {
			// baapi.JSONReply(c, "error", "verify fail", nil)
			baapi.JSONReply(c, "error", "验证码错误", nil)
			return
		}
	} else {
		verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Email, req.Code)
		if !verifySuccess {
			// baapi.JSONReply(c, "error", "verify fail", nil)
			baapi.JSONReply(c, "error", "验证码错误", nil)
			return
		}
	}

	// 登录验证
	switch req.Type {
	case baapi.TypeMaster:
		err := tables.BaMasterResetPassword(req.Phone, req.Email, req.Password)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeAgent:
		err := tables.BaAgentResetPassword(req.Phone, req.Email, req.Password)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeEnterprise:
		err := tables.BaEnterpriseResetPassword(req.Phone, req.Email, req.Password)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeDeveloper:
		err := tables.BaDeveloperResetPassword(req.Phone, req.Email, req.Password)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	case baapi.TypeUser:
		err := tables.BaUserResetPassword(req.Phone, req.Email, req.Password)
		if err != nil {
			baapi.JSONReply(c, "error", err, nil)
			return
		}
	default:
		baapi.JSONReply(c, "error", "not support", nil)
		return
	}
	baapi.JSONReply(c, "success", "重置成功")
}

// @Summary 重置密码 - 手机号 + 验证码（验证验证码）
// @Accept  json
// @Produce  json
// @Param phone query string true "手机号"
// @Param code query string true "验证码"
// @Param password query string true "密码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/reset_password/phone [post]
func ResetPasswordByPENew(c *gin.Context) {

	var req struct {
		Type   baapi.Type `form:"type" json:"type" binding:"required"`
		Device string     `form:"device" json:"device" binding:"-"`
		Phone  string     `form:"phone" json:"phone" binding:"-"`
		Email  string     `form:"email" json:"email" binding:"-"`
		Code   string     `form:"code" json:"code" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if req.Phone != "" {
		verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Phone, req.Code)
		if !verifySuccess {
			// baapi.JSONReply(c, "error", "verify fail", nil)
			baapi.JSONReply(c, "error", "验证码错误", nil)
			return
		}
	} else {
		verifySuccess := tables.BaVerifyCodeVerify(string(req.Type), req.Email, req.Code)
		if !verifySuccess {
			// baapi.JSONReply(c, "error", "verify fail", nil)
			baapi.JSONReply(c, "error", "验证码错误", nil)
			return
		}
	}

	baapi.JSONReply(c, "success", "验证码验证成功")
}

// @Summary 重置密码 - 邮箱 + 验证码
// @Accept  json
// @Produce  json
// @Param email query string true "邮箱"
// @Param code query string true "验证码"
// @Param password query string true "密码"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/reset_password/email [post]
func ResetPasswordByPE_(c *gin.Context) {}
