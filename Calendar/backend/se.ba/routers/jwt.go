package routers

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/syndtr/goleveldb/leveldb"
	"seasonalcal.life/rechy/glog"
	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
	"seasonalcal.life/rechy/se.ba/api"
	"seasonalcal.life/rechy/se.ba/keys"
	"seasonalcal.life/rechy/se.ba/tables"
)

const (
	JwtRealm         = "SE"
	JwtTokenHeadName = "SSE"
)

var identityKey = "id"

type KVDBInfo struct {
	aa *leveldb.DB
}

func (kv KVDBInfo) Get(key string, def string) (string, error) {
	resStr, err := kv.aa.Get([]byte(key), nil)
	if err != nil {
		return "", err
	}
	return string(resStr), nil
}
func (kv KVDBInfo) Put(key string, val string, def string) error {
	err := kv.aa.Put([]byte(key), []byte(val), nil)
	return err
}

func getJwtAuth() (*jwt.GinJWTMiddleware, error) {
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:            JwtRealm,
		SigningAlgorithm: baapi.JwtSigningAlgorithm,
		Timeout:          30 * time.Minute,
		MaxRefresh:       20 * time.Minute,
		IdentityKey:      identityKey,

		// 从data一个用户里找个唯一值做token存储的id
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*baapi.ModAuthor); ok {
				m := jwt.MapClaims{
					"author":    v.EncodeToGZip(),
					identityKey: fmt.Sprintf("%v-%v", v.Type, v.ID),
				}
				return m
			}
			return jwt.MapClaims{}
		},

		// 从token存储的id值, 再读取出用户的相关账户信息
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			author, _ := claims["author"].(string)
			return baapi.NewModAuthor([]byte(author), true)
		},

		// 登录时验证账号密码是否正常, 返回用户信息, err为nil就是登录成功
		Authenticator: api.Login,

		// 这个方法就是认证当前页面是否有权操作
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if v, ok := data.(*baapi.ModAuthor); ok {
				cacheDB := &KVDBInfo{tables.KVDB()}
				cacheAuthPaths, err := baapi.GetCacheAuthPaths(cacheDB, v.ID, v.Type, v.Auth)
				if err != nil {
					glog.Errorln(err)
					return false
				}
				// return v.HasAuth(c.Request)
				return baapi.DoHasAuthNew(c.Request, cacheAuthPaths)
			}

			return false
		},

		// 无权操作, 格式化回复内容
		Unauthorized: api.Unauthorized,

		// 登录返回
		LoginResponse: api.LoginResponse,

		// 出返回
		LogoutResponse: api.LogoutResponse,

		ModifyTokenHeader: modifyTokenHeader,
		KeyFuncVerify:     keyFuncVerify,
		KeyFuncSign:       keyFuncSign,

		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: JwtTokenHeadName,
		TimeFunc:      time.Now,
		SendCookie:    false,
	})

	if err != nil {
		return nil, err
	}

	errInit := authMiddleware.MiddlewareInit()
	if errInit != nil {
		return nil, errInit
	}

	return authMiddleware, nil
}

func modifyTokenHeader(header map[string]interface{}) map[string]interface{} {
	kmgr := keys.GetKeyManager()
	return kmgr.ModifyTokenHeader(header)
}

func keyFuncVerify(header map[string]interface{}) (interface{}, error) {
	kmgr := keys.GetKeyManager()
	return kmgr.KeyFuncVerify(header)
}

func keyFuncSign(header map[string]interface{}) (interface{}, error) {
	kmgr := keys.GetKeyManager()
	return kmgr.KeyFuncSign(header)
}
