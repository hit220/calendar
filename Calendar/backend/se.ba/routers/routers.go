package routers

import (
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	"seasonalcal.life/rechy/se.ba/api"
	"seasonalcal.life/rechy/se.ba/api/profile"
	v1 "seasonalcal.life/rechy/se.ba/api/v1"
)

func Init(e *gin.Engine) {

	/*******初始化操作权限路由范围 ******/
	baapi.InitBind(e, &PubHub{})

	/**********************/
	authMiddleware, err := getJwtAuth()
	if err != nil {
		glog.Fatalf("get jwt middleware err [%+v]", err)
		return
	}
	v1.SetJwtAuthMiddleware(authMiddleware)
	api.SetJwtAuthMiddleware(authMiddleware)

	// 无权限验证
	e.GET("/api/test", api.Test)
	e.POST(baapi.RouteApiUpload, api.OpenFileUploadHandler)
	e.GET(baapi.RouteApiFile, api.FileHandler)
	e.POST(baapi.RouteApiLogin, authMiddleware.LoginHandler)
	e.POST(baapi.RouteApiLoginByPhone, authMiddleware.LoginHandler)
	e.POST(baapi.RouteApiLoginByWxPub1, api.LoginByWxPub1)
	e.POST(baapi.RouteApiLoginByWxPub2, api.LoginByWxPub2)
	e.GET(baapi.RouteApiLogout, authMiddleware.LogoutHandler)
	e.POST(baapi.RouteApiRegisterUsername, api.RegisterByUE)
	e.POST(baapi.RouteApiRegisterEmail, api.RegisterByUE)
	e.POST(baapi.RouteApiRegisterPhone, api.RegisterByPhone)
	e.GET(baapi.RouteApiVerifyCodePhone, api.SendVerifyCodeByPhone)
	e.GET(baapi.RouteApiVerifyCodeEmail, api.SendVerifyCodeByEmail)
	e.POST(baapi.RouteApiVerifyCodePhone, api.SendVerifyCodeByPhone)
	e.POST(baapi.RouteApiActivationEmail, api.ActivationByEmail)
	e.POST(baapi.RouteApiVerifyCodeEmail, api.SendVerifyCodeByEmail)
	e.POST(baapi.RouteApiResetPasswordPhone, api.ResetPasswordByPE)
	e.POST(baapi.RouteApiResetPasswordEmail, api.ResetPasswordByPE)
	e.GET(baapi.RouteApiHeadImage, api.HeadImage)
	e.POST("/api/public/verifycode", api.ResetPasswordByPENew)
	e.POST("/api/public/get_auth", api.GetPathArr)

	profileGroup := e.Group(baapi.RouteApiProfile)
	profileGroup.Use(authMiddleware.MiddlewareFunc())
	{
		profileGroup.GET(baapi.RouteRefreshToken, profile.ProfileHandler)
		profileGroup.GET(baapi.RouteInfo, profile.ProfileHandler)
		profileGroup.POST(baapi.RouteInfo, profile.ProfileUpdateHandler)
		profileGroup.POST(baapi.RouteUpload, profile.FileUploadHandler)
		profileGroup.POST(baapi.RouteBindPhone, profile.BindPhoneHandler)
		profileGroup.POST(baapi.RouteBindEmail, profile.BindEmailHandler)
	}

	// 后台
	adminGroup := e.Group(baapi.RouteApiMaster)
	adminGroup.Use(authMiddleware.MiddlewareFunc())
	{
		adminGroup.GET("/v1/hello", v1.HelloHandler)
		adminGroup.GET("/v1/my/info", v1.MyInfoHandler)
		adminGroup.GET("/v1/app/list", v1.AppListHandler)
		adminGroup.GET("/v1/app/detail", v1.AppDetailHandler)
		adminGroup.POST("/v1/app/add_or_update", v1.AppAddOrUpdateHandler)
		adminGroup.POST("/v1/app/forbid", v1.AppForbidHandler)
		adminGroup.GET("/v1/app/login_to", v1.AppLoginToHandler)
		adminGroup.GET("/v1/agent/list", v1.AgentListHandler)
		adminGroup.GET("/v1/agent/detail", v1.AgentDetailHandler)
		adminGroup.POST("/v1/agent/add_or_update", v1.AgentAddOrUpdateHandler)
		adminGroup.GET("/v1/master/list", v1.MasterListHandler)
		adminGroup.GET("/v1/master/detail", v1.MasterDetailHandler)
		adminGroup.POST("/v1/master/add_or_update", v1.MasterAddOrUpdateHandler)
		adminGroup.GET("/v1/developer/list", v1.DeveloperListHandler)
		adminGroup.GET("/v1/developer/detail", v1.DeveloperDetailHandler)
		adminGroup.POST("/v1/developer/add_or_update", v1.DeveloperAddOrUpdateHandler)
		adminGroup.GET("/v1/enterprise/list", v1.EnterpriseListHandler)
		adminGroup.GET("/v1/enterprise/detail", v1.EnterpriseDetailHandler)
		adminGroup.POST("/v1/enterprise/add_or_update", v1.EnterpriseAddOrUpdateHandler)
		adminGroup.POST("/v1/enterprise/open_module", v1.EnterpriseOpenModuleHandler)
		adminGroup.GET("/v1/role/list", v1.RoleListHandler)
		adminGroup.GET("/v1/role/detail", v1.RoleDetailHandler)
		adminGroup.POST("/v1/role/add_or_update", v1.RoleAddOrUpdateHandler)
		adminGroup.GET("/v1/user/list", v1.UserListHandler)
		adminGroup.GET("/v1/user/detail", v1.UserDetailHandler)
		adminGroup.POST("/v1/user/add_or_update", v1.UserAddOrUpdateHandler)
		adminGroup.GET("/v1/auth_path/list", v1.AuthPathListHandler)
		adminGroup.GET("/v1/auth_path/detail", v1.AuthPathDetailHandler)
		adminGroup.POST("/v1/auth_path/add_or_update", v1.AuthPathAddOrUpdateHandler)
		adminGroup.GET("/v1/auth_path/ws_sync_routers", v1.AuthPathWSSyncRouters)
		adminGroup.POST("/v1/file/upload", v1.FileUploadHandler)
	}
	/**********************/

	e.StaticFS("/fe", newStaticDir("/fe", lib.EnsureDir("./web/fe"), ""))
	e.StaticFS("/static", newStaticDir("/static", lib.EnsureDir("./web/static"), ""))
	e.StaticFS("/ui", newStaticDir("/ui", lib.EnsureDir("./web/wwwx"), "index.html"))
	e.GET("/", func(c *gin.Context) {
		toURL := &url.URL{
			Path:     "/ui/",
			RawQuery: c.Request.URL.RawQuery, // 保留参数
			Fragment: c.Request.URL.Fragment, // 保留锚
		}
		c.Redirect(http.StatusFound, toURL.String())
	})
}
