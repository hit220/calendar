package sms

import (
	"bytes"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net/http"

	cfg "seasonalcal.life/rechy/config"
)

//Clearances is
type SMSQiYeXinShi struct {
	Userid   string
	Account  string
	Password string
	Tag      string
	UrlSend  string
}

type Returnsms struct {
	XMLName       xml.Name `xml:"returnsms"`
	Returnstatus  string   `xml:"returnstatus"`
	Message       string   `xml:"message"`
	Remainpoint   string   `xml:"remainpoint"`
	TaskID        string   `xml:"taskID"`
	SuccessCounts string   `xml:"successCounts"`
}

func init() {
	registerSender("qiyexinshi", NewSMSQiYeXinShi)
}

func NewSMSQiYeXinShi() SMSSender {
	s := &SMSQiYeXinShi{
		Account:  cfg.String("sms.qiyexinshi.account", ""),
		Userid:   cfg.String("sms.qiyexinshi.userid", ""),
		Password: cfg.String("sms.qiyexinshi.password", ""),
		Tag:      cfg.String("sms.qiyexinshi.tag", ""),
		UrlSend:  cfg.String("sms.qiyexinshi.url", ""),
	}
	return s
}

func (s *SMSQiYeXinShi) SendVerifyCode(to, code string) error {
	// file := "./var/www/soft/se.ba/" + "sendsms" + ".txt"
	// logFile, _ := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)

	// log.SetOutput(logFile) // 将文件设置为log输出的文件
	// log.SetPrefix("[qSkipTool]")
	// log.SetFlags(log.LstdFlags | log.Lshortfile | log.LUTC)

	content := "【" + s.Tag + "】 : 短信验证码" + code + "，有效时间5分钟，请尽快完成验证！"

	//构造请求参数
	var buff bytes.Buffer
	buff.WriteString("action=")
	buff.WriteString("send")
	buff.WriteString("&userid=")
	buff.WriteString(s.Userid)
	buff.WriteString("&account=")
	buff.WriteString(s.Account)
	buff.WriteString("&password=")
	buff.WriteString(s.Password)
	buff.WriteString("&mobile=")
	buff.WriteString(to)
	buff.WriteString("&content=")
	buff.WriteString(content)

	var resp *http.Response
	resp, err := http.Post(s.UrlSend, "application/x-www-form-urlencoded", &buff)
	if err != nil {
		return err
	}

	data1, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	v := Returnsms{}
	err = xml.Unmarshal(data1, &v)
	if err != nil {
		return err
	}

	if v.Returnstatus != "Success" || v.Message != "ok" {
		return errors.New(v.Message)
	}

	return nil
}
