package sms

import "errors"

type SMSSender interface {
	SendVerifyCode(to, code string) error
}

func SendVerifyCode(to, code string) error {
	if gSender == nil {
		return errors.New("no sms sender")
	}
	return gSender.SendVerifyCode(to, code)
}
