package sms

import cfg "seasonalcal.life/rechy/config"

var gSender SMSSender
var senders = map[string]func() SMSSender{}

func Init() {
	t := cfg.String("sms.channel", "")
	if t == "" {
		return
	}
	ob, ok := senders[t]
	if !ok {
		return
	}
	gSender = ob()
}

func registerSender(key string, s func() SMSSender) {
	senders[key] = s
}
