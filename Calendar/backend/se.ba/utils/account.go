package utils

import "regexp"

func IsPhone(p string) bool {
	is, _ := regexp.MatchString(`^[+0-9]{8,16}$`, p)
	return is
}

func IsEmail(p string) bool {
	is, _ := regexp.MatchString(`^[.\-_a-zA-Z0-9]+@[.\-_a-zA-Z0-9]+$`, p)
	return is
}

func IsUsername(p string) bool {
	return !IsPhone(p) && !IsEmail(p)
}
