package email

import cfg "seasonalcal.life/rechy/config"

var gSender EmailSender
var senders = map[string]func() EmailSender{}

func Init() {
	t := cfg.String("mailer.channel", "")
	if t == "" {
		return
	}
	ob, ok := senders[t]
	if !ok {
		return
	}
	gSender = ob()
}

func registerSender(key string, s func() EmailSender) {
	senders[key] = s
}
