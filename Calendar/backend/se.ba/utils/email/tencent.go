package email

import (
	"crypto/tls"

	"gopkg.in/gomail.v2"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

func init() {
	registerSender("tencent", NewTencentMailSenderByConfig)
}

type TencentMailSender struct {
	d        *gomail.Dialer
	from     string
	fromName string
}

func NewTencentMailSender(host string, port int, username, password,
	from, fromName string) *TencentMailSender {

	d := gomail.NewDialer(host, port, username, password)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	return &TencentMailSender{
		d:        d,
		from:     from,
		fromName: fromName,
	}

}

func NewTencentMailSenderByConfig() EmailSender {
	from := cfg.String("mailer.from", "admin@xxx.com")
	fromName := cfg.String("mailer.from_name", "XXX")
	host := cfg.String("mailer.tencent.host", "smtp.exmail.qq.com")
	port := cfg.Int("mailer.tencent.port", 465)
	username := cfg.String("mailer.tencent.username", "admin@xxx.com")
	password := cfg.String("mailer.tencent.password", "--")

	return NewTencentMailSender(host, port, username, password, from, fromName)
}

// 发送普通邮件
func (s *TencentMailSender) Send(to, subject, body string, addtions ...string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", m.FormatAddress(s.from, s.fromName))
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	return s.d.DialAndSend(m)
}

// 发送激活链接
func (s *TencentMailSender) SendVerify(to, subject, company, code string) error {
	host := cfg.String("mailer.base_url", "http://ba.example.com")
	body := `<div class="wrap" style="background-color: #fff; max-width: 640px;margin: 0 auto;font-family: 'lucida Grande',Verdana,'Microsoft YaHei';font-size: 12px;-webkit-font-smoothing: subpixel-antialiased;">
	    <div class="logo" style="margin-bottom: 20px;text-align: center;">
	        <img src="` + host + `/ui/fe/imgs/logo.png" style="width:90px"/>
	    </div>
	    <div class="cont" style="text-align: left;line-height: 1.6;">
	        <div class="welcome" style="font-size:20px;margin-bottom:20px;font-weight:600;">
	            Welcome to ` + company + `:
	        </div>
	        You have successfully joined the best place to watch, create, upload and share beautiful HD videos. We will provide more convenient conditions for your distribution, start exploring!
	    </div>
	    <div class="btn" style="text-align: center;font-size: 15px;background:#4e6ef2;margin: 30px auto;width:200px;heigth:40px;line-height:40px;text-align:center;">
			<a style="color: #fff;" href="` + lib.UrlJoin(host, baapi.RouteApiActivationEmail) + `?target=1&code=` + code + `&email=` + to + `">Start exploring</a>
	    </div>
	</div>            `
	return s.Send(to, subject, body)
}

// 发送验证码
func (s *TencentMailSender) SendVerifyCode(to, code string) error {
	subject := cfg.String("mailer.verify_subject", "Welcome to XXX!")
	company := cfg.String("mailer.verify_company", "XXXXX")
	body := `<div style="padding: 20px 16px;">欢迎使用` +
		company + `的服务！你的安全验证码为：<span style="font-size: 15px; font-weight: bold; margin-left: 6px; margin-right: 6px">` +
		code + `</span><br/><br/><br/>谢谢, <br/>` + company + `<br/></div>            ` // 多空白结尾, 否则可能乱码
	return s.Send(to, subject, body)
}
