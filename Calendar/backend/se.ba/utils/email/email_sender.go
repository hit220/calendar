package email

import "errors"

type EmailSender interface {
	SendVerifyCode(to, code string) error
}

func SendVerifyCode(to, code string) error {
	if gSender == nil {
		return errors.New("no email sender")
	}
	return gSender.SendVerifyCode(to, code)
}
