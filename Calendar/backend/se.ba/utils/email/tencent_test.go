package email

import (
	"testing"

	cfg "seasonalcal.life/rechy/config"
)

func TestTencent(t *testing.T) {

	err := cfg.TryInit("json")
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	cfg.Set("mailer.channel", "tencent")
	cfg.String("mailer.from", "admin@seasonalcal.life")
	cfg.String("mailer.from_name", "SeasonalcalLife")
	cfg.String("mailer.base_url", "http://ba.seasonalcal.life")
	cfg.String("mailer.subject", "SeasonalcalLife邮箱验证")
	cfg.String("mailer.company", "SeasonalcalLife")
	cfg.String("mailer.tencent.host", "smtp.qq.com")
	cfg.Int("mailer.tencent.port", 465)
	cfg.String("mailer.tencent.username", "admin@seasonalcal.life")
	cfg.String("mailer.tencent.password", "Abc1236465")

	// 初始化
	Init()

	err = SendVerifyCode("287150852@qq.com", "123456")
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
}
