package utils

import (
	"path/filepath"
	"regexp"
	"strings"

	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
)

func InArray(arr []string, key string) bool {
	for _, v := range arr {
		if v == key {
			return true
		}
	}
	return false
}

func HeadImageDir() string {
	dir := cfg.String("api.upload_dir_open", "./_data/head_images")
	return lib.EnsureDir(dir)
}

func UploadDir() string {
	dir := cfg.String("api.upload_dir", "./_data/upload")
	return lib.EnsureDir(dir)
}

func OpenUploadDir() string {
	dir := cfg.String("api.upload_dir_open", "./_data/upload_open")
	return lib.EnsureDir(dir)
}

func FileExt(p string) string {
	p = regexp.MustCompile(`(?is)[^0-9a-zA-Z.]+`).ReplaceAllString(p, "")
	ext := filepath.Ext(p)
	if ext != "" {
		ext2 := filepath.Ext(strings.TrimSuffix(p, ext))
		if ext2 != "" {
			ext = ext2 + ext
		}
	}
	return strings.ToLower(ext)
}
