package keys

import (
	"crypto/rsa"
	"errors"
	"sync"

	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
	"seasonalcal.life/rechy/se.ba/tables"
)

var km *KeyManager
var keyManagerLock sync.Mutex

func GetKeyManager() *KeyManager {
	keyManagerLock.Lock()
	defer keyManagerLock.Unlock()

	if km == nil {
		km = &KeyManager{}
	}

	if !km.isInited {
		err := km.initKeys()
		if err != nil {
			glog.Errorln("key manager init:", err)
			return nil
		}
	}
	return km
}

func ResetKeyManager() error {
	keyManagerLock.Lock()
	defer keyManagerLock.Unlock()

	k := &KeyManager{}
	err := k.initKeys()
	if err != nil {
		return err
	}
	km = k
	return nil
}

type BaKeys struct {
	MyPrivateKey *rsa.PrivateKey
	AimPublicKey *rsa.PublicKey
}

type KeyManager struct {
	isInited       bool
	selfPrivateKey *rsa.PrivateKey
	selfPublicKey  *rsa.PublicKey
	baKeys         sync.Map // string(module_name) => *BaKeys
	baPublicKey    *rsa.PublicKey
}

func (k *KeyManager) initKeys() error {
	keyModules := []string{}
	defer func() {
		// 不管是否成功初始化, 都设置为已经初始化
		k.isInited = true
		for _, m := range keyModules {
			glog.Infoln("Key Manager inited: ", m)
		}
	}()

	var err error
	privKey, pubKey, err := lib.GenKeyPair(2048)
	if err != nil {
		return err
	}

	selfPrivateKey := cfg.String("module.jwt.self.private_key", privKey)
	selfPublicKey := cfg.String("module.jwt.self.public_key", pubKey)
	baPublicKey := cfg.String("module.jwt.ba.public_key", "")

	k.selfPrivateKey, err = jwt.ParseRSAPrivateKeyFromPEM([]byte(selfPrivateKey))
	if err != nil {
		return err
	}
	k.selfPublicKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(selfPublicKey))
	if err != nil {
		return err
	}

	k.baPublicKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(baPublicKey))
	if err != nil {
		return err
	}

	apps, err := tables.BaAppAllKeys()
	if err != nil {
		return err
	}
	for _, app := range apps {
		myPriKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(app.PrivateKey))
		if err != nil {
			glog.Errorln("key manager: myPriKey ", app.Module, err)
			continue // 跳过
		}

		pubKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(app.PublicKeyClient))
		if err != nil {
			glog.Errorln("key manager: pubKey ", app.Module, err)
			continue // 跳过
		}

		keyModules = append(keyModules, app.Module)
		k.baKeys.Store(app.Module, &BaKeys{
			MyPrivateKey: myPriKey,
			AimPublicKey: pubKey,
		})
	}
	return nil
}

func (k *KeyManager) ModifyTokenHeader(header map[string]interface{}) map[string]interface{} {
	header["iss"] = baapi.ModuleName()
	return header
}

func (k *KeyManager) KeyFuncVerify(header map[string]interface{}) (interface{}, error) {
	iss, ok1 := header["iss"]
	aud, ok2 := header["aud"]
	if ok1 && ok2 && iss != "" && aud != "" {
		if iss == aud {
			return k.selfPublicKey, nil
		}
	}

	ks, ok := k.baKeys.Load(aud)
	if ok && ks != nil {
		keys := ks.(*BaKeys)
		if keys.MyPrivateKey != nil {
			return &keys.MyPrivateKey.PublicKey, nil
		}
	}

	return nil, jwt.ErrInvalidPubKey
}

func (k *KeyManager) KeyFuncSign(header map[string]interface{}) (interface{}, error) {

	iss, ok1 := header["iss"]
	aud, ok2 := header["aud"]
	if ok1 && ok2 && iss != "" && aud != "" {
		if iss == aud {
			if k.selfPrivateKey != nil {
				return k.selfPrivateKey, nil
			}
		} else {
			ks, ok := k.baKeys.Load(aud)
			if ok && ks != nil {
				keys := ks.(*BaKeys)
				if keys.MyPrivateKey != nil {
					return keys.MyPrivateKey, nil
				}
			}
		}
	}

	return nil, jwt.ErrInvalidPrivKey
}

func (k *KeyManager) Sign(isBA bool, moduleTo string, data string) (string, error) {
	if isBA {
		ks, ok := k.baKeys.Load(moduleTo)
		if ok && ks != nil {
			keys := ks.(*BaKeys)
			if keys.MyPrivateKey != nil {
				return jwt.RS512Sign(data, keys.MyPrivateKey)
			}
		}
	} else {
		return jwt.RS512Sign(string(data), k.selfPrivateKey)
	}
	return "", errors.New("sign fail")
}

func (k *KeyManager) Verify(isBA bool, moduleFrom string, data string, sign string) error {
	if isBA {
		ks, ok := k.baKeys.Load(moduleFrom)
		if ok && ks != nil {
			keys := ks.(*BaKeys)
			if keys.AimPublicKey != nil {
				return jwt.RS512Verify(data, sign, keys.AimPublicKey)
			}
		}
	} else {
		return jwt.RS512Verify(data, sign, k.baPublicKey)
	}
	return errors.New("verify fail")
}
