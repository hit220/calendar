// Package ws is to define a websocket server and client connect.
// Author: Arthur Zhang
// Create Date: 20190101
package ws

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/gorilla/websocket"
)

// ClientManager is a websocket manager
type WSClientManager struct {
	Clients    map[*WSClient]bool
	Register   chan *WSClient
	Unregister chan *WSClient

	broadcast chan []byte
}

// Client is a websocket client
type WSClient struct {
	ID     string
	Socket *websocket.Conn
	Send   chan []byte
	Recv   func(message []byte)
}

// Message is an object for websocket message which is mapped to json type
type WSMessage struct {
	Sender    string `json:"sender,omitempty"`
	Recipient string `json:"recipient,omitempty"`
	Datetiime string `json:"datetime,omitempty"`
	Action    string `json:"action,omitempty"`
	Content   string `json:"content,omitempty"`
}

type WSContent struct {
	Action string `json:"action,omitempty"`
	Data   string `json:"data,omitempty"`
}

// Manager define a ws server manager
var WSManager = WSClientManager{
	broadcast:  make(chan []byte),
	Register:   make(chan *WSClient),
	Unregister: make(chan *WSClient),
	Clients:    make(map[*WSClient]bool),
}

/*****************
	Serve(): 启动websocket服务
	Send():  向连接websocket的管道chan写入数据
	Read():  读取在websocket管道中的数据
	Write():  通过websocket协议向连接到ws的客户端发送数据
*****************/

// Start is to start a ws server
func (manager *WSClientManager) Serve(ctx context.Context) {
LABEL:
	for {
		select {
		case <-ctx.Done():
			for conn, _ := range manager.Clients {
				conn.Socket.Close()
				close(conn.Send)
			}
			break LABEL
		case conn := <-manager.Register:
			manager.Clients[conn] = true

			fmt.Println("manager.Register", conn.ID)

			manager.Send("", "register",
				[]byte("/A new socket has connected."), conn)
		case conn := <-manager.Unregister:

			fmt.Println("manager.Unregister", conn.ID)

			if _, ok := manager.Clients[conn]; ok {
				close(conn.Send)
				delete(manager.Clients, conn)

				manager.Send("", "unregister",
					[]byte("/A socket has disconnected."), conn)
			}
		case message := <-manager.broadcast:
			for conn := range manager.Clients {
				select {
				case conn.Send <- message:
				default:
					// 广播不要求100%到达率
				}
			}
		}
	}
}

// 发给所有人, 注意: 广播不要求100%到达率
func (manager *WSClientManager) Broadcast(action string, message []byte) {
	jsonMessage, _ := json.Marshal(&WSMessage{
		Datetiime: time.Now().Format("2006-01-02 15:04:05"),
		Action:    "cmd-" + action,
		Content:   string(message),
	})

	WSManager.broadcast <- jsonMessage
}

// 发给指定ID用户
func (manager *WSClientManager) Send(id, action string, message []byte, ignore *WSClient) {
	jsonMessage, _ := json.Marshal(&WSMessage{
		Sender:    id,
		Datetiime: time.Now().Format("2006-01-02 15:04:05"),
		Action:    "cmd-" + action,
		Content:   string(message),
	})

	for conn := range manager.Clients {
		if conn != ignore && (id == "" || conn.ID == id) {
			conn.Send <- jsonMessage
		}
	}
}

func NewWSClient(id string, conn *websocket.Conn) *WSClient {
	c := &WSClient{
		ID:     id,
		Socket: conn,
		Send:   make(chan []byte, 3),
	}

	go c.Read()
	go c.Write()
	return c
}

func (c *WSClient) Read() {
	defer func() {
		WSManager.Unregister <- c
		c.Socket.Close()
	}()

	for {
		_, message, err := c.Socket.ReadMessage()
		if err != nil {
			WSManager.Unregister <- c
			c.Socket.Close()
			break
		}

		if c.Recv != nil {
			c.Recv(message)
		}

	}
}

func (c *WSClient) Write() {
	defer func() {
		c.Socket.Close()
	}()

	for {
		select {
		case message, ok := <-c.Send:
			if !ok {
				c.Socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			c.Socket.WriteMessage(websocket.TextMessage, message)
		}
	}
}

func Serve(ctx context.Context) {
	WSManager.Serve(ctx)
}
