import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from 'vue-router';
import PageXError404 from '../pages/public/x-error-404/x-error-404';
import { AppStore } from '../app.store';
import { AppConfig } from '../app.config';
import PageExample from '../pages/public/example/example';
import PageTest from '../pages/public/test/test';
import PageBootstrap from '../pages/bootstrap/bootstrap';
import PageIconsDark from '../pages/public/icons-dark/icons';
import PageIconsLight from '../pages/public/icons-light/icons';
import { PagePlatformUI } from '../pages/app/platform-ui/platform';
import { PagePLUIHome } from '../pages/app/platform-ui/home/home';
import { PagePLUIButtons } from '../pages/app/platform-ui/ui-features/buttons/buttons';
import { PagePLUITypography } from '../pages/app/platform-ui/ui-features/typography/typography';
import { PagePLUIBasicElements } from '../pages/app/platform-ui/forms/basic_elements/basic_elements';
import { PagePLUIChartjs } from '../pages/app/platform-ui/charts/chartjs/chartjs';
import { PagePLUIBasicTable } from '../pages/app/platform-ui/tables/basic-table/basic-table';
import { PagePLUIThemify } from '../pages/app/platform-ui/icons/themify/themify';
import { PagePLUILogin } from '../pages/app/platform-ui/samples/login/login';
import { PagePLUILogin2 } from '../pages/app/platform-ui/samples/login-2/login-2';
import { PagePLUIRegister } from '../pages/app/platform-ui/samples/register/register';
import { PagePLUIRegister2 } from '../pages/app/platform-ui/samples/register-2/register-2';
import { PagePLUILockScreen } from '../pages/app/platform-ui/samples/lock-screen/lock-screen';
import { PagePLUIDocumentation } from '../pages/app/platform-ui/documentation/documentation/documentation';
import { PagePlatform } from '../pages/app/platform/platform';
import { PagePLHome } from '../pages/app/platform/home/home';
import { PagePLDetail } from '../pages/app/platform/detail/detail';
import { PagePLAppToList } from '../pages/app/platform/app-to/list/list';
import { PagePLAppList } from '../pages/app/platform/app/list/list';
import { PagePLAppAddOrUpdate } from '../pages/app/platform/app/add_or_update/add_or_update';
import { PagePLAppKeys } from '../pages/app/platform/app/keys/keys';
import { PagePLUserList } from '../pages/app/platform/user/list/list';
import { PagePLUserAddOrUpdate } from '../pages/app/platform/user/add_or_update/add_or_update';
import { PagePLAgentList } from '../pages/app/platform/agent/list/list';
import { PagePLAgentAddOrUpdate } from '../pages/app/platform/agent/add_or_update/add_or_update';
import { PagePLMasterList } from '../pages/app/platform/master/list/list';
import { PagePLMasterAddOrUpdate } from '../pages/app/platform/master/add_or_update/add_or_update';
import { PagePLRoleList } from '../pages/app/platform/role/list/list';
import { PagePLRoleAddOrUpdate } from '../pages/app/platform/role/add_or_update/add_or_update';
import { PagePLAuthPathList } from '../pages/app/platform/auth_path/list/list';
import { PagePLAuthPathAddOrUpdate } from '../pages/app/platform/auth_path/add_or_update/add_or_update';
import { PageHome } from '../pages/app/home/home';

const pageBaseName = ' - BA';

const routes: Array<RouteRecordRaw> = [
  { path: '', redirect: '/public/home' },

  /** 公共页面 **/
  { path: '/bootstrap', meta: { title: 'Bootstrap' + pageBaseName }, component: PageBootstrap },
  { path: '/public/example', meta: { title: 'Model example' + pageBaseName }, component: PageExample },
  { path: '/public/test', meta: { title: 'Test' + pageBaseName }, component: PageTest },
  { path: '/public/icons-dark', meta: { title: 'Dark icons' + pageBaseName }, component: PageIconsDark },
  { path: '/public/icons-light', meta: { title: 'Light icons' + pageBaseName }, component: PageIconsLight },
  { path: '/public/home', meta: { title: 'Home' + pageBaseName }, component: PageHome },

  /** PLATFORM 页面 **/
  {
    path: '/platform',
    component: PagePlatform,
    children: [
      { path: 'home', meta: { title: '平台首页' + pageBaseName }, component: PagePLHome },
      { path: 'detail', meta: { title: '记录详情' + pageBaseName }, component: PagePLDetail },
      { path: 'app-to/list', meta: { title: '应用导航' + pageBaseName }, component: PagePLAppToList },
      { path: 'app/list', meta: { title: '应用列表' + pageBaseName }, component: PagePLAppList },
      { path: 'app/add_or_update', meta: { title: '应用编辑' + pageBaseName }, component: PagePLAppAddOrUpdate },
      { path: 'app/keys', meta: { title: '应用密钥' + pageBaseName }, component: PagePLAppKeys },
      { path: 'user/list', meta: { title: '普通用户列表' + pageBaseName }, component: PagePLUserList },
      { path: 'user/add_or_update', meta: { title: '普通用户编辑' + pageBaseName }, component: PagePLUserAddOrUpdate },
      { path: 'agent/list', meta: { title: '代理列表' + pageBaseName }, component: PagePLAgentList },
      { path: 'agent/add_or_update', meta: { title: '代理编辑' + pageBaseName }, component: PagePLAgentAddOrUpdate },
      { path: 'master/list', meta: { title: '管理员列表' + pageBaseName }, component: PagePLMasterList },
      { path: 'master/add_or_update', meta: { title: '管理员编辑' + pageBaseName }, component: PagePLMasterAddOrUpdate },
      { path: 'role/list', meta: { title: '角色列表' + pageBaseName }, component: PagePLRoleList },
      { path: 'role/add_or_update', meta: { title: '角色编辑' + pageBaseName }, component: PagePLRoleAddOrUpdate },
      { path: 'auth_path/list', meta: { title: '权限路径列表' + pageBaseName }, component: PagePLAuthPathList },
      { path: 'auth_path/add_or_update', meta: { title: '权限路径编辑' + pageBaseName }, component: PagePLAuthPathAddOrUpdate },
      // ###### senpm-app
    ],
  },

  /** PLATFORM UI 页面 **/
  {
    path: '/platform-ui',
    component: PagePlatformUI,
    children: [
      { path: 'home', meta: { title: 'Home' + pageBaseName }, component: PagePLUIHome },
      { path: 'ui-features/buttons', meta: { title: 'Buttons' + pageBaseName }, component: PagePLUIButtons },
      { path: 'ui-features/typography', meta: { title: 'Typography' + pageBaseName }, component: PagePLUITypography },
      { path: 'forms/basic_elements', meta: { title: 'Forms' + pageBaseName }, component: PagePLUIBasicElements },
      { path: 'charts/chartjs', meta: { title: 'Charts' + pageBaseName }, component: PagePLUIChartjs },
      { path: 'tables/basic-table', meta: { title: 'Table' + pageBaseName }, component: PagePLUIBasicTable },
      { path: 'icons/themify', meta: { title: 'Icons' + pageBaseName }, component: PagePLUIThemify },
      { path: 'samples/login', meta: { title: 'Login' + pageBaseName }, component: PagePLUILogin },
      { path: 'samples/login-2', meta: { title: 'Login2' + pageBaseName }, component: PagePLUILogin2 },
      { path: 'samples/register', meta: { title: 'Register' + pageBaseName }, component: PagePLUIRegister },
      { path: 'samples/register-2', meta: { title: 'Register2' + pageBaseName }, component: PagePLUIRegister2 },
      { path: 'samples/lock-scree', meta: { title: 'LockScreen' + pageBaseName }, component: PagePLUILockScreen },
      { path: 'documentation/documentation', meta: { title: 'Documentation' + pageBaseName }, component: PagePLUIDocumentation },
      // ###### senpm-app
    ],
  },
  /** 控制台 **/
  /** 工作台 **/

  /* 1.所有页面name值不能重复, 否则将匹配到错误页面 */
  // --------------------------------
  /** 必须放最后 **/
  /** 公共页面   **/
  { path: '/:pathMatch(.*)*', meta: { title: 'not-found' }, component: PageXError404 },
];

// 修正vue路由bug, 同name路由无法访问
const router = createRouter({
  history: AppConfig.$useMao ? createWebHashHistory(process.env.BASE_URL) : createWebHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach(async (to: any) => {
  // 调用页面离开事件
  const yes = await AppConfig.clearOnLeavePage();
  if (!yes) {
    return false;
  }

  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }
  AppStore.commit('setRouters', { ...to });
  return true;
});

let routeFromRote = null;
export const getRouteFrom = () => routeFromRote;

router.beforeEach(async (to, from, next) => {
  // ...
  routeFromRote = from;

  // console.log('router.beforeEach ====> ', to, from, next);

  next();
});

export default router;
