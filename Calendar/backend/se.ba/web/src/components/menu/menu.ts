import { Options, Vue } from 'vue-class-component';

import { AppVue } from '../../app.vue';
import { defWeb3 } from '../../utils/web3';

@Options({
  style: require('./menu.scss'),
  template: require('./menu.html'),
})
export default class Menu extends AppVue {
  version = '';
  versionWeb3 = '';

  created() {}

  async mounted() {
    await defWeb3.checkInit();
    this.versionWeb3 = defWeb3.version();
  }
}
