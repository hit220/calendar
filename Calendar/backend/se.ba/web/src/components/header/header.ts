import { Options, Vue } from 'vue-class-component';

import { AppVue } from '../../app.vue';
import { defWeb3 } from '../../utils/web3';

@Options({
  style: require('./header.scss'),
  template: require('./header.html'),
})
export default class Header extends AppVue {
  async created() {
    defWeb3.checkRequestAccount();
  }
}
