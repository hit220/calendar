import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './basic_elements.scss';

@Options({
  template: require('./basic_elements.html'),
  name: 'PagePLUIBasicElements',
})
export class PagePLUIBasicElements extends AppVue {
  created() {}
  mounted() {}
}
