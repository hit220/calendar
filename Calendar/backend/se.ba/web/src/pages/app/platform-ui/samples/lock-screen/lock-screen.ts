import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './lock-screen.scss';

@Options({
  template: require('./lock-screen.html'),
  name: 'PagePLUILockScreen',
})
export class PagePLUILockScreen extends AppVue {
  created() {}
  mounted() {}
}
