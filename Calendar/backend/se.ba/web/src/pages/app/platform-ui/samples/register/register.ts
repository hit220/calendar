import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './register.scss';

@Options({
  template: require('./register.html'),
  name: 'PagePLUIRegister',
})
export class PagePLUIRegister extends AppVue {
  created() {}
  mounted() {}
}
