import { Options } from 'vue-class-component';
import { AppVue } from '../../../app.vue';
import { AppPLHeader } from './../platform/header/header';
import { AppPLUISlideMenu } from './slidemenu/slidemenu';
import './platform.scss';

@Options({
  template: require('./platform.html'),
  name: 'PagePlatform',
  components: {
    'app-pl-header': AppPLHeader,
    'app-pl-slidemenu': AppPLUISlideMenu,
  },
  props: {
    platformMenu: {
      type: Array,
    },
  },
})
export class PagePlatformUI extends AppVue {
  mounted() {
    // this. hrefToLogin();
    console.log('mounted');
    this.keepLive();
  }
  created() {
    console.log('created');
  }
  hrefToLogin() {}

  keepLive() {}
}
