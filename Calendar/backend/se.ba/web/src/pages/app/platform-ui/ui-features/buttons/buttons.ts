import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './buttons.scss';

@Options({
  template: require('./buttons.html'),
  name: 'PagePLUIButtons',
})
export class PagePLUIButtons extends AppVue {
  created() {}
  mounted() {}
}
