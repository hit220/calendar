import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './typography.scss';

@Options({
  template: require('./typography.html'),
  name: 'PagePLUITypography',
})
export class PagePLUITypography extends AppVue {
  created() {}
  mounted() {}
}
