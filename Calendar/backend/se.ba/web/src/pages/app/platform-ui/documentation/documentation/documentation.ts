import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './documentation.scss';

@Options({
  template: require('./documentation.html'),
  name: 'PagePLUIDocumentation',
})
export class PagePLUIDocumentation extends AppVue {
  created() {}
  mounted() {}
}
