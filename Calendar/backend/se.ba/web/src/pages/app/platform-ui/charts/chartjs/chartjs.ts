import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './chartjs.scss';
import { Chart } from 'chart.js';

@Options({
  template: require('./chartjs.html'),
  name: 'PagePLUIChartjs',
})
export class PagePLUIChartjs extends AppVue {
  created() {}

  mounted() {
    this.drawCharts();
  }

  drawCharts() {
    const data = {
      labels: ['2013', '2014', '2014', '2015', '2016', '2017'],
      datasets: [
        {
          label: '# of Votes',
          data: [10, 19, 3, 5, 2, 3],
          backgroundColor: 'rgba(255, 99, 132, 0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          fill: false,
        },
      ],
    };
    const multiLineData = {
      labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [
        {
          label: 'Dataset 1',
          data: [12, 19, 3, 5, 2, 3],
          borderColor: '#587ce4',
          borderWidth: 2,
          fill: false,
        },
        {
          label: 'Dataset 2',
          data: [5, 23, 7, 12, 42, 23],
          borderColor: '#ede190',
          borderWidth: 2,
          fill: false,
        },
        {
          label: 'Dataset 3',
          data: [15, 10, 21, 32, 12, 33],
          borderColor: '#f44252',
          borderWidth: 2,
          fill: false,
        },
      ],
    };
    const options = {
      scales: {
        y: {
          ticks: {
            z: 0,
          },
        },
      },
      legend: {
        display: false,
      },
      elements: {
        point: {
          radius: 0,
        },
      },
    };
    const doughnutPieData = {
      datasets: [
        {
          data: [30, 40, 30],
          backgroundColor: 'rgba(255, 99, 132, 0.5)',
          borderColor: 'rgba(255,99,132,1)',
        },
      ],

      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: ['Pink', 'Blue', 'Yellow'],
    };
    const doughnutPieOptions = {
      responsive: true,
      animation: {
        animateScale: true,
        animateRotate: true,
      },
    };
    const areaData = {
      labels: ['2013', '2014', '2015', '2016', '2017'],
      datasets: [
        {
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: 'rgba(255, 99, 132, 0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          fill: true, // 3: no fill
        },
      ],
    };

    const areaOptions = {
      plugins: {
        filler: {
          propagate: true,
        },
      },
    };

    const multiAreaData = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      datasets: [
        {
          label: 'Facebook',
          data: [8, 11, 13, 15, 12, 13, 16, 15, 13, 19, 11, 14],
          borderColor: 'rgba(255, 99, 132, 0.5)',
          backgroundColor: 'rgba(255, 99, 132, 0.5)',
          borderWidth: 1,
          fill: true,
        },
        {
          label: 'Twitter',
          data: [7, 17, 12, 16, 14, 18, 16, 12, 15, 11, 13, 9],
          borderColor: 'rgba(54, 162, 235, 0.5)',
          backgroundColor: 'rgba(54, 162, 235, 0.5)',
          borderWidth: 1,
          fill: true,
        },
        {
          label: 'Linkedin',
          data: [6, 14, 16, 20, 12, 18, 15, 12, 17, 19, 15, 11],
          borderColor: 'rgba(255, 206, 86, 0.5)',
          backgroundColor: 'rgba(255, 206, 86, 0.5)',
          borderWidth: 1,
          fill: true,
        },
      ],
    };

    const multiAreaOptions = {
      plugins: {
        filler: {
          propagate: true,
        },
      },
      elements: {
        point: {
          radius: 0,
        },
      },
      scales: {
        x: {
          grid: {
            display: false,
          },
        },

        y: {
          grid: {
            display: false,
          },
        },
      },
    };

    const scatterChartData = {
      datasets: [
        {
          label: 'First Dataset',
          data: [
            {
              x: -10,
              y: 0,
            },
            {
              x: 0,
              y: 3,
            },
            {
              x: -25,
              y: 5,
            },
            {
              x: 40,
              y: 5,
            },
          ],
          backgroundColor: 'rgba(255, 99, 132, 0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
        },
        {
          label: 'Second Dataset',
          data: [
            {
              x: 10,
              y: 5,
            },
            {
              x: 20,
              y: -30,
            },
            {
              x: -25,
              y: 15,
            },
            {
              x: -10,
              y: 5,
            },
          ],
          backgroundColor: 'rgba(54, 162, 235, 0.2)',
          borderColor: 'rgba(54, 162, 235, 1)',
          borderWidth: 1,
        },
      ],
    };

    // Get context with jQuery - using jQuery's .get() method.
    if (this.ge('barChart')) {
      const barChartCanvas = this.ge('barChart').getContext('2d');
      // This will get the first returned node in the jQuery collection.
      const barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: data,
        options: {
          scales: {
            x: {
              type: 'linear',
              position: 'bottom',
            },
          },
        },
      });
    }

    if (this.ge('lineChart')) {
      const lineChartCanvas = this.ge('lineChart').getContext('2d');
      const lineChart = new Chart(lineChartCanvas, {
        type: 'line',
        data: data,
        options: options,
      });
    }

    if (this.ge('linechart-multi')) {
      const multiLineCanvas = this.ge('linechart-multi').getContext('2d');
      const lineChart = new Chart(multiLineCanvas, {
        type: 'line',
        data: multiLineData,
        options: options,
      });
    }

    if (this.ge('areachart-multi')) {
      const multiAreaCanvas = this.ge('areachart-multi').getContext('2d');
      const multiAreaChart = new Chart(multiAreaCanvas, {
        type: 'line',
        data: multiAreaData,
        options: multiAreaOptions,
      });
    }

    if (this.ge('doughnutChart')) {
      const doughnutChartCanvas = this.ge('doughnutChart').getContext('2d');
      const doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: doughnutPieData,
        options: doughnutPieOptions,
      });
    }

    if (this.ge('pieChart')) {
      const pieChartCanvas = this.ge('pieChart').getContext('2d');
      const pieChart = new Chart(pieChartCanvas, {
        type: 'pie',
        data: doughnutPieData,
        options: doughnutPieOptions,
      });
    }

    if (this.ge('areaChart')) {
      const areaChartCanvas = this.ge('areaChart').getContext('2d');
      const areaChart = new Chart(areaChartCanvas, {
        type: 'line',
        data: areaData,
        options: areaOptions,
      });
    }

    if (this.ge('scatterChart')) {
      const scatterChartCanvas = this.ge('scatterChart').getContext('2d');
      const scatterChart = new Chart(scatterChartCanvas, {
        type: 'scatter',
        data: scatterChartData,
        options: {
          scales: {
            x: {
              type: 'linear',
              position: 'bottom',
            },
          },
        },
      });
    }

    if (this.ge('browserTrafficChart')) {
      const browserTrafficData = null;
      const doughnutChartCanvas = this.ge('browserTrafficChart').getContext('2d');
      const doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: browserTrafficData,
        options: doughnutPieOptions,
      });
    }
  }

  ge(id): HTMLCanvasElement {
    return document.getElementById(id) as HTMLCanvasElement;
  }
}
