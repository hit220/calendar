import { Options } from 'vue-class-component';
import { AppVue } from '../../../../app.vue';
import './home.scss';
import { Chart } from 'chart.js';
import { htmlLegendPlugin } from '../../../../utils/chart.js';

@Options({
  template: require('./home.html'),
  name: 'PagePLHome',
  props: {
    platformMenu: {
      type: Array,
    },
  },
})
export class PagePLUIHome extends AppVue {
  created() {}

  async mounted() {
    this.drawChart1();
    this.drawChart2();
    this.drawChart3();
  }

  /*-----------------*/
  drawChart1() {
    if (!this.$refs.orderChart) {
      return;
    }

    const areaData = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
      datasets: [
        {
          data: [175, 200, 130, 210, 40, 60, 25],
          backgroundColor: 'rgba(255, 193, 2, .8)',
          borderColor: 'transparent',
          borderWidth: 3,
          fill: 'origin',
          label: 'services',
        },
        {
          data: [175, 145, 190, 130, 240, 160, 200],
          backgroundColor: 'rgba(245, 166, 35, 1)',
          borderColor: 'transparent',
          borderWidth: 3,
          fill: 'origin',
          label: 'purchases',
        },
      ],
    };
    const areaOptions = {
      responsive: true,
      maintainAspectRatio: true,
      plugins: {
        filler: {
          propagate: false,
        },
      },
      scales: {
        x: {
          display: false,
          ticks: {
            display: true,
          },
          grid: {
            display: false,
            drawBorder: false,
            color: 'transparent',
            zeroLineColor: '#eeeeee',
          },
        },
        y: {
          display: false,
          ticks: {
            display: true,
            autoSkip: false,
            maxRotation: 0,
            stepSize: 100,
            min: 0,
            max: 260,
          },
          grid: {
            drawBorder: false,
          },
        },
      },
      legend: {
        display: false,
      },
      tooltips: {
        enabled: true,
      },
      elements: {
        line: {
          tension: 0.45,
        },
        point: {
          radius: 0,
        },
      },
    };

    const salesChartCanvas = this.$refs.orderChart as HTMLCanvasElement;
    const cc = new Chart(salesChartCanvas.getContext('2d'), {
      type: 'line',
      data: areaData,
      options: areaOptions,
    });
  }

  /*-------------------*/
  drawChart2() {
    if (!this.$refs.salesChart) {
      return;
    }

    const salesChartCanvas = this.$refs.salesChart as HTMLCanvasElement;
    const salesChart = new Chart(salesChartCanvas.getContext('2d'), {
      type: 'bar',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
        datasets: [
          {
            label: 'Offline Sales',
            data: [480, 230, 470, 210, 330],
            backgroundColor: '#8EB0FF',
          },
          {
            label: 'Online Sales',
            data: [400, 340, 550, 480, 170],
            backgroundColor: '#316FFF',
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 20,
            bottom: 0,
          },
        },
        scales: {
          y: {
            display: true,
            grid: {
              display: false,
              drawBorder: false,
            },
            ticks: {
              display: false,
              minRotation: 0,
              maxRotation: 500,
            },
          },
          x: {
            stacked: false,
            ticks: {
              z: 0,
              color: '#9fa0a2',
            },
            grid: {
              color: 'rgba(0, 0, 0, 0)',
              display: false,
            },
          },
        },
        htmlLegend: {
          // ID of the container to put the legend in
          container: this.$refs.salesLegend,
        },
        legend: {
          display: false,
        },
        elements: {
          point: {
            radius: 0,
          },
        },
      } as any,
      plugins: [htmlLegendPlugin],
    });
  }

  /*-------------------*/
  drawChart3() {
    if (!this.$refs.northAmericaChart) {
      return;
    }

    const areaData = {
      labels: ['Jan', 'Feb', 'Mar'],
      datasets: [
        {
          data: [100, 50, 50],
          backgroundColor: ['#71c016', '#8caaff', '#248afd'],
          borderColor: 'rgba(0,0,0,0)',
        },
      ],
    };
    const areaOptions = {
      responsive: true,
      maintainAspectRatio: true,
      segmentShowStroke: false,
      cutoutPercentage: 78,
      elements: {
        arc: {
          borderWidth: 4,
        },
      },
      htmlLegend: {
        // ID of the container to put the legend in
        container: this.$refs.northAmericaLegend,
      },
      legend: {
        display: false,
      },
      tooltips: {
        enabled: true,
      },
      legendCallback: function (chart) {
        const text = [];
        text.push('<div class="report-chart">');
        text.push(
          '<div class="d-flex justify-content-between mx-4 mx-xl-5 mt-3"><div class="d-flex align-items-center"><div class="mr-3" style="width:20px; height:20px; border-radius: 50%; background-color: ' +
            chart.data.datasets[0].backgroundColor[0] +
            '"></div><p class="mb-0">Offline sales</p></div>',
        );
        text.push('<p class="mb-0">22789</p>');
        text.push('</div>');
        text.push(
          '<div class="d-flex justify-content-between mx-4 mx-xl-5 mt-3"><div class="d-flex align-items-center"><div class="mr-3" style="width:20px; height:20px; border-radius: 50%; background-color: ' +
            chart.data.datasets[0].backgroundColor[1] +
            '"></div><p class="mb-0">Online sales</p></div>',
        );
        text.push('<p class="mb-0">94678</p>');
        text.push('</div>');
        text.push(
          '<div class="d-flex justify-content-between mx-4 mx-xl-5 mt-3"><div class="d-flex align-items-center"><div class="mr-3" style="width:20px; height:20px; border-radius: 50%; background-color: ' +
            chart.data.datasets[0].backgroundColor[2] +
            '"></div><p class="mb-0">Returns</p></div>',
        );
        text.push('<p class="mb-0">12097</p>');
        text.push('</div>');
        text.push('</div>');
        return text.join('');
      },
    };
    const northAmericaChartPlugins = {
      id: 'northAmericaChart',
      beforeDraw: function (chart) {
        const width = chart.width,
          height = chart.height,
          ctx = chart.ctx;

        ctx.restore();
        const fontSize = 3.125;
        ctx.font = '600 ' + fontSize + 'em sans-serif';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = '#000';

        const text = '63',
          textX = Math.round((width - ctx.measureText(text).width) / 2),
          textY = height / 2;

        ctx.fillText(text, textX, textY);
        ctx.save();
      },
    };
    const northAmericaChartCanvas = this.$refs.northAmericaChart as HTMLCanvasElement;
    const northAmericaChart = new Chart(northAmericaChartCanvas.getContext('2d'), {
      type: 'doughnut',
      data: areaData,
      options: areaOptions,
      plugins: [northAmericaChartPlugins, htmlLegendPlugin],
    });
  }
}
