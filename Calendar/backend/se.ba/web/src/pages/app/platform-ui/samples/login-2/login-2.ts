import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './login-2.scss';

@Options({
  template: require('./login-2.html'),
  name: 'PagePLUILogin2',
})
export class PagePLUILogin2 extends AppVue {
  created() {}
  mounted() {}
}
