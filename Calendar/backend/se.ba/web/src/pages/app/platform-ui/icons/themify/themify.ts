import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './themify.scss';

@Options({
  template: require('./themify.html'),
  name: 'PagePLUIThemify',
})
export class PagePLUIThemify extends AppVue {
  created() {}
  mounted() {}
}
