import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './basic-table.scss';

@Options({
  template: require('./basic-table.html'),
  name: 'PagePLUIBasicTable',
})
export class PagePLUIBasicTable extends AppVue {
  created() {}
  mounted() {}
}
