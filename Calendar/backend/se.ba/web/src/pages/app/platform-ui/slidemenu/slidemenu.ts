import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../app.config';
import { AppNet } from '../../../../app.net';
import { AppVue } from '../../../../app.vue';
import './slidemenu.scss';

@Options({
  template: require('./slidemenu.html'),
  name: 'AppPLSlideMenu',
  props: {
    platformMenu: {
      type: Array,
    },
  },
})
export class AppPLUISlideMenu extends AppVue {
  created() {}
  mounted() {}
}
