import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './login.scss';

@Options({
  template: require('./login.html'),
  name: 'PagePLUILogin',
})
export class PagePLUILogin extends AppVue {
  created() {}
  mounted() {}
}
