import { Options } from 'vue-class-component';
import { AppVue } from '../../../../../app.vue';
import './register-2.scss';

@Options({
  template: require('./register-2.html'),
  name: 'PagePLUIRegister2',
})
export class PagePLUIRegister2 extends AppVue {
  created() {}
  mounted() {}
}
