import { Options } from 'vue-class-component';
import { AppNet } from '../../../../../app.net';
import { AppVue } from '../../../../../app.vue';
import './keys.scss';
import ClipboardJS from 'clipboard';

@Options({
  template: require('./keys.html'),
  name: 'PagePLDetail',
})
export class PagePLAppKeys extends AppVue {
  itemID = '';
  item = null;

  created() {}

  async mounted() {
    this.itemID = this.$route.query.id as string;

    this.getData();
  }

  async getData() {
    if (!this.itemID) {
      this.toast('无法查询记录信息');
      return;
    }
    const res = await AppNet.get('/api/master/v1/app/detail', {
      id: this.itemID,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    this.item = res.data;
  }

  onCopy(id, clickId) {
    const text = document.getElementById(id)?.innerText;
    const cp = new ClipboardJS('#' + clickId, {
      text: (obj) => {
        return text;
      },
    });
    cp.on('success', (e) => {
      e.clearSelection();
      this.toast('复制成功:' + this.xstr(e.text, 20, 30), 'success');
    });
  }
}
