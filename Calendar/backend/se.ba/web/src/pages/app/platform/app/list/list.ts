import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../../app.config';
import { AppNet } from '../../../../../app.net';
import { AppUtil } from '../../../../../app.util';
import { AppVue } from '../../../../../app.vue';
import './list.scss';

@Options({
  template: require('./list.html'),
  name: 'PagePLAppList',
})
export class PagePLAppList extends AppVue {
  reqData = [];
  dataForce = false;
  pgn = null; // 分页导航栏

  created() {}

  async mounted() {
    this.getData();
  }

  async getData() {
    const res = await AppNet.get('/api/master/v1/app/list');
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    this.reqData = res.data.data;
    this.pgn = AppUtil.getPagination(res.data);
  }

  fruitPath(id) {
    if (id < 1) {
      id = 1;
    }
    id = id % 49;
    return 'svg/fruits/' + id + '.svg';
  }

  async forbid(item) {
    const p = {
      id: item.id,
      status: item.status === 10 ? '1' : '0',
    };
    const data = await AppNet.post('/api/master/v1/app/forbid', this.strParams(p));
    if (!data || data.code !== 'success') {
      this.alert(data ? data.message : '操作失败');
      return;
    }

    this.getData();
  }

  async loginTo(item) {
    // console.log(this.selectUser, this.selectApp);
    const sess = await AppConfig.getSession();

    const data = await AppNet.get('/api/master/v1/app/login_to', {
      type: 'master',
      user_id: sess.id,
      app_id: item.id,
    });
    if (data.code !== 'success') {
      this.alert(data.message);
      return;
    }

    this.hrefBlank(data.data.url);
  }
}
