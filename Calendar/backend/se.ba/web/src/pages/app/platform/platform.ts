import { AppVue } from '../../../app.vue';
import { AppPLHeader } from './header/header';
import { AppPLSlideMenu } from './slidemenu/slidemenu';
import './platform.scss';
import { Options } from 'vue-class-component';

@Options({
  template: require('./platform.html'),
  name: 'PagePlatform',
  components: {
    'app-pl-header': AppPLHeader,
    'app-pl-slidemenu': AppPLSlideMenu,
  },
  props: {
    platformMenu: {
      type: Array,
    },
  },
})
export class PagePlatform extends AppVue {
  mounted() {
    this.keepLive();
  }
  created() {}

  hrefToLogin() {
    if (!(this as any).$store.state.isLogin) {
      // this.showNeedLogin();
      this.hrefTo('/public/home');
    }
  }

  keepLive() {}
}
