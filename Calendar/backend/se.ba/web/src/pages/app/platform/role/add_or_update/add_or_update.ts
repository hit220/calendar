import { Options } from 'vue-class-component';
import { AppNet } from '../../../../../app.net';
import { AppVue } from '../../../../../app.vue';
import { uploadFileByAccept } from '../../../../../utils/upload';
import './add_or_update.scss';

@Options({
  template: require('./add_or_update.html'),
  name: 'PagePLRoleAddOrUpdate',
})
export class PagePLRoleAddOrUpdate extends AppVue {
  itemId = '';

  modelTitle = '创建';
  modelButton = '保存';

  form = {
    id: '',
    name: '',
    auth_paths: '',
    user_type: '',
  };

  created() {}

  async mounted() {
    // console.log('query', this.$route.query);
    this.itemId = this.$route.query.id as string;

    this.getData();
  }

  async getData() {
    if (!this.itemId) {
      return;
    }
    const res = await AppNet.get('/api/master/v1/role/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;

    this.modelTitle = '编辑';
    this.modelButton = '保存';

    this.form.id = '' + v.id;
    this.form.name = v.name;
    this.form.auth_paths = v.auth_paths;
    this.form.user_type = v.user_type;
  }

  async submitFormEdit() {
    const res = await AppNet.post('/api/master/v1/role/add_or_update', this.strParams(this.form));
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : '操作失败');
      return;
    }

    this.toast(res.message, 'success', 1500);

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }
    this.getData();
  }
}
