import { Options } from 'vue-class-component';
import { AppNet } from '../../../../../app.net';
import { AppVue } from '../../../../../app.vue';
import { uploadFileByAccept } from '../../../../../utils/upload';
import './add_or_update.scss';

@Options({
  template: require('./add_or_update.html'),
  name: 'PagePLAppAddOrUpdate',
})
export class PagePLAppAddOrUpdate extends AppVue {
  itemId = '';

  modelTitle = '创建';
  modelButton = '保存';

  form = {
    id: '',
    name: '',
    module: '',
    head_image: '',
    data_url: '',
    visit_url_master: '',
    visit_url_enterprise: '',
    visit_url_user: '',
    visit_url_agent: '',
    visit_url_developer: '',
    description: '',
    ips: '',
    public_key_client: '',
  };

  created() {}

  async mounted() {
    // console.log('query', this.$route.query);
    this.itemId = this.$route.query.id as string;

    this.getData();
  }

  async getData() {
    if (!this.itemId) {
      return;
    }
    const res = await AppNet.get('/api/master/v1/app/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;

    this.modelTitle = '编辑';
    this.modelButton = '保存';

    this.form.id = '' + v.id;
    this.form.name = v.name;
    this.form.module = v.module;
    this.form.head_image = v.head_img;
    this.form.data_url = v.data_url;
    this.form.visit_url_master = v.visit_url_master;
    this.form.visit_url_enterprise = v.visit_url_enterprise;
    this.form.visit_url_user = v.visit_url_user;
    this.form.visit_url_agent = v.visit_url_agent;
    this.form.visit_url_developer = v.visit_url_developer;
    this.form.description = v.description;
    this.form.ips = v.ips;
    this.form.public_key_client = v.public_key_client;
  }

  async submitFormEdit() {
    const res = await AppNet.post('/api/master/v1/app/add_or_update', this.strParams(this.form));
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : '操作失败');
      return;
    }

    this.toast(res.message, 'success', 1500);

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }
    this.getData();
  }

  /***************** 上传 */
  loading = false;
  uploadFile() {
    uploadFileByAccept('/api/upload', 'image/*', {
      onStart: (file, formData) => {
        this.loading = true;
      },
      onFinish: (err, re) => {
        this.loading = false;

        if (err) {
          this.alert(err);
          return;
        }

        this.form.head_image = re.uuid;
      },
    });
  }
}
