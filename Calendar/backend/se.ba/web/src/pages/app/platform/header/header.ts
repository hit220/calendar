import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../app.config';
import { AppNet } from '../../../../app.net';
import { AppStore } from '../../../../app.store';
import { AppVue } from '../../../../app.vue';
import './header.scss';

@Options({
  template: require('./header.html'),
  name: 'AppPLHeader',
  props: {
    platformMenu: {
      type: Array,
    },
  },
})
export class AppPLHeader extends AppVue {
  created() {
    this.updateCollapsed();
  }

  mounted() {
    this.checkLogin();
  }

  async onLogout() {
    await AppConfig.setSession(null);
    await AppConfig.setSessToken(null);
    console.log('登出成功');

    this.hrefTo('/public/home');
  }

  onSetting() {}

  onNavbarToggler() {
    AppStore.commit('toggleCollapsed');
    this.updateCollapsed();
  }

  updateCollapsed() {
    if (this.$store.state.collapsed) {
      document.body.classList.add('sidebar-icon-only');
    } else {
      document.body.classList.remove('sidebar-icon-only');
    }
  }

  async checkLogin() {
    if (!this.$store.state.sess) {
      this.toast('未登录', 'info', 2000);
      this.hrefTo('/public/home');
      return;
    }

    const res = await AppNet.get('/api/master/v1/hello');
    if (res && res.code === 'nologin') {
      this.toast(res.message, 'info', 2000);
      this.hrefTo('/public/home');
    }
  }
}
