import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../../app.config';
import { AppNet } from '../../../../../app.net';
import { AppUtil } from '../../../../../app.util';
import { AppVue } from '../../../../../app.vue';
import './list.scss';

@Options({
  template: require('./list.html'),
  name: 'PagePLAgentList',
})
export class PagePLAgentList extends AppVue {
  reqData = [];
  dataForce = false;
  pgn = null; // 分页导航栏

  created() {}

  async mounted() {
    this.getData();
  }

  async getData() {
    const res = await AppNet.get('/api/master/v1/agent/list');
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    this.reqData = res.data.data;
    this.pgn = AppUtil.getPagination(res.data);
  }
}
