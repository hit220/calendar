import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../../app.config';
import { AppNet } from '../../../../../app.net';
import { AppUtil } from '../../../../../app.util';
import { AppVue } from '../../../../../app.vue';
import './list.scss';

@Options({
  template: require('./list.html'),
  name: 'PagePLAuthPathList',
})
export class PagePLAuthPathList extends AppVue {
  reqData = [];
  dataForce = false;
  pgn = null; // 分页导航栏

  isUpdateAuth = false;

  // websocket object
  ws: WebSocket = null;

  upMessages = [];

  created() {}

  async mounted() {
    this.getData();
  }

  async getData() {
    const res = await AppNet.get('/api/master/v1/auth_path/list');
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    this.reqData = res.data.data;
    this.pgn = AppUtil.getPagination(res.data);
  }
  async updateAuthList() {
    this.isUpdateAuth = !this.isUpdateAuth;
    if (this.isUpdateAuth) {
      if (this.ws) {
        return;
      }

      const sessToken = await AppConfig.getSessToken();
      if (!sessToken) {
        return;
      }
      const params = {};
      params[sessToken.key] = sessToken.value;

      let wsUrl = AppConfig.getBaseUrl() + '/api/master/v1/auth_path/ws_sync_routers';
      wsUrl = AppUtil.updateUrl(wsUrl, params);
      wsUrl = wsUrl.replace('https', 'ws').replace('http', 'ws');

      console.log('open', wsUrl);
      const ws = new WebSocket(wsUrl);
      ws.onclose = (e) => console.log('onclose: ', e);
      ws.onopen = (e) => console.log('onopen: ', e);
      ws.onerror = (e) => {
        console.log('onerror: ', e.AT_TARGET.toString());
        ws.close();
      };
      ws.onmessage = (e) => {
        this.onUpdate(e.data);
      };

      this.ws = ws;
    } else {
      if (this.ws) {
        this.ws.close();
        this.ws = null;
      }
    }
  }
  onUpdate(m) {
    const message = this.messageDecode(m);
    if (!message) {
      console.log('message解析错误: ', m, message);
      return;
    }
    this.upMessages.push(message);
    if (this.upMessages.length > 20) {
      this.upMessages.unshift();
    }

    if (message.action === 'cmd-update-over') {
      this.getData();
    }

    this.scrollToBottom('auth-update-log-box');
  }
  messageDecode(m) {
    try {
      return JSON.parse(m);
    } catch (e) {}
    return null;
  }
}
