import { Options } from 'vue-class-component';
import { AppNet } from '../../../../../app.net';
import { AppVue } from '../../../../../app.vue';
import { uploadFileByAccept } from '../../../../../utils/upload';
import './add_or_update.scss';

@Options({
  template: require('./add_or_update.html'),
  name: 'PagePLUserAddOrUpdate',
})
export class PagePLUserAddOrUpdate extends AppVue {
  itemId = '';

  modelTitle = '创建';
  modelButton = '保存';

  form = {
    id: '',
    rid: '0',
    name: '',
    phone: '',
    head_image: '',
    address: '',
    email: '',
    password: '',
    username: '',
  };

  created() {}

  async mounted() {
    // console.log('query', this.$route.query);
    this.itemId = this.$route.query.id as string;

    this.getData();
  }

  async getData() {
    if (!this.itemId) {
      return;
    }
    const res = await AppNet.get('/api/master/v1/user/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;

    this.modelTitle = '编辑';
    this.modelButton = '保存';

    this.form.id = '' + v.id;
    this.form.rid = '' + v.rid;
    this.form.name = v.name;
    this.form.username = v.username;
    this.form.head_image = v.head_image;
    this.form.email = v.email;
    this.form.phone = v.phone;
    this.form.address = v.address;
  }

  async submitFormEdit() {
    const res = await AppNet.post('/api/master/v1/user/add_or_update', this.strParams(this.form));
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : '操作失败');
      return;
    }

    this.toast(res.message, 'success', 1500);

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }
    this.getData();
  }

  /***************** 上传 */
  loading = false;
  uploadFile() {
    uploadFileByAccept('', 'image/*', {
      onStart: (file, formData) => {
        this.loading = true;
      },
      onFinish: (err, re) => {
        this.loading = false;

        if (err) {
          this.alert(err);
          return;
        }

        this.form.head_image = re.uuid;
      },
    });
  }
}
