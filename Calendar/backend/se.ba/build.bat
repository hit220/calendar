:: 删除目录
rd /s /q _build
md _build

:: 编译主程序
set GOOS=linux
set GOARCH=amd64
::go build -ldflags "-s -w" -o "_build/se.ba"
go build -o "_build/se.ba"

:: 构建目录
::echo F | xcopy /y /c /q .\conf\app_svr.conf .\_build\conf\app.conf
::echo F | xcopy /y /c /q .\conf\default_svr.conf .\_build\conf\default.conf
xcopy /y /e /c /i /q .\static .\_build\static
xcopy /y /e /c /i /q .\views .\_build\views
