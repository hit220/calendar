package glog

import (
	"os"
	"syscall"
	//"log"
)

func RecordFatal(file string) {
	logFile, err := os.OpenFile(EnsureFilePath(file), os.O_CREATE|os.O_APPEND|os.O_WRONLY|os.O_SYNC, 0660)
	if err != nil {
		println(err.Error())
		return
	}
	syscall.Dup2(int(logFile.Fd()), int(os.Stderr.Fd()))
}
