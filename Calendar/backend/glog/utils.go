package glog

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strings"
)

func GetCurrentDir() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		c, _ := os.Getwd()
		return c
	}
	return filepath.ToSlash(dir)
}

func GetAbsolutePath(path string) string {
	if strings.Index(path, "./") == 0 || strings.Index(path, ".\\") == 0 {
		r, _ := filepath.Abs(GetCurrentDir() + path[1:])
		return filepath.ToSlash(r)
	}
	r, _ := filepath.Abs(path)
	return filepath.ToSlash(r)
}

func GetDir(path string) string {
	if strings.Index(path, "./") == 0 || strings.Index(path, ".\\") == 0 {
		return filepath.ToSlash(filepath.Dir(GetCurrentDir() + path[1:]))
	}
	return filepath.ToSlash(filepath.Dir(path))
}

func EnsureDir(dir string) string {
	fullDir := GetAbsolutePath(dir)
	if IsExists(fullDir) {
		return fullDir
	}

	os.MkdirAll(fullDir, 777)
	return fullDir
}

func EnsureFilePath(filePath string) string {
	filePath = GetAbsolutePath(filePath)
	EnsureDir(GetDir(filePath))
	return filePath
}

func IsExists(path string) bool {
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

func StructToString(obj interface{}) string {
	if s, ok := obj.(string); ok {
		return s
	}
	if s, ok := obj.(*string); ok {
		return *s
	}
	if s, ok := obj.([]byte); ok {
		n := len(s)
		if n > 0 && s[0] == '{' && s[n-1] == '}' {
			return string(s)
		}
	}
	res, err := json.MarshalIndent(obj, "", "  ")
	if err != nil {
		Errorln(err)
	}
	s := string(res)
	return s
}
