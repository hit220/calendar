module seasonalcal.life/rechy/calendar

go 1.17

replace seasonalcal.life/rechy/glog => ./../../../../seasonalcal.life/rechy/glog

replace seasonalcal.life/rechy/config => ./../../../../seasonalcal.life/rechy/config

replace seasonalcal.life/rechy/lib => ./../../../../seasonalcal.life/rechy/lib

replace seasonalcal.life/rechy/se.ba.api => ./../../../../seasonalcal.life/rechy/se.ba.api

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/go-sql-driver/mysql v1.6.0
	github.com/hunzsig/graphics v0.0.0-20190929035859-45587367f25f
	github.com/satori/go.uuid v1.2.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/takama/daemon v1.0.0
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
	seasonalcal.life/rechy/config v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/glog v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/lib v0.0.0-00010101000000-000000000000
	seasonalcal.life/rechy/se.ba.api v0.0.0-00010101000000-000000000000
	xorm.io/xorm v1.2.5
)

require (
	github.com/bmatcuk/doublestar/v2 v2.0.4 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/goccy/go-json v0.7.4 // indirect
	github.com/gofrs/flock v0.8.0 // indirect
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/gookit/color v1.2.9 // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/ugorji/go/codec v1.1.7 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	xorm.io/builder v0.3.9 // indirect
)
