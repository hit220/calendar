package main

import (
	"fmt"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/keys"
	"seasonalcal.life/rechy/calendar/router"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/calendar/utils"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
)

const (
	srvName        = "Calendar Manager"
	srvDescription = "Calendar admin manage systtem"
)

var (
	ServicePort int = 8121
)

func (app *exitApp) main() {
	defer app.exit()

	utils.Init()

	xormDebug := cfg.Bool("debug.xorm", true)
	httpDebug := cfg.Bool("debug.http", true)

	_, err := tables.DBInit(xormDebug)
	if err != nil {
		glog.Errorln("Database connect fail:", err)
		return
	}
	defer tables.DBClose()
	glog.Infoln("Database connect success")

	kvDBPath := cfg.String("kv_db", "./_data/kv_cache")
	kvDBPath = lib.EnsureDir(kvDBPath)
	_, err = tables.KVDBInit(kvDBPath)
	if err != nil {
		glog.Errorln("KVDB库初始化失败:", err)
		return
	}
	defer tables.KVDBClose()

	/************init keys*************/
	kmr := keys.GetKeyManager()
	if kmr == nil {
		glog.Errorln("Key manager init success")
		return
	}

	ServicePort = cfg.Int("port", ServicePort)
	if httpDebug {
		gin.SetMode(gin.DebugMode)
		// gin.DefaultWriter = ioutil.Discard
	} else {
		gin.SetMode(gin.ReleaseMode)
		gin.DefaultWriter = ioutil.Discard
	}

	e := gin.Default()

	// init routes
	router.Init(e)

	err = e.Run(fmt.Sprintf(":%v", ServicePort))
	if err != nil {
		glog.Errorln("Server exit by exception:", err)
		return
	}

}
