const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionWebpackPlugin = require('compression-webpack-plugin');

module.exports = {
    // 公共路径(必须有的)
    publicPath: "./",
    // 输出文件目录
    outputDir: "www",
    // 静态资源存放的文件夹(相对于ouputDir)
    assetsDir: "",
    // eslint-loader 是否在保存的时候检查(果断不用，这玩意儿我都没装)
    // lintOnSave:false,
    // 我用的only，打包后小些
    //runtimeCompiler: false,
    // productionSourceMap: false, // 不需要生产环境的设置false可以减小dist文件大小，加速构建
    runtimeCompiler: true,
    configureWebpack: { // 这里是webpack的配置
        entry: {
            polyfill: '@babel/polyfill',
            app: './src/main.ts',
        },
        module: {
            rules: [
                { test: /\.html$/, loader: 'html-loader', exclude: '/node_modules/' },
                { test: /\.css$/, loader: 'sass-loader', exclude: '/node_modules/' },
                { test: /\.scss$/, loader: 'sass-loader', exclude: '/node_modules/' },
            ],
        },
        resolve: { // 这里是指定引入模块省略扩展名时的查找顺序
            extensions: ['.ts', '.js'],
        },
        plugins: [
            new HtmlWebpackPlugin({
                title: '欢迎访问',
                filename: 'index.html',
                template: 'src/index.html', // 使用src/index.html替换webpack默认index.html
            }),
            new CompressionWebpackPlugin({
                test: /\.js$|\.html$|\.css/,
                threshold: 8192,
                deleteOriginalAssets: false, // 是否删除原文件
                exclude: /.map$/,
                algorithm: "gzip",
            }),
        ],
        optimization: {
            splitChunks: {
                cacheGroups: {
                    common: {
                        name: 'chunk-common', // 打包后的文件名
                        chunks: 'initial', // 
                        minChunks: 1,
                        maxInitialRequests: 5,
                        minSize: 0,
                        priority: 1,
                        reuseExistingChunk: true
                    },
                    vendors: {
                        name: 'chunk-vendors',
                        test: /[\\/]node_modules[\\/]/,
                        chunks: 'initial',
                        priority: 2,
                        reuseExistingChunk: true,
                        enforce: true
                    },
                    vue: {
                        name: 'chunk-vue',
                        test: /[\\/]node_modules[\\/]vue/,
                        chunks: 'initial',
                        priority: 10,
                        reuseExistingChunk: true,
                        enforce: true
                    },
                    bootstrap: {
                        name: 'chunk-bootstrap',
                        test: /[\\/]node_modules[\\/](?:bootstrap|popper.js)/,
                        chunks: 'initial',
                        priority: 11,
                        reuseExistingChunk: true,
                        enforce: true
                    }
                }
            }
        }
    },
}
