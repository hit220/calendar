const data = {
    'Language': 'Language',
};

export const LangEn = {
    name: 'English',
    type: 'en',
    lang: (key: string) => {
        return data[key] ? data[key] : '';
    }
};