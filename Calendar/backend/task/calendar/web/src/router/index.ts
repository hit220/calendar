import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from 'vue-router';
import PageXError404 from '../pages/public/x-error-404/x-error-404';
import { AppStore } from '../app.store';
import { AppConfig } from '../app.config';
import PageExample from '../pages/public/example/example';
import PageTest from '../pages/public/test/test';
import PageBootstrap from '../pages/bootstrap/bootstrap';
import PageIconsDark from '../pages/public/icons-dark/icons';
import PageIconsLight from '../pages/public/icons-light/icons';
import { PageHome } from '../pages/app/home/home';
import { PagePlatform } from '../pages/app/platform/platform';
import { PagePLHome } from '../pages/app/platform/home/home';
import PagePLUserList from '../pages/app/platform/user/user-list/user-list';
import PagePLCalendarList from '../pages/app/platform/calendar/calendar-list/calendar-list';
import PagePLCalendarAddOrUp from '../pages/app/platform/calendar/calendar-add-or-up/calendar-add-or-up';
import { PagePLDetail } from '../pages/app/platform/detail/detail';
import PagePLBookList from '../pages/app/platform/book/book-list/book-list';
import PagePLBookAddOrUp from '../pages/app/platform/book/book-add-or-up/book-add-or-up';

const pageBaseName = ' - Calendar';

const routes: Array<RouteRecordRaw> = [
  { path: '', redirect: '/public/home' },

  /** 公共页面 **/
  { path: '/bootstrap', meta: { title: 'Bootstrap' + pageBaseName }, component: PageBootstrap },
  { path: '/public/example', meta: { title: 'Model example' + pageBaseName }, component: PageExample },
  { path: '/public/test', meta: { title: 'Test' + pageBaseName }, component: PageTest },
  { path: '/public/icons-dark', meta: { title: 'Dark icons' + pageBaseName }, component: PageIconsDark },
  { path: '/public/icons-light', meta: { title: 'Light icons' + pageBaseName }, component: PageIconsLight },
  { path: '/public/home', meta: { title: 'Home' + pageBaseName }, component: PageHome },

  /** PLATFORM 页面 **/
  {
    path: '/platform',
    component: PagePlatform,
    children: [
      { path: 'home', meta: { title: '平台首页' + pageBaseName }, component: PagePLHome },
      { path: 'detail', meta: { title: '信息详情' + pageBaseName }, component: PagePLDetail },
      { path: 'user/list', meta: { title: '用户' + pageBaseName }, component: PagePLUserList },
      { path: 'calendar/list', meta: { title: '日历 - 列表' + pageBaseName }, component: PagePLCalendarList },
      { path: 'calendar/add_or_up', meta: { title: '日历 - 发布' + pageBaseName }, component: PagePLCalendarAddOrUp },
      { path: 'book/list', meta: { title: '日历 - 列表' + pageBaseName }, component: PagePLBookList },
      { path: 'book/add_or_up', meta: { title: '日历 - 发布' + pageBaseName }, component: PagePLBookAddOrUp },
      // ###### senpm-app
    ],
  },
  /** 控制台 **/
  /** 工作台 **/

  /* 1.所有页面name值不能重复, 否则将匹配到错误页面 */
  // --------------------------------
  /** 必须放最后 **/
  /** 公共页面   **/
  { path: '/:pathMatch(.*)*', meta: { title: 'not-found' }, component: PageXError404 },
];

// 修正vue路由bug, 同name路由无法访问
const router = createRouter({
  history: AppConfig.$useMao ? createWebHashHistory(process.env.BASE_URL) : createWebHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach(async (to: any) => {
  // 调用页面离开事件
  const yes = await AppConfig.clearOnLeavePage();
  if (!yes) {
    return false;
  }

  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }
  AppStore.commit('setRouters', { ...to });
  return true;
});

let routeFromRote = null;
export const getRouteFrom = () => routeFromRote;

router.beforeEach(async (to, from, next) => {
  // ...
  routeFromRote = from;

  // console.log('router.beforeEach ====> ', to, from, next);

  next();
});

export default router;
