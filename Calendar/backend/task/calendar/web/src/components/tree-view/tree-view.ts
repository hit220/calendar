import { Options, Vue } from 'vue-class-component';
import TreeNode from './tree-node/tree-node';
import { TreeViewDefaultOptions } from './tree-view-def';
import { AppUtil } from '../../app.util';
import { AppConfig } from '../../app.config';

@Options({
  style: require('./tree-view.scss'),
  template: require('./tree-view.html'),
  components: {
    'md-tree-node': TreeNode,
  },
  props: {
    data: Object, // TreeViewData,
    options: Object, // TreeViewOptions
    useLink: Boolean, // 使用a:href还是@click
    click: Function,
  },
  watch: {
    data(val) {
      this.$props.data = val;
      this.updateTree(val);
    },
    options(val) {
      this.$props.options = val;
      this.updateTree(this.$props.data);
    },
  },
})
export default class TreeView extends Vue {
  theData = null;
  theOptions = null;
  theUseLink = false;
  theIsLinkExpendAll = true; // 链接树时, 初始进入展开所有子节点

  created() {
    AppConfig.addOnLeavePage(this);
    const data = AppUtil.copy((this.$props as any).data);
    const useLink = (this.$props as any).useLink;
    if (useLink) {
      const pathCur: string = (this as any).$route.fullPath;
      this.ergodicNode(data, null, (node, parentNode) => {
        if (!node.children || node.children.length < 1) {
          node.selected = pathCur.startsWith(node.path);
        }
        if (node.expand || node.selected) {
          if (parentNode) {
            parentNode.expand = true;
          }
        }
      });
    }

    this.updateTree(data);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {
    if (this.theUseLink && this.theIsLinkExpendAll) {
      this.nodeExpandAll();
    }
  }

  updateTree(data) {
    const useLink = (this.$props as any).useLink;
    this.theUseLink = useLink ? true : false;
    const options = (this.$props as any).options;
    this.theOptions = { ...TreeViewDefaultOptions, ...options };

    data = AppUtil.copy(data);
    let nodeId = 1; // 重新分配id
    this.ergodicNode(data, null, (node, parentNode) => {
      node.nodeId = 'node-' + nodeId++;
    });
    this.theData = data;
  }

  ergodicNode(node, parentNode, nodeFunc) {
    if (!node) {
      return;
    }
    if (node.children && node.children.length > 0) {
      for (let i = 0; i < node.children.length; i++) {
        this.ergodicNode(node.children[i], node, nodeFunc);
      }
    }
    nodeFunc(node, parentNode);
  }

  nodeClick(data, e: Event) {
    // 阻止href直接跳转事件
    e = e || window.event; // 获取event对象
    if (e.preventDefault) {
      e.preventDefault();
    } else {
      e.returnValue = false;
    }
    if (e.stopPropagation) {
      e.stopPropagation();
    } else {
      e.cancelBubble = true;
    }

    if (data.nodeId) {
      this.ergodicNode(this.theData, null, (node, parentNode) => {
        if (node.nodeId === data.nodeId) {
          if (!node.children || node.children.length < 1) {
            node.selected = true;
          } else {
            node.expand = !node.expand; // 切换
          }
        } else {
          if (!node.children || node.children.length < 1) {
            node.selected = false;
          }
        }
      });
      this.updateTree(this.theData);
    }

    const props = this.$props as any;
    if (props.click) {
      props.click(data, e);
    }
  }

  nodeExpandAll() {
    this.ergodicNode(this.theData, null, (node, parentNode) => {
      if (node.children && node.children.length > 0) {
        node.expand = true; // 切换
      }
    });
    this.updateTree(this.theData);
  }

  nodeShrinkAll() {
    this.ergodicNode(this.theData, null, (node, parentNode) => {
      if (node.children && node.children.length > 0) {
        node.expand = false; // 切换
      }
    });
    this.updateTree(this.theData);
  }
}
