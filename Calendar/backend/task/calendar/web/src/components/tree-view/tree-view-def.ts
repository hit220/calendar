export class TreeViewOptions {
  showLine?: boolean;
  showIcons?: boolean;
}

export const TreeViewDefaultOptions = {
  showLine: true,
  showIcons: true,
};

export class TreeViewData {
  name: string;
  expand?: boolean; // 展开
  children?: TreeViewData[];
  isLink?: boolean; // 是否是链接
  path?: string;
  target?: string;
  isopen?: boolean;
  icon?: string; //icon图片地址
}
