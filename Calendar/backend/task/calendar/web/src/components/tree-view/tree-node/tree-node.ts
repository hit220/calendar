import { Options, Vue } from 'vue-class-component';
import { AppStore } from '../../../app.store';
import { AppUtil } from '../../../app.util';
@Options({
  style: require('./tree-node.scss'),
  template: require('./tree-node.html'),
  components: {
    /* eslint no-use-before-define: 0 */
    'md-tree-node': TreeNode,
  },
  props: {
    data: Object, // TreeViewData,
    options: Object, // TreeViewOptions,
    level: Number,
    expand: Boolean,
    parentExpand: Boolean,
    useLink: Boolean, // 使用a:href还是@click
    nodeClick: Function,
  },
})
export default class TreeNode extends Vue {
  mao(v: string) {
    return AppUtil.mao(v, AppStore.state.isMao);
  }
}
