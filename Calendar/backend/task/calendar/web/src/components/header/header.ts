import { AppConfig } from './../../app.config';
import { AppNet } from './../../app.net';
import { AppStore } from './../../app.store';
import { AppUtil } from './../../app.util';
import { Options, Vue } from 'vue-class-component';

import { AppVue } from '../../app.vue';

@Options({
  style: require('./header.scss'),
  template: require('./header.html'),
})
export default class Header extends AppVue {

  created() {}

  logout() {
    AppConfig.setSession(null);
    AppConfig.setSessToken(null);
    this.hrefTo('/public/home');
  }
}
