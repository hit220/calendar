import { Options, Vue } from 'vue-class-component';
import { AppStore } from '../../app.store';
import { inject } from 'vue';
import ClipboardJS from 'clipboard';
import { AppUtil } from '../../app.util';
import { AppConfig } from '../../app.config';

@Options({
  style: require('./link-bar.scss'),
  template: require('./link-bar.html'),
  props: {
    routers: Array,
    filters: Array,
  },
  watch: {
    routers(val) {
      this.watchRouters(val);
    },
  },
})
export default class LinkBar extends Vue {
  filterRouters = [];
  emitContext = inject('emitContext') as (event: Event, dataId: Record<string, unknown>) => void;
  el: HTMLElement;

  scrollTimer = 0;
  scrollRunning = false;

  created() {
    AppConfig.addOnLeavePage(this);
    this.watchRouters((this.$props as any).routers);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {
    this.el = document.getElementById('link-box-scroll');
    this.handleWheelEvent();
  }

  watchRouters(rs) {
    const filters = (this.$props as any).filters;

    const fr = [];
    for (const item of rs) {
      for (const f of filters) {
        if ((item.router.fullPath as string).includes(f)) {
          fr.push(item);
        }
      }
    }
    this.filterRouters = fr;
    for (const k in fr) {
      if (fr[k].selected) {
        setTimeout(() => {
          this.scrollToPos(k, fr.length);
        }, 300);
        break;
      }
    }
  }

  linkClose(v) {
    const selected = v.selected;
    AppStore.commit('setDeleteRoutersByKey', {
      key: v.key,
      filters: (this.$props as any).filters,
    });
    if (selected) {
      this.$router.go(-1);
    }
  }

  linkCloseOther(v) {
    AppStore.commit('setDeleteOtherRoutersByKey', {
      key: v.key,
      filters: (this.$props as any).filters,
    });
  }

  linkClear() {
    AppStore.commit('setClearRouters', {
      filters: (this.$props as any).filters,
    });
  }

  openContextMenu(e: any, v) {
    this.emitContext(e, {
      name: 'context-menu-link-bar',
      data: {
        ...v,
        filters: (this.$props as any).filters,
      },
    });
  }

  itemClickEvent(e: any, action) {
    switch (action) {
      case 'close-current':
        this.linkClose(e.data);
        break;
      case 'close-all':
        this.linkClear();
        break;
      case 'close-other':
        this.linkCloseOther(e.data);
        break;
      case 'copy-link':
        this.linkCopyIt(e.data);
        break;
      case 'open':
        this.linkOpen(e.data);
        break;
    }
  }

  linkCopyIt(data) {
    const url = location.protocol + '//' + location.host + data.router.href;
    const cp = new ClipboardJS('body', {
      text: (obj) => {
        return url;
      },
    });
    cp.on('success', (e) => {
      e.clearSelection();
      AppUtil.toast('复制成功:' + e.text);
    });
  }

  linkOpen(data) {
    const url = location.protocol + '//' + location.host + data.router.href;
    AppUtil.openUrl(url, '_blank');
  }

  linkTo(item, e: Event) {
    // 阻止href直接跳转事件
    AppUtil.stopEvent(e);

    this.$router.push(item.router);
  }

  closeOne() {
    const rs = (this.$props as any).routers;
    for (const item of rs) {
      if (item.selected) {
        this.linkClose(item);
        return;
      }
    }
  }

  closeOther() {
    const rs = (this.$props as any).routers;
    for (const item of rs) {
      if (item.selected) {
        this.linkCloseOther(item);
        return;
      }
    }
  }

  mao(v: string) {
    return AppUtil.mao(v, AppStore.state.isMao);
  }

  handleWheelEvent() {
    const add = window.addEventListener ? 'addEventListener' : 'attachEvent';
    const remove = window.removeEventListener ? 'removeEventListener' : 'detachEvent';
    let wheel = '';
    console.log(this.el, 1111);
    if ('onmousewheel' in this.el) {
      wheel = 'mousewheel';
    } else if ('onwheel' in this.el) {
      wheel = 'wheel';
    } else if ('attachEvent' in window) {
      wheel = 'onmousewheel';
    } else {
      wheel = 'DOMMouseScroll';
    }
    this.el[remove](wheel, this.scroll);
    this.el[add](wheel, this.scroll);
  }

  scroll(e) {
    e = e || window.event; // 获取event对象
    if (e.preventDefault) {
      e.preventDefault();
    } else {
      e.returnValue = false;
    }
    if (e.stopPropagation) {
      e.stopPropagation();
    } else {
      e.cancelBubble = true;
    }

    if (this.el.clientWidth >= this.el.scrollWidth) {
      return;
    }
    this.el.scrollLeft += e.deltaX ? e.deltaX : e.detail && e.detail !== 0 ? e.detail : -e.wheelDelta;
  }

  scrollTo(d) {
    if (this.el.clientWidth >= this.el.scrollWidth) {
      return;
    }

    this.scrollRunning = true;
    this.scrollTimer = setTimeout(() => {
      this.doScrollTo(d);
    }, 10);
  }

  doScrollTo(d) {
    if (!this.scrollRunning) {
      return;
    }

    if (d === 'left') {
      this.el.scrollLeft += 10;
    } else if (d === 'right') {
      this.el.scrollLeft -= 10;
    }

    this.scrollTimer = setTimeout(() => {
      this.doScrollTo(d);
    }, 10);
  }

  cancelScroll() {
    clearTimeout(this.scrollTimer);
    this.scrollRunning = false;
  }

  scrollToPos(pos, length) {
    const x1 = (this.el.scrollWidth * pos) / length;
    const x2 = this.el.clientWidth / 2;
    this.el.scrollLeft += x1 - x2;
    console.log('=====', pos, length, x1, x2, x1 - x2);
  }
}
