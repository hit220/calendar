const commands = ['M', 'm', 'L', 'l', 'H', 'h', 'V', 'v', 'Z', 'z', 'C', 'c', 'S', 's', 'Q', 'q', 'A', 'a'];

interface Options {
  id?: string;
  elementID?: string;
  viewBox?: string;
  width?: number;
  height?: number;
  fill?: string;
  cx?: number;
  cy?: number;
  preserveAspectRatio?: string;
  strokeWidth?: number;
  strokeLinejoin?: string;
  strokeOpacity?: number;
  strokeLinecap?: string;
  stroke?: string;
  style?: string;
}

interface optionResult {
  header: string;
  elementProperties: string;
}

function getOptions(opts?: Options): optionResult {
  let header = '';
  let elementProperties = '';
  if (opts) {
    if (opts.id) {
      header += ' id="' + opts.id + '"';
    }
    if (opts.viewBox) {
      header += ' viewBox="' + opts.viewBox + '"';
    }
    if (opts.preserveAspectRatio) {
      // header += ' preserveAspectRatio="' + opts.preserveAspectRatio + '"';
    } else {
      header += ' preserveAspectRatio="none"';
    }
    if (opts.width) {
      header += ' width="' + opts.width + '"';
    }
    if (opts.height) {
      header += ' height="' + opts.height + '"';
    }
    if (opts.elementID) {
      elementProperties += ' id="' + opts.elementID + '"';
    }
    if (opts.fill) {
      elementProperties += ' fill="' + opts.fill + '"';
    }
    if (opts.fill) {
      elementProperties += ' fill="' + opts.fill + '"';
    }
    if (opts.style) {
      elementProperties += ' style="' + opts.style + '"';
    }
    if (opts.fill) {
      elementProperties += ' fill="' + opts.fill + '"';
    }
    if (opts.stroke) {
      elementProperties += ' stroke="' + opts.stroke + '"';
    }
    if (opts.strokeLinejoin) {
      elementProperties += ' strok-linejoin="' + opts.strokeLinejoin + '"';
    }
    if (opts.strokeWidth) {
      elementProperties += ' stroke-width="' + opts.strokeWidth + '"';
    }
    if (opts.strokeOpacity !== null) {
      elementProperties += ' stroke-opacity="' + opts.strokeOpacity + '"';
    }
    if (opts.strokeLinecap) {
      elementProperties += ' stroke-linecap="' + opts.strokeLinecap + '"';
    }
    if (opts.cx) {
      elementProperties += ' cx="' + opts.cx + '"';
    } else {
      elementProperties += ' cx="0"';
    }

    if (opts.cy) {
      elementProperties += ' cy="' + opts.cy + '"';
    } else {
      elementProperties += ' cy="0"';
    }
  } else {
    header += ' preserveAspectRatio="none"';
    elementProperties += ' cx="0"';
    elementProperties += ' cy="0"';
  }
  return {
    header,
    elementProperties,
  };
}

/*

*/
export const svgCreatePath = (arr: any, opts?: Options): string => {
  let p = '';
  for (const item of arr) {
    if (typeof item === 'string' && commands.includes(item)) {
      p = p.trim();
      p += item;
    } else if (typeof item !== 'object') {
      const r = Number(item);
      if (!isNaN(r)) {
        p += r.toString() + ' ';
      }
    } else {
      const cms = Object.values(item);
      for (const cmItem of cms) {
        if (typeof cmItem === 'string' && commands.includes(cmItem)) {
          p = p.trim();
          p += cmItem;
        } else if (typeof cmItem !== 'object') {
          const r = Number(cmItem);
          if (!isNaN(r)) {
            p += r.toString() + ' ';
          }
        }
      }
    }
  }
  try {
    p = p.trim().replaceAll(' ', ',');
  } catch (_) {}
  if (p === '') {
    return '';
  }

  const res = getOptions(opts);
  return (
    '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"\
 y="0px"  ' +
    res.header +
    '>\
<path d="' +
    p +
    '" ' +
    res.elementProperties +
    '  />\
</svg>'
  );
};

/*

*/
export const svgCreateCircle = (r: number, opts?: any): string => {
  let elementProperties = ' r="' + r + '"';

  const res = getOptions(opts);
  elementProperties += res.elementProperties;

  return (
    '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"\
   y="0px"  ' +
    res.header +
    '>\
  <circle ' +
    elementProperties +
    '  />\
  </svg>'
  );
};
