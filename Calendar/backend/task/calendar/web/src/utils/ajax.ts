import { AppNet } from '../app.net';

export class AjaxOption {
  dataType: string;
  showLog: boolean;
}

export const ajax = function(method: string, url: string, text?: string, opts?: AjaxOption) {
  return new Promise<any>((resolve, reject) => {
    let xhr: XMLHttpRequest;
    if (window.XMLHttpRequest) {
      xhr = new XMLHttpRequest();
    } else {
      xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }
    if (!method) {
      method = 'GET';
    } else {
      method = method.toUpperCase();
    }
    const bgcolor = AppNet.randBgColode();
    AppNet.blog(!(opts && opts.showLog), bgcolor, method, url, text ? text : '');

    xhr.open(method, url, true);
    if (opts && opts.dataType === 'json') {
      xhr.setRequestHeader('content-type', 'application/json');
    }
    xhr.onerror = (e) => {
      AppNet.blog(!(opts && opts.showLog), bgcolor, method, url, e);
    };
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          const data = xhr.responseText;
          if (!data) {
            AppNet.blog(!(opts && opts.showLog), bgcolor, 'RESP', 'null');
            resolve(null);
            return;
          }
          if (
            xhr
              .getResponseHeader('content-type')
              .toLowerCase()
              .includes('application/json')
          ) {
            try {
              const idata = { code: 'success', data: JSON.parse(data) };
              AppNet.blog(!(opts && opts.showLog), bgcolor, 'RESP', idata);
              resolve(idata);
            } catch (_) {
              const idata = { code: 'error', data };
              AppNet.blog(!(opts && opts.showLog), bgcolor, 'RESP', idata);
              resolve(idata);
            }
          } else {
            const idata = { code: 'success', data };
            AppNet.blog(!(opts && opts.showLog), bgcolor, 'RESP', idata);
            resolve(idata);
          }
        } else {
          const error = 'error_code:' + xhr.status;
          resolve({ code: 'error', data: error + ',' + xhr.responseText });
        }
      }
    };

    switch (method) {
      case 'GET':
        xhr.send();
        break;
      case 'POST':
        xhr.send(text);
        break;
      default:
    }
  });
};
