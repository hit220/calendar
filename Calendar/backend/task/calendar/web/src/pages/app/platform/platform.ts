import { AppVue } from '../../../app.vue';
import './platform.scss';
import { Options } from 'vue-class-component';
import TreeView from '../../../components/tree-view/tree-view';
import Header from '../../../components/header/header';
import Footer from '../../../components/footer/footer';
import LinkBar from '../../../components/link-bar/link-bar';
import { AppConfig } from '../../../app.config';
import { TreeViewData, TreeViewOptions } from '../../../components/tree-view/tree-view-def';

@Options({
  template: require('./platform.html'),
  name: 'PagePlatform',
  components: {
    'md-tree-view': TreeView,
    'md-header': Header,
    'md-footer': Footer,
    'md-link-bar': LinkBar,
  },
  props: {
    platformMenu: {
      type: Array,
    },
  },
})
export class PagePlatform extends AppVue {
  navType = 'menuData';
  menuData: TreeViewData = {
    name: 'Calendar',
    children: [
      {
        name: '首页',
        path: '/platform/home',
      },
      {
        name: '用户',
        path: '/platform/user/list',
      },
      {
        name: '日历',
        children: [
          {
            name: '日历列表',
            path: '/platform/book/list',
          },
          {
            name: '新日历',
            path: '/platform/book/add_or_up',
          },
        ],
      },
      {
        name: '日历项',
        children: [
          {
            name: '项列表',
            path: '/platform/calendar/list',
          },
          {
            name: '新加项',
            path: '/platform/calendar/add_or_up',
          },
        ],
      },
    ],
  };

  menuOptions: TreeViewOptions = {
    showLine: true,
    showIcons: false,
  };

  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {}

  onMenuClick(data, e) {
    if (!data) {
      return;
    }

    // isLink:<a></a>不做处理, path为空不做处理
    if (data.isLink || !data.path) {
      return;
    }

    this.hrefTo(data.path);
  }

  headerNav(i) {
    this.navType = i;
  }

  hrefToLogin() {
    if (!(this as any).$store.state.isLogin) {
      // this.showNeedLogin();
      this.hrefTo('/public/home');
    }
  }
}
