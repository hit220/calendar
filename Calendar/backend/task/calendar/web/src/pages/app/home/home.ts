import { Options } from 'vue-class-component';
import { AppVue } from '../../../app.vue';
import { AppNet } from '../../../app.net';
import './home.scss';
import { AppConfig } from '../../../app.config';
import { AppUtil } from '../../../app.util';
import Swal from 'sweetalert2';

@Options({
  template: require('./home.html'),
  components: {},
  props: {},
})
export class PageHome extends AppVue {
  checkedMe = false;
  ruleForm = {
    account: '',
    password: '',
  };
  async created() {
    this.checkedMe = await AppConfig.getWithTimeout('loginRememberAccount', 30 * 86400, false);
    if (this.checkedMe) {
      this.ruleForm.account = await AppConfig.get('loginAccount', '');
      this.ruleForm.password = await AppConfig.get('loginPassword', '');
      // 更新记住状态
      AppConfig.set('loginRememberAccount', this.checkedMe);
    }
  }

  mounted() {
    const urlObject = AppUtil.parseURL(location.href);
    const user = urlObject ? urlObject.params['user'] : '';
    const token = urlObject ? urlObject.params['token'] : '';
    if (user && token) {
      this.loginByQuery(urlObject, user, token);
      return;
    }

    // 已经登录
    if (AppConfig.isLogin()) {
      this.hrefTo('/platform/home');
    }
  }

  // 免登录
  changeCheckedMe(e) {
    this.checkedMe = e.target.checked;
    AppConfig.set('loginRememberAccount', this.checkedMe);
  }

  async submitForm() {
    if (this.ruleForm.account.length < 4 || this.ruleForm.password.length < 4) {
      alert('请输入有效值');
      return;
    }
    const req = await AppNet.get('/pub/login_apply');
    if (!req || req.code !== 'success') {
      this.alert(req.message);
      return;
    }
    const apply = req.data;

    const params = {
      type: 'master',
      device: '123456',
      apply_token: apply.apply_token,
      module: apply.module,
      account: this.ruleForm.account,
      password: AppUtil.toSha256(this.ruleForm.password),
    };

    const url = apply.url;

    const data = await AppNet.post(url, this.strParams(params));

    if (!data || data.code !== 'success') {
      this.alert(data.message);
      return;
    }
    await AppConfig.setSessToken({ token: data.data.token });
    await AppConfig.setSession(data.data.user);
    await AppConfig.set('loginAccount', this.ruleForm.account);
    await AppConfig.set('loginPassword', this.ruleForm.password);

    await this.toast(data.message, 'success', 1300);

    this.hrefTo('/platform/home');
  }

  async loginByQuery(urlObject, userinfo, inToken) {
    try {
      const uinfo = JSON.parse(userinfo);
      if (uinfo.id) {
        await AppConfig.setSessToken({ token: inToken });
        await AppConfig.setSession(uinfo);

        delete urlObject.params['user'];
        delete urlObject.params['token'];

        location.href = AppUtil.rebuildUrl(urlObject);
      }
    } catch (_) {
    }
  }

  forgetPassword() {
    this.alert('请联系管理员');
  }
}
