import { Options } from 'vue-class-component';
import { AppNet } from '../../../../app.net';
import { AppVue } from '../../../../app.vue';
import './detail.scss';

@Options({
  template: require('./detail.html'),
  name: 'PagePLDetail',
})
export class PagePLDetail extends AppVue {
  itemId = '';
  title = '详情';
  path = '';
  item = null;

  created() {}

  async mounted() {
    this.path = this.$route.query.path as string;
    this.itemId = this.$route.query.id as string;
    this.title = this.$route.query.title as string;

    this.getData();
  }

  async getData() {
    if (!this.itemId || !this.path) {
      this.toast('无法查询记录信息');
      return;
    }
    const res = await AppNet.get(this.path, {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    this.item = res.data;
  }
}
