import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../../app.config';
import { AppNet } from '../../../../../app.net';
import { AppVue } from '../../../../../app.vue';
import { uploadFileByAccept } from '../../../../../utils/upload';

@Options({
  style: require('./book-add-or-up.scss'),
  template: require('./book-add-or-up.html'),
})
export default class PagePLBookAddOrUp extends AppVue {
  itemId = '';
  modelTitle = '创建';
  modelButton = '保存';

  form = {
    id: '',
    uid: '',
    title: '',
    content: '',
    images: '',
  };
  imagesArr = [];

  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {
    this.itemId = this.$route.query.id as string;

    this.getData();
  }

  async getData() {
    if (!this.itemId) {
      return;
    }
    const res = await AppNet.get('/api/master/v1/book/detail', {
      id: this.itemId,
    });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }

    const v = res.data;

    this.modelTitle = '编辑';
    this.modelButton = '保存';

    this.form.id = '' + v.id;
    this.form.uid = '' + v.uid;
    this.form.title = '' + v.title;
    this.form.content = v.content;
    this.form.images = v.images;

    const arr = [];
    const iarr = this.form.images.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    this.imagesArr = arr;
  }

  async submitFormEdit() {
    const res = await AppNet.post('/api/master/v1/book/add_or_update', this.strParams(this.form));
    if (!res || res.code !== 'success') {
      this.alert(res ? res.message : 'Fail');
      return;
    }

    await this.alert(res.message, 'success');

    if (res.data && res.data.is_add) {
      this.itemId = res.data.offw_or_id;
    }
    
    this.hrefTo('/platform/book/list');
  }

  /***************** 上传 */
  loading = false;
  uploadFile() {
    uploadFileByAccept('/api/upload', 'image/*', {
      onStart: (file, formData) => {
        this.loading = true;
      },
      onFinish: (err, re) => {
        this.loading = false;

        if (err) {
          this.alert(err);
          return;
        }

        this.imagesArr.push(re.uuid);
        this.form.images = this.imagesArr.join(',');
      },
    });
  }

  onRemove(f) {
    const nArr = [];
    for (const item of this.imagesArr) {
      if (item !== f) {
        nArr.push(item);
      }
    }
    this.imagesArr = nArr;
    this.form.images = nArr.join(',');
  }
}
