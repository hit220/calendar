import { Options } from 'vue-class-component';
import { AppConfig } from '../../../../../app.config';
import { AppNet } from '../../../../../app.net';
import { AppUtil } from '../../../../../app.util';
import { AppVue } from '../../../../../app.vue';

@Options({
  style: require('./book-list.scss'),
  template: require('./book-list.html'),
})
export default class PagePLBookList extends AppVue {
  reqData = [];
  dataForce = false;
  pgn = null; // 分页导航栏
  kw = '';

  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {
    this.getData();
  }

  async getData(kw = '') {
    const res = await AppNet.get('/api/master/v1/book/list', { kw: kw });
    if (res && res.code !== 'success') {
      this.toast(res.message);
      return;
    }
    this.reqData = res.data.data;
    this.pgn = AppUtil.getPagination(res.data);
  }

  splitImages(imgs) {
    const arr = [];
    if (!imgs) {
      return arr;
    }
    const iarr = imgs.split(',');
    for (const item of iarr) {
      if (item && item.length > 4) {
        arr.push(item);
      }
    }
    return arr;
  }
}
