import { Options } from 'vue-class-component';
import { AppVue } from '../../../app.vue';

@Options({
  style: require('./tpl.scss'),
  template: require('./tpl.html'),
})
export default class PageTpl extends AppVue {
  created() {}
}
