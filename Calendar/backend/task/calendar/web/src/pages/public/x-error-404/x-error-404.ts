import { Options } from 'vue-class-component';
import { AppVue } from '../../../app.vue';

@Options({
  style: require('./x-error-404.scss'),
  template: require('./x-error-404.html'),
})
export default class PageXError404 extends AppVue {}
