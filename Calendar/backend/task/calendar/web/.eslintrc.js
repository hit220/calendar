module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'semi': ["error", "always"],    // 严格控制代码观感
    'quotes': ["error", "single"],  // 严格控制代码观感
    'new-cap': 0,
    'one-var': 0,
    'comma-dangle': 0,
    'padded-blocks': 0,
    'indent': 0,
    'dot-notation': 0,
    'eol-last': 0,
    'spaced-comment': 0,
    'space-before-function-paren': 0,
    'no-unneeded-ternary': 0,
    'no-trailing-spaces': 0,
    'no-prototype-builtins': 0,
    'no-multiple-empty-lines': 0,
    'no-useless-escape': 0,
    'no-multi-str': 0,
    'lines-between-class-members': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    '@typescript-eslint/no-unused-vars': 0,
    '@typescript-eslint/no-empty-function': 0,
    '@typescript-eslint/no-var-requires': 0,
  }
}
