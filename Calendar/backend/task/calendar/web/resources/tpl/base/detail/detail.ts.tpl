import { Options } from 'vue-class-component';
import { AppVue } from '{{RelativePath "src/app.vue"}}';

@Options({
  style: require('./detail.scss'),
  template: require('./detail.html')
})
export default class {{.__CLASSNAME__}} extends AppVue {
  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {}
}
