import { Options } from 'vue-class-component';
import { AppVue } from '{{RelativePath "src/app.vue"}}';

@Options({
  style: require('./edit.scss'),
  template: require('./edit.html')
})
export default class {{.__CLASSNAME__}} extends AppVue {
  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {}
}
