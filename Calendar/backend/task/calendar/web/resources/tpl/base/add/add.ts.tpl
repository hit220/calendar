import { Options } from 'vue-class-component';
import { AppVue } from '{{RelativePath "src/app.vue"}}';

@Options({
  style: require('./add.scss'),
  template: require('./add.html')
})
export default class {{.__CLASSNAME__}} extends AppVue {
  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {}
}
