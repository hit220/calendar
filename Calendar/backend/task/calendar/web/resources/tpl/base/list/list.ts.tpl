import { Options } from 'vue-class-component';
import { AppVue } from '{{RelativePath "src/app.vue"}}';

@Options({
  style: require('./list.scss'),
  template: require('./list.html')
})
export default class {{.__CLASSNAME__}} extends AppVue {
  created() {
    AppConfig.addOnLeavePage(this);
  }

  viewDidLeave() {
    return true;
  }

  mounted() {}
}
