package keys

import (
	"crypto/rsa"
	"errors"
	"sync"

	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
)

var km *KeyManager
var keyManagerLock sync.Mutex

func GetKeyManager() *KeyManager {
	keyManagerLock.Lock()
	defer keyManagerLock.Unlock()

	if km == nil {
		km = &KeyManager{}
	}

	if !km.isInited {
		km.initKeys()
	}
	return km
}

func ResetKeyManager() error {
	keyManagerLock.Lock()
	defer keyManagerLock.Unlock()

	k := &KeyManager{}
	err := k.initKeys()
	if err != nil {
		return err
	}
	km = k
	return nil
}

type BaKeys struct {
	MyPrivateKey *rsa.PrivateKey
	AimPublicKey *rsa.PublicKey
}

type KeyManager struct {
	isInited       bool
	selfPrivateKey *rsa.PrivateKey
	selfPublicKey  *rsa.PublicKey
	baPrivKeys     sync.Map // string(module_name) => *BaKeys
	baPublicKey    *rsa.PublicKey
}

func (k *KeyManager) initKeys() error {
	defer func() {
		// 不管是否成功初始化, 都设置为已经初始化
		k.isInited = true
	}()

	var err error
	privKey, pubKey, err := lib.GenKeyPair(2048)
	if err != nil {
		return err
	}

	selfPrivateKey := cfg.String("module.jwt.self.private_key", privKey)
	selfPublicKey := cfg.String("module.jwt.self.public_key", pubKey)
	baPublicKey := cfg.String("module.jwt.ba.public_key", "")

	k.selfPrivateKey, err = jwt.ParseRSAPrivateKeyFromPEM([]byte(selfPrivateKey))
	if err != nil {
		return err
	}
	k.selfPublicKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(selfPublicKey))
	if err != nil {
		return err
	}

	k.baPublicKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(baPublicKey))
	if err != nil {
		return err
	}
	return nil
}

func (k *KeyManager) ModifyTokenHeader(header map[string]interface{}) map[string]interface{} {
	header["iss"] = baapi.ModuleName()
	return header
}

func (k *KeyManager) KeyFuncVerify(header map[string]interface{}) (interface{}, error) {
	iss, ok1 := header["iss"]
	aud, ok2 := header["aud"]
	if ok1 && ok2 && iss != "" && aud != "" {
		if iss == aud {
			return k.selfPublicKey, nil
		}
	}

	if k.baPublicKey == nil {
		return nil, jwt.ErrInvalidPubKey
	}

	return k.baPublicKey, nil
}

func (k *KeyManager) KeyFuncSign(header map[string]interface{}) (interface{}, error) {

	iss, ok1 := header["iss"]
	aud, ok2 := header["aud"]
	if ok1 && ok2 && iss != "" && aud != "" {
		if iss == aud {
			if k.selfPrivateKey != nil {
				return k.selfPrivateKey, nil
			}
		} else {
			ks, ok := k.baPrivKeys.Load(aud)
			if ok && ks != nil {
				keys := ks.(*BaKeys)
				if keys.MyPrivateKey != nil {
					return keys.MyPrivateKey, nil
				}
			}
		}
	}

	return nil, jwt.ErrInvalidPrivKey
}

func (k *KeyManager) Sign(isBA bool, moduleTo string, data string) (string, error) {
	if isBA {
		ks, ok := k.baPrivKeys.Load(moduleTo)
		if ok && ks != nil {
			keys := ks.(*BaKeys)
			if keys.MyPrivateKey != nil {
				return jwt.RS512Sign(data, keys.MyPrivateKey)
			}
		}
	} else {
		return jwt.RS512Sign(string(data), k.selfPrivateKey)
	}
	return "", errors.New("sign fail")
}

func (k *KeyManager) Verify(isBA bool, moduleFrom string, data string, sign string) error {
	if isBA {
		ks, ok := k.baPrivKeys.Load(moduleFrom)
		if ok && ks != nil {
			keys := ks.(*BaKeys)
			if keys.AimPublicKey != nil {
				return jwt.RS512Verify(data, sign, keys.AimPublicKey)
			}
		}
	} else {
		return jwt.RS512Verify(data, sign, k.baPublicKey)
	}
	return errors.New("verify fail")
}
