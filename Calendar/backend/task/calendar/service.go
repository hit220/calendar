package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/takama/daemon"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
)

type exitApp struct {
	ctx     context.Context
	exit    context.CancelFunc
	baseDir string
}

func imain(ctx context.Context, baseDir string) *exitApp {
	app := &exitApp{
		ctx:     ctx,
		baseDir: baseDir,
	}

	go app.main()

	return app
}

func (app *exitApp) WillExit() error {
	return nil
}

func (app *exitApp) SetExitCtrl(exit context.CancelFunc) {
	app.exit = exit
}

//Service is
type Service struct {
	daemon.Daemon
	baseDir string
}

//Manage is
func (s *Service) Manage() (string, error) {

	usage := "Usage: " + srvName + " install | remove | start | stop | status"
	if len(os.Args) > 1 {
		command := os.Args[1]
		if strings.Index(command, "-") != 0 {
			switch command {
			case "install":
				return s.Install()
			case "remove":
				return s.Remove()
			case "start":
				return s.Start()
			case "stop":
				return s.Stop()
			case "status":
				return s.Status()
			default:
				return usage, nil
			}
		}
	}
	/*************** CLI ***************/
	cliLog := &log.Logger{}
	glog.LoggerOutput(cliLog, &glog.LogBridgeOptions{
		Prefix:          "CLI",
		BaseDepth:       3,
		ReplaceMap:      nil,
		ShowHeaderTrace: true,
	})
	cli, ctx := lib.NewCLI(s.baseDir, cliLog)
	defer cli.Shutdown()

	/***************  ***************/
	//go http.ListenAndServe(":6060", nil)

	/*************** 核心 ***************/
	defer cfg.WaitSave()
	defer glog.Flush()

	sp := imain(ctx, s.baseDir)
	cli.Add(sp)

	/************** CLI LOOP **************/
	cli.Loop()
	return "Daemon was killed", nil
}

func main() {
	currentDir := ""
	baseDir := ""
	flag.StringVar(&currentDir, "current-dir", "", "running in a directory")
	flag.StringVar(&baseDir, "dir", "./_data", "running in a directory")
	flag.Parse()

	if currentDir != "" {
		lib.SetCurrentDir(currentDir)
		os.Chdir(currentDir)
		cdir, _ := os.Getwd()
		fmt.Println("current dir:", cdir)
	}

	baseDir = lib.EnsureDir(baseDir)

	pathGLogDir := filepath.Join(baseDir, "glog")
	pathLogDir := filepath.Join(baseDir, "log")

	// 独立于baseDir
	confDir := lib.EnsureDir("./conf")
	pathConfig := filepath.Join(confDir, "default.conf")

	// os.RemoveAll(pathPageDir)
	// os.RemoveAll(pathCaptureDir)
	// os.RemoveAll(pathGLogDir)

	cfg.InitWithFile("json", pathConfig)
	glog.DefaultInit(glog.OtherLog, true, pathGLogDir, pathLogDir)

	/*************** Service ***************/
	srv, err := daemon.New(srvName, srvDescription, daemon.SystemDaemon)
	if err != nil {
		glog.Errorln("Error: ", err)
		os.Exit(1)
	}
	s := &Service{srv, baseDir}
	status, err := s.Manage()
	if err != nil {
		glog.Errorln(status, "\nError: ", err)
		os.Exit(1)
	}
	fmt.Println(status)
}
