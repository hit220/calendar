package tables

import (
	"fmt"
	"math"
	"strconv"
	"time"

	"seasonalcal.life/rechy/calendar/utils"
	"seasonalcal.life/rechy/lib"
)

/*********************************************

 *
 **********************************************/

// AppCalendarTableName is 表名
const AppCalendarTableName = "app_calendar"

// AppCalendar is
type AppCalendar struct {
	ID             int64        `json:"id" xorm:"pk autoincr 'id'"`
	BID            int64        `json:"bid" xorm:"notnull default(0) 'bid'"`
	Title          string       `json:"title" xorm:"text(2000) notnull default('') 'title'"`
	Content        string       `json:"content" xorm:"mediumtext notnull default('') 'content'"`
	Images         string       `json:"images" xorm:"mediumtext notnull default('') 'images'"`
	MonthCell      string       `json:"month_cell" xorm:"char(10) notnull default('') index 'month_cell'"`
	CreatedAt      lib.DateTime `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxM  string       `json:"-" xorm:"char(10) notnull default('') index 'created_at_idx_m'"`
	CreatedAtIdxMD string       `json:"created_at_idx_md" xorm:"char(10) notnull default('') index 'created_at_idx_md'"`
	CreatedAtIdxYM int          `json:"-" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt      lib.DateTime `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int          `json:"-" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
	Likes          int          `json:"likes" xorm:"notnull default(0) index 'likes'"`
	LikeUsers      string       `json:"-" xorm:"mediumtext notnull default('') 'like_users'"`
	Public         int          `json:"pub" xorm:"notnull default(1) index 'pub'"`
}

func init() {
	tables = append(tables, new(AppCalendar))
}

func AppCalendarByID(id int64) (*AppCalendar, bool, error) {
	re := new(AppCalendar)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

// public 公开/私有: 1公开 0私有 -1不限
func AppCalendarList(bid int64, public int, currentPage, pageSize int, kw, month string) ([]*AppCalendar, int64, error) {

	var query string
	var args []interface{}
	if public < 0 {
		if bid > 0 {
			if kw == "" {
				query, args = utils.Where("bid = ?", bid)
			} else {
				query, args = utils.Where("bid = ? and (title LIKE ? or content LIKE ? or created_at_idx_m = ?)", bid, "%"+kw+"%", "%"+kw+"%", kw)
			}
		} else {
			if kw == "" {
				query, args = utils.Where("")
			} else {
				query, args = utils.Where("title LIKE ? or content LIKE ? or created_at_idx_m = ?", "%"+kw+"%", "%"+kw+"%", kw)
			}
		}
	} else {
		if bid > 0 {
			if kw == "" {
				query, args = utils.Where("bid = ? and pub = ?", bid, public)
			} else {
				query, args = utils.Where("bid = ? and pub = ? and (title LIKE ? or content LIKE ? or created_at_idx_m = ?)", bid, public, "%"+kw+"%", "%"+kw+"%", kw)
			}
		} else {
			if kw == "" {
				query, args = utils.Where("pub = ?", public)
			} else {
				query, args = utils.Where("pub = ? and ( title LIKE ? or content LIKE ? or created_at_idx_m = ?)", public, "%"+kw+"%", "%"+kw+"%", kw)
			}
		}
	}

	if month != "" {
		query += " and created_at_idx_m = ?"
		args = append(args, month)
	}
	total, err := _db.Where(query, args...).Count(new(AppCalendar))
	if err != nil {
		return nil, 0, err
	}

	var res []*AppCalendar
	err = _db.Where(query, args...).OrderBy("created_at DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func AppCalendarImage(bid int64, currentPage, pageSize int) ([]interface{}, int64, error) {

	var query string
	var args []interface{}

	if bid > 0 {
		query, args = utils.Where("bid = ? and pub = ?", bid, 1)
	} else {
		query, args = utils.Where("pub = ?", 1)

	}

	total, err := _db.Where(query, args...).Count(new(AppCalendar))
	if err != nil {
		return nil, 0, err
	}

	sql := `select id,bid,images,created_at from ` + AppCalendarTableName + ` where ` + query + ` order by created_at desc 
			limit ?,?`
	var sqlArgs []interface{}
	sqlArgs = append(sqlArgs, sql)
	sqlArgs = append(sqlArgs, args...)
	sqlArgs = append(sqlArgs, pageSize*(currentPage-1))
	sqlArgs = append(sqlArgs, pageSize)

	rows, err := _db.QueryInterface(sqlArgs...)
	if err != nil {
		return nil, 0, err
	}

	res := make([]interface{}, len(rows))
	for k, v := range rows {
		bean := make(map[string]interface{})
		bean["id"] = v["id"]
		bean["bid"] = v["bid"]
		bean["created_at"] = v["created_at"]
		bean["images"] = v["images"]
		res[k] = bean
	}
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func AppCalendarAddOrUpdate(
	reqID,
	reqBID int64,
	reqTitle,
	reqContent,
	reqImages,
	reqMonthCell string) (isAdd bool, offwOrID int64, err error) {

	if reqID < 1 {
		isAdd = true
	}

	info := new(AppCalendar)
	info.BID = reqBID
	info.Title = reqTitle
	info.Content = reqContent
	info.Images = reqImages

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = lib.DateTime(now)
	info.UpdatedAtIdxYM = idxTM
	info.MonthCell = reqMonthCell

	mv := lib.ValueToInt(info.MonthCell, 0)
	info.CreatedAtIdxM = fmt.Sprintf("%02d", int(math.Floor(float64(mv)/100.0)))

	// 移除重复项
	_db.Exec("delete from "+AppCalendarTableName+" where id != ? and bid = ? and month_cell = ?",
		reqID, reqBID, info.MonthCell)

	if !isAdd {

		minfo := map[string]interface{}{
			"title":             info.Title,
			"content":           info.Content,
			"images":            info.Images,
			"updated_at":        time.Time(info.UpdatedAt),
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
			"month_cell":        info.MonthCell,
		}

		offwOrID, err = _db.Table(AppCalendarTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	info.Public = 1
	info.CreatedAt = lib.DateTime(now)
	cidxTM, _ := strconv.Atoi(now.Format("200601"))
	cidxMD := now.Format("0102")
	info.CreatedAtIdxMD = cidxMD
	info.CreatedAtIdxYM = cidxTM

	_, err = _db.Insert(info)
	return isAdd, info.ID, err
}

func AppCalendarMonthCells(bid int64) ([]map[string]interface{}, error) {
	rows, err := _db.QueryInterface("select id,images , month_cell, created_at_idx_m from "+
		AppCalendarTableName+" where bid = ? group by month_cell", bid)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func AppCalendarDelete(bid int64, id string) error {
	_, err := _db.Exec("delete from "+AppCalendarTableName+" where bid = ? and id = ?", bid, id)
	return err
}

func AppCalendarDeleteByBook(uid int64, bid string) error {
	_, err := _db.Exec("delete from "+AppCalendarTableName+" where bid = ? and bid = ?", bid, bid)
	return err
}
