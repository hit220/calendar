package tables

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

/*********************************************
 *
 *   用户
 *   -- 普通个人用户
 *
 **********************************************/

// AppUserTableName is table name
const AppUserTableName = "app_user"

// AppUser
type AppUser struct {
	// ModeUser
	ID             int64        `json:"id" xorm:"pk autoincr 'id'"`
	UUID           string       `json:"uuid" xorm:"char(40) notnull default('') 'uuid'"`
	Account        string       `json:"account" xorm:"char(64) notnull default('') 'account'"`
	Phone          string       `json:"phone" xorm:"varchar(30) notnull default('') 'phone'"`
	Auth           string       `json:"-" xorm:"mediumtext notnull default('') 'auth'"`
	HeadImage      string       `json:"head_image" xorm:"varchar(255) notnull default('') 'head_image'"`
	Activation     int          `json:"activation" xorm:"notnull default(0) index 'activation'"`
	Nickname       string       `json:"nickname" xorm:"varchar(200) notnull default('') 'nickname'"`
	ExtInfo        string       `json:"ext_info" xorm:"mediumtext notnull default('') 'ext_info'"`
	CreatedAt      lib.DateTime `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxYM int          `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt      lib.DateTime `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int          `json:"updated_at_idx_ym" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
	Balance        int64        `json:"balance" xorm:"notnull default(0) 'balance'"`
}

func init() {
	tables = append(tables, new(AppUser))
}

func AppUserByID(id int64) (*AppUser, bool, error) {
	re := &AppUser{}
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		return nil, exists, err
	}
	return re, exists, err
}

func AppUserList(currentPage, pageSize int, kw string) ([]*AppUser, int64, error) {
	sql := "select * from " + AppUserTableName + " where id > 0"
	sqlAll := "select count(*) from " + AppUserTableName + " where id > 0"
	arrAll := make([]interface{}, 0)
	arr := make([]interface{}, 0)

	if kw != "" {
		sql += " and (id = ? or username LIKE ? or phone LIKE ?)"
		sqlAll += " and (id = ? or username LIKE ? or phone LIKE ?)"
		arr = append(arr, kw)
		arrAll = append(arrAll, kw)
		arr = append(arr, "%"+kw+"%")
		arrAll = append(arrAll, "%"+kw+"%")
		arr = append(arr, "%"+kw+"%")
		arrAll = append(arrAll, "%"+kw+"%")
	}

	sql += " order by id DESC limit ?,?"
	arr = append(arr, pageSize*(currentPage-1))
	arr = append(arr, pageSize)

	var res []*AppUser
	err := _db.SQL(sql, arr...).Find(&res)
	if err != nil {
		return nil, 0, err
	}

	count, err := _db.SQL(sqlAll, arrAll...).Count()
	if err != nil {
		return nil, 0, err
	}
	if len(res) < 1 {
		return nil, 0, nil
	} else {
		return res, count, nil
	}
}

func AppUserAddOrUpdate(reqUser *baapi.ModAuthor) (isAdd bool, offwOrID int64, err error) {

	if reqUser == nil {
		return isAdd, offwOrID, errors.New("request user is nil")
	}

	info := new(AppUser)
	info.ID = reqUser.ID
	info.UUID = reqUser.UUID
	info.Auth = reqUser.Auth
	info.HeadImage = reqUser.HeadImage
	info.Account = reqUser.Account
	info.Activation = reqUser.Activation
	info.Nickname = reqUser.Nickname
	info.Phone = reqUser.Phone

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = lib.DateTime(now)
	info.UpdatedAtIdxYM = idxTM

	if info.UUID != "" {
		c, err := _db.Where("uuid = ?", info.UUID).Count(new(AppUser))
		fmt.Println("cccccc", c)
		if err != nil {
			return isAdd, offwOrID, err
		}

		if c > 0 {

			minfo := map[string]interface{}{
				"id":                info.ID,
				"nickname":          info.Nickname,
				"head_image":        info.HeadImage,
				"activation":        info.Activation,
				"phone":             info.Phone,
				"ext_info":          info.ExtInfo,
				"updated_at":        time.Time(info.UpdatedAt),
				"auth":              info.Auth,
				"account":           info.Account,
				"updated_at_idx_ym": info.UpdatedAtIdxYM,
			}

			offwOrID, err = _db.Table(AppUserTableName).Where("uuid = ?", info.UUID).Update(minfo)
			return isAdd, offwOrID, err
		}
	}
	isAdd = true
	info.CreatedAt = lib.DateTime(now)
	info.CreatedAtIdxYM = idxTM
	_, err = _db.Insert(info)
	return isAdd, info.ID, err
}
