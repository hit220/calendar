package tables

import (
	"seasonalcal.life/rechy/lib"
	"xorm.io/xorm"
)

/*********************************************
 *
 *   文件
 *   -- 上传文件管理表
 *
 **********************************************/

// AppFileTableName is 表名
const AppFileTableName = "app_file"

// AppFile is
type AppFile struct {
	ID             int64        `json:"id" xorm:"pk autoincr 'id'"`
	UUID           string       `json:"uuid" xorm:"varchar(255) notnull index default('') 'uuid'"`
	UserType       string       `json:"user_type" xorm:"char(32) notnull index default('') 'user_type'"`
	UserID         int64        `json:"user_id" xorm:"notnull index default(0) 'user_id'"`
	Hsah           string       `json:"hash" xorm:"varchar(32) notnull default('') 'hash'"`
	Name           string       `json:"name" xorm:"varchar(200) notnull default('') 'name'"`
	ContentType    string       `json:"content_type" xorm:"varchar(200) notnull default('') 'content_type'"`
	Path           string       `json:"path" xorm:"varchar(200) notnull default('') 'path'"`
	Ext            string       `json:"ext" xorm:"char(32) notnull default('') 'ext'"`
	FileSize       int64        `json:"file_size" xorm:"notnull default(0) 'file_size'"`
	CreatedAt      lib.DateTime `json:"created_at" xorm:"notnull default(0) 'created_at'"`
	CreatedAtIdxYM int          `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
}

func init() {
	tables = append(tables, new(AppFile))
}

func AppFileByID(id int64) (*AppFile, bool, error) {
	re := new(AppFile)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

func AppFileList(currentPage, pageSize int, kw string) ([]*AppFile, int64, error) {
	var qs *xorm.Session
	if kw == "" {
		qs = _db.Where("")
	} else {
		qs = _db.Where("name LIKE ?", "%"+kw+"%")
	}
	total, err := qs.Count(new(AppFile))
	if err != nil {
		return nil, 0, err
	}

	var res []*AppFile
	err = qs.OrderBy("id DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func AppFileAdd(
	userType string,
	userID int64,
	size int64,
	uuid,
	filename,
	ext,
	contentType string) error {
	info := new(AppFile)
	info.UserType = userType
	info.UserID = userID
	info.Name = filename
	info.UUID = uuid
	info.Ext = ext
	info.ContentType = contentType
	_, err := _db.Insert(info)

	return err
}
