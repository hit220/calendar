package tables

import (
	"errors"
	"fmt"

	"seasonalcal.life/rechy/lib"
)

/*********************************************
 *
 *   点赞
 *   -- 点赞管理表
 *
 **********************************************/

// AppLikesTableName is 表名
const AppLikesTableName = "app_likes"

// AppLikes is
type AppLikes struct {
	ID        int64        `json:"id" xorm:"pk autoincr 'id'"`
	UID       int64        `json:"uid" xorm:"notnull default(0) 'uid'"`                               // 点赞者
	KeyType   int          `json:"key_type" xorm:"notnull default(0) 'key_type'"`                     // 引用ID
	FromKey   string       `json:"from_key" xorm:"varchar(255) notnull index default('') 'from_key'"` // 关键键
	Like      int          `json:"like" xorm:"notnull default(0) 'like'"`                             // value: 1=赞, 0=无状态, -1=踩
	CreatedAt lib.DateTime `json:"created_at" xorm:"notnull default(0) 'created_at'"`
}

func init() {
	tables = append(tables, new(AppLikes))
}

func AppLikesList(uid int64, keyType int, keys []string, isNumber bool) (map[interface{}]int, error) {

	inStr, inArgs := lib.SqlArrayToInArgs(keys)
	qArgs := make([]interface{}, 0)
	qArgs = append(qArgs, "select from_key from "+AppLikesTableName+" where uid = ? and key_type = ? and from_key in ("+inStr+") limit 0, 999999")
	qArgs = append(qArgs, uid, keyType)
	qArgs = append(qArgs, inArgs...)

	rows, err := _db.QueryString(qArgs...)
	if err != nil {
		return nil, err
	}

	res := make(map[interface{}]int)
	for _, v := range rows {
		var kk interface{}
		if isNumber {
			kk = lib.ValueToInt64(v["from_key"], 0)
		} else {
			kk = v["from_key"]
		}
		res[kk] = lib.ValueToInt(v["like"], 0)
	}

	return res, nil
}

// value: 1=赞, 0=无状态, -1=踩
func AppLikesSet(uid int64, keyType int, key interface{}, value int) error {
	if value != 1 && value != 0 && value != -1 {
		return errors.New(fmt.Sprintf("unknow value: %d", value))
	}

	one := new(AppLikes)
	sess := _db.NewSession()
	exists, err := sess.Where("uid = ? and key_type = ? and from_key = ?", uid, keyType, key).Get(one)
	if err != nil {
		return err
	}

	if value == 0 {
		if exists {
			_, err := sess.Where("uid = ? and key_type = ? and from_key = ?", uid, keyType, key).Delete(new(AppLikes))
			if err != nil {
				sess.Rollback()
				return err
			}

			sess.Commit()

			if one.Like == 1 {
				err = AppLikesCountLikesUpdate(keyType, key, -1, 0)
			} else if one.Like == -1 {
				err = AppLikesCountLikesUpdate(keyType, key, 0, -1)
			}
			return nil
		}
		return err
	}

	if !exists {
		info := new(AppLikes)
		info.UID = uid
		info.KeyType = keyType
		info.FromKey = lib.ValueToString(key, "")
		info.Like = value
		info.CreatedAt = lib.DateTime(lib.Now())

		_, err := _db.Insert(info)
		if err != nil {
			return err
		}

		if value == 1 {
			err = AppLikesCountLikesUpdate(keyType, key, 1, 0)
		} else if value == -1 {
			err = AppLikesCountLikesUpdate(keyType, key, 0, 1)
		}

		return err
	}

	minfo := map[string]interface{}{
		"like": value,
	}

	offwOrID, err := _db.Table(AppLikesTableName).Where("id = ?", one.ID).Update(minfo)
	if err != nil {
		return err
	}

	if offwOrID > 0 {
		if one.Like == 1 && value == -1 {
			err = AppLikesCountLikesUpdate(keyType, key, -1, 1)
		} else if one.Like == -1 && value == 1 {
			err = AppLikesCountLikesUpdate(keyType, key, 1, -1)
		}
	}
	return err
}
