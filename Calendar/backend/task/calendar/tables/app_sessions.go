package tables

import (
	"seasonalcal.life/rechy/calendar/utils"
)

/*********************************************
 *

 *
 **********************************************/

// AppXXXTableName is 表名
const AppSeasonsTableName = "app_seasons"

// AppXXX is
type AppSeasons struct {
	ID          int64  `json:"id" xorm:"pk autoincr 'id'"`
	BID         int64  `json:"bid" xorm:"notnull default(0) 'bid'"`
	Content     string `json:"content" xorm:"text notnull default('') 'content'"`
	Description string `json:"description" xorm:"mediumtext notnull default('') 'description'"`
	TagBgColor  string `json:"tag_bg_color" xorm:"varchar(32) notnull default('') 'tag_bg_color'"`
	TagFgColor  string `json:"tag_fg_color" xorm:"varchar(32) notnull default('') 'tag_fg_color'"`
	MonthStart  int    `json:"month_start" xorm:"int notnull default(0) 'month_start'"`
	MonthEnd    int    `json:"month_end" xorm:"int notnull default(0) 'month_end'"`
}

func init() {
	tables = append(tables, new(AppSeasons))
}

func AppSeasonsByID(id int64) (*AppSeasons, bool, error) {
	re := new(AppSeasons)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

// public 公开/私有: 1公开 0私有 -1不限
func AppSeasonsList(bid int64, currentPage, pageSize int) ([]*AppSeasons, int64, error) {

	var query string
	var args []interface{}

	if bid > 0 {
		query, args = utils.Where("bid = ?", bid)
	} else {
		query, args = utils.Where("")
	}

	total, err := _db.Where(query, args...).Count(new(AppSeasons))
	if err != nil {
		return nil, 0, err
	}

	var res []*AppSeasons
	err = _db.Where(query, args...).OrderBy("month_start ASC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func AppSeasonsAddOrUpdate(
	reqID,
	reqBID int64,
	reqContent,
	reqDescription,
	reqTagBgColor,
	reqTagFgColor string,
	reqMonthStart,
	reqMonthEnd int) (isAdd bool, offwOrID int64, err error) {

	if reqID < 1 {
		isAdd = true
	}

	info := new(AppSeasons)
	info.BID = reqBID
	info.Content = reqContent
	info.Description = reqDescription
	info.MonthStart = reqMonthStart
	info.MonthEnd = reqMonthEnd
	info.TagBgColor = reqTagBgColor
	info.TagFgColor = reqTagFgColor

	if !isAdd {

		minfo := map[string]interface{}{
			"content":      info.Content,
			"description":  info.Description,
			"month_start":  info.MonthStart,
			"month_end":    info.MonthEnd,
			"tag_bg_color": info.TagBgColor,
			"tag_fg_color": info.TagFgColor,
		}

		offwOrID, err = _db.Table(AppSeasonsTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	_, err = _db.Insert(info)
	return isAdd, info.ID, err
}

func AppSeasonsDelete(bid int64, id string) error {
	_, err := _db.Exec("delete from "+AppSeasonsTableName+" where bid = ? and id = ?", bid, id)
	if err != nil {
		return err
	}
	return nil
}

func AppSeasonsHas(bid, id int64) (bool, error) {
	c, err := _db.Where("id = ? and bid = ?", id, bid).Count(new(AppSeasons))
	if err != nil {
		return false, err
	}
	return c > 0, nil
}
