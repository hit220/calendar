package tables

import (
	"strconv"
	"time"

	"seasonalcal.life/rechy/calendar/utils"
	"seasonalcal.life/rechy/lib"
)

/*********************************************
 *

 *
 **********************************************/

// AppXXXTableName is 表名
const AppBookTableName = "app_book"

// AppXXX is
type AppBook struct {
	ID             int64        `json:"id" xorm:"pk autoincr 'id'"`
	UID            int64        `json:"uid" xorm:"notnull default(0) 'uid'"`
	Title          string       `json:"title" xorm:"text(2000) notnull default('') 'title'"`
	Content        string       `json:"content" xorm:"mediumtext notnull default('') 'content'"`
	Images         string       `json:"images" xorm:"mediumtext notnull default('') 'images'"`
	CreatedAt      lib.DateTime `json:"created_at" xorm:"notnull default('0000-00-00 00:00:00') 'created_at'"`
	CreatedAtIdxYM int          `json:"-" xorm:"notnull default(0) index 'created_at_idx_ym'"`
	UpdatedAt      lib.DateTime `json:"updated_at" xorm:"notnull default('0000-00-00 00:00:00') 'updated_at'"`
	UpdatedAtIdxYM int          `json:"-" xorm:"notnull default(0) index 'updated_at_idx_ym'"`
	Public         int          `json:"pub" xorm:"notnull default(1) index 'pub'"`
	Fabulous       string       `json:"fabulous" xorm:"mediumtext notnull default('') 'fabulous'"` //是否赞（平时不用 在列表的时候返回用）
	FabulousNum    int64        `json:"fabulous_num" xorm:"notnull default(0) 'fabulous_num'"`     //点赞数（平时不用 在列表的时候返回用）
}

func init() {
	tables = append(tables, new(AppBook))
}

func AppBookByID(id int64) (*AppBook, bool, error) {
	re := new(AppBook)
	exists, err := _db.ID(id).Get(re)
	if err != nil {
		// 不返回re, 减少内存引用, 提早释放
		return nil, exists, err
	}
	return re, exists, err
}

// public 公开/私有: 1公开 0私有 -1不限
func AppBookList(uid int64, public int, currentPage, pageSize int, kw string) ([]*AppBook, int64, error) {

	var query string
	var args []interface{}
	if public < 0 {
		if uid > 0 {
			if kw == "" {
				query, args = utils.Where("uid = ?", uid)
			} else {
				query, args = utils.Where("uid = ? and (title LIKE ? or content LIKE ?)", uid, "%"+kw+"%", "%"+kw+"%")
			}
		} else {
			if kw == "" {
				query, args = utils.Where("")
			} else {
				query, args = utils.Where("title LIKE ? or content LIKE ?", "%"+kw+"%", "%"+kw+"%")
			}
		}
	} else {
		if uid > 0 {
			if kw == "" {
				query, args = utils.Where("uid = ? and pub = ?", uid, public)
			} else {
				query, args = utils.Where("uid = ? and pub = ? and (title LIKE ? or content LIKE ?)", uid, public, "%"+kw+"%", "%"+kw+"%")
			}
		} else {
			if kw == "" {
				query, args = utils.Where("pub = ?", public)
			} else {
				query, args = utils.Where("pub = ? and ( title LIKE ? or content LIKE ?)", public, "%"+kw+"%", "%"+kw+"%")
			}
		}
	}

	total, err := _db.Where(query, args...).Count(new(AppBook))
	if err != nil {
		return nil, 0, err
	}

	var res []*AppBook
	err = _db.Where(query, args...).OrderBy("created_at DESC").Limit(pageSize, pageSize*(currentPage-1)).Find(&res)
	if err != nil {
		return nil, 0, err
	}
	return res, total, nil
}

func AppBookAddOrUpdate(
	reqID,
	reqUID int64,
	reqTitle,
	reqContent,
	reqImages string) (isAdd bool, offwOrID int64, err error) {

	if reqID < 1 {
		isAdd = true
	}

	info := new(AppBook)
	info.UID = reqUID
	info.Title = reqTitle
	info.Content = reqContent
	info.Images = reqImages

	now := lib.Now()
	idxTM, _ := strconv.Atoi(now.Format("200601"))
	info.UpdatedAt = lib.DateTime(now)
	info.UpdatedAtIdxYM = idxTM

	if !isAdd {

		minfo := map[string]interface{}{
			"title":             info.Title,
			"content":           info.Content,
			"images":            info.Images,
			"updated_at":        time.Time(info.UpdatedAt),
			"updated_at_idx_ym": info.UpdatedAtIdxYM,
		}

		offwOrID, err = _db.Table(AppBookTableName).Where("id = ?", reqID).Update(minfo)
		return isAdd, offwOrID, err
	}

	info.Public = 1
	info.CreatedAt = lib.DateTime(now)
	cidxTM, _ := strconv.Atoi(now.Format("200601"))
	info.CreatedAtIdxYM = cidxTM

	_, err = _db.Insert(info)
	return isAdd, info.ID, err
}

func AppBookDelete(uid int64, id string) error {
	_, err := _db.Exec("delete from "+AppBookTableName+" where uid = ? and id = ?", uid, id)
	if err != nil {
		return err
	}

	return AppCalendarDeleteByBook(uid, id)
}

func AppBookHas(uid, id int64) (bool, error) {
	c, err := _db.Where("id = ? and uid = ?", id, uid).Count(new(AppBook))
	if err != nil {
		return false, err
	}
	return c > 0, nil
}
