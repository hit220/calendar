package tables

import (
	"seasonalcal.life/rechy/calendar/utils"
	"seasonalcal.life/rechy/lib"
)

/*********************************************
 *
 *   点赞
 *   -- 点赞管理表
 *
 **********************************************/

// AppLikesTableName is 表名
const NewLikesTableName = "new_likes"

// NewLikes is
type NewLikes struct {
	ID        int64        `json:"id" xorm:"pk autoincr 'id'"`
	UID       int64        `json:"uid" xorm:"notnull default(0) 'uid'"` // 点赞者ID
	BID       int64        `json:"bid" xorm:"notnull default(0) 'bid'"` // 日历ID
	CreatedAt lib.DateTime `json:"created_at" xorm:"notnull default(0) 'created_at'"`
}

func init() {
	tables = append(tables, new(NewLikes))
}

func NewLikesList(uid int64) ([]map[string]interface{}, error) {
	sql := "select app_book.* from new_likes inner join app_book on app_book.id = new_likes.bid where new_likes.uid = ?"
	arr := make([]interface{}, 0)
	arr = append(arr, uid)
	sql += " order by app_book.id DESC limit 0,99999"

	var sqlArgs []interface{}
	sqlArgs = append(sqlArgs, sql)
	sqlArgs = append(sqlArgs, arr...)

	res, err := DB().QueryInterface(sqlArgs...)
	if err != nil {
		return nil, err
	}

	if len(res) < 1 {
		return nil, nil
	} else {
		return res, nil
	}
}

func NewLikesSet(uid, bid int64) error {
	one := new(NewLikes)
	sess := _db.NewSession()
	exists, err := sess.Where("uid = ? and bid = ?", uid, bid).Get(one)
	if err != nil {
		return err
	}

	if exists {
		_, err := sess.Where("uid = ? and bid = ?", uid, bid).Delete(new(NewLikes))
		if err != nil {
			sess.Rollback()
			return err
		}

		sess.Commit()
		return nil
	}

	if !exists {
		info := new(NewLikes)
		info.UID = uid
		info.BID = bid
		info.CreatedAt = lib.DateTime(lib.Now())

		_, err := _db.Insert(info)
		if err != nil {
			return err
		}

		return err
	}

	return err
}

func NewLikesOne(uid, bid int64) (bool, error) {
	one := new(NewLikes)
	sess := _db.NewSession()
	exists, err := sess.Where("uid = ? and bid = ?", uid, bid).Get(one)
	return exists, err
}

func NewLikesCount(bid int64) (int64, error) {
	var query string
	var args []interface{}
	query, args = utils.Where("bid = ?", bid)
	total, err := _db.Where(query, args...).Count(new(NewLikes))
	if err != nil {
		return 0, err
	}
	return total, err
}
