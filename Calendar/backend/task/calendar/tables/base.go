package tables

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/rand"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/syndtr/goleveldb/leveldb"
	"golang.org/x/crypto/bcrypt"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"xorm.io/xorm"
	xlog "xorm.io/xorm/log"
)

var _db *xorm.Engine
var tables []interface{} = make([]interface{}, 0)

type ULevel int

// 初始化数据库连接, 连接池
func DBInit(debug bool) (*xorm.Engine, error) {
	driver := cfg.String("db.driver", "mysql")
	datasourceName := cfg.String("db.source", "root:123456@tcp(127.0.0.1:3306)/my_db?charset=utf8")
	if driver == "sqlite" {
		driver = "sqlite3"
	}
	engine, err := xorm.NewEngine(driver, datasourceName)
	if err != nil {
		return nil, err
	}

	// 同步表结构
	err = engine.Sync2(tables...)
	if err != nil {
		return nil, err
	}

	/*************** 数据库日志 ***************/
	wt := glog.WriterExt("DB", &glog.LogBridgeOptions{
		Prefix:          "DB",
		BaseDepth:       3,
		ReplaceMap:      nil,
		ShowHeaderTrace: true,
	})
	engine.SetLogger(xlog.NewSimpleLogger(wt))
	if debug {
		engine.ShowSQL(true) // 则会在控制台打印出生成的SQL语句
	}

	_db = engine
	return engine, nil
}

func DBClose() error {
	if _db != nil {
		return _db.Close()
	}
	return nil
}

func DB() *xorm.Engine {
	return _db
}

func FormatInterface(arr []map[string]interface{}) []map[string]interface{} {
	for _, v := range arr {
		for ki, vi := range v {
			switch vi.(type) {
			case []byte:
				v[ki] = string(vi.([]byte))
			default:
			}
		}
	}
	return arr
}

func GenUserSha256(password string) string {
	if password == "" {
		return ""
	}
	h := sha256.New()
	h.Write([]byte(password))
	buf := h.Sum(nil)
	return hex.EncodeToString(buf)
}

// GenUserHashPassword is 加密密码
func GenUserHashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// GenValidateCode  is  生成随机数
func GenValidateCode(width int) string {
	numeric := [10]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	r := len(numeric)
	rand.Seed(time.Now().UnixNano())

	var sb strings.Builder
	for i := 0; i < width; i++ {
		fmt.Fprintf(&sb, "%d", numeric[rand.Intn(r)])
	}
	return sb.String()
}

var _kvdb *leveldb.DB

func KVDBInit(kvDBPath string) (*leveldb.DB, error) {
	db, err := leveldb.OpenFile(kvDBPath, nil)
	if err != nil {
		return nil, err
	}
	_kvdb = db
	return _kvdb, nil
}
func KVDB() *leveldb.DB {
	return _kvdb
}
func KVDBClose() error {
	if _kvdb != nil {
		return _kvdb.Close()
	}

	return nil
}
