package tables

import "seasonalcal.life/rechy/lib"

/*********************************************
 *
 *   点赞
 *   -- 点赞管理表
 *
 **********************************************/

// AppLikesTableName is 表名
const AppLikesCountTableName = "app_likes_count"

// AppLikes is
type AppLikesCount struct {
	ID      int64  `json:"id" xorm:"pk autoincr 'id'"`
	KeyType int    `json:"key_type" xorm:"notnull default(0) 'key_type'"`
	FromKey string `json:"from_key" xorm:"varchar(255) notnull index default('') 'from_key'"` // 关键键
	Likes   int64  `json:"likes" xorm:"notnull default(0) 'likes'"`
	Unlikes int64  `json:"unlikes" xorm:"notnull default(0) 'unlikes'"`
}

func init() {
	tables = append(tables, new(AppLikesCount))
}

func AppLikesCountList(keyType int, keys []interface{}) ([]*AppLikesCount, error) {

	retryCount := 0
RETRY_1:
	retryCount++

	inStr, inArgs := lib.SqlArrayToInArgs(keys)
	inArgs = append(inArgs, keyType)

	var res []*AppLikesCount
	err := _db.Where("from_key in ("+inStr+") and key_type = ?", inArgs...).Limit(0, 999999).Find(res)
	if err != nil {
		return nil, err
	}

	if len(keys) != len(res) {
		AppLikesCountModify(keyType, keys)

		if retryCount < 3 {
			goto RETRY_1
		}
	}

	return res, nil
}

func AppLikesCountModify(keyType int, keys []interface{}) error {
	for _, key := range keys {
		sess := _db.NewSession()
		c, err := sess.Where("key_type = ? and from_key = ?", keyType, key).Count(new(AppLikesCount))
		if err != nil {
			sess.Rollback()
			return err
		}

		if c > 0 {
			continue
		}

		info := new(AppLikesCount)
		info.KeyType = keyType
		info.FromKey = lib.ValueToString(key, "")
		_, err = sess.Insert(info)
		if err != nil {
			return err
		}

		sess.Commit()
	}
	return nil
}

func AppLikesCountLikesUpdate(keyType int, fromKey interface{}, like int, unlike int) error {
	retryCount := 0
RETRY_1:
	retryCount++
	res, err := _db.Exec("update "+AppLikesCountTableName+" set like+=?,unlikes+=? where key_type = ? and from_key = ?",
		like, unlike, keyType, fromKey)
	if err != nil {
		return err
	}

	offw, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if offw < 1 {
		err = AppLikesCountModify(keyType, []interface{}{fromKey})
		if err != nil {
			return err
		}
		if retryCount < 3 {
			goto RETRY_1
		}
	}
	return nil
}
