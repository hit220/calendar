package tables

import "seasonalcal.life/rechy/lib"

/*********************************************
 *
 *   评论
 *   -- 评论管理表
 *
 **********************************************/

// AppCommentTableName is 表名
const AppCommentTableName = "app_comment"

// AppComment is
type AppComment struct {
	ID             int64        `json:"id" xorm:"pk autoincr 'id'"`
	FID            int64        `json:"fid" xorm:"notnull default(0) 'fid'"`                               // 引用ID
	UID            int64        `json:"uid" xorm:"notnull default(0) 'uid'"`                               // 发布者
	FromKey        string       `json:"from_key" xorm:"varchar(255) notnull index default('') 'from_key'"` // 关键键
	Content        string       `json:"content" xorm:"mediumtext notnull default('') 'content'"`           // 内容
	CreatedAt      lib.DateTime `json:"created_at" xorm:"notnull default(0) 'created_at'"`
	CreatedAtIdxYM int          `json:"created_at_idx_ym" xorm:"notnull default(0) index 'created_at_idx_ym'"`
}

func init() {
	tables = append(tables, new(AppComment))
}
