package utils

import (
	"strings"

	cfg "seasonalcal.life/rechy/config"
)

var likeTypes map[string]int

func Init() {
	p := cfg.String("like_types", "default")
	arr := strings.Split(p, ",")
	mp := make(map[string]int)
	for k, v := range arr {
		mp[v] = k + 1
	}
}

func InArray(arr []string, key string) bool {
	for _, v := range arr {
		if v == key {
			return true
		}
	}
	return false
}

func LikeType(t string) int {
	if likeTypes == nil {
		return 0
	}

	return likeTypes[t]
}
