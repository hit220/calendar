package utils

import (
	"errors"
	"fmt"
	"image"
	"os"
	"path/filepath"
	"strings"

	"github.com/hunzsig/graphics"
	"golang.org/x/image/bmp"
	"golang.org/x/image/tiff"
	"golang.org/x/image/webp"
	"seasonalcal.life/rechy/lib"
)

func ImageResize(in, out, t, ext string, width, height int) error {

	if in == "" {
		return errors.New("image file not found")
	}

	if !lib.ArrayHave([]string{"cover", "contain"}, t) {
		t = "contain"
	}

	if !lib.IsExists(in) {
		return fmt.Errorf("image file not exists")
	}

	//
	inExt := strings.ToLower(filepath.Ext(in))

	var fn lib.ImageSourceFunc
	if inExt == ".webp" || inExt == ".tiff" || inExt == ".bmp" {
		fn = parseAddition
	}
	src, err := lib.ImageRGBA(in, fn)
	if err != nil {
		return fmt.Errorf("image open fail: %v", err)
	}

	if src == nil {
		return fmt.Errorf("image read fail: %v", in)
	}

	bounds := src.Bounds()
	dx := bounds.Dx()
	dy := bounds.Dy()
	if width < 1 {
		width = dx
	}
	if height < 1 {
		height = dy
	}
	if width < 0 || height < 0 {
		return fmt.Errorf("error size: %dx%d", width, height)
	}
	srcRate := float64(dx) / float64(dy)

	r1 := dx / width
	r2 := dy / height
	switch t {
	case "cover":
		if r1 > r2 {
			// 以高计算
			width = int(float64(height) * srcRate)
		} else {
			height = int(float64(width) / srcRate)
		}
	case "contain":
		if dx > width || dy > height {
			if r1 > r2 {
				// 以宽计算
				height = int(float64(width) / srcRate)
			} else {
				width = int(float64(height) * srcRate)
			}
		}
	default:
	}

	dst := image.NewRGBA(image.Rect(0, 0, width, height))
	_ = graphics.Scale(dst, src)

	f, err := os.Create(out)
	if err != nil {
		return err
	}

	defer f.Close()
	ext = strings.Trim(ext, ".")
	err = lib.ImageEncode(f, dst, ext)
	if err != nil {
		return err
	}

	return nil
}

func parseAddition(p string) (image.Image, error) {
	f, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	if strings.HasSuffix(p, ".webp") {
		return webp.Decode(f)
	} else if strings.HasSuffix(p, ".tiff") {
		return tiff.Decode(f)
	} else if strings.HasSuffix(p, ".bmp") {
		return bmp.Decode(f)
	}

	return nil, errors.New("image parse not support:" + filepath.Ext(p))
}
