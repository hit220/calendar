package api

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin"
	guuid "github.com/satori/go.uuid"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/calendar/utils"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

const (
	DefaultMultipartMemory = 32 * 1024 * 1024 // 32MB
	MaxFileSize            = 80 * 1024 * 1024 // 80MB
	ImageMaxFileSize       = 800 * 1024       // 800KB
	ImageFormatSize        = 2500             // px
)

var (
	// 这几种搁置做整理
	ImageFormat = []string{".jpg", ".jpeg", ".bmp", ".gif", ".png", ".webp", ".tiff", ".jfif"}
)

// @Summary 访问文件
// @Produce  json
// @Param uuid path string true "UUID"
// @Success 200 {string} string "file bytes"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/file/{uuid} [get]
func FileHandler(c *gin.Context) {
	uuid := c.Param("uuid")
	if uuid == "" {
		baapi.JSONReply(c, "error", "not found")
		return
	}

	filepath := filepath.Join(OpenUploadDir(), uuid, uuid)
	fmt.Println(filepath)
	c.File(filepath)
}

// @Summary 文件上传 - 开放(不需要登录)
// @Accept  multipart/form-data
// @Produce  json
// @Param   file formData file true  "文件上传"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 400 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/upload [post]
func OpenFileUploadHandler(c *gin.Context) {

	err := c.Request.ParseMultipartForm(DefaultMultipartMemory)
	form := c.Request.MultipartForm
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	files := form.File["file"]
	if len(files) < 1 {
		baapi.JSONReply(c, "error", "Upload fail: #001")
		return
	}

	if files[0].Size > MaxFileSize {
		baapi.JSONReply(c, "error", "Upload fail: #002")
		return
	}

	ext := FileExt(files[0].Filename)
	oldExt := ""
	if lib.InArrayString(ImageFormat, strings.ToLower(ext)) {
		if files[0].Size > ImageMaxFileSize {
			oldExt = ext
			ext = ".jpg"
		}
	}

	// 文件唯一id
	uuid := guuid.NewV4().String()
	uuid = uuid + strings.ToLower(ext)

	// 添加到数据库
	err = tables.AppFileAdd(
		"",
		0,
		files[0].Size,
		uuid, files[0].Filename,
		ext,
		files[0].Header.Get("Content-Type"))
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	// 保存文件
	uploadDir := OpenUploadDir()
	// {baseDir}/{userId}/{uuid}
	filename := lib.EnsureFilePath(filepath.Join(uploadDir, uuid, uuid))
	err = c.SaveUploadedFile(files[0], filename+oldExt)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	if oldExt != "" {
		err = utils.ImageResize(filename+oldExt,
			filename,
			"contain",
			".jpg",
			ImageFormatSize,
			ImageFormatSize,
		)
		if err != nil {
			baapi.JSONReply(c, "error", err)
			return
		}
	}

	formValues := url.Values(form.Value)
	name := formValues.Get("name")
	if name != "" {
		name = lib.ToFilename(name) + ".txt"
	} else {
		name = files[0].Filename + ".txt"
	}

	filenameInfo := lib.EnsureFilePath(filepath.Join(uploadDir,
		uuid, name))
	err = ioutil.WriteFile(filenameInfo,
		[]byte(lib.JsonEncodeIndent(lib.MapHttpMap(formValues))),
		0777)
	if err != nil {
		baapi.JSONReply(c, "error", err)
		return
	}

	baapi.JSONReply(c, "success", "Success", gin.H{
		"uuid":         uuid,
		"ext":          ext,
		"size":         files[0].Size,
		"filename":     files[0].Filename,
		"content_type": files[0].Header.Get("Content-Type"),
	})
}

func UploadDir() string {
	dir := cfg.String("api.upload_dir", "./_data/upload")
	return lib.EnsureDir(dir)
}

func OpenUploadDir() string {
	dir := cfg.String("api.upload_dir_open", "./_data/upload_open")
	return lib.EnsureDir(dir)
}

func FileExt(p string) string {
	p = regexp.MustCompile(`(?is)[^0-9a-zA-Z.]+`).ReplaceAllString(p, "")
	ext := filepath.Ext(p)
	if ext != "" {
		ext2 := filepath.Ext(strings.TrimSuffix(p, ext))
		if ext2 != "" {
			ext = ext2 + ext
		}
	}
	return strings.ToLower(ext)
}
