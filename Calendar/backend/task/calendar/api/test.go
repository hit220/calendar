package api

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/glog"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 测试
// @Accept  json
// @Produce  json
// @Param param1 query string false "参数1"
// @Param param2 query string false "参数2"
// @Success 200 {object} baapi.ResponseMessage "{code:success, message:...}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/test [get]
func Test(c *gin.Context) {
	var req struct {
		Param1 string `form:"param1" json:"param1" binding:"-"`
		Param2 string `form:"param2" json:"param2" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	glog.Infolns(req)

	baapi.JSONReply("success", "测试成功", gin.H{
		"test": "测试内容",
	})

}
