package v1

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 日历分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/seasons/list [get]
func SeasonsListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		BID string `form:"bid" json:"bid" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	bid := lib.ValueToInt64(req.BID, 0)
	data, total, err := tables.AppSeasonsList(bid, pg, pgz)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 日历详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/seasons/detail [get]
func SeasonsDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.AppSeasonsByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "Success", one)
}

// @Summary 日历新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Param auth_paths query string true "权限值"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/seasons/add_or_update [post]
func SeasonsAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID          string `form:"id" json:"id" binding:"-"`
		BID         string `form:"bid" json:"bid" binding:"-"`
		Content     string `form:"content" json:"content" binding:"required"`
		Description string `form:"description" json:"description" binding:"-"`
		MonthStart  string `form:"month_start" json:"month_start" binding:"-"`
		MonthEnd    string `form:"month_end" json:"month_end" binding:"-"`
		TagBgColor  string `form:"tag_bg_color" json:"tag_bg_color" binding:"-"`
		TagFgColor  string `form:"tag_fg_color" json:"tag_fg_color" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	isAdd, offwOrID, err := tables.AppSeasonsAddOrUpdate(
		lib.ValueToInt64(req.ID, 0),
		lib.ValueToInt64(req.BID, 0),
		req.Content,
		req.Description,
		req.TagBgColor,
		req.TagFgColor,
		lib.ValueToInt(req.MonthStart, 0),
		lib.ValueToInt(req.MonthEnd, 0),
	)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}
