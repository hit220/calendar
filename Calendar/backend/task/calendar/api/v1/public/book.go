package public

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 日历分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/v1/book/list [get]
func BookListHandler(c *gin.Context) {
	var req struct {
		Pg  string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw  string `form:"kw" json:"kw" binding:"-"`
		Uid string `form:"uid" json:"uid" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.AppBookList(-1, 1, pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	if req.Uid != "" {
		for _, v := range data {
			ok, err := tables.NewLikesOne(lib.ValueToInt64(req.Uid, 0), v.ID)
			if err != nil {
				baapi.JSONReply(c, "error", err, nil)
				return
			}
			if ok {
				v.Fabulous = "yes"
			} else {
				v.Fabulous = "no"
			}
			//统计点赞数
			countNum, err := tables.NewLikesCount(v.ID)
			if err != nil {
				baapi.JSONReply(c, "error", err, nil)
				return
			}
			v.FabulousNum = countNum
		}
	}

	baapi.JSONReply(c, "success", "Success",
		baapi.ToPageData(data, pg, pgz, int(total)))
}
