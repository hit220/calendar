package public

import (
	"strings"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/calendar/utils"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

func LikeListHandler(c *gin.Context) {
	var req struct {
		IsNumber string `form:"is_num" json:"is_num" binding:"-"`
		Type     string `form:"type" json:"type" binding:"required"`
		Keys     string `form:"keys" json:"keys" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	isNumber := lib.ValueToBool(req.IsNumber, false)
	keyType := utils.LikeType(req.Type)
	keys := strings.Split(req.Keys, ",")

	data, err := tables.AppLikesList(uid, keyType, keys, isNumber)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", data)
}
