package public

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 日历分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/v1/calendar/list [get]
func CalendarListHandler(c *gin.Context) {
	var req struct {
		Pg    string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz   string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		BID   string `form:"bid" json:"bid" binding:"required"`
		Kw    string `form:"kw" json:"kw" binding:"-"`
		Month string `form:"month" json:"month" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	bid := lib.ValueToInt64(req.BID, 0)
	data, total, err := tables.AppCalendarList(bid, 1, pg, pgz, req.Kw, req.Month)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

func CalendarImagesHandler(c *gin.Context) {
	var req struct {
		ByUID string `form:"by_uid" json:"by_uid" binding:"-"` // 单用户
		Pgz   string `form:"pgz" json:"pgz" binding:"-"`       // 每页记录数
		UID   string `form:"uid" json:"uid" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(1, lib.ValueToInt(req.Pgz, 0))

	var uid int64 = -1
	if req.UID != "" {
		uid = lib.ValueToInt64(req.UID, 0)
	}
	data, total, err := tables.AppCalendarImage(uid, 1, pgz)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 日历详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/v1/calendar/detail [get]
func CalendarDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.AppCalendarByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	} else if one.Public != 1 {
		one = nil
	}

	baapi.JSONReply(c, "success", "Success", one)
}

//
func CalendarMonths(c *gin.Context) {
	var req struct {
		BID string `form:"bid" json:"bid" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	bid := lib.ValueToInt64(req.BID, 0)
	monthCells, err := tables.AppCalendarMonthCells(bid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", monthCells)
}
