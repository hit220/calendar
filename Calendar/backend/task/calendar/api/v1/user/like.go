package user

import (
	"strings"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/calendar/utils"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

func LikeListHandler(c *gin.Context) {
	var req struct {
		IsNumber string `form:"is_num" json:"is_num" binding:"-"`
		Type     string `form:"type" json:"type" binding:"required"`
		Keys     string `form:"keys" json:"keys" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	isNumber := lib.ValueToBool(req.IsNumber, false)
	keys := strings.Split(req.Keys, ",")
	fromType := utils.LikeType(req.Type)
	data, err := tables.AppLikesList(uid, fromType, keys, isNumber)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", data)
}

func LikeListNew(c *gin.Context) {
	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	data, err := tables.NewLikesList(uid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	for _, v := range data {
		v["fabulous"] = "yes"
		// ok, err := tables.NewLikesOne(uid, lib.ValueToInt64(v["id"], 0))
		// if err != nil {
		// 	baapi.JSONReply(c, "error", err, nil)
		// 	return
		// }
		// if ok {
		// 	data[k]["fabulous"] = "yes"
		// } else {
		// 	data[k]["fabulous"] = "no"
		// }
	}

	baapi.JSONReply(c, "success", "Success", data)
}

func LikeSet(c *gin.Context) {
	var req struct {
		Bid string `form:"bid" json:"bid" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	err = tables.NewLikesSet(uid, lib.ValueToInt64(req.Bid, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", nil)
}
