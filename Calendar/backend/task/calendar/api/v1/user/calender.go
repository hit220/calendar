package user

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 日历分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/user/v1/calendar/list [get]
func CalendarListHandler(c *gin.Context) {
	var req struct {
		BID   string `form:"bid" json:"bid" binding:"required"` //
		Pg    string `form:"pg" json:"pg" binding:"-"`          // 当前页
		Pgz   string `form:"pgz" json:"pgz" binding:"-"`        // 每页记录数
		Month string `form:"month" json:"month" binding:"-"`
		Kw    string `form:"kw" json:"kw" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	bid := lib.ValueToInt64(req.BID, 0)
	has, err := tables.AppBookHas(uid, bid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !has {
		baapi.JSONReply(c, "error", "no record", nil)
		return
	}

	data, total, err := tables.AppCalendarList(bid, -1, pg, pgz, req.Kw, req.Month)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 日历详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/user/v1/calendar/detail [get]
func CalendarDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.AppCalendarByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	} else {
		if one.Public != 1 {
			has, err := tables.AppBookHas(uid, one.BID)
			if err != nil {
				baapi.JSONReply(c, "error", err, nil)
				return
			}

			if !has {
				baapi.JSONReply(c, "error", "no record", nil)
				return

			}
		}

	}

	baapi.JSONReply(c, "success", "Success", one)
}

// @Summary 日历新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Param auth_paths query string true "权限值"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/user/v1/calendar/add_or_update [post]
func CalendarAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID        string `form:"id" json:"id" binding:"-"`
		BID       string `form:"bid" json:"bid" binding:"required"`
		Title     string `form:"title" json:"title" binding:"required"`
		Content   string `form:"content" json:"content" binding:"required"`
		CreatedAt string `form:"created_at" json:"created_at" binding:"-"`
		Images    string `form:"images" json:"images" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	bid := lib.ValueToInt64(req.BID, 0)
	has, err := tables.AppBookHas(uid, bid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !has {
		baapi.JSONReply(c, "error", "no record", nil)
		return
	}

	isAdd, offwOrID, err := tables.AppCalendarAddOrUpdate(
		lib.ValueToInt64(req.ID, 0),
		bid,
		req.Title,
		req.Content,
		req.Images,
		req.CreatedAt)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}

//
func CalendarMonths(c *gin.Context) {
	var req struct {
		BID string `form:"bid" json:"bid" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	bid := lib.ValueToInt64(req.BID, 0)
	has, err := tables.AppBookHas(uid, bid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !has {
		baapi.JSONReply(c, "error", "no record", nil)
		return
	}

	monthCells, err := tables.AppCalendarMonthCells(bid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", monthCells)
}

func CalendarDelete(c *gin.Context) {
	var req struct {
		ID  string `form:"id" json:"id" binding:"required"`
		BID string `form:"bid" json:"bid" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	uid, err := baapi.GetUserID(c)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	bid := lib.ValueToInt64(req.BID, 0)
	has, err := tables.AppBookHas(uid, bid)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !has {
		baapi.JSONReply(c, "error", "no record", nil)
		return
	}

	err = tables.AppCalendarDelete(bid, req.ID)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success")
}
