package v1

import (
	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/tables"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

// @Summary 日历分页列表
// @Accept  json
// @Produce  json
// @Param pg query string false "当前第几页"
// @Param pgz query string false "每页记录数"
// @Param kw query string false "关键词"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/book/list [get]
func BookListHandler(c *gin.Context) {
	var req struct {
		Pg    string `form:"pg" json:"pg" binding:"-"`   // 当前页
		Pgz   string `form:"pgz" json:"pgz" binding:"-"` // 每页记录数
		Kw    string `form:"kw" json:"kw" binding:"-"`
		Month string `form:"month" json:"month" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}
	pg, pgz := baapi.FormatPageParams(lib.ValueToInt(req.Pg, 0), lib.ValueToInt(req.Pgz, 0))

	data, total, err := tables.AppBookList(-1, -1, pg, pgz, req.Kw)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success",
		baapi.ToPageData(data, pg, pgz, int(total)))
}

// @Summary 日历详情
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/book/detail [get]
func BookDetailHandler(c *gin.Context) {
	var req struct {
		ID string `form:"id" json:"id" binding:"required"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	one, exists, err := tables.AppBookByID(lib.ValueToInt64(req.ID, 0))
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if !exists {
		one = nil
	}

	baapi.JSONReply(c, "success", "Success", one)
}

// @Summary 日历新加或编辑
// @Accept  json
// @Produce  json
// @Param id query string true "ID"
// @Param name query string true "名称"
// @Param auth_paths query string true "权限值"
// @Success 200 {object} baapi.ResponsePageData "{code:error, message:..., data: {[age_size: 12...]}}"
// @Failure 203 {object} baapi.ResponseMessage "{code:error, message:...}"
// @Router /api/master/v1/book/add_or_update [post]
func BookAddOrUpdateHandler(c *gin.Context) {
	var req struct {
		ID      string `form:"id" json:"id" binding:"-"`
		UID     string `form:"uid" json:"uid" binding:"-"`
		Title   string `form:"title" json:"title" binding:"required"`
		Content string `form:"content" json:"content" binding:"required"`
		Images  string `form:"images" json:"images" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	isAdd, offwOrID, err := tables.AppBookAddOrUpdate(
		lib.ValueToInt64(req.ID, 0),
		lib.ValueToInt64(req.UID, 0),
		req.Title,
		req.Content,
		req.Images)
	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	if err != nil {
		baapi.JSONReply(c, "error", err, nil)
		return
	}

	baapi.JSONReply(c, "success", "Success", gin.H{
		"is_add":     isAdd,
		"offw_or_id": offwOrID,
	})
}
