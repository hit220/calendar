package router

import (
	"errors"
	"fmt"
	"math"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/syndtr/goleveldb/leveldb"
	"seasonalcal.life/rechy/calendar/api"
	"seasonalcal.life/rechy/calendar/keys"
	"seasonalcal.life/rechy/calendar/tables"
	cfg "seasonalcal.life/rechy/config"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
	jwt "seasonalcal.life/rechy/se.ba.api/jwt-v2"
)

const (
	JwtRealm               = "SE"
	JwtTokenHeadName       = "SSE"
	SyncUpdateUserDuration = time.Minute * 30
)

var syncUserClock = lib.NewVarClock(SyncUpdateUserDuration)

var identityKey = "id"

type KVDBInfo struct {
	aa *leveldb.DB
}

func (kv KVDBInfo) Get(key string, def string) (string, error) {
	resStr, err := kv.aa.Get([]byte(key), nil)
	if err != nil {
		return "", err
	}
	return string(resStr), nil
}
func (kv KVDBInfo) Put(key string, val string, def string) error {
	err := kv.aa.Put([]byte(key), []byte(val), nil)
	return err
}

func getJwtAuth() (*jwt.GinJWTMiddleware, error) {
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:            JwtRealm,
		SigningAlgorithm: baapi.JwtSigningAlgorithm,
		Timeout:          30 * time.Minute,
		MaxRefresh:       20 * time.Minute,
		IdentityKey:      identityKey,

		// 从data一个用户里找个唯一值做token存储的id
		// BA执行
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			fmt.Println("payload+++")
			if v, ok := data.(*baapi.ModAuthor); ok {

				m := jwt.MapClaims{
					"author":    v.EncodeToGZip(),
					identityKey: fmt.Sprintf("%v-%v", v.Type, v.ID),
				}
				return m
			}
			return jwt.MapClaims{}
		},

		// 从token存储的id值, 再读取出用户的相关账户信息
		// APP执行
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			author, _ := claims["author"].(string)
			user := baapi.NewModAuthor([]byte(author), true)

			// 同步用户信息
			if user.Type == baapi.TypeUser {
				if syncUserClock.IsExpire(user.ID) {
					_, _, err := tables.AppUserAddOrUpdate(user)
					if err == nil {
						syncUserClock.Update(user.ID)
					}
				}
			}

			return user
		},

		// 登录时验证账号密码是否正常, 返回用户信息, err为nil就是登录成功
		// BA/APP执行
		Authenticator: DebugLoginHandler,

		// 这个方法就是认证当前页面是否有权操作
		// BA/APP执行
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if v, ok := data.(*baapi.ModAuthor); ok {
				cacheDB := &KVDBInfo{tables.KVDB()}
				cacheAuthPaths, err := baapi.GetCacheAuthPaths(cacheDB, v.ID, v.Type, v.Auth)
				if err != nil {
					glog.Errorln(err)
					return false
				}
				// return v.HasAuth(c.Request)
				return baapi.DoHasAuthNew(c.Request, cacheAuthPaths)
			}

			return false
		},

		// 无权操作, 格式化回复内容
		// BA/APP执行
		Unauthorized: api.Unauthorized,

		// 登录返回
		// BA执行
		LoginResponse: api.LoginResponse,

		// 出返回
		// BA执行
		LogoutResponse: api.LogoutResponse,

		ModifyTokenHeader: modifyTokenHeader,
		KeyFuncVerify:     keyFuncVerify,
		KeyFuncSign:       keyFuncSign,

		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: JwtTokenHeadName,
		TimeFunc:      time.Now,
		SendCookie:    false,
	})

	if err != nil {
		return nil, err
	}

	errInit := authMiddleware.MiddlewareInit()
	if errInit != nil {
		return nil, errInit
	}

	return authMiddleware, nil
}

func modifyTokenHeader(header map[string]interface{}) map[string]interface{} {
	kmgr := keys.GetKeyManager()
	return kmgr.ModifyTokenHeader(header)
}

func keyFuncVerify(header map[string]interface{}) (interface{}, error) {
	kmgr := keys.GetKeyManager()
	return kmgr.KeyFuncVerify(header)
}

func keyFuncSign(header map[string]interface{}) (interface{}, error) {
	kmgr := keys.GetKeyManager()
	return kmgr.KeyFuncSign(header)
}

func DebugLoginHandler(c *gin.Context) (interface{}, time.Duration, error) {
	debug := cfg.Bool("module.debug_login", false)
	if !debug {
		return nil, 0, errors.New("this is ba client")
	}

	var req struct {
		Type       baapi.Type `form:"type" json:"type" binding:"required"`
		Module     string     `form:"module" json:"module" binding:"required"`
		Username   string     `form:"username" json:"username" binding:"required"`
		Password   string     `form:"password" json:"password" binding:"required"`
		ApplyToken string     `form:"apply_token" json:"apply_token" binding:"-"`
		Device     string     `form:"device" json:"device" binding:"-"`
	}
	if err := c.ShouldBind(&req); err != nil {
		return "", 0, jwt.ErrMissingLoginValues
	}

	// if req.Type != baapi.TypeMaster {
	// return nil, errors.New("not support")
	// }

	timeToken := time.Hour
	if req.Type == baapi.TypeMaster {
		timeToken = 3 * time.Hour
	} else if req.Type == baapi.TypeUser {
		timeToken = 24 * time.Hour
	} else {
		timeToken = 1 * time.Hour
	}

	username := cfg.String("module.debug_username", "admin")
	password := cfg.String("module.debug_password", "123456")
	if username != req.Username || password != req.Password {
		return nil, 0, errors.New(" incorrect username or password")
	}

	user := &baapi.ModAuthor{
		Type:       req.Type,
		ID:         int64(math.MaxUint32),
		Nickname:   "Debug " + string(req.Type),
		HeadImage:  "",
		ApplyToken: req.ApplyToken,
		Device:     req.Device,
		Auth:       "** [ANY]",
	}

	c.Set("user", user)
	c.Set("JWT_AUD", req.Module)

	return user, timeToken, nil
}
