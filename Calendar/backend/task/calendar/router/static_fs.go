package router

import (
	"errors"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type staticDir struct {
	prefix       string
	dir          string
	defaultIndex string
}

func newStaticDir(prefix, dir, defaultIndex string) *staticDir {
	return &staticDir{
		prefix:       prefix,
		dir:          dir,
		defaultIndex: defaultIndex,
	}
}

func mapDirOpenError(originalErr error, name string) error {
	if os.IsNotExist(originalErr) || os.IsPermission(originalErr) {
		return originalErr
	}

	parts := strings.Split(name, string(filepath.Separator))
	for i := range parts {
		if parts[i] == "" {
			continue
		}
		fi, err := os.Stat(strings.Join(parts[:i+1], string(filepath.Separator)))
		if err != nil {
			return originalErr
		}
		if !fi.IsDir() {
			return os.ErrNotExist
		}
	}
	return originalErr
}

func (d *staticDir) Handler(w http.ResponseWriter, r *http.Request) {
	fileServer := http.StripPrefix(d.prefix, http.FileServer(d))
	fileServer.ServeHTTP(w, r)
}

func (d *staticDir) Open(name string) (http.File, error) {
	// 无扩展名请求定位至index.html
	if d.defaultIndex != "" && name != "/" && path.Ext(name) == "" {
		name = "/" + d.defaultIndex
	}

	if filepath.Separator != '/' && strings.ContainsRune(name, filepath.Separator) {
		return nil, errors.New("http: invalid character in file path")
	}
	dir := d.dir
	if dir == "" {
		dir = "."
	}
	fullName := filepath.Join(dir, filepath.FromSlash(path.Clean("/"+name)))
	f, err := os.Open(fullName)
	// glog.Infoln(fullName)
	if err != nil {
		return nil, mapDirOpenError(err, fullName)
	}
	return f, nil
}
