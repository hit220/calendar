package router

import (
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
	"seasonalcal.life/rechy/calendar/api"
	v1 "seasonalcal.life/rechy/calendar/api/v1"
	"seasonalcal.life/rechy/calendar/api/v1/public"
	"seasonalcal.life/rechy/calendar/api/v1/user"
	"seasonalcal.life/rechy/calendar/keys"
	"seasonalcal.life/rechy/glog"
	"seasonalcal.life/rechy/lib"
	baapi "seasonalcal.life/rechy/se.ba.api"
)

func Init(e *gin.Engine) {

	/*******初始化操作权限路由范围 ******/
	baapi.InitBind(e, &PubHub{})

	// 初始化密钥管理器
	keys.GetKeyManager()

	/**********************/
	authMiddleware, err := getJwtAuth()
	if err != nil {
		glog.Fatalf("get jwt middleware err [%+v]", err)
		return
	}
	apiGroup := e.Group("/api")
	{

		// 无权限验证
		apiGroup.GET("/test", api.Test)
		apiGroup.POST("/login", authMiddleware.LoginHandler)
		e.POST("/api/upload", api.OpenFileUploadHandler)
		e.GET("/api/file/:uuid", api.FileHandler)
		e.GET("/api/v1/calendar/list", public.CalendarListHandler)
		e.GET("/api/v1/calendar/images", public.CalendarImagesHandler)
		e.GET("/api/v1/calendar/detail", public.CalendarDetailHandler)
		e.GET("/api/v1/calendar/month_cells", public.CalendarMonths)
		e.GET("/api/v1/book/list", public.BookListHandler)
		e.GET("/api/v1/seasons/list", public.SeasonsListHandler)

	}

	userGroup := e.Group(baapi.RouteApiUser)
	userGroup.Use(authMiddleware.MiddlewareFunc())
	{
		userGroup.GET("/v1/calendar/list", user.CalendarListHandler)
		userGroup.GET("/v1/calendar/detail", user.CalendarDetailHandler)
		userGroup.POST("/v1/calendar/add_or_update", user.CalendarAddOrUpdateHandler)
		userGroup.GET("/v1/calendar/delete", user.CalendarDelete)
		userGroup.GET("/v1/calendar/month_cells", user.CalendarMonths)
		userGroup.GET("/v1/book/list", user.BookListHandler)
		userGroup.GET("/v1/book/detail", user.BookDetailHandler)
		userGroup.POST("/v1/book/add_or_update", user.BookAddOrUpdateHandler)
		userGroup.GET("/v1/book/delete", user.BookDelete)
		userGroup.GET("/v1/seasons/list", user.SeasonsListHandler)
		userGroup.GET("/v1/seasons/detail", user.SeasonsDetailHandler)
		userGroup.POST("/v1/seasons/add_or_update", user.SeasonsAddOrUpdateHandler)
		userGroup.GET("/v1/seasons/delete", user.SeasonsDelete)
		userGroup.GET("/v1/like/list", user.LikeListNew)
		userGroup.POST("/v1/like/like_set", user.LikeSet)
	}

	adminGroup := e.Group(baapi.RouteApiMaster)
	adminGroup.Use(authMiddleware.MiddlewareFunc())
	{
		adminGroup.GET("/v1/hello", v1.HelloHandler)
		adminGroup.GET("/v1/user/list", v1.UserListHandler)
		adminGroup.GET("/v1/user/detail", v1.UserDetailHandler)
		adminGroup.GET("/v1/calendar/list", v1.CalendarListHandler)
		adminGroup.GET("/v1/calendar/detail", v1.CalendarDetailHandler)
		adminGroup.POST("/v1/calendar/add_or_update", v1.CalendarAddOrUpdateHandler)
		adminGroup.GET("/v1/book/list", v1.BookListHandler)
		adminGroup.GET("/v1/book/detail", v1.BookDetailHandler)
		adminGroup.POST("/v1/book/add_or_update", v1.BookAddOrUpdateHandler)
		adminGroup.GET("/v1/seasons/list", v1.SeasonsListHandler)
		adminGroup.GET("/v1/seasons/detail", v1.SeasonsDetailHandler)
		adminGroup.POST("/v1/seasons/add_or_update", v1.SeasonsAddOrUpdateHandler)
	}
	/**********************/

	e.StaticFS("/fe", newStaticDir("/fe", lib.EnsureDir("./web/fe"), ""))
	e.StaticFS("/static", newStaticDir("/static", lib.EnsureDir("./web/static"), ""))
	e.StaticFS("/ui", newStaticDir("/ui", lib.EnsureDir("./web/wwwx"), "index.html"))
	e.GET("/", func(c *gin.Context) {
		toURL := &url.URL{
			Path:     "/ui/",
			RawQuery: c.Request.URL.RawQuery, // 保留参数
			Fragment: c.Request.URL.Fragment, // 保留锚
		}
		c.Redirect(http.StatusFound, toURL.String())
	})
}
