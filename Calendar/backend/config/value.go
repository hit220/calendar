package config

import (
	"reflect"
	"strconv"
	"strings"
)

func valueType(in interface{}) string {
	if in == nil {
		return "<nil>"
	}
	switch in.(type) {
	case bool:
		return "bool"
	case *bool:
		return "*bool"
	case int:
		return "int"
	case int8:
		return "int8"
	case int16:
		return "int16"
	case int32:
		return "int32"
	case int64:
		return "int64"
	case uint:
		return "uint"
	case uint8:
		return "uint8"
	case uint16:
		return "uint16"
	case uint32:
		return "uint32"
	case uint64:
		return "uint64"
	case float32:
		return "float32"
	case float64:
		return "float64"
	case []byte:
		return "[]byte"
	case string:
		return "string"
	case []string:
		return "[]string"
	case *[]byte:
		return "*[]byte"
	case *string:
		return "*string"
	case []*string:
		return "[]*string"
	default:
	}
	rv := reflect.ValueOf(in)
	switch rv.Kind() {
	case reflect.Map:
		return "reflect.Map"
	case reflect.Array:
		return "reflect.Array"
	case reflect.Slice:
		return "reflect.Slice"
	}

	return "[" + rv.Kind().String() + "]" + reflect.TypeOf(in).Name()
}

func valueToInt(in interface{}, def int) int {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return 1
		} else {
			return 0
		}
	case *bool:
		if *(in.(*bool)) {
			return 1
		} else {
			return 0
		}
	case int:
		return in.(int)
	case int8:
		return int(in.(int8))
	case int16:
		return int(in.(int16))
	case int32:
		return int(in.(int32))
	case int64:
		return int(in.(int64))
	case uint:
		return int(in.(uint))
	case uint8:
		return int(in.(uint8))
	case uint16:
		return int(in.(uint16))
	case uint32:
		return int(in.(uint32))
	case uint64:
		return int(in.(uint64))
	case float32:
		return int(in.(float32))
	case float64:
		return int(in.(float64))
	case []byte:
		ii, _ := strconv.ParseInt(string(in.([]byte)), 10, 64)
		return int(ii)
	case string:
		ii, _ := strconv.ParseInt(in.(string), 10, 64)
		return int(ii)
	case *[]byte:
		ii, _ := strconv.ParseInt(string(*(in.(*[]byte))), 10, 64)
		return int(ii)
	case *string:
		ii, _ := strconv.ParseInt(*(in.(*string)), 10, 64)
		return int(ii)
	default:
	}
	return def
}

func valueToInt64(in interface{}, def int64) int64 {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return 1
		} else {
			return 0
		}
	case *bool:
		if *(in.(*bool)) {
			return 1
		} else {
			return 0
		}
	case int:
		return int64(in.(int))
	case int8:
		return int64(in.(int8))
	case int16:
		return int64(in.(int16))
	case int32:
		return int64(in.(int32))
	case int64:
		return in.(int64)
	case uint:
		return int64(in.(uint))
	case uint8:
		return int64(in.(uint8))
	case uint16:
		return int64(in.(uint16))
	case uint32:
		return int64(in.(uint32))
	case uint64:
		return int64(in.(uint64))
	case float32:
		return int64(in.(float32))
	case float64:
		return int64(in.(float64))
	case []byte:
		ii, _ := strconv.ParseInt(string(in.([]byte)), 10, 64)
		return int64(ii)
	case string:
		ii, _ := strconv.ParseInt(in.(string), 10, 64)
		return int64(ii)
	case *[]byte:
		ii, _ := strconv.ParseInt(string(*(in.(*[]byte))), 10, 64)
		return int64(ii)
	case *string:
		ii, _ := strconv.ParseInt(*(in.(*string)), 10, 64)
		return int64(ii)
	default:
	}
	return def
}

func valueToFloat(in interface{}, def float64) float64 {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return 1.0
		} else {
			return 0.0
		}
	case *bool:
		if *(in.(*bool)) {
			return 1.0
		} else {
			return 0.0
		}
	case int:
		return float64(in.(int))
	case int8:
		return float64(in.(int8))
	case int16:
		return float64(in.(int16))
	case int32:
		return float64(in.(int32))
	case int64:
		return float64(in.(int64))
	case uint:
		return float64(in.(uint))
	case uint8:
		return float64(in.(uint8))
	case uint16:
		return float64(in.(uint16))
	case uint32:
		return float64(in.(uint32))
	case uint64:
		return float64(in.(uint64))
	case float32:
		return float64(in.(float32))
	case float64:
		return in.(float64)
	case []byte:
		ii, _ := strconv.ParseFloat(string(in.([]byte)), 64)
		return ii
	case string:
		ii, _ := strconv.ParseFloat(in.(string), 64)
		return ii
	case *[]byte:
		ii, _ := strconv.ParseFloat(string(*(in.(*[]byte))), 64)
		return ii
	case *string:
		ii, _ := strconv.ParseFloat(*(in.(*string)), 64)
		return ii
	default:
	}
	return def
}

func valueToString(in interface{}, def string) string {
	return valueToStringIn(in, def, 0)
}

func valueToStringIn(in interface{}, def string, fixed int) string {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return "true"
		} else {
			return "false"
		}
	case *bool:
		if *(in.(*bool)) {
			return "true"
		} else {
			return "false"
		}
	case int:
		return strconv.FormatInt(int64(in.(int)), 10)
	case int8:
		return strconv.FormatInt(int64(in.(int8)), 10)
	case int16:
		return strconv.FormatInt(int64(in.(int16)), 10)
	case int32:
		return strconv.FormatInt(int64(in.(int32)), 10)
	case int64:
		return strconv.FormatInt(in.(int64), 10)
	case uint:
		return strconv.FormatUint(uint64(in.(uint)), 10)
	case uint8:
		return strconv.FormatUint(uint64(in.(uint8)), 10)
	case uint16:
		return strconv.FormatUint(uint64(in.(uint16)), 10)
	case uint32:
		return strconv.FormatUint(uint64(in.(uint32)), 10)
	case uint64:
		return strconv.FormatUint(in.(uint64), 10)
	case float32:
		if fixed > 0 {
			return strconv.FormatFloat(float64(in.(float32)), 'f', fixed, 32)
		}
		return strconv.FormatFloat(float64(in.(float32)), 'f', 10, 32)
	case float64:
		if fixed > 0 {
			return strconv.FormatFloat(in.(float64), 'f', fixed, 64)
		}
		return strconv.FormatFloat(in.(float64), 'f', 10, 64)
	case []byte:
		return string(in.([]byte))
	case string:
		return in.(string)
	case *[]byte:
		return string(*(in.(*[]byte)))
	case *string:
		return *(in.(*string))
	default:
	}
	return def
}

func valueToBool(in interface{}, def bool) bool {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		return in.(bool)
	case *bool:
		return *(in.(*bool))
	case int:
		return in.(int) != 0
	case int8:
		return in.(int8) != 0
	case int16:
		return in.(int16) != 0
	case int32:
		return in.(int32) != 0
	case int64:
		return in.(int64) != 0
	case uint:
		return in.(uint) != 0
	case uint8:
		return in.(uint8) != 0
	case uint16:
		return in.(uint16) != 0
	case uint32:
		return in.(uint32) != 0
	case uint64:
		return in.(uint64) != 0
	case float32:
		return in.(float32) != 0
	case float64:
		return in.(float64) != 0
	case []byte:
		return strings.ToUpper(string(in.([]byte))) == "TRUE"
	case string:
		return strings.ToUpper(in.(string)) == "TRUE"
	case *[]byte:
		return strings.ToUpper(string(*(in.(*[]byte)))) == "TRUE"
	case *string:
		return strings.ToUpper(*(in.(*string))) == "TRUE"
	default:
	}
	return def
}
func valueIsEmpty(in interface{}) bool {
	if in == nil {
		return true
	}

	rv := reflect.ValueOf(in)
	switch rv.Kind() {
	case reflect.Map, reflect.Array, reflect.Slice:
		return rv.Len() == 0
	}

	switch in.(type) {
	case bool:
		return !in.(bool)
	case *bool:
		return !(*(in.(*bool)))
	case int:
		return in.(int) == 0
	case int8:
		return in.(int8) == 0
	case int16:
		return in.(int16) == 0
	case int32:
		return in.(int32) == 0
	case int64:
		return in.(int64) == 0
	case uint:
		return in.(uint) == 0
	case uint8:
		return in.(uint8) == 0
	case uint16:
		return in.(uint16) == 0
	case uint32:
		return in.(uint32) == 0
	case uint64:
		return in.(uint64) == 0
	case float32:
		return in.(float32) == 0.0
	case float64:
		return in.(float64) == 0.0
	case string:
		return len(in.(string)) == 0
	case *string:
		return len(*(in.(*string))) == 0
	default:
	}
	return rv.IsNil()
}

func valueToArray(obj interface{}) []interface{} {
	if obj == nil {
		return nil
	}

	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Slice:
		data := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			data[i] = v.Index(i).Interface()
		}
		return data
	default:
	}
	return nil
}

func valueToStringArray(obj interface{}) []string {
	if obj == nil {
		return nil
	}
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Slice:
		data := make([]string, v.Len())
		for i := 0; i < v.Len(); i++ {
			data[i] = valueToString(v.Index(i).Interface(), "")
		}
		return data
	default:
	}
	return nil
}

func valueToIntArray(obj interface{}) []int {
	if obj == nil {
		return nil
	}

	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Slice:
		data := make([]int, v.Len())
		for i := 0; i < v.Len(); i++ {
			data[i] = valueToInt(v.Index(i).Interface(), 0)
		}
		return data
	default:
	}
	return nil
}

func valueToInt64Array(obj interface{}) []int64 {
	if obj == nil {
		return nil
	}

	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Slice:
		data := make([]int64, v.Len())
		for i := 0; i < v.Len(); i++ {
			data[i] = valueToInt64(v.Index(i).Interface(), 0)
		}
		return data
	default:
	}
	return nil
}

func valueToFloat64Array(obj interface{}) []float64 {
	if obj == nil {
		return nil
	}

	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Slice:
		data := make([]float64, v.Len())
		for i := 0; i < v.Len(); i++ {
			data[i] = valueToFloat(v.Index(i).Interface(), 0)
		}
		return data
	default:
	}
	return nil
}

func valueToMap(obj interface{}) map[string]interface{} {
	vo, vok := obj.(map[string]interface{})
	if vok {
		return vo
	}

	if obj == nil {
		return nil
	}
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Map:
		data := make(map[string]interface{})
		iter := v.MapRange()
		for iter.Next() {
			key := valueToString(iter.Key().Interface(), "")
			if key == "" {
				continue
			}
			data[key] = iter.Value().Interface()
		}
		return data
	case reflect.Struct:
		data := make(map[string]interface{})
		for i := 0; i < t.NumField(); i++ {
			name := t.Field(i).Name
			if name != "" && name[0] >= 'A' && name[0] <= 'Z' {
				data[name] = v.Field(i).Interface()
			}
		}
		return data
	default:
	}
	return nil
}
