package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/gofrs/flock"
)

var bootTimeTag string = ""

func init() {
	bootTimeTag = time.Now().Format(".20060102.150405")
}

type configParser interface {
	Marshal(v interface{}) ([]byte, error)
	MarshalIndent(v interface{}) ([]byte, error)
	Unmarshal([]byte, interface{}) error
}

type configImpl struct {
	filepath   string
	parserType string
	saveTimer  *time.Timer
	wg         sync.WaitGroup

	parser configParser

	rootLock sync.RWMutex // 专用于保证root操作安全性
	root     map[string]interface{}
}

func newConfigImplByContent(parserType string, data []byte, filepath ...string) (*configImpl, error) {
	var parser configParser
	switch parserType {
	case "json":
		parser = &ParserJson{}
	case "yaml":
		parser = &ParserYaml{}
	default:
		return nil, errors.New("parser not exists: " + parserType)
	}

	var root map[string]interface{}
	if len(data) > 0 {
		err := parser.Unmarshal(data, &root)
		if err != nil {
			return nil, err
		}
	} else {
		root = make(map[string]interface{})
	}

	c := &configImpl{
		parserType: parserType,
		parser:     parser,
		root:       root,
	}
	if len(filepath) > 0 {
		c.filepath = filepath[0]
	}
	c.exportContent(c.data(), ".cur")
	return c, nil
}

func newConfigImplByFile(parserType string, filepath string) (*configImpl, error) {
	filepath = ensureFilePath(filepath)
	var data []byte
	var err error
	if !isExists(filepath) {
		f, err := os.Create(filepath)
		if err != nil {
			return nil, err
		}
		defer f.Close()

		data, err = ioutil.ReadAll(f)
		if err != nil {
			return nil, err
		}
	} else {
		data, err = ioutil.ReadFile(filepath)
		if err != nil {
			return nil, err
		}
	}
	c, err := newConfigImplByContent(parserType, data, filepath)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (c *configImpl) reload() error {
	if c.filepath == "" {
		return errors.New("filepath or filelock is empty")
	}
	fileLock := flock.New(c.filepath + ".lock")
	defer fileLock.Close()

	locked, err := fileLock.TryRLock()
	if err != nil {
		return err
	}
	if !locked {
		return fmt.Errorf("config is locked")
	}
	defer fileLock.Unlock()

	data, err := ioutil.ReadFile(c.filepath)
	if err != nil {
		println(err.Error())
		c.exportContent(err.Error(), ".reload.ReadFile.err.log")
		return err
	}

	// 重载空值不处理
	if len(data) == 0 {
		return nil
	}

	var root map[string]interface{}
	err = c.parser.Unmarshal(data, &root)
	if err != nil {
		println(err.Error())
		c.exportContent(err.Error(), ".reload.Unmarshal.err.log")
		return err
	}

	c.rootLock.Lock()
	c.root = root
	c.rootLock.Unlock()

	c.exportContent(c.data(), ".cur")
	return nil
}

func (c *configImpl) exportContent(data string, fext string) {
	if c.filepath == "" {
		return
	}
	dir := filepath.Dir(c.filepath)
	fname := filepath.Base(c.filepath)
	f := ensureFilePath(filepath.Join(dir, "curs", fname+bootTimeTag+fext))
	ioutil.WriteFile(f, []byte(data), 0554)
}

func (c *configImpl) saveConfig() error {
	defer func() {
		c.rootLock.Lock()
		c.saveTimer = nil
		c.wg.Done()
		c.rootLock.Unlock()
	}()

	if c.filepath == "" {
		return errors.New("filepath or filelock is empty")
	}
	fileLock := flock.New(c.filepath + ".lock")
	defer fileLock.Close()

	locked, err := fileLock.TryLock()
	if err != nil {
		return err
	}
	if !locked {
		return fmt.Errorf("config is locked")
	}
	defer fileLock.Unlock()

	data := c.data()
	err = ioutil.WriteFile(c.filepath, []byte(data), 0644)
	if err != nil {
		return err
	}

	c.exportContent(data, ".cur")
	return nil
}

// 输出美观json
func (c *configImpl) data() string {
	c.rootLock.RLock()
	b, err := c.parser.MarshalIndent(c.root)
	c.rootLock.RUnlock()

	if err != nil {
		println(err.Error())
		c.exportContent(err.Error(), ".data.MarshalIndent.err.log")
		return ""
	}
	return string(b)
}

func (c *configImpl) set(key string, val interface{}) {
	mp := c.root
	keys := strings.Split(key, ".")
	last := len(keys) - 1
	for index, ikey := range keys {
		c.rootLock.RLock()
		v, ok := mp[ikey]
		c.rootLock.RUnlock()
		if !ok {
			if index < last {
				newMP := make(map[string]interface{})
				c.rootLock.Lock()
				mp[ikey] = newMP
				c.rootLock.Unlock()
				mp = newMP
				continue
			} else {
				c.rootLock.Lock()
				mp[ikey] = val
				if c.saveTimer != nil {
					c.wg.Done()
					c.saveTimer.Stop()
				}
				c.saveTimer = time.AfterFunc(DelayAutoSaveDuration, func() { c.saveConfig() })
				c.wg.Add(1)
				c.rootLock.Unlock()
				return
			}
		} else {
			if index < last {
				mp = valueToMap(v)
			} else {
				c.rootLock.Lock()
				mp[ikey] = val
				if c.saveTimer != nil {
					c.wg.Done()
					c.saveTimer.Stop()
				}
				c.saveTimer = time.AfterFunc(DelayAutoSaveDuration, func() { c.saveConfig() })
				c.wg.Add(1)
				c.rootLock.Unlock()
				return
			}
		}
	}
}

func (c *configImpl) getVal(key string, defCount int, defVal interface{}) interface{} {
	mp := c.root
	keys := strings.Split(key, ".")
	last := len(keys) - 1
	for index, ikey := range keys {
		c.rootLock.RLock()
		v, ok := mp[ikey]
		c.rootLock.RUnlock()
		if !ok {
			if index != last {
				newMP := make(map[string]interface{})
				c.rootLock.Lock()
				mp[ikey] = newMP
				c.rootLock.Unlock()
				mp = newMP
				continue
			}
			if defCount < 1 {
				panic("config key: " + key + " not exists")
			}
			if defVal != nil {
				c.rootLock.Lock()
				mp[ikey] = defVal
				if c.saveTimer != nil {
					c.wg.Done()
					c.saveTimer.Stop()
				}
				c.saveTimer = time.AfterFunc(DelayAutoSaveDuration, func() { c.saveConfig() })
				c.wg.Add(1)
				c.rootLock.Unlock()
			}
			return defVal
		} else {
			if index == last {
				return v
			} else {
				mp = valueToMap(v)
			}
		}
	}
	if defCount < 1 {
		panic("config key: " + key + " not exists")
	}
	return defVal
}

func (c *configImpl) get(key string, def ...interface{}) interface{} {
	var defVal interface{}
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return c.getVal(key, defCount, defVal)
}

func (c *configImpl) string(key string, def ...string) string {
	var defVal string
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToString(c.getVal(key, defCount, defVal), defVal)
}

func (c *configImpl) int(key string, def ...int) int {
	var defVal int
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToInt(c.getVal(key, defCount, defVal), defVal)
}

func (c *configImpl) int64(key string, def ...int64) int64 {
	var defVal int64
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToInt64(c.getVal(key, defCount, defVal), defVal)
}

func (c *configImpl) float64(key string, def ...float64) float64 {
	var defVal float64
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToFloat(c.getVal(key, defCount, defVal), defVal)
}

func (c *configImpl) bool(key string, def ...bool) bool {
	var defVal bool
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToBool(c.getVal(key, defCount, defVal), defVal)
}

func (c *configImpl) array(key string, def ...[]interface{}) []interface{} {
	var defVal []interface{}
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToArray(c.getVal(key, defCount, defVal))
}

func (c *configImpl) arrayInt(key string, def ...[]int) []int {
	var defVal []int
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToIntArray(c.getVal(key, defCount, defVal))
}

func (c *configImpl) arrayInt64(key string, def ...[]int64) []int64 {
	var defVal []int64
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToInt64Array(c.getVal(key, defCount, defVal))
}

func (c *configImpl) arrayFloat64(key string, def ...[]float64) []float64 {
	var defVal []float64
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToFloat64Array(c.getVal(key, defCount, defVal))
}

func (c *configImpl) arrayString(key string, def ...[]string) []string {
	var defVal []string
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	return valueToStringArray(c.getVal(key, defCount, defVal))
}

func (c *configImpl) map_(key string, def ...map[string]interface{}) map[string]interface{} {
	var defVal map[string]interface{}
	defCount := len(def)
	if defCount > 0 {
		defVal = def[0]
	}
	v := c.getVal(key, defCount, defVal)
	t := reflect.TypeOf(v)
	if t != nil {
		k := t.Kind()
		if k != reflect.Ptr && k != reflect.Interface && k != reflect.Map &&
			k != reflect.Slice && k != reflect.Struct {
			panic("config key: " + key + " type isn't map")
		}
	}
	return valueToMap(v)
}

func (c *configImpl) waitSave() {
	c.wg.Wait()
}
