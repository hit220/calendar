package config

import (
	"encoding/json"
)

type ParserJson struct {
}

func (p *ParserJson) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (p *ParserJson) MarshalIndent(v interface{}) ([]byte, error) {
	return json.MarshalIndent(v, "", "\t")
}

func (p *ParserJson) Unmarshal(b []byte, v interface{}) error {
	return json.Unmarshal(b, v)
}
