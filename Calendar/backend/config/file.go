package config

import (
	"os"
	"path/filepath"
	"strings"
)

func getCurrentDir() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	dir = strings.Replace(dir, "\\", "/", -1)
	if err != nil {
		c, _ := os.Getwd()
		return c
	}
	return (dir)
}

func getAbsolutePath(path string) string {
	p := getAbsolutePathNoFilter(path)
	p = strings.Replace(p, "\\", "/", -1) // 统一使用/
	for {
		a := strings.Index(p, "//")
		if a == -1 {
			break
		}
		p = strings.Replace(p, "//", "/", -1)
	}
	return p
}

func getAbsolutePathNoFilter(path string) string {
	if strings.Index(path, "file:/") == 0 {
		path = path[5:]
		mpos := strings.Index(path, ":")
		if mpos >= 0 {
			//去除开头斜杠, 如: ///C:/test
			for {
				if len(path) > 0 && (path[0] == '/' || path[0] == '\\') {
					path = path[1:]
				} else {
					break
				}
			}
			return (path)
		}

		for {
			if len(path) > 1 && (path[0] == '/' || path[0] == '\\') && (path[1] == '/' || path[1] == '\\') {
				path = path[1:]
			} else {
				break
			}
		}
		path, _ = filepath.Abs(path)
		return (path)
	} else if strings.Index(path, "./") == 0 || strings.Index(path, ".\\") == 0 {
		r, _ := filepath.Abs(getCurrentDir() + path[1:])
		return (r)
	}
	r, _ := filepath.Abs(path)
	return (r)
}

func getDir(path string) string {
	if strings.Index(path, "./") == 0 || strings.Index(path, ".\\") == 0 {
		return filepath.ToSlash(filepath.Dir(getCurrentDir() + path[1:]))
	}
	return filepath.ToSlash(filepath.Dir(path))
}

func ensureDir(dir string) string {
	fullDir := getAbsolutePath(dir)
	if isExists(fullDir) {
		return fullDir
	}

	os.MkdirAll(fullDir, 777)
	return fullDir
}

func ensureFilePath(filePath string) string {
	filePath = getAbsolutePath(filePath)
	ensureDir(getDir(filePath))
	return filePath
}

func isExists(path string) bool {
	if path == "" {
		return false
	}
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}
