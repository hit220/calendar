package config

import (
	"time"
)

const (
	DelayAutoSaveDuration = time.Millisecond * 500
)

var configPtr *configImpl

func TryInit(parserType string) error {
	if configPtr == nil {
		return InitWithString(parserType, nil)
	}
	return nil
}

func InitWithString(parserType string, con []byte, filepath ...string) error {
	cp, err := newConfigImplByContent(parserType, con, filepath...)
	if err != nil {
		return err
	}
	configPtr = cp
	return nil
}

func InitWithFile(parserType string, filepath string) {
	cp, err := newConfigImplByFile(parserType, filepath)
	if err != nil {
		panic(err)
	}
	configPtr = cp
}

func WaitSave() {
	configPtr.waitSave()
}

func IsInit() bool                                         { return configPtr != nil }
func Reload() error                                        { return configPtr.reload() }
func Data() string                                         { return configPtr.data() }
func Set(key string, val interface{})                      { configPtr.set(key, val) }
func Get(key string, def ...interface{}) interface{}       { return configPtr.get(key, def...) }
func String(key string, def ...string) string              { return configPtr.string(key, def...) }
func Int(key string, def ...int) int                       { return configPtr.int(key, def...) }
func Int64(key string, def ...int64) int64                 { return configPtr.int64(key, def...) }
func Float64(key string, def ...float64) float64           { return configPtr.float64(key, def...) }
func Bool(key string, def ...bool) bool                    { return configPtr.bool(key, def...) }
func Array(key string, def ...[]interface{}) []interface{} { return configPtr.array(key, def...) }
func ArrayInt(key string, def ...[]int) []int              { return configPtr.arrayInt(key, def...) }
func ArrayInt64(key string, def ...[]int64) []int64        { return configPtr.arrayInt64(key, def...) }
func ArrayFloat64(key string, def ...[]float64) []float64  { return configPtr.arrayFloat64(key, def...) }
func ArrayString(key string, def ...[]string) []string     { return configPtr.arrayString(key, def...) }
func Map(key string, def ...map[string]interface{}) map[string]interface{} {
	return configPtr.map_(key, def...)
}
