package config

import (
	"gopkg.in/yaml.v2"
)

type ParserYaml struct {
}

func (p *ParserYaml) Marshal(v interface{}) ([]byte, error) {
	return yaml.Marshal(v)
}

func (p *ParserYaml) MarshalIndent(v interface{}) ([]byte, error) {
	return yaml.Marshal(v)
}

func (p *ParserYaml) Unmarshal(b []byte, v interface{}) error {
	return yaml.Unmarshal(b, v)
}
