package lib

import (
	"net/url"
	"strings"
)

func UrlJoin(args ...string) string {
	res := ""
	for _, v := range args {
		if v == "" {
			continue
		}
		if res == "" {
			res = v
			continue
		}
		v = strings.ReplaceAll(v, "\\", "/")
		if res[len(res)-1] == '/' {
			if v[0] == '/' {
				res += v[1:]
			} else {
				res += v
			}
		} else {
			if v[0] == '/' {
				res += v
			} else {
				res += "/" + v
			}
		}
	}
	return res
}

// url1: http//aa.bb/cc?rr=1122&cc=3
// url2: http//dd.ee/mm?cc=55
// result: http//dd.ee/mm?cc=55&rr=11
func UrlRedirectTo(url1 string, url2 string) string {
	qr1 := strings.Split(url1, "#")
	qr2 := strings.Split(url2, "#")

	u1 := strings.Split(qr1[0], "?")
	if len(u1) < 2 {
		return url2
	}
	u2 := strings.Split(qr2[0], "?")

	if len(u2) < 2 {
		if len(qr2) < 2 {
			return u2[0] + "?" + u1[1]
		} else {
			return u2[0] + "?" + u1[1] + "#" + qr2[1]
		}
	}

	qrq1, _ := url.ParseQuery(u1[1])
	qrq2, _ := url.ParseQuery(u2[1])
	for k := range qrq2 {
		qrq1.Set(k, qrq2.Get(k))
	}

	var qstr string
	if qrq1 != nil {
		qstr = "?" + qrq1.Encode()
	}

	if len(qr2) < 2 {
		return u2[0] + qstr
	}
	return u2[0] + qstr + "#" + qr2[1]
}
