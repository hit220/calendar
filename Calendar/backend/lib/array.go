package lib

import (
	"reflect"
)

func InArrayString(arr []string, str string) bool {
	for _, v := range arr {
		if v == str {
			return true
		}
	}
	return false
}

func InArrayInt(arr []int, k int) bool {
	for _, v := range arr {
		if v == k {
			return true
		}
	}
	return false
}

func ArrayKeys(obj interface{}) []string {
	if obj == nil {
		return nil
	}
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()

		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Map:
		iter := v.MapRange()
		res := make([]string, 0)
		for iter.Next() {
			key := ValueToString(iter.Key().Interface(), "")
			if key == "" {
				continue
			}
			res = append(res, key)
		}
		return res
	case reflect.Struct:
		res := make([]string, 0)
		for i := 0; i < t.NumField(); i++ {
			res = append(res, t.Field(i).Name)
		}
		return res
	default:
	}
	return nil
}

