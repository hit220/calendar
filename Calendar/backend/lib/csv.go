package lib

import (
	"encoding/csv"
	"net/http"
	"net/url"
)

const (
	CsvFlushLineMax = 500
)

//
type CsvWriter struct {
	w     *csv.Writer
	count int
}

//
func CsvNewWriter(w http.ResponseWriter, filename string) *CsvWriter {
	w.Header().Set("Content-Type", "application/csv")
	w.Header().Set("Content-Disposition", "attachment; filename=\""+url.QueryEscape(filename)+"\"")
	w.Write([]byte("\xEF\xBB\xBF"))

	return &CsvWriter{w: csv.NewWriter(w)}
}

//
func (c *CsvWriter) Write(data ...interface{}) error {
	if c.w == nil || len(data) < 1 {
		return nil
	}
	dataStrs := make([]string, len(data))
	for k, v := range data {
		dataStrs[k] = ValueToString(v, "")
	}

	c.count++
	err := c.w.Write(dataStrs)
	if c.count > CsvFlushLineMax {
		c.w.Flush()
		c.count = 0
	}
	return err
}

//
func (c *CsvWriter) WriteString(data ...string) error {
	if c.w == nil || len(data) < 1 {
		return nil
	}
	c.count++
	err := c.w.Write(data)
	if c.count > CsvFlushLineMax {
		c.w.Flush()
		c.count = 0
	}
	return err
}

//
func (c *CsvWriter) Close() {
	c.w.Flush()
}
