package lib

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/gofrs/flock"
)

const (
	PidFile = "cli.pid"
)

type ExitNotify interface {
	WillExit() error
	SetExitCtrl(exit context.CancelFunc)
}

type CLILogger interface {
	Println(a ...interface{})
	Errorln(a ...interface{})
}

type CLI struct {
	pidPath  string
	lockPath string
	fileLock *flock.Flock

	notifyLock sync.RWMutex
	notifyApps []ExitNotify

	log    *log.Logger
	ctx    context.Context
	cancel context.CancelFunc
}

func NewCLI(dir string, log *log.Logger) (*CLI, context.Context) {
	ctx, cancel := context.WithCancel(context.Background())
	c := &CLI{
		pidPath:    filepath.Join(dir, "cli.pid"),
		lockPath:   filepath.Join(dir, "cli.lock"),
		ctx:        ctx,
		cancel:     cancel,
		log:        log,
		notifyApps: make([]ExitNotify, 0),
	}

	if err := c.storePid(); err != nil {
		panic(err)

	}
	return c, c.ctx
}

func (c *CLI) Loop() int {
	code := c.doLoop()
	c.log.Println("Exit code:", code)
	return code
}

func (c *CLI) Add(app ExitNotify) {
	c.notifyLock.Lock()
	defer c.notifyLock.Unlock()
	app.SetExitCtrl(c.cancel)
	c.notifyApps = append(c.notifyApps, app)
}

func (c *CLI) doLoop() int {
	defer c.Shutdown()

	signalCh := make(chan os.Signal, 10)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)

	for {
		var sig os.Signal
		select {
		case s := <-signalCh:
			sig = s
		case <-c.ctx.Done():
			sig = os.Interrupt
		}

		switch sig {
		case syscall.SIGPIPE:
			continue

		case syscall.SIGHUP:
			c.log.Println("Caught signal: ", sig)
			// 重载

		default:
			c.log.Println("Caught signal: ", sig)

			graceful := (sig == os.Interrupt || sig == syscall.SIGTERM)
			if !graceful {
				c.log.Println("Graceful shutdown disabled. Exiting")
				return 1
			}
			wg := &sync.WaitGroup{}
			c.notifyLock.RLock()
			for _, app := range c.notifyApps {
				wg.Add(1)
				go func(_app ExitNotify) {
					defer wg.Done()
					if err := _app.WillExit(); err != nil {
						c.log.Println("Error on leave:", err)
						return
					}
				}(app)
			}
			c.notifyLock.RUnlock()

			c.log.Println("Gracefully shutting down cli...")
			gracefulCh := make(chan struct{})
			go func() {
				wg.Wait()
				close(gracefulCh)
			}()

			gracefulTimeout := 15 * time.Second
			select {
			case <-signalCh:
				c.log.Println("Caught second signal Exiting:", sig.String())
				return 1
			case <-time.After(gracefulTimeout):
				c.log.Println("Timeout on graceful leave. Exiting")
				return 1
			case <-gracefulCh:
				c.log.Println("Graceful exit completed")
				return 0
			}
		}
	}

}

func (c *CLI) storePid() error {
	// Quit fast if no pidfile
	if c.pidPath == "" {
		return nil
	}

	c.fileLock = flock.New(c.lockPath)
	locked, err := c.fileLock.TryLock()
	if err != nil {
		return err
	}
	if !locked {
		return fmt.Errorf("run only once")
	}

	// Open the PID file
	pidFile, err := os.OpenFile(c.pidPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return fmt.Errorf("Could not open pid file: %v", err)
	}
	defer pidFile.Close()

	// Write out the PID
	pid := os.Getpid()
	_, err = pidFile.WriteString(fmt.Sprintf("%d", pid))
	if err != nil {
		return fmt.Errorf("Could not write to pid file: %s", err)
	}
	return nil
}

func (c *CLI) deletePid() error {

	// 锁定解除
	c.fileLock.Unlock()
	c.fileLock.Close()

	// Quit fast if no pidfile
	if c.pidPath == "" {
		return nil
	}

	stat, err := os.Stat(c.pidPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return fmt.Errorf("Could not remove pid file: %s", err)
	}

	if stat.IsDir() {
		return fmt.Errorf("Specified pid file path is directory")
	}

	err = os.Remove(c.lockPath)
	if err != nil {
		return fmt.Errorf("Could not remove lock file: %s", err)
	}

	err = os.Remove(c.pidPath)
	if err != nil {
		return fmt.Errorf("Could not remove pid file: %s", err)
	}
	return nil
}

func (c *CLI) Shutdown() {
	c.log.Println("Requesting shutdown")

	pidErr := c.deletePid()
	if pidErr != nil && !strings.Contains(pidErr.Error(), "cannot find the file") {
		c.log.Println("could not delete pid file ", pidErr)
	}

	c.cancel()
}
