package lib

import (
	"fmt"
	"strings"
)

type ProgressBar struct {
	percent int    //百分比
	cur     int64  //当前进度位置
	total   int64  //总进度
	rate    string //进度条
	graph   string //显示符号

	curText   string
	totalText string
}

func NewProgressBar(graph ...string) *ProgressBar {
	bar := &ProgressBar{}
	if len(graph) > 0 {
		bar.graph = graph[0]
	} else {
		bar.graph = "="
	}
	return bar
}

func (bar *ProgressBar) getPercent() int {
	if bar.total < 1 {
		return 0
	}
	r := int(float32(bar.cur) / float32(bar.total) * 100)
	if r > 100 {
		r = 100
	}
	return r
}

func (bar *ProgressBar) Update(cur, total int64) {
	if bar.cur != cur {
		bar.cur = cur
		bar.curText = FormatFileSize(bar.cur)
	}

	if bar.total != total {
		bar.total = total
		bar.totalText = FormatFileSize(bar.total)
	}

	bar.draw()
	fmt.Printf("\r[%-50s]%3d%%  %8s/%s", bar.rate, bar.percent, bar.curText, bar.totalText)
}

func (bar *ProgressBar) draw() {
	bar.percent = bar.getPercent()

	p := bar.percent / 2
	bar.rate = ""
	if p > 0 {
		bar.rate = strings.Repeat(bar.graph, p-1)
	}
	if p >= 50 {
		bar.rate += "="
	} else {
		bar.rate += "-"
	}
}

func (bar *ProgressBar) Finish() {
	fmt.Println()
}

func FormatFileSize(n int64) (size string) {
	if n < 1024 {
		return fmt.Sprintf("%.2fB", float64(n)/float64(1))
	} else if n < (1024 * 1024) {
		return fmt.Sprintf("%.2fKB", float64(n)/float64(1024))
	} else if n < (1024 * 1024 * 1024) {
		return fmt.Sprintf("%.2fMB", float64(n)/float64(1024*1024))
	} else if n < (1024 * 1024 * 1024 * 1024) {
		return fmt.Sprintf("%.2fGB", float64(n)/float64(1024*1024*1024))
	} else if n < (1024 * 1024 * 1024 * 1024 * 1024) {
		return fmt.Sprintf("%.2fTB", float64(n)/float64(1024*1024*1024*1024))
	} else { //if n < (1024 * 1024 * 1024 * 1024 * 1024 * 1024)
		return fmt.Sprintf("%.2fEB", float64(n)/float64(1024*1024*1024*1024*1024))
	}
}
