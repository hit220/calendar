package lib

import "testing"

func TestValueToArray(t *testing.T) {
	println(ValueToMap(0))

	a := []int{1, 2, 3}
	a2 := ValueToArray(a)
	println(JsonEncode(a2))

	a3 := map[string]int{"11": 1, "22": 2, "33": 3}
	a4 := ValueToArray(a3)
	println(JsonEncode(a4))

	println(JsonEncode(ValueToArrayPair(a4)))
}
