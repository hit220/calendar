package lib

import (
	crand "crypto/rand"
	"encoding/hex"
	"math"
	"math/big"
	"math/rand"
	"sync"
	"time"
)

var (
	once sync.Once

	// SeededSecurely is set to true if a cryptographically secure seed
	// was used to initialize rand.  When false, the start time is used
	// as a seed.
	SeededSecurely bool
)

func init() {
	SeedMathRand()
}

// SeedMathRand provides weak, but guaranteed seeding, which is better than
// running with Go's default seed of 1.  A call to SeedMathRand() is expected
// to be called via init(), but never a second time.
func SeedMathRand() {
	once.Do(func() {
		n, err := crand.Int(crand.Reader, big.NewInt(math.MaxInt64))
		if err != nil {
			rand.Seed(time.Now().UTC().UnixNano())
			return
		}
		rand.Seed(n.Int64())
		SeededSecurely = true
	})
}

const (
	IDMinLength  = 16
	BitMinLength = IDMinLength * 8
)

//log or print
func BytesToHex(b []byte) string {
	return hex.EncodeToString(b)
}

func RandomBytes(size int) []byte {
	buff := make([]byte, size)
	rand.Read(buff)
	return buff
}

func RandomHexString(size int) string {
	buff := make([]byte, size)
	rand.Read(buff)
	return hex.EncodeToString(buff)
}

const (
	KC_RAND_KIND_NUM   = 0 // 纯数字
	KC_RAND_KIND_LOWER = 1 // 小写字母
	KC_RAND_KIND_UPPER = 2 // 大写字母
	KC_RAND_KIND_ALL   = 3 // 数字、大小写字母
)

// 随机字符串
func KindRand(size int, kind int) string {
	ikind, kinds, result := kind, [][]int{{10, 48}, {26, 97}, {26, 65}}, make([]byte, size)
	is_all := kind > 2 || kind < 0
	for i := 0; i < size; i++ {
		if is_all { // random ikind
			ikind = rand.Intn(3)
		}
		scope, base := kinds[ikind][0], kinds[ikind][1]
		result[i] = uint8(base + rand.Intn(scope))
	}
	return string(result)
}
