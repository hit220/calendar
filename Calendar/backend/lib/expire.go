package lib

import (
	"sync"
	"time"
)

type VarClock struct {
	m       sync.Map
	timeout time.Duration
}

func NewVarClock(timeout time.Duration) *VarClock {
	return &VarClock{timeout: timeout}
}

func (e *VarClock) IsExpire(key interface{}, timeout ...time.Duration) bool {
	tm, ok := e.m.Load(key)
	if !ok {
		return true
	}

	expire := e.timeout
	if len(timeout) > 0 {
		expire = timeout[0]
	}

	if time.Since(tm.(time.Time)) > expire {
		return true
	}
	return false
}

func (e *VarClock) Update(key interface{}) {
	e.m.Store(key, time.Now())
}
