package lib

import (
	"testing"
	"time"
)

func Test_Signal(t *testing.T) {
	s := NewCond()
	for i := 0; i < 10; i++ {
		go func(id int) {

			for {
				select {
				case <-s.Done():
					println("cc-", id)
				}
				//s.Wait()
			}
		}(i)
	}

	time.Sleep(time.Second)
	for i := 0; i < 3; i++ {
		println("--------------")
		s.Broadcast()
		time.Sleep(time.Second * 3)
		println("==============")
	}
}
