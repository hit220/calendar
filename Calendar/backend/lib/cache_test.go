package lib

import (
	"errors"
	"fmt"
	"testing"
	"time"
)

var nn = 0

func getValue() (interface{}, error) {
	fmt.Println("------------")
	if nn%3 == 0 {
		nn++
		return time.Now(), nil
	}
	nn++
	return time.Time{}, errors.New("error ~~~")
}

func TestCacheRunner(t *testing.T) {
	cr := NewCacheRunner(getValue, time.Second*3)
	for i := 0; i < 30; i++ {
		fmt.Println(cr.Get("abc"))
		time.Sleep(time.Millisecond * 500)
	}
}
