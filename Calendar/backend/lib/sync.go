package lib

import (
	"sync"
)

/*
	增加chen操作Done(), 与别的chan一起工作
	减少Lock初始化
*/

type condChan struct {
	ch             chan struct{}
	isClose        bool
	frontBroadcast int // 先执行broadcase则为1,先执行done则为-1,默认为0
}

type Cond struct {
	mu sync.Mutex
	c  *condChan
}

func NewCond() *Cond {
	s := &Cond{
		c: &condChan{
			ch:             make(chan struct{}),
			isClose:        false,
			frontBroadcast: 0,
		},
	}
	return s
}

func (s *Cond) Broadcast() {
	s.mu.Lock()
	if !s.c.isClose {
		s.c.isClose = true
		close(s.c.ch)
	}
	if s.c.frontBroadcast == 0 {
		s.c.frontBroadcast = 1
	}
	s.mu.Unlock()
}

func (s *Cond) Done() <-chan struct{} {
	s.mu.Lock()
	if s.c.frontBroadcast != 1 && s.c.isClose {
		s.c.ch = make(chan struct{})
		s.c.isClose = false
	}
	if s.c.frontBroadcast != -1 {
		s.c.frontBroadcast = -1
	}
	ch := s.c.ch
	s.mu.Unlock()
	return ch
}
