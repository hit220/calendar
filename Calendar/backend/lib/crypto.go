package lib

import (
	"bytes"
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/des"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"strings"
)

func GenKeyPair(bits int) (string, string, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return "", "", err
	}

	derStream := x509.MarshalPKCS1PrivateKey(privateKey)
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derStream,
	}

	bufPri := bytes.NewBuffer(nil)
	bufPub := bytes.NewBuffer(nil)

	err = pem.Encode(bufPri, block)
	if err != nil {
		return "", "", err
	}

	derPkix, err := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
	if err != nil {
		return "", "", err
	}
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPkix,
	}
	err = pem.Encode(bufPub, block)
	if err != nil {
		return "", "", err
	}

	return bufPri.String(), bufPub.String(), nil

}

func MD5(data string) string {
	t := md5.New()
	io.WriteString(t, data)
	return fmt.Sprintf("%x", t.Sum(nil))
}

/***************** RSA ************************/

func GetPrivateKey(k string) (*rsa.PrivateKey, error) {
	if k == "" {
		return nil, fmt.Errorf("key string is empty")
	}

	var der []byte
	var err error
	if strings.Contains(k, "----") {
		var block *pem.Block
		if block, _ = pem.Decode([]byte(k)); block == nil {
			return nil, errors.New("block parse fail")
		}
		der = block.Bytes
	} else {
		der, err = base64.StdEncoding.DecodeString(k)
		if err != nil {
			return nil, err
		}
	}

	var priv interface{}
	if priv, err = x509.ParsePKCS1PrivateKey(der); err != nil {
		if priv, err = x509.ParsePKCS8PrivateKey(der); err != nil {
			return nil, err
		}
	}

	pkey, ok := priv.(*rsa.PrivateKey)
	if !ok {
		return nil, fmt.Errorf("format private key fail")
	}
	return pkey, nil
}

func GetPublicKey(k string) (*rsa.PublicKey, error) {
	if len(k) == 0 {
		return nil, fmt.Errorf("key string is empty")
	}

	var der []byte
	var err error
	if strings.Contains(k, "----") {
		var block *pem.Block
		if block, _ = pem.Decode([]byte(k)); block == nil {
			return nil, errors.New("block parse fail")
		}
		der = block.Bytes
	} else {
		der, err = base64.StdEncoding.DecodeString(k)
		if err != nil {
			return nil, err
		}
	}

	var pub interface{}
	pub, err = x509.ParsePKIXPublicKey(der)
	if err != nil {
		if cert, err := x509.ParseCertificate(der); err == nil {
			pub = cert.PublicKey
		} else {
			return nil, err
		}
	}
	pkey, ok := pub.(*rsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("format public key fail")
	}
	return pkey, nil
}

func RSAEncrypt(origData []byte, pub *rsa.PublicKey) ([]byte, error) {
	maxLen := pub.Size() - 11
	res := make([]byte, 0)
	for len(origData) > 0 {
		input := origData
		if len(origData) > maxLen {
			input = origData[:maxLen]
			origData = origData[maxLen:]
		} else {
			origData = nil
		}

		cdata, err := rsa.EncryptPKCS1v15(rand.Reader, pub, input)
		if err != nil {
			return nil, err
		}
		res = append(res, cdata...)
	}
	return res, nil
}

func RSADecrypt(cipherText []byte, priv *rsa.PrivateKey) ([]byte, error) {
	maxLen := priv.Size()
	res := make([]byte, 0)
	for len(cipherText) > 0 {
		input := cipherText
		if len(cipherText) > maxLen {
			input = cipherText[:maxLen]
			cipherText = cipherText[maxLen:]
		} else {
			cipherText = nil
		}

		cdata, err := rsa.DecryptPKCS1v15(rand.Reader, priv, input)
		if err != nil {
			return nil, err
		}
		res = append(res, cdata...)
	}
	return res, nil
}

func RSAEncryptObject(obj interface{}, pub *rsa.PublicKey) (string, error) {
	str, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}
	cstr, err := RSAEncrypt([]byte(str), pub)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(cstr), nil
}

func RSADecryptObject(cstr string, priv *rsa.PrivateKey, outer interface{}) error {
	bstr, err := base64.StdEncoding.DecodeString(cstr)
	if err != nil {
		return err
	}
	pstr, err := RSADecrypt(bstr, priv)
	if err != nil {
		return err
	}
	return json.Unmarshal(pstr, outer)
}

/***************** AES ************************/

func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS5Unpadding(origData []byte, blockSize int) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	if unpadding > blockSize {
		return nil
	}
	return origData[:(length - unpadding)]
}

func NoPadding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	if padding == blockSize {
		return ciphertext
	}
	padtext := bytes.Repeat([]byte{0}, padding)
	return append(ciphertext, padtext...)
}

func UnNoPadding(origData []byte, blockSize int) []byte {
	rlen := len(origData)
	if rlen < blockSize {
		return origData
	}
	for i := 0; i < blockSize; i++ {
		pos := rlen - i
		if origData[pos-1] != 0 {
			return origData[:pos]
		}
	}
	return origData[:rlen-blockSize]
}

// ECB
func AESEncryptECB(src []byte, key []byte) (out []byte, err error) {
	defer func() {
		if err2 := recover(); err2 != nil {
			err = fmt.Errorf("panic: %v", err2)
			out = nil
		}
	}()

	if len(src) == 0 {
		return src, nil
	}

	var block cipher.Block
	block, err = aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	bs := block.BlockSize()
	src = PKCS5Padding(src, bs)
	if len(src)%bs != 0 {
		return nil, errors.New("encrypt src length is error")
	}
	out = make([]byte, len(src))
	dst := out
	for len(src) > 0 {
		block.Encrypt(dst, src[:bs])
		src = src[bs:]
		dst = dst[bs:]
	}
	return out, nil
}

// ECB
func AESDecryptECB(src []byte, key []byte) (out []byte, err error) {
	defer func() {
		if err2 := recover(); err2 != nil {
			err = fmt.Errorf("panic: %v", err2)
			out = nil
		}
	}()

	if len(src) == 0 {
		return src, nil
	}

	var block cipher.Block
	block, err = aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	out = make([]byte, len(src))
	dst := out
	bs := block.BlockSize()
	if len(src)%bs != 0 {
		return nil, errors.New("decrypt src length is error")
	}

	for len(src) > 0 {
		block.Decrypt(dst, src[:bs])
		src = src[bs:]
		dst = dst[bs:]
	}
	out = PKCS5Unpadding(out, block.BlockSize())

	return out, nil
}

func AESEncryptCBC(src []byte, key []byte, iv []byte, noPadding ...bool) (out []byte, err error) {
	defer func() {
		if err2 := recover(); err2 != nil {
			err = fmt.Errorf("panic: %v", err2)
			out = nil
		}
	}()

	rLen := len(src)
	if rLen < 1 {
		return nil, nil
	}
	var block cipher.Block
	block, err = aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	if len(noPadding) < 1 || !noPadding[0] {
		src = PKCS5Padding(src, blockSize)
	} else {
		src = NoPadding(src, blockSize)
	}

	blockMode := cipher.NewCBCEncrypter(block, iv)
	out = make([]byte, len(src))
	blockMode.CryptBlocks(out, src)
	if rLen > 0 && len(out) < 1 {
		return nil, errors.New("decrypto error: result is empty")
	}

	return out, nil
}

func AESDecryptCBC(src []byte, key []byte, iv []byte, noPadding ...bool) (out []byte, err error) {
	defer func() {
		if err2 := recover(); err2 != nil {
			err = fmt.Errorf("panic: %v", err2)
			out = nil
		}
	}()

	rLen := len(src)
	if rLen < 1 {
		return nil, nil
	}

	var block cipher.Block
	block, err = aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockMode := cipher.NewCBCDecrypter(block, iv)
	out = make([]byte, rLen)
	blockSize := block.BlockSize()

	blockMode.CryptBlocks(out, src)
	if len(noPadding) > 0 && noPadding[0] {
		out = UnNoPadding(out, blockSize)
		return out, nil
	}
	out = PKCS5Unpadding(out, blockSize)
	if rLen > 0 && len(out) < 1 {
		return nil, errors.New("decrypto error: result is empty")
	}

	return out, nil
}

// 3DES加密
func TripleDesEncryptCBC(origData, key []byte) ([]byte, error) {
	block, err := des.NewTripleDESCipher(key)
	if err != nil {
		return nil, err
	}
	origData = PKCS5Padding(origData, block.BlockSize())
	// origData = ZeroPadding(origData, block.BlockSize())
	blockMode := cipher.NewCBCEncrypter(block, key[:8])
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}

// 3DES解密
func TripleDesDecryptCBC(crypted, key []byte) ([]byte, error) {
	block, err := des.NewTripleDESCipher(key)
	if err != nil {
		return nil, err
	}
	blockMode := cipher.NewCBCDecrypter(block, key[:8])
	origData := make([]byte, len(crypted))
	// origData := crypted
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	// origData = ZeroUnPadding(origData)
	return origData, nil
}

func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	// 去掉最后一个字节 unpadding 次
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

//签名：采用sha1算法进行签名（私钥PKCS8格式）
func RsaSignWithSha1Hex(data string, prvKey string) (string, error) {
	keyByts, err := base64.StdEncoding.DecodeString(prvKey)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	privateKey, err := x509.ParsePKCS8PrivateKey(keyByts)
	if err != nil {
		fmt.Println("ParsePKCS8PrivateKey err", err)
		return "", err
	}
	h := sha1.New()
	h.Write([]byte([]byte(data)))
	hash := h.Sum(nil)
	signature, err := rsa.SignPKCS1v15(rand.Reader, privateKey.(*rsa.PrivateKey), crypto.SHA1, hash[:])
	if err != nil {
		fmt.Printf("Error from signing: %s\n", err)
		return "", err
	}
	fmt.Println(base64.StdEncoding.EncodeToString(signature))
	out := base64.StdEncoding.EncodeToString(signature)
	// out := hex.EncodeToString(signature)
	return out, nil
}

//验签：对采用sha1算法进行签名后转base64格式的数据进行验签
func RsaVerySignWithSha1Base64(originalData, signData, pubKey string) error {
	sign, err := base64.StdEncoding.DecodeString(signData)
	if err != nil {
		return err
	}
	public, _ := base64.StdEncoding.DecodeString(pubKey)
	pub, err := x509.ParsePKIXPublicKey(public)
	if err != nil {
		return err
	}
	hash := sha1.New()
	hash.Write([]byte(originalData))
	return rsa.VerifyPKCS1v15(pub.(*rsa.PublicKey), crypto.SHA1, hash.Sum(nil), sign)
}
