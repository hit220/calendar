package lib

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
)

const (
	VarCurrentDirTag    = "__dir__"
	VarCurrentDirLength = 7
)

var currentDir = ""

func SetCurrentDir(dir string) {
	currentDir = dir
}

func GetCurrentDir() string {
	if currentDir != "" {
		return currentDir
	}
	file, err := exec.LookPath(os.Args[0])
	if err != nil {
		c, _ := os.Getwd()
		return c
	}
	return strings.Replace(filepath.Dir(file), "\\", "/", -1)
}

func GetAbsolutePath(path string) string {
	p := GetAbsolutePathNoFilter(path)
	p = strings.Replace(p, "\\", "/", -1) // 统一使用/
	for {
		a := strings.Index(p, "//")
		if a == -1 {
			break
		}
		p = strings.Replace(p, "//", "/", -1)
	}
	return p
}

func GetAbsolutePathNoFilter(path string) string {
	if strings.Index(path, "file:/") == 0 {
		path = path[5:]
		mpos := strings.Index(path, ":")
		if mpos >= 0 {
			//去除开头斜杠, 如: ///C:/test
			for {
				if len(path) > 0 && (path[0] == '/' || path[0] == '\\') {
					path = path[1:]
				} else {
					break
				}
			}
			return (path)
		}

		for {
			if len(path) > 1 && (path[0] == '/' || path[0] == '\\') && (path[1] == '/' || path[1] == '\\') {
				path = path[1:]
			} else {
				break
			}
		}
		path, _ = filepath.Abs(path)
		return (path)
	} else if strings.Index(path, "./") == 0 || strings.Index(path, ".\\") == 0 {
		r, _ := filepath.Abs(filepath.Join(GetCurrentDir(), path[1:]))
		return (r)
	}
	r, _ := filepath.Abs(path)
	return (r)
}

func GetDir(path string) string {
	if strings.Index(path, "./") == 0 || strings.Index(path, ".\\") == 0 {
		return filepath.ToSlash(filepath.Dir(filepath.Join(GetCurrentDir(), path[1:])))
	}
	return filepath.ToSlash(filepath.Dir(path))
}

func EnsureDir(dir string) string {
	dir = executePath(dir)
	fullDir := GetAbsolutePath(dir)
	if IsExists(fullDir) {
		return fullDir
	}

	os.MkdirAll(fullDir, 0777)
	return fullDir
}

func EnsureFilePath(filePath string) string {
	filePath = executePath(filePath)
	filePath = GetAbsolutePath(filePath)
	EnsureDir(GetDir(filePath))
	return filePath
}

func IsExists(path string) bool {
	if path == "" {
		return false
	}
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

func ToFilename(str string) string {
	if str == "" {
		return "file.default"
	}
	str = regexp.MustCompile(`(?is)[^0-9a-zA-Z_\-.\s]+`).ReplaceAllString(str, "-")
	str = regexp.MustCompile(`(?is)([_\-.\s])+`).ReplaceAllString(str, "$1")
	if len(str) > 200 {
		str = str[:132] + "-" + MD5(str)
		if len(str) > 208 {
			return str[:208]
		}
	}

	str = strings.TrimSpace(str)
	str = strings.Trim(str, "-")
	str = strings.Trim(str, "_")
	str = strings.Trim(str, ".")

	if str == "" {
		return "file.default"
	}

	return str
}

func ToDir(str string) string {
	if str == "" {
		return ""
	}
	str = strings.ReplaceAll(str, "\\", "/")
	str = regexp.MustCompile(`(?is)[^0-9a-zA-Z_\-.\s/]+`).ReplaceAllString(str, "-")
	str = regexp.MustCompile(`(?is)([_\-\s/])+`).ReplaceAllString(str, "$1")

	str = strings.TrimSpace(str)
	str = strings.Trim(str, "-")
	str = strings.Trim(str, "_")
	str = strings.Trim(str, ".")

	if str == "" {
		return ""
	}

	return str
}

func executePath(r string) string {
	index := strings.Index(r, VarCurrentDirTag)
	if index < 0 {
		return r
	}
	d, err := os.Getwd()
	if err != nil {
		return r
	}
	return filepath.Join(d, r[index+VarCurrentDirLength:])
}

func CopyFile(from string, to string) error {
	b, err := ioutil.ReadFile(from)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(to, b, 0777)
}
