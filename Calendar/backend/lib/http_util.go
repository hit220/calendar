package lib

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const (
	MaxBodySize         = 10 * 1024 * 1024 // 10MB
	ProgressUpdateDelay = time.Millisecond * 500
)

var (
	ErrStopRedirect = errors.New("[[StopRedirect]]")
)

type ResPack struct {
	r   *http.Response
	err error
}

func forbidRedirect(req *http.Request, via []*http.Request) error {
	return ErrStopRedirect
}

func HttpRequest(proxy func(_ *http.Request) (*url.URL, error), url, method string, postData []byte, header http.Header, jar http.CookieJar, allowRedirect bool, timeout time.Duration, connTimeout time.Duration) (http.Header, []byte, http.Header, int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	tr := &http.Transport{
		Proxy:           proxy,
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},

		//DisableKeepAlives: true, //拒绝保活
		Dial: func(netw, addr string) (net.Conn, error) {
			//读写超60秒则超时
			deadline := time.Now().Add(timeout)
			//5秒未连接成功则超时
			conn, err := net.DialTimeout(netw, addr, connTimeout)
			if err != nil {
				return nil, err
			}
			conn.SetDeadline(deadline)
			return conn, nil
		},
	}
	client := &http.Client{
		Transport: tr,
		Jar:       jar,
	}
	if !allowRedirect {
		client.CheckRedirect = forbidRedirect
	}

	c := make(chan ResPack, 1)

	var req *http.Request
	var err error

	req, err = http.NewRequest(method, url, bytes.NewReader(postData))
	if err != nil {
		return nil, nil, nil, 0, err
	}
	if len(postData) > 0 {
		req.Header.Add("Content-Length", strconv.Itoa(len(postData)))
	}

	if len(header) > 0 {
		for k, v := range header {
			for _, vi := range v {
				req.Header.Add(k, vi)
			}
		}
	} else {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	//req.Close = true //拒绝保活

	go func() {
		// println(JsonEncode(req.URL))
		// println(JsonEncode(req.Cookies()))
		resp, err := client.Do(req)
		// println(JsonEncode(resp.Cookies()))
		pack := ResPack{r: resp, err: err}
		c <- pack
	}()

	select {
	case <-ctx.Done():
		tr.CancelRequest(req)
		<-c
		return nil, nil, nil, 0, errors.New("timeout")

	case res := <-c:
		if res.r == nil {
			if res.err != nil {
				return nil, nil, nil, 0, res.err
			}
			return nil, nil, nil, 0, errors.New("resp is nil")
		}

		if res.r.Body == nil {
			if res.err != nil {
				return nil, nil, nil, 0, res.err
			}
			return nil, nil, nil, 0, errors.New("resp.Body is nil")
		}

		defer res.r.Body.Close()

		if res.err != nil {
			if strings.Contains(res.err.Error(), ErrStopRedirect.Error()) {
				return req.Header, nil, res.r.Header, res.r.StatusCode, nil
			}
			return nil, nil, nil, 0, res.err
		}

		buf, err := ioutil.ReadAll(res.r.Body)
		if err != nil {
			return nil, nil, nil, 0, err
		}

		return req.Header, buf, res.r.Header, res.r.StatusCode, err
	}
}

type progressConn struct {
	net.Conn
	n          int64
	total      int64
	fn         func(cur, total int64, exited bool)
	tm         time.Time
	headerOver bool
	header     string
	exited     bool
}

func (p *progressConn) Write(b []byte) (int, error) {
	n, err := p.Conn.Write(b)

	// 修正总包长度
	if err == nil && !p.headerOver {
		p.header += string(b)
		index := strings.Index(p.header, "\r\n\r\n")
		if index >= 0 {
			p.headerOver = true
			p.total += p.n + int64(index+4)
		}
	}
	p.n += int64(n)

	if p.headerOver && p.fn != nil {
		tm := time.Now()
		exited := err != nil || p.n >= p.total
		if !p.exited && (exited || p.tm.Before(time.Now())) {
			p.fn(p.n, p.total, exited)
			p.tm = tm.Add(ProgressUpdateDelay)
		}
		p.exited = exited
	}

	return n, err
}

func (p *progressConn) Close() error {
	if p.headerOver && p.fn != nil && !p.exited {
		p.fn(p.n, p.total, true)
		p.exited = true
	}
	return p.Conn.Close()
}

func HttpUploadFile(url string, params map[string]string, files map[string]string,
	header http.Header, jar http.CookieJar, connTimeout time.Duration,
	progressFn func(cur, total int64, exited bool)) ([]byte, http.Header, int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	c := make(chan ResPack, 1)

	var req *http.Request
	var err error

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	pnum := 0 // http 分块数
	for key, val := range params {
		err = writer.WriteField(key, val)
		if err != nil {
			return nil, nil, 0, err
		}
		pnum++
	}

	for fname, fpath := range files {
		ff, err := os.Open(fpath)
		if err != nil {
			return nil, nil, 0, err
		}
		defer ff.Close()

		part, err := writer.CreateFormFile(fname, filepath.Base(fpath))
		if err != nil {
			return nil, nil, 0, err
		}
		_, err = io.Copy(part, ff)
		if err != nil {
			return nil, nil, 0, err
		}
		pnum++
	}
	err = writer.Close()
	if err != nil {
		return nil, nil, 0, err
	}

	dataLen := int64(body.Len())
	req, err = http.NewRequest("POST", url, body)
	if err != nil {
		return nil, nil, 0, err
	}
	for k, v := range header {
		for _, vi := range v {
			req.Header.Add(k, vi)
		}
	}
	req.Header.Add("Content-Type", writer.FormDataContentType())

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},

		//DisableKeepAlives: true, //拒绝保活
		Dial: func(netw, addr string) (net.Conn, error) {
			//读写超60秒则超时
			deadline := time.Now().Add(time.Hour * 2)
			//5秒未连接成功则超时
			conn, err := net.DialTimeout(netw, addr, connTimeout)
			if err != nil {
				return nil, err
			}
			conn.SetDeadline(deadline)
			return &progressConn{Conn: conn, total: dataLen, fn: progressFn}, nil
		},
	}
	client := &http.Client{
		Transport: tr,
		Jar:       jar,
	}
	go func() {
		// println(JsonEncode(req.URL))
		// println(JsonEncode(req.Cookies()))
		resp, err := client.Do(req)
		// println(JsonEncode(resp.Cookies()))
		pack := ResPack{r: resp, err: err}
		c <- pack
	}()

	select {
	case <-ctx.Done():
		tr.CancelRequest(req)
		<-c
		return nil, nil, 0, errors.New("timeout")

	case res := <-c:
		if res.r == nil {
			if res.err != nil {
				return nil, nil, 0, res.err
			}
			return nil, nil, 0, errors.New("resp is nil")
		}

		if res.r.Body == nil {
			if res.err != nil {
				return nil, nil, 0, res.err
			}
			return nil, nil, 0, errors.New("resp.Body is nil")
		}

		defer res.r.Body.Close()

		if res.err != nil {
			if strings.Contains(res.err.Error(), ErrStopRedirect.Error()) {
				return nil, res.r.Header, res.r.StatusCode, nil
			}
			return nil, nil, 0, res.err
		}

		buf, err := ioutil.ReadAll(res.r.Body)
		if err != nil {
			return nil, nil, 0, err
		}

		return buf, res.r.Header, res.r.StatusCode, err
	}
}

func HttpMakeHeader(str string) http.Header {
	arr := strings.Split(str, "\n")
	res := make(http.Header)
	for _, v := range arr {
		pos := strings.Index(v, ": ")
		if pos > 0 {
			key := v[:pos]
			ikey := strings.ToLower(key)
			if ikey == "content-length" || ikey == "cookie" {
				continue
			}
			res[key] = []string{strings.TrimSpace(v[pos+2:])}
		}
	}
	return res
}

func MapHttpMap(val interface{}) map[string]string {
	var items map[string][]string
	res := make(map[string]string)
	switch val.(type) {
	case http.Header:
		val2 := val.(http.Header)
		items = (map[string][]string)(val2)
		for k := range items {
			res[k] = val2.Get(k)
		}
	case *http.Header:
		val2 := val.(*http.Header)
		items = (map[string][]string)(*val2)
		for k := range items {
			res[k] = val2.Get(k)
		}
	case url.Values:
		val2 := val.(url.Values)
		items = (map[string][]string)(val2)
		for k := range items {
			res[k] = val2.Get(k)
		}
	case *url.Values:
		val2 := val.(*url.Values)
		items = (map[string][]string)(*val2)
		for k := range items {
			res[k] = val2.Get(k)
		}
	default:
	}
	return res
}

func JsonEncodeMapHttpHeader(header map[string]interface{}) string {
	return JsonEncodeIndent(header)
}

func JsonDecodeMapHttpHeader(header string) map[string]interface{} {
	var res map[string]interface{}
	err := json.Unmarshal([]byte(header), &res)
	if err != nil {
		return nil
	}
	return res
}
