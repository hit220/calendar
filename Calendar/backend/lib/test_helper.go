package lib

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

func GetCurrentFileDir() string {
	_, filename, _, _ := runtime.Caller(2)
	return path.Dir(filename)
}

func GetTestDataDir() string {
	d := GetCurrentFileDir()
	return filepath.Join(d, "testdata")
}

func GetTestTempDir() string {
	return filepath.Join(GetCurrentFileDir(), "_data")
}

func OpenTestFile(path string) (*os.File, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return f, err
}

// RSA公钥私钥产生
func GenRsaKey(bits int) error {
	dir := GetCurrentFileDir()

	// 生成私钥文件
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return err
	}

	derStream := x509.MarshalPKCS1PrivateKey(privateKey)
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derStream,
	}
	file, err := os.Create(filepath.Join(dir, "private.pem"))
	if err != nil {
		return err
	}
	err = pem.Encode(file, block)
	if err != nil {
		return err
	}
	// 生成公钥文件
	publicKey := &privateKey.PublicKey
	derPkix, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return err
	}
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPkix,
	}
	file, err = os.Create(filepath.Join(dir, "public.pem"))
	if err != nil {
		return err
	}
	err = pem.Encode(file, block)
	if err != nil {
		return err
	}
	return nil
}
