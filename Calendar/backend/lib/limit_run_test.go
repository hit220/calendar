package lib

import (
	"fmt"
	"sync/atomic"
	"testing"
	"time"
)

func Test_LimitRun(t *testing.T) {
	wokerLimit := NewLimitRun(2)

	total := int32(0)
	time.AfterFunc(10*time.Second, func() {
		wokerLimit.Close() // 10秒关闭
		nt := atomic.LoadInt32(&total)
		if nt >= 23 || nt <= 19 {
			t.Fatal("limit run is failed")
		}
	})
	for i := 0; i < 100; i++ {
		err := wokerLimit.Wait()
		if err != nil {
			t.Log(err)
			return
		}
		go func(threadID int) {
			defer wokerLimit.Done()
			fmt.Printf("%d - %d\n", threadID, atomic.AddInt32(&total, 1))

			time.Sleep(time.Second)
		}(i)
	}
}
