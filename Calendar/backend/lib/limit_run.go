package lib

import (
	"errors"
	"fmt"
	"sync"
)

type LimitRun struct {
	shutdown     bool
	shutdownCh   chan struct{}
	shutdownLock sync.Mutex

	signal chan struct{}
	n      int
}

func NewLimitRun(n int) *LimitRun {
	lrun := &LimitRun{
		signal:     make(chan struct{}, n),
		shutdownCh: make(chan struct{}),
		n:          n,
	}
	for i := 0; i < n; i++ {
		lrun.signal <- struct{}{}
	}

	return lrun
}

func (lrun *LimitRun) Wait() error {
	select {
	case <-lrun.shutdownCh:
		return errors.New("chan close")
	case _, ok := <-lrun.signal:
		if !ok {
			return errors.New("chan close")
		}
	}
	return nil
}

func (lrun *LimitRun) Done() {
	select {
	case lrun.signal <- struct{}{}:
	case <-lrun.shutdownCh:
		return
	}
}

func (lrun *LimitRun) Close() error {
	lrun.shutdownLock.Lock()
	defer lrun.shutdownLock.Unlock()

	if lrun.shutdown {
		return nil
	}

	lrun.shutdown = true
	close(lrun.shutdownCh)
	return nil
}

func (lrun *LimitRun) String() string {
	return fmt.Sprintf("Signal: %d/%d", len(lrun.signal), lrun.n)
}
