package lib

import (
	"encoding/json"
)

func JsonEncodeIndent(data interface{}) string {
	if data == nil {
		return ""
	}
	out, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		return ""
	}
	return string(out)
}

func JsonEncode(data interface{}) string {
	if data == nil {
		return ""
	}
	out, err := json.Marshal(data)
	if err != nil {
		return ""
	}
	return string(out)
}

func JsonDecode(b []byte) map[string]interface{} {
	var r map[string]interface{}
	err := json.Unmarshal(b, &r)
	if err != nil {
		return nil
	}
	return r
}
