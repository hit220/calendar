package lib

import (
	"errors"
	"sync"
	"time"
)

type WaitKey struct {
	alwaysWait chan struct{}
	limit      chan struct{}
	chans      map[interface{}]chan struct{}
	chansLock  sync.RWMutex

	shutdown     bool
	shutdownCh   chan struct{}
	shutdownLock sync.Mutex
}

func NewWaitKey(max int) *WaitKey {
	return &WaitKey{
		alwaysWait: make(chan struct{}),
		chans:      make(map[interface{}]chan struct{}),
		limit:      make(chan struct{}, max),
		shutdownCh: make(chan struct{}),
	}
}

func (ws *WaitKey) Add(k interface{}) {
	ws.chansLock.RLock()
	_, ok := ws.chans[k]
	ws.chansLock.RUnlock()
	if ok {
		return
	}

	// wait
	select {
	case ws.limit <- struct{}{}:
	}

	ws.chansLock.Lock()
	ws.chans[k] = make(chan struct{})
	ws.chansLock.Unlock()
	return
}

func (ws *WaitKey) Done(k interface{}) {
	ws.chansLock.RLock()
	defer ws.chansLock.RUnlock()
	ch, ok := ws.chans[k]
	if !ok {
		return
	}

	close(ch)
}

func (ws *WaitKey) Remove(k interface{}) {
	ws.chansLock.Lock()
	defer ws.chansLock.Unlock()

	_, ok := ws.chans[k]
	if !ok {
		return
	}

	delete(ws.chans, k)
	<-ws.limit

}

func (ws *WaitKey) WaitChan(k interface{}) <-chan struct{} {
	ws.chansLock.RLock()
	ch, ok := ws.chans[k]
	ws.chansLock.RUnlock()
	if !ok {
		return ws.alwaysWait
	}
	return ch
}

func (ws *WaitKey) Wait(key interface{}, timeout time.Duration, ch ...chan struct{}) error {
	var shutCh chan struct{}
	if len(ch) > 0 {
		shutCh = ch[0]
	} else {
		shutCh = make(chan struct{})
	}
	tmr := time.NewTimer(timeout)
	defer tmr.Stop()
	select {
	case <-ws.WaitChan(key):
		ws.Remove(key)
	case <-tmr.C:
		return errors.New("wait key timeout")
	case <-shutCh:
	case <-ws.shutdownCh:
	}
	return nil
}

func (ws *WaitKey) Close() error {
	ws.shutdownLock.Lock()
	defer ws.shutdownLock.Unlock()
	if ws.shutdown {
		return nil
	}
	ws.shutdown = true

	close(ws.shutdownCh)

	ws.chansLock.Lock()
	defer ws.chansLock.Unlock()
	for k := range ws.chans {
		delete(ws.chans, k)
		<-ws.limit
	}
	close(ws.alwaysWait)
	return nil
}
