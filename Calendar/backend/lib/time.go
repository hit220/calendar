package lib

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

const (
	TimeLayout = "2006-01-02 15:04:05"
)

var cstZone = time.FixedZone("CST", 8*3600)

func Now() time.Time {
	return time.Now().In(cstZone)
}

func TimeZone() *time.Location {
	return cstZone
}

type ITimer struct {
	*time.Timer
	name      string
	exited    bool
	isRunning bool
	d         time.Duration
	fn        func()
}

func itimerDefaultFunc() {}

func NewTimer(d time.Duration) *ITimer {
	tm := &ITimer{
		Timer:     time.NewTimer(d),
		exited:    false,
		isRunning: true,
		d:         d,
		fn:        itimerDefaultFunc,
	}
	tm.Pause()
	return tm
}

func (tm *ITimer) SetName(nm string) {
	tm.name = nm
}

func (tm *ITimer) Name() string {
	return tm.name
}

func (tm *ITimer) Exit() bool {
	tm.exited = tm.Timer.Reset(0)
	return tm.exited
}

func (tm *ITimer) IsExit() bool {
	return tm.exited
}

func (tm *ITimer) reset() bool {
	if tm.exited {
		return false
	}
	tm.isRunning = true
	return tm.Timer.Reset(tm.d)
}

func (tm *ITimer) Pause() bool {
	tm.isRunning = false
	return tm.Timer.Reset(time.Duration(math.MaxInt64))
}

func (tm *ITimer) Continue() bool {
	tm.isRunning = true
	return tm.reset()
}

func (tm *ITimer) IsRunning() bool {
	if tm.exited {
		return false
	}
	return tm.isRunning
}

func (tm *ITimer) SetFunc(fn func()) {
	tm.fn = fn
}

func (tm *ITimer) NowExecFunc() {
	if tm.fn != nil {
		go tm.fn()
	}
	tm.reset()
}

func (tm *ITimer) Run() {
	for {
		select {
		case <-tm.C:
			if tm.IsExit() {
				tm.isRunning = false
				return
			}
			tm.reset()
			if tm.fn != nil {
				go tm.fn()
			}
		}
	}
}

//format time like java, such as: yyyy-MM-dd HH:mm:ss

func TimeFormat(t time.Time, format string) string {

	//year
	if strings.ContainsAny(format, "y") {

		year := strconv.Itoa(t.Year())

		if strings.Count(format, "yy") == 1 && strings.Count(format, "y") == 2 {
			format = strings.Replace(format, "yy", year[2:], 1)
		} else if strings.Count(format, "yyyy") == 1 && strings.Count(format, "y") == 4 {
			format = strings.Replace(format, "yyyy", year, 1)
		} else {
			return ""
		}
	}

	//month
	if strings.ContainsAny(format, "M") {

		var month string

		if int(t.Month()) < 10 {
			month = "0" + strconv.Itoa(int(t.Month()))
		} else {
			month = strconv.Itoa(int(t.Month()))
		}

		if strings.Count(format, "MM") == 1 && strings.Count(format, "M") == 2 {
			format = strings.Replace(format, "MM", month, 1)
		} else {
			return ""
		}
	}

	//day
	if strings.ContainsAny(format, "d") {

		var day string

		if t.Day() < 10 {
			day = "0" + strconv.Itoa(t.Day())
		} else {
			day = strconv.Itoa(t.Day())
		}

		if strings.Count(format, "dd") == 1 && strings.Count(format, "d") == 2 {
			format = strings.Replace(format, "dd", day, 1)
		} else {
			return ""
		}
	}

	//hour
	if strings.ContainsAny(format, "H") {

		var hour string

		if t.Hour() < 10 {
			hour = "0" + strconv.Itoa(t.Hour())
		} else {
			hour = strconv.Itoa(t.Hour())
		}

		if strings.Count(format, "HH") == 1 && strings.Count(format, "H") == 2 {
			format = strings.Replace(format, "HH", hour, 1)
		} else {
			return ""
		}
	}

	//minute
	if strings.ContainsAny(format, "m") {

		var minute string

		if t.Minute() < 10 {
			minute = "0" + strconv.Itoa(t.Minute())
		} else {
			minute = strconv.Itoa(t.Minute())
		}
		if strings.Count(format, "mm") == 1 && strings.Count(format, "m") == 2 {
			format = strings.Replace(format, "mm", minute, 1)
		} else {
			return ""
		}
	}

	//second
	if strings.ContainsAny(format, "s") {

		var second string

		if t.Second() < 10 {
			second = "0" + strconv.Itoa(t.Second())
		} else {
			second = strconv.Itoa(t.Second())
		}

		if strings.Count(format, "ss") == 1 && strings.Count(format, "s") == 2 {
			format = strings.Replace(format, "ss", second, 1)
		} else {
			return ""
		}
	}

	return format
}

//2007-11-23 10:02:14/20071123100214
func TimeParse(str string) (time.Time, error) {
	ll := len(str)
	if ll == 19 {
		loc, _ := time.LoadLocation("Local")
		return time.ParseInLocation("2006-01-02 15:04:05", str, loc)
	} else if ll == 14 {
		loc, _ := time.LoadLocation("Local")
		return time.ParseInLocation("20060102150405", str, loc)
	}
	return time.Time{}, errors.New("str len is error")
}

//17:02:03/170203
func TimeParseHHmmss(str string) (time.Time, error) {
	ll := len(str)
	if ll != 8 && ll != 6 {
		return time.Time{}, errors.New("input str is error")
	}
	if ll == 8 {
		return TimeParse(TimeFormat(time.Now().In(cstZone), "yyyy-MM-dd "+str))
	}
	return TimeParse(TimeFormat(time.Now().In(cstZone), "yyyyMMdd"+str))
}

//17:02/1702
func TimeParseHHmm(str string) (time.Time, error) {
	ll := len(str)
	if ll != 5 && ll != 4 {
		return time.Time{}, errors.New("input str is error")
	}
	if ll == 5 {
		return TimeParse(TimeFormat(time.Now().In(cstZone), "yyyy-MM-dd "+str+":00"))
	}
	return TimeParse(TimeFormat(time.Now().In(cstZone), "yyyyMMdd"+str+"00"))
}

//2007-05-69/20070569
func TimeParseyyyyMMdd(str string) (time.Time, error) {
	ll := len(str)
	if ll != 10 && ll != 8 {
		return time.Time{}, errors.New("input str is error")
	}
	if ll == 10 {
		return TimeParse(TimeFormat(time.Now().In(cstZone), str+" 00:00:00"))
	}
	return TimeParse(TimeFormat(time.Now().In(cstZone), str+"000000"))
}

type DateTime time.Time

func (t *DateTime) UnmarshalJSON(data []byte) error {
	return ((*time.Time)(t)).UnmarshalJSON(data)
}

func (t DateTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(t).Format("2006-01-02 15:04:05"))
	return []byte(stamp), nil
}

func (t DateTime) String() string {
	return time.Time(t).String()
}
