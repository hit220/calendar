module 3g3e.com/ymdx/lib

go 1.14

require (
	github.com/gofrs/flock v0.8.0
	github.com/hunzsig/graphics v0.0.0-20190929035859-45587367f25f
	golang.org/x/net v0.0.0-20201216054612-986b41b23924
)
