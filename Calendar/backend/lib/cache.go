package lib

import (
	"sync"
	"time"
)

type CacheRunner struct {
	sync.Mutex
	fn    func() (interface{}, error)
	delay time.Duration

	inited bool
	val    interface{}
	err    error
	ct     time.Time
}

func NewCacheRunner(f func() (interface{}, error), delay time.Duration) *CacheRunner {
	return &CacheRunner{
		fn:    f,
		delay: delay,
	}
}

func (c *CacheRunner) Get(def interface{}, force ...bool) interface{} {
	c.Lock()
	defer c.Unlock()

	if !c.inited || c.ct.Before(time.Now()) {
		c.val, c.err = c.fn()
		c.inited = true
		c.ct = time.Now().Add(c.delay)
	}

	if c.err != nil {
		return def
	}
	return c.val

}
