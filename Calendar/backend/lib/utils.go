package lib

import (
	"os"
	"os/exec"
)

func StartFirefox(url string) {
	cmd := exec.Command("C:\\Windows\\System32\\cmd.exe", "/c", "start", "firefox", url)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}

func StartChrome(url string) {
	cmd := exec.Command("C:\\Windows\\System32\\cmd.exe", "/c", "start", "chrome", url)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}
