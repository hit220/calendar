package lib

import (
	"errors"
	"reflect"
	"strings"
	"sync"
	"time"
)

var (
	ZeroValue reflect.Value
	fieldMap  sync.Map
)

func init() {
	ZeroValue = reflect.Value{}
}

type sqlRows interface {
	Columns() ([]string, error)
	Next() bool
	Scan(...interface{}) error
	Close() error
}

func RowsToMap(rows sqlRows) ([]map[string]interface{}, error) {
	var list []map[string]interface{} // 返回的切片
	if rows == nil {
		return list, nil
	}
	columns, _ := rows.Columns()
	columnLength := len(columns)
	cache := make([]interface{}, columnLength) // 临时存储每行数据
	for index, _ := range cache {              // 为每一列初始化一个指针
		var a interface{}
		cache[index] = &a
	}
	for rows.Next() {
		_ = rows.Scan(cache...)

		item := make(map[string]interface{})
		for i, data := range cache {
			item[columns[i]] = *data.(*interface{}) //取实际类型
		}
		list = append(list, item)
	}
	_ = rows.Close()
	return list, nil
}

func MapToStruct(from map[string]interface{}, to interface{}, tag ...string) error {
	tagName := "json"
	if len(tag) > 0 {
		tagName = tag[0]
	}

	objValue := reflect.ValueOf(to)
	if objValue == ZeroValue {
		return errors.New("to obj value does not exist")
	}

	if objValue.Type().Kind() == reflect.Ptr {
		objValue = objValue.Elem()
	}

	if objValue.Type().Kind() != reflect.Struct {
		return errors.New("to obj is not struct")
	}

	var keyArr []string
	typeName := objValue.Type().String()
	val, ok := fieldMap.Load(typeName)
	if ok {
		keyArr = val.([]string)
	} else {
		for i := 0; i < objValue.NumField(); i++ {
			keyArr = append(keyArr, getFieldName(objValue, i, tagName))
		}
		fieldMap.Store(typeName, keyArr)
	}

	if objValue.NumField() != len(keyArr) {
		return errors.New("obj field num not equal")
	}

	for i := 0; i < objValue.NumField(); i++ {
		key := keyArr[i]
		if key != "" {
			value, ok := from[key]
			if ok {
				field := objValue.Field(i)
				if field.Type().Kind() == reflect.Ptr {
					field = field.Elem()
				}

				switch field.Kind() {
				case reflect.Int:
					field.Set(reflect.ValueOf(ValueToInt(value, 0)))
				case reflect.Int8:
					field.Set(reflect.ValueOf(int8(ValueToInt64(value, 0))))
				case reflect.Int16:
					field.Set(reflect.ValueOf(int16(ValueToInt64(value, 0))))
				case reflect.Int32:
					field.Set(reflect.ValueOf(int32(ValueToInt64(value, 0))))
				case reflect.Int64:
					field.Set(reflect.ValueOf(ValueToInt64(value, 0)))
				case reflect.Uint:
					field.Set(reflect.ValueOf(uint(ValueToInt(value, 0))))
				case reflect.Uint8:
					field.Set(reflect.ValueOf(uint8(ValueToInt64(value, 0))))
				case reflect.Uint16:
					field.Set(reflect.ValueOf(uint16(ValueToInt64(value, 0))))
				case reflect.Uint32:
					field.Set(reflect.ValueOf(uint32(ValueToInt64(value, 0))))
				case reflect.Uint64:
					field.Set(reflect.ValueOf(uint64(ValueToInt64(value, 0))))
				case reflect.Float32:
					field.Set(reflect.ValueOf(float32(ValueToFloat(value, 0))))
				case reflect.Float64:
					field.Set(reflect.ValueOf(ValueToFloat(value, 0)))
				case reflect.Bool:
					field.Set(reflect.ValueOf(ValueToBool(value, false)))
				case reflect.String:
					field.Set(reflect.ValueOf(ValueToString(value, "")))
				default:
					typeStr := field.Type().String()
					if typeStr == "time.Time" {
						tm := ValueToTime(value, time.Time{})
						field.Set(reflect.ValueOf(tm))
					} else if typeStr == "lib.DateTime" {
						tm := ValueToTime(value, time.Time{})
						field.Set(reflect.ValueOf(DateTime(tm)))
					}
				}
			}
		}
	}

	return nil

}

func getFieldName(objElem reflect.Value, index int, tagKey string) string {
	field := objElem.Type().Field(index)
	fieldName := field.Name

	tag := strings.TrimSpace(field.Tag.Get(tagKey))
	if tag != "" && tag != "-" {
		if strings.Contains(tag, " ") {
			tags := strings.Split(tag, " ")
			fieldName = strings.Trim(tags[len(tags)-1], "'")
		} else {
			fieldName = tag
		}
	}
	return fieldName
}

func SqlArrayToInArgs(in interface{}) (string, []interface{}) {
	args := ValueToArray(in)
	s := len(args)
	if s < 1 {
		// 防止错误: in () => in (-1)
		return "-1", nil
	}
	r := strings.Repeat("?,", s)
	return r[:len(r)-1], args
}
