package lib

import (
	"bytes"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

//
type HttpWriter struct {
	r   *http.Response
	buf *bytes.Buffer
}

func NewHttpWriter(r *http.Response) *HttpWriter {
	buf := bytes.NewBuffer(nil)
	r.Body = ioutil.NopCloser(buf)
	return &HttpWriter{
		r:   r,
		buf: buf,
	}
}

//
func (h *HttpWriter) Header() http.Header {
	return h.r.Header
}

//
func (h *HttpWriter) Write(b []byte) (int, error) {
	return h.buf.Write(b)
}

//
func (h *HttpWriter) WriteHeader(statusCode int) {
	h.r.StatusCode = statusCode
}

//
func FilterFlags(content string) string {
	for i, char := range content {
		if char == ' ' || char == ';' {
			return content[:i]
		}
	}
	return content
}

//
func ContentType(r *http.Request) string {
	return FilterFlags(r.Header.Get("Content-Type"))
}

//
func SaveUploadedFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	return err
}
