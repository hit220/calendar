package lib

import (
	"errors"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type StringInterface interface {
	String() string
}

func ValueType(in interface{}) string {
	if in == nil {
		return "<nil>"
	}
	switch in.(type) {
	case bool:
		return "bool"
	case *bool:
		return "*bool"
	case int:
		return "int"
	case int8:
		return "int8"
	case int16:
		return "int16"
	case int32:
		return "int32"
	case int64:
		return "int64"
	case uint:
		return "uint"
	case uint8:
		return "uint8"
	case uint16:
		return "uint16"
	case uint32:
		return "uint32"
	case uint64:
		return "uint64"
	case float32:
		return "float32"
	case float64:
		return "float64"
	case []byte:
		return "[]byte"
	case string:
		return "string"
	case []string:
		return "[]string"
	case *[]byte:
		return "*[]byte"
	case *string:
		return "*string"
	case []*string:
		return "[]*string"
	default:
	}
	rv := reflect.ValueOf(in)
	switch rv.Kind() {
	case reflect.Map:
		return "reflect.Map"
	case reflect.Array:
		return "reflect.Array"
	case reflect.Slice:
		return "reflect.Slice"
	}

	return "[" + rv.Kind().String() + "]" + reflect.TypeOf(in).Name()
}

func ValueToInt(in interface{}, def int) int {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return 1
		} else {
			return 0
		}
	case *bool:
		if *(in.(*bool)) {
			return 1
		} else {
			return 0
		}
	case int:
		return in.(int)
	case int8:
		return int(in.(int8))
	case int16:
		return int(in.(int16))
	case int32:
		return int(in.(int32))
	case int64:
		return int(in.(int64))
	case uint:
		return int(in.(uint))
	case uint8:
		return int(in.(uint8))
	case uint16:
		return int(in.(uint16))
	case uint32:
		return int(in.(uint32))
	case uint64:
		return int(in.(uint64))
	case float32:
		return int(in.(float32))
	case float64:
		return int(in.(float64))
	case []byte:
		ii, _ := strconv.ParseInt(string(in.([]byte)), 10, 64)
		return int(ii)
	case string:
		ii, _ := strconv.ParseInt(in.(string), 10, 64)
		return int(ii)
	case *[]byte:
		ii, _ := strconv.ParseInt(string(*(in.(*[]byte))), 10, 64)
		return int(ii)
	case *string:
		ii, _ := strconv.ParseInt(*(in.(*string)), 10, 64)
		return int(ii)
	default:
	}
	return def
}

func ValueToInt64(in interface{}, def int64) int64 {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return 1
		} else {
			return 0
		}
	case *bool:
		if *(in.(*bool)) {
			return 1
		} else {
			return 0
		}
	case int:
		return int64(in.(int))
	case int8:
		return int64(in.(int8))
	case int16:
		return int64(in.(int16))
	case int32:
		return int64(in.(int32))
	case int64:
		return in.(int64)
	case uint:
		return int64(in.(uint))
	case uint8:
		return int64(in.(uint8))
	case uint16:
		return int64(in.(uint16))
	case uint32:
		return int64(in.(uint32))
	case uint64:
		return int64(in.(uint64))
	case float32:
		return int64(in.(float32))
	case float64:
		return int64(in.(float64))
	case []byte:
		ii, _ := strconv.ParseInt(string(in.([]byte)), 10, 64)
		return int64(ii)
	case string:
		ii, _ := strconv.ParseInt(in.(string), 10, 64)
		return int64(ii)
	case *[]byte:
		ii, _ := strconv.ParseInt(string(*(in.(*[]byte))), 10, 64)
		return int64(ii)
	case *string:
		ii, _ := strconv.ParseInt(*(in.(*string)), 10, 64)
		return int64(ii)
	default:
	}
	return def
}

func ValueToFloat(in interface{}, def float64) float64 {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return 1.0
		} else {
			return 0.0
		}
	case *bool:
		if *(in.(*bool)) {
			return 1.0
		} else {
			return 0.0
		}
	case int:
		return float64(in.(int))
	case int8:
		return float64(in.(int8))
	case int16:
		return float64(in.(int16))
	case int32:
		return float64(in.(int32))
	case int64:
		return float64(in.(int64))
	case uint:
		return float64(in.(uint))
	case uint8:
		return float64(in.(uint8))
	case uint16:
		return float64(in.(uint16))
	case uint32:
		return float64(in.(uint32))
	case uint64:
		return float64(in.(uint64))
	case float32:
		return float64(in.(float32))
	case float64:
		return in.(float64)
	case []byte:
		ii, _ := strconv.ParseFloat(string(in.([]byte)), 64)
		return ii
	case string:
		ii, _ := strconv.ParseFloat(in.(string), 64)
		return ii
	case *[]byte:
		ii, _ := strconv.ParseFloat(string(*(in.(*[]byte))), 64)
		return ii
	case *string:
		ii, _ := strconv.ParseFloat(*(in.(*string)), 64)
		return ii
	default:
	}
	return def
}

func ValueToString(in interface{}, def string) string {
	return ValueToStringIn(in, def, 0)
}

func ValueToStringIn(in interface{}, def string, fixed int) string {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		if in.(bool) {
			return "true"
		} else {
			return "false"
		}
	case *bool:
		if *(in.(*bool)) {
			return "true"
		} else {
			return "false"
		}
	case int:
		return strconv.FormatInt(int64(in.(int)), 10)
	case int8:
		return strconv.FormatInt(int64(in.(int8)), 10)
	case int16:
		return strconv.FormatInt(int64(in.(int16)), 10)
	case int32:
		return strconv.FormatInt(int64(in.(int32)), 10)
	case int64:
		return strconv.FormatInt(in.(int64), 10)
	case uint:
		return strconv.FormatUint(uint64(in.(uint)), 10)
	case uint8:
		return strconv.FormatUint(uint64(in.(uint8)), 10)
	case uint16:
		return strconv.FormatUint(uint64(in.(uint16)), 10)
	case uint32:
		return strconv.FormatUint(uint64(in.(uint32)), 10)
	case uint64:
		return strconv.FormatUint(in.(uint64), 10)
	case float32:
		if fixed > 0 {
			return strconv.FormatFloat(float64(in.(float32)), 'f', fixed, 32)
		}
		r := strings.TrimRight(strconv.FormatFloat(float64(in.(float32)), 'f', 10, 32), "0")
		if r == "" || r == "0." {
			return "0"
		}
		return r
	case float64:
		if fixed > 0 {
			return strconv.FormatFloat(in.(float64), 'f', fixed, 64)
		}
		r := strings.TrimRight(strconv.FormatFloat(in.(float64), 'f', 10, 64), "0")
		if r == "" || r == "0." {
			return "0"
		}
		return r
	case []byte:
		return string(in.([]byte))
	case string:
		return in.(string)
	case *[]byte:
		return string(*(in.(*[]byte)))
	case *string:
		return *(in.(*string))
	case time.Time:
		return in.(time.Time).In(cstZone).Format(TimeLayout)
	default:
	}
	o, ok := in.(StringInterface)
	if ok {
		return o.String()
	}
	return def
}

func ValueToBool(in interface{}, def bool) bool {
	if in == nil {
		return def
	}
	switch in.(type) {
	case bool:
		return in.(bool)
	case *bool:
		return *(in.(*bool))
	case int:
		return in.(int) != 0
	case int8:
		return in.(int8) != 0
	case int16:
		return in.(int16) != 0
	case int32:
		return in.(int32) != 0
	case int64:
		return in.(int64) != 0
	case uint:
		return in.(uint) != 0
	case uint8:
		return in.(uint8) != 0
	case uint16:
		return in.(uint16) != 0
	case uint32:
		return in.(uint32) != 0
	case uint64:
		return in.(uint64) != 0
	case float32:
		return in.(float32) != 0
	case float64:
		return in.(float64) != 0
	case []byte:
		return strings.ToUpper(string(in.([]byte))) == "TRUE"
	case string:
		return strings.ToUpper(in.(string)) == "TRUE"
	case *[]byte:
		return strings.ToUpper(string(*(in.(*[]byte)))) == "TRUE"
	case *string:
		return strings.ToUpper(*(in.(*string))) == "TRUE"
	default:
	}
	return def
}

func ValueToTime(in interface{}, def time.Time) time.Time {
	if in == nil {
		return def
	}
	switch in.(type) {
	case time.Time:
		return in.(time.Time)
	case *time.Time:
		return *(in.(*time.Time))
	case int, int32, int64, uint32, uint64, float32, float64:
		tm := time.Time{}
		tm.Add(time.Duration(ValueToInt64(in, 0)))
		return tm
	default:
		tm := ValueToString(in, "")
		if strings.Contains(tm, "T") {
			tmi, err := time.Parse(time.RFC3339, tm)
			if err == nil {
				return tmi
			}
		} else {
			tmi, err := time.Parse("2006-01-02 15:04:05", tm)
			if err == nil {
				return tmi
			}
		}

		return def
	}
}

func ValueIsEmpty(in interface{}) bool {
	if in == nil {
		return true
	}

	rv := reflect.ValueOf(in)
	switch rv.Kind() {
	case reflect.Map, reflect.Array, reflect.Slice:
		return rv.Len() == 0
	}

	switch in.(type) {
	case bool:
		return !in.(bool)
	case *bool:
		return !(*(in.(*bool)))
	case int:
		return in.(int) == 0
	case int8:
		return in.(int8) == 0
	case int16:
		return in.(int16) == 0
	case int32:
		return in.(int32) == 0
	case int64:
		return in.(int64) == 0
	case uint:
		return in.(uint) == 0
	case uint8:
		return in.(uint8) == 0
	case uint16:
		return in.(uint16) == 0
	case uint32:
		return in.(uint32) == 0
	case uint64:
		return in.(uint64) == 0
	case float32:
		return in.(float32) == 0.0
	case float64:
		return in.(float64) == 0.0
	case string:
		return len(in.(string)) == 0
	case *string:
		return len(*(in.(*string))) == 0
	default:
	}
	return rv.IsNil()
}

func ValueToStringArray(in interface{}) []string {
	if in == nil {
		return nil
	}

	arr, ok := in.([]interface{})
	if !ok {
		return nil
	}
	res := make([]string, 0)
	for _, v := range arr {
		res = append(res, ValueToString(v, ""))
	}
	return res
}

func ValueToIntArray(in interface{}) []int {
	if in == nil {
		return nil
	}

	arr, ok := in.([]interface{})
	if !ok {
		return nil
	}
	res := make([]int, 0)
	for _, v := range arr {
		res = append(res, ValueToInt(v, 0))
	}
	return res
}

func ValueToFloatArray(in interface{}) []float64 {
	if in == nil {
		return nil
	}

	arr, ok := in.([]interface{})
	if !ok {
		return nil
	}
	res := make([]float64, 0)
	for _, v := range arr {
		res = append(res, ValueToFloat(v, 0.0))
	}
	return res
}

func ValueFromStringArray(in []string) []interface{} {
	out := make([]interface{}, len(in))
	for k, v := range in {
		out[k] = v
	}
	return out
}

func ValueFromIntArray(in []int) []interface{} {
	out := make([]interface{}, len(in))
	for k, v := range in {
		out[k] = v
	}
	return out
}

func ValueFromFloatArray(in []float64) []interface{} {
	out := make([]interface{}, len(in))
	for k, v := range in {
		out[k] = v
	}
	return out
}

func ValueToMap(obj interface{}) map[string]interface{} {
	if obj == nil {
		return nil
	}
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
		return nil
	}

	if t.Kind() == reflect.Ptr {
		v = v.Elem()
		t = v.Type()

		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}
	}

	switch t.Kind() {
	case reflect.Map:
		data := make(map[string]interface{})
		iter := v.MapRange()
		for iter.Next() {
			key := ValueToString(iter.Key().Interface(), "")
			if key == "" {
				continue
			}
			data[key] = iter.Value().Interface()
		}
		return data
	case reflect.Struct:
		data := make(map[string]interface{})
		for i := 0; i < t.NumField(); i++ {
			name := t.Field(i).Name
			if name != "" && name[0] >= 'A' && name[0] <= 'Z' {
				data[name] = v.Field(i).Interface()
			}
		}
		return data
	default:
	}
	return nil
}

func ValueToArray(obj interface{}) []interface{} {
	r, ok := obj.([]interface{})
	if ok {
		return r
	}
	if obj == nil {
		return nil
	}
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
		return nil
	}

	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
	}

	switch t.Kind() {
	case reflect.Map:
		data := make([]interface{}, 0)
		iter := v.MapRange()
		for iter.Next() {
			key := ValueToString(iter.Key().Interface(), "")
			if key == "" {
				continue
			}
			data = append(data, iter.Value().Interface())
		}
		return data
	case reflect.Struct:
		data := make([]interface{}, 0)
		for i := 0; i < t.NumField(); i++ {
			name := t.Field(i).Name
			if name != "" && name[0] >= 'A' && name[0] <= 'Z' {
				data = append(data, v.Field(i).Interface())
			}
		}
		return data
	case reflect.Slice, reflect.Array:
		data := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			data[i] = v.Index(i).Interface()
		}
		return data
	default:
	}
	return nil
}

type Pair struct {
	Key   string
	Value interface{}
}

func ValueToArrayPair(obj interface{}) []*Pair {
	if obj == nil {
		return nil
	}
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
		return nil
	}

	if t.Kind() == reflect.Ptr {
		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return nil
		}

		v = v.Elem()
		t = v.Type()
	}

	switch t.Kind() {
	case reflect.Map:
		data := make([]*Pair, 0)
		iter := v.MapRange()
		for iter.Next() {
			key := ValueToString(iter.Key().Interface(), "")
			if key == "" {
				continue
			}
			data = append(data, &Pair{key, iter.Value().Interface()})
		}
		return data
	case reflect.Struct:
		data := make([]*Pair, 0)
		for i := 0; i < t.NumField(); i++ {
			name := t.Field(i).Name
			if name != "" && name[0] >= 'A' && name[0] <= 'Z' {
				data = append(data, &Pair{name, v.Field(i).Interface()})
			}
		}
		return data
	default:
	}
	return nil
}

type SortPairArrayASC []*Pair

func (s SortPairArrayASC) Len() int {
	return len(s)
}
func (s SortPairArrayASC) Less(i, j int) bool {
	return strings.Compare(s[i].Key, s[j].Key) < 0
}

func (s SortPairArrayASC) Swap(i, j int) {
	t := s[i]
	s[i] = s[j]
	s[j] = t
}

type SortPairArrayDESC []*Pair

func (s SortPairArrayDESC) Len() int {
	return len(s)
}
func (s SortPairArrayDESC) Less(i, j int) bool {
	return strings.Compare(s[i].Key, s[j].Key) > 0
}

func (s SortPairArrayDESC) Swap(i, j int) {
	t := s[i]
	s[i] = s[j]
	s[j] = t
}

// return: result, isDefault
func getMapValue(obj interface{}, skey string, def interface{}) (interface{}, bool) {
	if obj == nil {
		return def, true
	}

	mp, ok := obj.(map[string]interface{})
	if ok {
		val, have := mp[skey]
		if !have {
			return def, true
		}
		return val, false
	}

	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
		return def, true
	}

	if t.Kind() == reflect.Ptr {
		v = v.Elem()
		t = v.Type()

		if !v.IsValid() || (t.Kind() == reflect.Ptr && v.IsNil()) {
			return def, true
		}
	}

	skey = strings.ToLower(skey)
	switch t.Kind() {
	case reflect.Map:
		iter := v.MapRange()
		for iter.Next() {
			key := ValueToString(iter.Key().Interface(), "")
			if key == "" {
				continue
			}
			if strings.ToLower(key) == skey {
				return iter.Value().Interface(), false
			}
		}
	case reflect.Struct:
		for i := 0; i < t.NumField(); i++ {
			name := t.Field(i).Name
			if name != "" && name[0] >= 'A' && name[0] <= 'Z' && strings.ToLower(name) == skey {
				return v.Field(i).Interface(), false
			}
		}
	default:
	}
	return def, true
}

// keys: a.bb.cc (lower string join by point)
func MapValue(obj interface{}, keys string, def interface{}) interface{} {
	var iter interface{}
	var isDefault bool

	iter = obj

	keyArr := strings.Split(keys, ".")
	for _, key := range keyArr {
		iter, isDefault = getMapValue(iter, key, def)
		if isDefault {
			return iter
		}
	}

	return iter
}

func MapValueToString(obj interface{}, keys string, def string) string {
	return ValueToString(MapValue(obj, keys, def), def)
}

func MapValueToInt(obj interface{}, keys string, def int) int {
	return ValueToInt(MapValue(obj, keys, def), def)
}

/*
* ------------ Array ---------
 */
func ArrayHave(arr []string, val string) bool {
	if arr == nil {
		return false
	}
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}

func ArrayContains(arr []string, val string) bool {
	if arr == nil {
		return false
	}
	for _, v := range arr {
		if strings.Contains(v, val) {
			return true
		}
	}
	return false
}

func MapSet(root map[string]interface{}, key string, val interface{}) error {
	mp := root
	keys := strings.Split(key, ".")
	last := len(keys) - 1
	for index, ikey := range keys {
		v, ok := mp[ikey]
		if !ok {
			if index < last {
				newMP := make(map[string]interface{})
				mp[ikey] = newMP
				mp = newMP
				continue
			} else {
				mp[ikey] = val
				return nil
			}
		} else {
			if index < last {
				m, ok := v.(map[string]interface{})
				if ok {
					mp = m
				} else {
					return errors.New("config key: " + key + "=>" + ikey + " type isn't map")
				}
			} else {
				mp[ikey] = val
				return nil
			}
		}
	}
	return nil
}

type UrlSortValues struct {
	values []*Pair
	keys   map[string]int
}

func NewUrlSortValues() *UrlSortValues {
	return &UrlSortValues{
		values: make([]*Pair, 0),
		keys:   make(map[string]int),
	}
}

func (v *UrlSortValues) Set(key, value string) {
	p := &Pair{key, value}
	index, ok := v.keys[key]
	if ok {
		v.values[index] = p
		return
	}
	v.values = append(v.values, p)
	v.keys[key] = len(v.values) - 1
}

func (v *UrlSortValues) Get(key string) string {
	index, ok := v.keys[key]
	if !ok {
		return ""
	}
	p := v.values[index]
	return p.Value.(string)
}

func (v *UrlSortValues) Delete(key string) string {
	index, ok := v.keys[key]
	if !ok {
		return ""
	}
	delete(v.keys, key)

	val := v.values[index]
	if index >= (len(v.values) - 1) {
		v.values = v.values[:index]
	} else {
		v.values = append(v.values[:index], v.values[index+1:]...)
	}
	return val.Value.(string)
}

func (v *UrlSortValues) Encode() string {
	var e string
	for _, val := range v.values {
		e += val.Key + "=" + url.QueryEscape(val.Value.(string)) + "&"
	}
	if e == "" {
		return e
	}

	return e[:len(e)-1]
}
