package lib

import "testing"

func TestUrlRedirectTo(t *testing.T) {
	url1 := "http//aa.bb/cc?rr=1122&cc=3#/uu/cc?tt=2"
	url2 := "http//dd.ee/mm?cc=553#/uu/cc?pp=abc"
	u := UrlRedirectTo(url1, url2)

	if u != "http//dd.ee/mm?cc=553&rr=1122#/uu/cc?pp=abc" &&
		u != "http//dd.ee/mm?rr=1122&cc=553#/uu/cc?pp=abc" {
		t.FailNow()
	}
}
